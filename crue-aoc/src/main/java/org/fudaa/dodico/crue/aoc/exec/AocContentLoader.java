package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.aoc.io.CrueFileFormatBuilderAOC;
import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.CoeurConfig;
import org.fudaa.dodico.crue.config.coeur.CoeurManager;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.CrueFileFormatBuilderLHPT;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.metier.aoc.LoiMesuresPost;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierLHPTSupport;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.EMHProjetController;
import org.fudaa.dodico.crue.projet.ScenarioLoader;
import org.fudaa.dodico.crue.projet.ScenarioLoaderOperation;

import java.io.File;

/**
 * @author deniger
 */
public class AocContentLoader {
  private final CoeurManager coeurManager;

  public AocContentLoader(CoeurManager coeurManager) {
    this.coeurManager = coeurManager;
  }

  public static CrueIOResu<LoiMesuresPost> loadLHPT(EMHProjet projet, ManagerEMHScenario managerEMHScenario) {
    final File lhptFile = FichierLHPTSupport.getLHPTFile(projet, managerEMHScenario);
    if (lhptFile.exists()) {
      CtuluLog log = new CtuluLog();
      CrueDataImpl crueData = new CrueDataImpl(projet.getPropDefinition());
      return new CrueFileFormatBuilderLHPT().getFileFormat(projet.getCoeurConfig())
          .read(lhptFile, log, crueData);
    }
    return null;
  }

  public CrueIOResu<AocCampagne> loadCampagne(CtuluLogGroup logGroup, File aocFile) {
    //on charge la campagne
    final CoeurConfig coeurConfigDefault = coeurManager.getCoeurConfigDefault(coeurManager.getHigherGrammaire());
    final Crue10FileFormat<AocCampagne> fileFormat = new CrueFileFormatBuilderAOC().getFileFormat(coeurConfigDefault);
    final CtuluLog log = logGroup.createLog();
    final CrueIOResu<AocCampagne> read = fileFormat.read(aocFile, log, null);
    if (read != null && read.getMetier() != null) {
      read.getMetier().setAocFile(aocFile);
    }
    return read;
  }

  CrueOperationResult<EMHProjet> loadProjet(File etuFile) {
    Crue10FileFormatFactory.VersionResult findVersion = Crue10FileFormatFactory.findVersion(etuFile);
    if (findVersion.getLog().isNotEmpty()) {
      return new CrueOperationResult<>(null, findVersion.getLog());
    }
    final CoeurConfig coeurConfig = coeurManager.getCoeurConfigDefault(findVersion.getVersion());

    if (coeurConfig == null) {
      CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
      log.setDesc("load.project");
      log.setDescriptionArgs(etuFile.getName());
      log.addError("project.defaultCoeurNotFound", findVersion.getVersion());
      return new CrueOperationResult<>(null, log);
    }
    return EMHProjetController.readProjet(etuFile, coeurConfig, true);
  }

  ScenarioLoaderOperation loadScenario(EMHProjet projet, ManagerEMHScenario scenario) {
    ScenarioLoader loader = new ScenarioLoader(scenario, projet, projet.getCoeurConfig());
    return loader.load(null);
  }
}
