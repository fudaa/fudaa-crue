package org.fudaa.dodico.crue.aoc.validation;

import org.fudaa.dodico.crue.aoc.helper.AocParamCalcSorter;
import org.fudaa.dodico.crue.aoc.projet.*;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPermItem;
import org.fudaa.dodico.crue.metier.emh.DonCLimM;

import java.util.*;

/**
 * Permet de mettre à jour les listes de calculs et les validations croisees en fonction des calculs définis dans les associations lois <-> liste de calcul
 *
 * @author deniger
 */
public class AocCalculUsageUpdater {
  /**
   * Met à jour les données de validation croisee en fonction des lois permanentes définies dans les associations.
   *
   * @param donnees les données à mettre à jour
   */
  public void updateValidationCroiseeFromLoisCalcul(AocCampagneDonnees donnees) {
    final AocValidationCroisee validationCroisee = donnees.getValidationCroisee();
    final List<AocValidationGroupe> groupes = validationCroisee.getGroupes();
    final List<AocValidationGroupe> newGroupes = new ArrayList<>();

    final Set<String> availableLois = new HashSet<>(Arrays.asList(AocValidationCroiseeValidator.getAvailableLois(donnees)));
    //on va supprimer dans les groupes les lois qui ne sont pas comprises dans les availables lois
    for (AocValidationGroupe groupe : groupes) {
      final List<AocValidationLoi> lois = groupe.getLois();
      if (lois != null) {
        final List<AocValidationLoi> newLois = new ArrayList<>();
        for (AocValidationLoi validationLoi : lois) {
          if (availableLois.contains(validationLoi.getLoiRef())) {
            newLois.add(validationLoi);
          }
        }
        groupe.setLois(newLois);
        if (!newLois.isEmpty()) {
          newGroupes.add(groupe);
          //on met à jour l'effectif d'apprentissage si necessaire
          if (newLois.size() != lois.size()) {
            groupe.setEffectifApprentissage(Math.min(newLois.size(), groupe.getEffectifApprentissage()));
          }
        }
      }
    }
    donnees.getValidationCroisee().setGroupes(newGroupes);
  }

  /**
   * Met à jour les liste de calculs.
   *
   * @param donnees les données a mettre à jour
   * @param aocValidationHelper un helper pour retrouver les dclm
   */
  public void updateListCalculFromLoisCalcul(AocCampagneDonnees donnees, AocValidationHelper aocValidationHelper) {
    List<AocParamCalc> paramCalcs = new ArrayList<>();
    AocParamCalcSorter sortedValues = new AocParamCalcSorter(donnees.getListeCalculs());

    for (Map.Entry<String, List<CalcPseudoPermItem>> entry : aocValidationHelper.getAvailableDclmByCalcul().entrySet()) {
      String nomCalcul = entry.getKey();
      final List<CalcPseudoPermItem> donCLimMs = entry.getValue();
      for (DonCLimM donCLimM : donCLimMs) {
        final String nomEMH = donCLimM.getEmh().getNom();
        AocParamCalc value = sortedValues.getAndRemove(nomCalcul, nomEMH);
        if (value == null) {
          value = new AocParamCalc(nomCalcul, nomEMH);
        } else {
          value = new AocParamCalc(value);
        }
        paramCalcs.add(value);
      }
    }

    donnees.getListeCalculs().setParamCalcs(paramCalcs);
  }
}
