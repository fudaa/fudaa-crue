package org.fudaa.dodico.crue.aoc.dclm;

import java.util.Objects;

/**
 * @author deniger
 */
public class AocDclmKey implements Comparable<AocDclmKey> {
  private final String emhRef;
  private final String calculRef;

  public AocDclmKey(String calculRef, String emhRef) {
    this.emhRef = emhRef;
    this.calculRef = calculRef;
  }

  @Override
  public String toString() {
    return calculRef + " / " + emhRef;
  }

  @Override
  public int compareTo(AocDclmKey o) {
    int res = calculRef.compareTo(o.calculRef);
    if (res == 0) {
      res = emhRef.compareTo(o.emhRef);
    }
    return res;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AocDclmKey that = (AocDclmKey) o;
    return Objects.equals(emhRef, that.emhRef) &&
        Objects.equals(calculRef, that.calculRef);
  }

  @Override
  public int hashCode() {

    return Objects.hash(emhRef, calculRef);
  }

  public String getCalculRef() {
    return calculRef;
  }

  public String getEmhRef() {
    return emhRef;
  }
}
