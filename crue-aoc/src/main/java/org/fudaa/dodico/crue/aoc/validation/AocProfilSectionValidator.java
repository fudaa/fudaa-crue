package org.fudaa.dodico.crue.aoc.validation;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluDefaultLogFormatter;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.aoc.ParametrageProfilSection;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.fudaa.dodico.crue.validation.ValidatingItem;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;

import java.util.*;

/**
 * Classe permettant de valider une liste de ProfilSections
 */
public class AocProfilSectionValidator {
    /**
     * @param nodes             lesdonnées a valider
     * @param avalaibleSections les sections disponibles
     */
    public CtuluLog validate(List<ValidatingItem<ParametrageProfilSection>> nodes, String[] avalaibleSections) {
        return validate(nodes, new HashSet<>(Arrays.asList(avalaibleSections)));
    }

    /**
     * @param nodes             lesdonnées a valider
     * @param avalaibleSections les sections disponibles
     */
    public CtuluLog validate(List<ValidatingItem<ParametrageProfilSection>> nodes, Set<String> avalaibleSections) {
        Set<String> usedProfils = new HashSet<>();
        Set<String> usedSections = new HashSet<>();
        CtuluLog res = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
        res.setDesc("aoc.validation.profileSection");
        int idx = 0;
        for (ValidatingItem<ParametrageProfilSection> node : nodes) {
            idx++;
            String profil = node.getObjectToValidate().getPk();
            String section = node.getObjectToValidate().getSectionRef();
            List<CtuluLogRecord> errorMsg = new ArrayList<>();
            //Validation du nom de profil
            if (StringUtils.isBlank(profil)) {
                errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.profilSection.blankProfil", idx));
            } else if (usedProfils.contains(profil.toUpperCase())) {
                errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.profilSection.profilNameAlreadyUsed", idx, profil));
            } else {
                usedProfils.add(profil.toUpperCase());
            }

            //Validation du nom de section
            if (StringUtils.isBlank(section)) {
                errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.profilSection.blankSection", idx));
            } else if (!ValidationPatternHelper.isNameValide(section, EnumCatEMH.SECTION)) {
                errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.profilSection.sectionNameInvalid", idx, section));
            } else if (!avalaibleSections.contains(section)) {
                errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.profilSection.sectionNotFound", idx, section));
            } else if (usedSections.contains(section.toUpperCase())) {
                errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.profilSection.sectionNameAlreadyUsed", idx, section));
            } else {
                usedSections.add(section.toUpperCase());
            }
            if (!errorMsg.isEmpty()) {
                node.setErrorMessage(CtuluDefaultLogFormatter.asHtml(errorMsg, BusinessMessages.RESOURCE_BUNDLE));
            } else {
                node.clearMessage();
            }
        }
        res.updateLocalizedMessage(BusinessMessages.RESOURCE_BUNDLE);
        return res;
    }
}
