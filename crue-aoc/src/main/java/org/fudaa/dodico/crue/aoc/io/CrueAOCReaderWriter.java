package org.fudaa.dodico.crue.aoc.io;

import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;

/**
 *
 */
public class CrueAOCReaderWriter extends CrueDataXmlReaderWriterImpl<CrueDaoAOC, AocCampagne> {
  public static final String FILE_TYPE = "aoc";

  /**
   * @param version version
   * @param crueConfigMetier on l'utilise pour initialiser si besoin les paramètres numériques.
   */
  public CrueAOCReaderWriter(final String version, final CrueConfigMetier crueConfigMetier) {
    super(FILE_TYPE, version, new CrueConverterAOC(crueConfigMetier), new CrueDaoStructureAOC());
  }
}
