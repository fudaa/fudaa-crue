package org.fudaa.dodico.crue.aoc.helper;

import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculPermanent;
import org.fudaa.dodico.crue.aoc.projet.AocLoisCalculsPermanents;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.metier.emh.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Permet d'extraire les dclm utilisable pour les {@link org.fudaa.dodico.crue.aoc.projet.AocParamCalc}
 *
 * @author deniger
 */
public class AocParamCalcExtract {
  public Map<String, List<CalcPseudoPermItem>> extract(AocCampagne aocCampagne, EMHScenario scenario) {
    return extract(aocCampagne.getDonnees().getLoisCalculsPermanents(), scenario.getDonCLimMScenario().getCalcPseudoPerm());
  }

  public Map<String, List<CalcPseudoPermItem>> extract(AocCampagne aocCampagne, DonCLimMScenario donCLimMScenario) {
    return extract(aocCampagne.getDonnees().getLoisCalculsPermanents(), donCLimMScenario.getCalcPseudoPerm());
  }

  /**
   * @param loisCalculsPermanents la liste des calcul permanent définies dans la campagne
   * @param scenarioCalculs la liste des calcul dans le scenario
   * @return emh configurables pour definir les {@link org.fudaa.dodico.crue.aoc.projet.AocParamCalc}
   */
  public TreeMap<String, List<CalcPseudoPermItem>> extract(AocLoisCalculsPermanents loisCalculsPermanents, List<CalcPseudoPerm> scenarioCalculs) {
    final Set<String> calculs = loisCalculsPermanents.getLois().stream().map(AocLoiCalculPermanent::getCalculRef).collect(Collectors.toSet());
    final Map<String, CalcPseudoPerm> calcPseudoPermMap = TransformerHelper.toMapOfNom(scenarioCalculs);
    final TreeMap<String, List<CalcPseudoPermItem>> result = new TreeMap<>();
    Set<Class> allowedDcln = new HashSet<>(
        Arrays.asList(CalcPseudoPermBrancheSaintVenantQruis.class, CalcPseudoPermCasierProfilQruis.class, CalcPseudoPermNoeudQapp.class));
    for (String calcul : calculs) {
      CalcPseudoPerm calcPseudoPerm = calcPseudoPermMap.get(calcul);
      if (calcPseudoPerm != null) {
        List<CalcPseudoPermItem> dclm = new ArrayList<>();
        result.put(calcul, dclm);
        final List<CalcPseudoPermItem> donCLimMS = calcPseudoPerm.getlisteDCLMPseudoPerm();
        for (CalcPseudoPermItem donCLimM : donCLimMS) {
          if (donCLimM.getActuallyActive() && allowedDcln.contains(donCLimM.getClass())) {
            dclm.add(donCLimM);
          }
        }
      }
    }
    return result;
  }
}
