package org.fudaa.dodico.crue.aoc.projet;

import org.fudaa.ctulu.CtuluLibString;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deniger on 28/06/2017.
 */
public class AocValidationCroisee {
    List<AocValidationGroupe> groupes = new ArrayList<>();

    public AocValidationGroupe addGroupe(String id, String nom, int effectifApprentissage, String commentaire) {
        AocValidationGroupe groupe = new AocValidationGroupe(id, nom, effectifApprentissage, commentaire);
        groupes.add(groupe);
        return groupe;
    }

    public List<AocValidationGroupe> getGroupes() {
        return groupes;
    }

    public void setGroupes(List<AocValidationGroupe> groupes) {
        this.groupes = groupes;
    }

    public void setClonedGroupes(List<AocValidationGroupe> clonedGroupes) {
        groupes.clear();
        int idx = 1;
        for (AocValidationGroupe clonedGroupe : clonedGroupes) {
            String num = Integer.toString(idx++);
            if (num.length() < 2) {
                num = CtuluLibString.ZERO + num;
            }
            clonedGroupe.setId("GR_" + num);
            groupes.add(clonedGroupe.clone());
        }
    }
}
