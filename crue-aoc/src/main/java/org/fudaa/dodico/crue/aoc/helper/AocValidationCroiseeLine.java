package org.fudaa.dodico.crue.aoc.helper;

import java.util.*;

/**
 * Utilise pour l'algo de validation croisée. Contient la liste pour les apprentissage et pour validation
 *
 * @author deniger
 */
public class AocValidationCroiseeLine {
  private final List<String> ligneEauApprentissage = new ArrayList<>();
  private final List<String> ligneEauValidation = new ArrayList<>();
  private final Set<String> calculApprentissage = new HashSet<>();
  private final Set<String> calculValidation = new HashSet<>();

  public void updateCalculNames(Map<String, String> calculByLoi) {
    ligneEauApprentissage.forEach(line -> calculApprentissage.add(calculByLoi.get(line)));
    ligneEauValidation.forEach(line -> calculValidation.add(calculByLoi.get(line)));
  }


  public Set<String> getCalculApprentissage() {
    return calculApprentissage;
  }

  public Set<String> getCalculValidation() {
    return calculValidation;
  }

  public List<String> getLigneEauApprentissage() {
    return ligneEauApprentissage;
  }

  public List<String> getLigneEauValidation() {
    return ligneEauValidation;
  }

  /**
   * Permet d'initialiser la liste des lignes d'eau de validation avec le complement des lignes d'eau d'apprentissage.
   *
   * @param allLigneEau
   */
  public void updateValidationWithComplement(List<String> allLigneEau) {
    ligneEauValidation.clear();
    ligneEauValidation.addAll(allLigneEau);
    ligneEauValidation.removeAll(ligneEauApprentissage);
  }

  public boolean isApprentissage(String calcul) {
    return calculApprentissage.contains(calcul);
  }

  public boolean isLigneEauApprentissage(String loi) {
    return ligneEauApprentissage.contains(loi);
  }

  public boolean isValidation(String calcul) {
    return calculValidation.contains(calcul);
  }
}
