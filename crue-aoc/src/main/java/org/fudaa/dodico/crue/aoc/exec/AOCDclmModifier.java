package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.dclm.AocDclmDataItem;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.CrueFileFormatBuilderDCLM;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.io.dclm.CrueDaoDCLM;
import org.fudaa.dodico.crue.io.dclm.CrueDaoDCLMContents;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.DonCLimMScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Modifie le fichier dclm d'un scenario avec valeurs générées par l'algo
 *
 * @author deniger
 */
public class AOCDclmModifier {
  /**
   * @param projet le projet parent
   * @param scenario le scenario a modifier
   * @param items les nouvelles valeurs pour les dclms
   * @return les log
   */
  public static CtuluLog applyDclms(EMHProjet projet, ManagerEMHScenario scenario, List<AocDclmDataItem> items) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    if (items == null || items.isEmpty()) {
      return log;
    }

    final FichierCrue dclmFile = scenario.getListeFichiers().getFile(CrueFileType.DCLM);
    final File file = dclmFile.getProjectFile(projet);
    //lecture du dao:
    Crue10FileFormat<DonCLimMScenario> dclmFileFormat = new CrueFileFormatBuilderDCLM().getFileFormat(projet.getCoeurConfig());
    final CrueDataImpl crueData = new CrueDataImpl(projet.getPropDefinition());
    final CrueDataXmlReaderWriterImpl<CrueDaoDCLM, DonCLimMScenario> readerWriter = (CrueDataXmlReaderWriterImpl) dclmFileFormat.getReaderWriter();
    final CrueDaoDCLM crueDaoDCLM = readerWriter.readDao(file, log, crueData);
    if (log.containsErrorOrSevereError()) {
      return log;
    }
    //on modifie les valeurs
    Map<String, AocDataContainer> valuesByCalcul = createDataContainer(items);
    //pour toutes les valeurs stockées dans le fichier
    final List<CrueDaoDCLMContents.CalculAbstractPersist> listeCalculs = crueDaoDCLM.listeCalculs;
    for (CrueDaoDCLMContents.CalculAbstractPersist listeCalcul : listeCalculs) {
      //on modifie les valeurs si calcul defini
      String calculRef = listeCalcul.Nom;
      if (valuesByCalcul.containsKey(calculRef) && listeCalcul instanceof CrueDaoDCLMContents.CalculPermanentPersist) {
        final AocDataContainer dataContainer = valuesByCalcul.get(calculRef);
        CrueDaoDCLMContents.CalculPermanentPersist dao = (CrueDaoDCLMContents.CalculPermanentPersist) listeCalcul;
        //on modifie ensuite par emh
        for (CrueDaoDCLMContents.RefDCLMAbstractPersist refDCLMAbstractPersist : dao.listeElementsCalculPermanent) {
          if (dataContainer.contains(refDCLMAbstractPersist.NomRef)) {
            refDCLMAbstractPersist.setValue(dataContainer.get(refDCLMAbstractPersist.NomRef));
          }
        }
      }
    }
    //on ecrit
    readerWriter.writeDAO(file, crueDaoDCLM, log, projet.getPropDefinition());
    return log;
  }

  public static Map<String, AocDataContainer> createDataContainer(List<AocDclmDataItem> items) {
    Map<String, AocDataContainer> valuesByCalcul = new HashMap<>();
    for (AocDclmDataItem item : items) {
      String calc = item.getCalculRef();
      AocDataContainer dataContainer = valuesByCalcul.get(calc);
      if (dataContainer == null) {
        dataContainer = new AocDataContainer();
        valuesByCalcul.put(calc, dataContainer);
      }
      dataContainer.addValue(item.getEmhRef(), item.getValue());
    }
    return valuesByCalcul;
  }
}
