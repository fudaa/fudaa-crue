package org.fudaa.dodico.crue.aoc.exec;

import org.apache.commons.io.Charsets;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.dclm.AocDclmDataItem;
import org.fudaa.dodico.crue.aoc.exec.writer.AocExecFormatterHelper;
import org.fudaa.dodico.crue.aoc.exec.writer.AocExecWriterHelper;

import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author deniger
 */
public class AocExecAllLogger {
  private final AocExecContainer container;
  private PrintWriter writer;
  private final CtuluLog log;

  public AocExecAllLogger(AocExecContainer result, CtuluLog log) {
    this.container = result;
    this.log = log;
  }

  public void stop() {
    if (writer != null) {
      writer.close();
      writer = null;
    }
  }

  /**
   * @param fileSuffix le suffixe pour le fichier
   */
  public void start(String fileSuffix) {
    if (container.getCampagne().isLogAll()) {
      File target = new File(container.getReportDirectory(), "log" + fileSuffix + ".csv");
      if (target.exists()) {
        boolean deleted = target.delete();
        if (!deleted) {
          log.addError("cantDeleteFile", target.getAbsolutePath());
        }
      }
      final String canWrite = CtuluLibFile.canWrite(target);
      if (canWrite != null) {
        log.addError(canWrite);
      } else {
        try {
          writer = new PrintWriter(Files.newBufferedWriter(target.toPath(), Charsets.ISO_8859_1));
        } catch (Exception exception) {

        }
      }
    }
  }

  public boolean isOn() {
    return writer != null;
  }

  public void addLogDfrtDclm(String value, Map<String, Double> dfrtValues, List<AocDclmDataItem> dclmValues) {
    if (writer != null) {
      writer.append('\n');
      writer.append(value);
      writer.append('\n');
      List<String> lines = new ArrayList<>();
      AocExecWriterHelper.addFrottementValuesToLines(lines, AocExecFormatterHelper.getDfrtFormater(this.container), dfrtValues);
      writer.append(";;DFRT Nom;");
      writer.append(lines.get(0));
      writer.append('\n');
      writer.append(";;DFRT Valeurs;");
      writer.append(lines.get(1));
      if (dclmValues != null) {
        lines.clear();
        AocExecWriterHelper.addDebitValueToLines(lines, dclmValues, AocExecFormatterHelper.getQFormater(container));
        writer.append('\n');
        writer.append(";;DCLM;");
        writer.append(lines.get(0));
        writer.append('\n');
        writer.append(";;DCLM Values;");
        writer.append(lines.get(1));
      }
    }
  }

  public void addLogFormated(String s, Object... parameters) {
    if (writer != null) {
      writer.append('\n');
      writer.append(String.format(s, parameters));
    }
  }

  public void flush() {
    if (writer != null) {
      writer.flush();
    }
  }
}
