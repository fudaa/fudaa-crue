package org.fudaa.dodico.crue.aoc.exec.writer;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.dclm.AocDclmDataItem;
import org.fudaa.dodico.crue.aoc.dclm.AocDclmDataItemByEMH;
import org.fudaa.dodico.crue.aoc.exec.*;
import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculPermanent;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.metier.emh.LoiDF;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * @author deniger
 */
public class AocExecWriterHelper {
  public static final String DELIMITER = ";";

  private AocExecWriterHelper() {
  }

  public static void writeToFiles(CtuluLog log, List<String> lines, File resultFile) {
    try {
      FileUtils.writeLines(resultFile, Charsets.ISO_8859_1.name(), lines);
    } catch (Exception ex) {
      log.addError("aoc.exec.write.result.cantWriteFile", resultFile.getAbsolutePath(), ex.getMessage());
      Logger.getLogger(AocExecWriterHelper.class.getName()).log(Level.SEVERE, "cantwriteFile", ex);
    }
  }

  /**
   * @param formatter le formateur pour les valeurs reelees
   * @param entry les données dclm rangés par emh
   * @param calculs la liste des calculs pour lesquels les valeurs sont attendues
   * @return la chaine formattée.
   */
  public static String formatDclmLines(NumberFormat formatter, AocDclmDataItemByEMH entry, Collection<String> calculs) {
    StringJoiner sj = new StringJoiner(DELIMITER);
    sj.add(entry.getNom());
    calculs.forEach(calculRef -> sj.add(AocExecFormatterHelper.formatValue(formatter, entry.getValue(calculRef))));
    return sj.toString();
  }

  public static void addDebitValueToLines(List<String> lines, List<AocDclmDataItem> dclmValues, final NumberFormat qFormater) {
    final Map<String, Double> values = new TreeMap<>();
    dclmValues.forEach(e -> values.put(e.getCalculRef() + " / " + e.getEmhRef(), e.getValue()));

    StringJoiner qName = new StringJoiner(DELIMITER);
    StringJoiner qValues = new StringJoiner(DELIMITER);
    values.forEach((key, value) -> {
      qName.add(key);
      qValues.add(AocExecFormatterHelper.formatValue(qFormater, value));
    });

    lines.add(qName.toString());
    lines.add(qValues.toString());
  }

  public static void addFrottementValuesToLines(List<String> lines, NumberFormat dfrtFormater, Map<String, Double> dfrtValues) {
    final Map<String, Double> dfrtValuesRetained = new TreeMap<>(dfrtValues);
    StringJoiner frtName = new StringJoiner(DELIMITER);
    StringJoiner frtValues = new StringJoiner(DELIMITER);

    dfrtValuesRetained.forEach((key, value) -> {
      frtName.add(key);
      frtValues.add(AocExecFormatterHelper.formatValue(dfrtFormater, value));
    });
    lines.add(frtName.toString());
    lines.add(frtValues.toString());
  }

  public static List<String> createBilanHeader(AocExecContainer result) {
    return createBilanHeader(result.getResultContainer().getTimeInfo());
  }

  public static List<String> createBilanHeader(AocExecTimeInfo timeInfo) {
    List<String> lines = new ArrayList<>();
    NumberFormat fm = new DecimalFormat();
    fm.setMaximumFractionDigits(0);
    lines.add("Date Démarrage;" + timeInfo.getStartDateTime());
    lines.add("Date Fin;" + timeInfo.getEndDateTime());
    lines.add("Duree simulation (en sec);" + fm.format(timeInfo.getSimulationDurationInSec()));
    lines.add("Duree simulation Crue X (en sec);" + fm.format(timeInfo.getSimulationDurationCrueXInSec()));
    return lines;
  }

  /**
   * A utiliser que dans le cas permanent. Dans le cas transitoire, l'iteration ne contient pas de DCLM donc ne doit rien ecrire.
   * si pas de debit on n'ecrit pas.
   *
   * @param aocExecContainer le conteneur
   * @param resultIteration l'iteration a prendre en compte
   * @param log le log
   * @param debitResultFile le fichier de resultat
   * @param combinaison true si ecriture pour une combinaison
   */
  public static void writeDclmResults(boolean combinaison, AocExecContainer aocExecContainer, AocExecResultIteration resultIteration, CtuluLog log, File debitResultFile) {
    final List<AocDclmDataItemByEMH> byEMHSorted = resultIteration.getDclmSortedByEMH();
    //pas de dclm -> on ne fait rien
    if (byEMHSorted == null) {
      return;
    }
    //Les calculs permanents sont pris en compte uniquement.
    final Set<String> calculs = aocExecContainer.getCampagne().getDonnees().getLoisCalculsPermanents().getLois().stream().map(AocLoiCalculPermanent::getCalculRef).collect(
        Collectors.toCollection(LinkedHashSet::new));
    List<String> lines = new ArrayList<>();

    if (combinaison) {
      lines.add(String.format("#Valeurs DCLM pour la combinaison %d", resultIteration.getIndex() + 1));
    }

    lines.add("EMH;" + String.join(DELIMITER, calculs));
    final NumberFormat formatter = AocExecFormatterHelper.getQFormater(aocExecContainer);
    byEMHSorted.forEach(entry -> lines.add(formatDclmLines(formatter, entry, calculs)));

    writeToFiles(log, lines, debitResultFile);
  }

  /**
   * @param aocExecContainer le conteneur de resultat. Les données obtenues avec les stricklers en tant qu'entier sont prise en compte.
   * @param log le log
   * @param iterationTroncature les valeurs tronquées.
   * @param iterationOptimum les valeurs tronquées.
   * @param dfrt le fichier qui doit contenir les valeurs des frottements
   * @param combinaison true si ecriture pour une combinaison
   */
  public static void writeDfrtResults(boolean combinaison, AocExecContainer aocExecContainer, AocExecResultIteration iterationTroncature, AocExecResultIteration iterationOptimum,
                                      CtuluLog log,
                                      File dfrt) {
    final Map<String, Double> troncatureValues = new TreeMap<>(iterationTroncature.getDfrtValues());
    final Map<String, Double> allInitFrottements = aocExecContainer.getResultContainer().getResultIterations().get(0).getDfrtValues();
    List<String> lines = new ArrayList<>();
    final NumberFormat formatter = AocExecFormatterHelper.getDfrtFormater(aocExecContainer);

    if (combinaison) {
      String entete;
      entete = "#Valeurs DFRT pour la combinaison %d";
      lines.add(String.format(entete, iterationTroncature.getIndex() + 1));
    }

    lines.add("Nom loi;Valeur initiale;Valeur Optimum;Valeur Troncature");
    troncatureValues
        .forEach((key, troncatureValue) -> lines.add(AocExecFormatterHelper.format(formatter, key, troncatureValue, iterationOptimum.getDfrtValues(), allInitFrottements)));
    writeToFiles(log, lines, dfrt);
  }

  public static void writeErrorControlPointsPermanent(AocExecResultIteration iteration, AocExecContainer result, List<String> lines) {
    //Construction des lignes titre
    StringJoiner pkJoiner = new StringJoiner(DELIMITER);
    StringJoiner sectionJoiner = new StringJoiner(DELIMITER);

    pkJoiner.add("Calcul");
    pkJoiner.add("Loi");
    pkJoiner.add("Variable");
    pkJoiner.add("Profils");

    sectionJoiner.add("Calcul");
    sectionJoiner.add("Loi");
    sectionJoiner.add("Variable");
    sectionJoiner.add("Sections");
    List<String> sections = new ArrayList<>();
    result.getLhpt().getEchellesSections().getEchellesSectionList().forEach(aocProfilSection -> {
      sections.add(aocProfilSection.getSectionRef());
      sectionJoiner.add(aocProfilSection.getSectionRef());
      pkJoiner.add(aocProfilSection.getPk());
    });
    lines.add(pkJoiner.toString());
    lines.add(sectionJoiner.toString());

    for (AocExecResult resultByCalcul : iteration.getAllResults()) {
      for (AocExecResultByEMH resultByEMH : resultByCalcul.getResultsByVariableName().values()) {
        final String firstCol = resultByCalcul.getNomCalcul() + AocExecWriterHelper.DELIMITER + resultByCalcul.getNomLoi();
        lines.add(resultByEMH.getObservedValuesAsString(sections, firstCol, resultByEMH.getNomVariableObserved() + AocExecWriterHelper.DELIMITER));
        lines.add(resultByEMH.getComputedValuesAsString(sections, firstCol, resultByEMH.getNomVariableComputed() + AocExecWriterHelper.DELIMITER));
        lines.add(resultByEMH.getDeltaValuesAsString(sections, firstCol, resultByEMH.getNomVariableDelta() + AocExecWriterHelper.DELIMITER));
      }
    }
  }

  /**
   * @param resultatByLoi les resultats
   * @param loi
   * @return la valeur ou vide si non definie
   */
  public static String getLoiValue(Map<String, AocExecResult> resultatByLoi, String loi) {
    if (resultatByLoi.containsKey(loi)) {
      return Double.toString(resultatByLoi.get(loi).getCritere());
    }
    return StringUtils.EMPTY;
  }

  public static void writeControlPointsTransitoire(AocExecResultIteration iteration, AocExecContainer aocExecContainer, List<String> lines, String prefix) {
    for (AocExecResult resultByCalcul : iteration.getAllResults()) {
      //les valeurs des criteres aux points de controle (une seule section en fait).
      writeControlPointsTransitoireCriteres(lines, resultByCalcul, prefix);
      final LoiDF hydroOrLimniObserved = resultByCalcul.getHydroOrLimniObserved();
      String courbeName = "#Valeurs sur " + aocExecContainer.getCampagne().getTypeCalageDonnees().getCourbeName() + " " + hydroOrLimniObserved.getNom();
      if (hydroOrLimniObserved.getDateZeroLoiDF() != null) {
        courbeName = courbeName + " [DateZeroLoiDF=" + hydroOrLimniObserved.getDateZeroLoiDF().toString() + "]";
      }
      lines.add(StringUtils.defaultString(prefix) + courbeName);
      final ItemVariable variableOrdonnee = AocExecFormatterHelper.getVariableOrdonnee(aocExecContainer, hydroOrLimniObserved);
      final ItemVariable variableAbscisse = AocExecFormatterHelper.getVariableAbscisse(aocExecContainer, hydroOrLimniObserved);

      //le writer permettant de ranger les valeurs pas temps
      AocExecTransitoireControlPointWriter controlPointWriter = AocExecTransitoireControlPointWriter.create(hydroOrLimniObserved, aocExecContainer);
      hydroOrLimniObserved.getEvolutionFF().getPtEvolutionFF()
          .forEach(ptEvolutionFF -> controlPointWriter.addValueObserved(ptEvolutionFF.getAbscisse(), ptEvolutionFF.getOrdonnee()));

      resultByCalcul.getHydroOrLimniCompute().getEvolutionFF().getPtEvolutionFF()
          .forEach(ptEvolutionFF -> controlPointWriter.addValueComputed(ptEvolutionFF.getAbscisse(), ptEvolutionFF.getOrdonnee()));

      controlPointWriter.addToLines(
          StringUtils.defaultString(prefix) + variableAbscisse.getDisplayNom(),
          StringUtils.defaultString(prefix) + AocExecVariableHelper.getNomVariableObserved(variableOrdonnee.getDisplayNom()),
          StringUtils.defaultString(prefix) + AocExecVariableHelper.getNomVariableComputed(variableOrdonnee.getDisplayNom()),
          StringUtils.defaultString(prefix) + AocExecVariableHelper.getNomVariableDelta(variableOrdonnee.getDisplayNom()),
          lines
      );
    }
  }

  private static void writeControlPointsTransitoireCriteres(List<String> lines, AocExecResult resultByCalcul, String prefix) {
    String section = resultByCalcul.getSection();
    StringJoiner enteteJoiner = new StringJoiner(DELIMITER);
    enteteJoiner.add("Calcul");
    enteteJoiner.add("Loi");
    enteteJoiner.add("Section");
    StringJoiner valuesJoiner = new StringJoiner(DELIMITER);
    valuesJoiner.add(resultByCalcul.getNomCalcul());
    valuesJoiner.add(resultByCalcul.getNomLoi());
    valuesJoiner.add(section);
    for (AocExecResultByEMH resultByEMH : resultByCalcul.getResultsByVariableName().values()) {
      //dans le Quadratique on n'a pas de valeur observe/calcule
      if (resultByEMH.containsObservedValue()) {
        enteteJoiner.add(resultByEMH.getNomVariableObserved());
        enteteJoiner.add(resultByEMH.getNomVariableComputed());

        valuesJoiner.add(resultByEMH.getObservedValueAsString(section));
        valuesJoiner.add(resultByEMH.getComputedValueAsString(section));
      }
      enteteJoiner.add(resultByEMH.getNomVariableDelta());
      valuesJoiner.add(resultByEMH.getDeltaValueAsString(section));
    }
    lines.add(StringUtils.defaultString(prefix) + enteteJoiner.toString());
    lines.add(StringUtils.defaultString(prefix) + valuesJoiner.toString());
  }

  /**
   * @param aocExecContainer le conteneur
   * @param lines les lines
   * @param loiComputed la loi a ecrire
   * @param abscissePrefix le prefix pour les absicsses
   * @param ordonneePrefix le prefix pour les ordonnees
   */
  public static void writeLoi(AocExecContainer aocExecContainer, List<String> lines, LoiDF loiComputed, String abscissePrefix, String ordonneePrefix) {
    //Ecriture des x
    writeLoiAbscisse(aocExecContainer, lines, loiComputed, abscissePrefix);

    //Ecriture des y
    writeLoiOrdonnee(aocExecContainer, lines, loiComputed, ordonneePrefix);
  }

  private static void writeLoiOrdonnee(AocExecContainer aocExecContainer, List<String> lines, LoiDF loiComputed, String ordonneePrefix) {
    //le nom de la variable en ordonnée
    final NumberFormat yFormater = AocExecFormatterHelper.getOrdonneeFormater(aocExecContainer, loiComputed);
    StringJoiner ordonneeJoiner = new StringJoiner(DELIMITER);
    if (ordonneePrefix != null) {
      ordonneeJoiner.add(ordonneePrefix);
    }
    ordonneeJoiner.add(AocExecFormatterHelper.getVariableOrdonnee(aocExecContainer, loiComputed).getDisplayNom());
    loiComputed.getEvolutionFF().getPtEvolutionFF().forEach(point -> ordonneeJoiner.add(yFormater.format(point.getOrdonnee())));
    lines.add(ordonneeJoiner.toString());
  }

  private static void writeLoiAbscisse(AocExecContainer aocExecContainer, List<String> lines, LoiDF loiComputed, String abscissePrefix) {
    final NumberFormat tFormater = AocExecFormatterHelper.getAbscisseFormater(aocExecContainer, loiComputed);
    StringJoiner abscisseJoiner = new StringJoiner(DELIMITER);
    if (abscissePrefix != null) {
      abscisseJoiner.add(abscissePrefix);
    }
    abscisseJoiner.add(AocExecFormatterHelper.getVariableAbscisse(aocExecContainer, loiComputed).getDisplayNom());
    loiComputed.getEvolutionFF().getPtEvolutionFF().forEach(point -> abscisseJoiner.add(tFormater.format(point.getAbscisse())));
    lines.add(abscisseJoiner.toString());
  }

  public static void writeCrue10Failures(AocExecContainer result, CtuluLog log, File failuresReportFile) {
    final List<AocExecAlgoResult> errorInCrue10 = result.getResultContainer().getDataProducingFatalErrorInCrue10();
    List<String> lines = new ArrayList<>();
    final NumberFormat dfrtFormater = AocExecFormatterHelper.getDfrtFormater(result);
    final NumberFormat qFormater = AocExecFormatterHelper.getQFormater(result);
    if (CollectionUtils.isNotEmpty(errorInCrue10)) {

      errorInCrue10.forEach(res -> {
        lines.add("iteration " + (res.getIteration() + 1));
        AocExecWriterHelper.addFrottementValuesToLines(lines, dfrtFormater, res.getDfrtValues());
        if (res.getDclmValues() != null) {
          AocExecWriterHelper.addDebitValueToLines(lines, res.getDclmValues(), qFormater);
        }
        lines.add("");
      });
      AocExecWriterHelper.writeToFiles(log, lines, failuresReportFile);
    }
  }
}
