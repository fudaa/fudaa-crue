package org.fudaa.dodico.crue.aoc.io;

import org.fudaa.dodico.crue.aoc.batch.AocCampagneResult;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;

/**
 * Permet de sauvegarder le résultat d'une campagne AOC
 *
 * @author Fred Deniger
 */
public class CrueRAOCReaderWriter extends CrueDataXmlReaderWriterImpl<CrueDaoRAOC, AocCampagneResult> {
  public static final String LAST_VERSION = "1.3";
  public static final String FILE_TYPE = "raoc";

  public CrueRAOCReaderWriter(final String version) {
    super(FILE_TYPE, version, new CrueConverterRAOC(), new CrueDaoStructureRAOC());
  }
}
