package org.fudaa.dodico.crue.aoc.projet;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;

/**
 * Created by deniger on 28/06/2017.
 */
public class AocParamCalc implements ObjetWithID {
    private String calculRef;
    private String emhRef;
    private double deltaQ;

    public AocParamCalc() {

    }

    public AocParamCalc(AocParamCalc other) {
        if (other != null) {
            calculRef = other.calculRef;
            emhRef = other.emhRef;
            deltaQ = other.deltaQ;
        }
    }

    public AocParamCalc(String calculRef, String emhRef, double deltaQ) {
        this.calculRef = calculRef;
        this.emhRef = emhRef;
        this.deltaQ = deltaQ;
    }

    public AocParamCalc(String calculRef, String emhRef) {
        this(calculRef, emhRef, AocTempoCCM.DEFAULT_DELTA);
    }

    public String getParamIdentifiant() {
        return calculRef + " / " + emhRef;
    }

    public String getCalculRef() {
        return calculRef;
    }

    public void setCalculRef(String calculRef) {
        this.calculRef = calculRef;
    }

    public String getEmhRef() {
        return emhRef;
    }

    public void setEmhRef(String emhRef) {
        this.emhRef = emhRef;
    }

    @PropertyDesc(i18n = "aoc.deltaQ.property")
    public double getDeltaQ() {
        return deltaQ;
    }

    public void setDeltaQ(double deltaQ) {
        this.deltaQ = deltaQ;
    }

    @Override
    public String getNom() {
        return getId();
    }

    @Override
    public String getId() {
        return getParamIdentifiant();
    }
}
