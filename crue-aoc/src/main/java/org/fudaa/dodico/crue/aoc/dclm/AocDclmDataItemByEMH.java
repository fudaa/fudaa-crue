package org.fudaa.dodico.crue.aoc.dclm;

import org.fudaa.dodico.crue.common.contrat.ObjetWithID;

import java.util.HashMap;
import java.util.Objects;

/**
 * @author deniger
 */
public class AocDclmDataItemByEMH implements ObjetWithID, Comparable<AocDclmDataItemByEMH> {
  private final String emhRef;
  private final HashMap<String, Double> valuesByCalul = new HashMap<>();

  public AocDclmDataItemByEMH(String emhRef) {
    this.emhRef = emhRef;
  }

  public Double getValue(String calculRef) {
    return valuesByCalul.get(calculRef);
  }


  public void addValues(AocDclmDataItem dataItem){
    valuesByCalul.put(dataItem.getCalculRef(), dataItem.getValue());
  }

  @Override
  public String getId() {
    return emhRef;
  }

  @Override
  public String getNom() {
    return emhRef;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AocDclmDataItemByEMH that = (AocDclmDataItemByEMH) o;
    return emhRef.equals(that.emhRef);
  }

  @Override
  public int hashCode() {
    return Objects.hash(emhRef);
  }

  @Override
  public int compareTo(AocDclmDataItemByEMH o) {
    return emhRef.compareTo(o.emhRef);
  }
}
