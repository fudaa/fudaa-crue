package org.fudaa.dodico.crue.aoc.compute;

/**
 * Calcul du RMSE
 *
 * @author deniger
 */
public class RmseCompute {
  private long n = 0;
  private double sum = 0;
  private double max = 0;
  private double min = 0;

  /**
   * Attention ce sont les valeurs observees et calculees
   *
   * @param expected observe
   * @param actual calculé
   */
  public void add(double expected, double actual) {
    updateMinMaxExpected(expected);
    n++;
    sum = sum + (expected - actual) * (expected - actual);
  }

  public void updateMinMaxExpected(double expected) {
    if (n == 0) {
      max = expected;
      min = expected;
    } else {
      max = Math.max(this.max, expected);
      min = Math.min(this.min, expected);
    }
  }

  public long getN() {
    return n;
  }

  public double getRMSE() {
    if(n==0){
      return Double.NaN;
    }
    return Math.sqrt(this.sum / ((double) this.n));
  }

  public double getNormalized() {
    return this.getRMSE() / (this.max - this.min);
  }

  @Override
  public String toString() {
    return "RMSE [n=" + n + ", value=" + getRMSE() + ", max=" + max + ", min=" + min + "]";
  }
}
