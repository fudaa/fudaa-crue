package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.crue.aoc.dclm.AocDclmDataItem;
import org.fudaa.dodico.crue.aoc.helper.AocValidationCroiseeLine;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Contient les résultats d'une simulation
 *
 * @author deniger
 */
public class AocExecResultContainer {
  private List<AocExecResultIteration> resultIterations = new ArrayList<>();
  private AocExecResultIteration iterationWithMinCritere;
  private final AocExecTimeInfo timeInfo = new AocExecTimeInfo();
  private String suffix;
  /**
   * utilise que dans le cas de la validation croisee
   */
  private AocValidationCroiseeLine validationCroiseeLine = null;
  private EnumAocExecCrue10Failure iterationsStoppedDueToCrue10Failure;

  public AocValidationCroiseeLine getValidationCroiseeLine() {
    return validationCroiseeLine;
  }

  public void setValidationCroiseeLine(AocValidationCroiseeLine validationCroiseeLine) {
    this.validationCroiseeLine = validationCroiseeLine;
  }

  /**
   * @param calcul le nom du calcul a tester
   * @return true si c'est un calcul d'apprentissage
   */
  public boolean isApprentissage(String calcul) {
    return validationCroiseeLine != null && validationCroiseeLine.isApprentissage(calcul);
  }

  /**
   * @param calcul le nom du calcul a tester
   * @return true si c'est un calcul de validation
   */
  public boolean isValidation(String calcul) {
    return validationCroiseeLine != null && validationCroiseeLine.isValidation(calcul);
  }

  public AocExecResultIteration getIterationWithOptimumStricklerAsInteger() {
    final Optional<AocExecResultIteration> first = resultIterations.stream().filter(AocExecResultIteration::isOptimumtricklerAsInt).findFirst();
    return first.orElse(null);
  }

  public String getSuffixToAddForFile() {
    return suffix == null ? CtuluLibString.EMPTY_STRING : suffix;
  }

  public String getSuffix() {
    return suffix;
  }

  public void setSuffix(String suffix) {
    this.suffix = suffix;
  }

  /**
   * @return resultat des iterations
   */
  public List<AocExecResultIteration> getResultIterations() {
    return resultIterations;
  }

  /**
   * @param resultIterations resultat des iterations
   */
  public void setResultIterations(List<AocExecResultIteration> resultIterations) {
    this.resultIterations = resultIterations;
  }

  public AocExecResultIteration getIterationWithMinCritere() {
    return iterationWithMinCritere;
  }

  public void setIterationWithMinCritere(AocExecResultIteration iterationWithMinCritere) {
    this.iterationWithMinCritere = iterationWithMinCritere;
  }

  public AocExecTimeInfo getTimeInfo() {
    return timeInfo;
  }

  public void setiterationsStoppedDueToCrue10Failure(EnumAocExecCrue10Failure iterationsStoppedDueToCrue10Failure) {
    this.iterationsStoppedDueToCrue10Failure = iterationsStoppedDueToCrue10Failure;
  }

  /**
   * @return true si les itérations ont été stoppées à causes d'erreurs crue10
   * @see EnumAocExecCrue10Failure
   */
  public boolean isStoppedDueToCrue10Error() {
    return iterationsStoppedDueToCrue10Failure != null;
  }

  public EnumAocExecCrue10Failure getIterationsStoppedDueToCrue10Failure() {
    return iterationsStoppedDueToCrue10Failure;
  }


  private final List<AocExecAlgoResult> dataProducingFatalErrorInCrue10=new ArrayList<>();

  public List<AocExecAlgoResult> getDataProducingFatalErrorInCrue10() {
    return dataProducingFatalErrorInCrue10;
  }

  public void addFatalErrorCrue10(Map<String, Double> dfrtValues, List<AocDclmDataItem> dclmValues, int iteration) {
    AocExecAlgoResult result = new AocExecAlgoResult(dfrtValues,dclmValues);
    result.setIteration(iteration);
    dataProducingFatalErrorInCrue10.add(result);
  }
}
