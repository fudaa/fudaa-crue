package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.ResultatTimeKey;
import org.fudaa.dodico.crue.metier.result.ModeleResultatTimeKeyContent;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
public class AocExecResultReader {
  private final ModeleResultatTimeKeyContent timeContent;
  private final EMHScenario scenario;
  private final Map<String, EMH> emhByName;
  private final Map<String, ResultatTimeKey> permanentTimeByCalculName;
  private final Map<String, List<ResultatTimeKey>> transientTimeByCalculName;

  public AocExecResultReader(EMHScenario scenario) {
    this.scenario = scenario;
    final EMHModeleBase defaultModele = scenario.getModeles().get(0);
    timeContent = ModeleResultatTimeKeyContent.extract(defaultModele);
    emhByName = scenario.getIdRegistry().getEmhByNom();
    Map<String, List<ResultatTimeKey>> timesByCalculName = ModeleResultatTimeKeyContent.createMapByCalcul(timeContent.getPermanentsTimes());
    permanentTimeByCalculName = new HashMap<>();
    for (Map.Entry<String, List<ResultatTimeKey>> stringListEntry : timesByCalculName.entrySet()) {
      permanentTimeByCalculName.put(stringListEntry.getKey(), stringListEntry.getValue().get(0));
    }
    transientTimeByCalculName = ModeleResultatTimeKeyContent.createMapByCalcul(timeContent.getTransitoiresTimes());
  }

  public ResultatTimeKey getPermanentResultatKey(String calculName){
    return permanentTimeByCalculName.get(calculName);
  }

  public  List<ResultatTimeKey> getTransientKey(String calculName){
    return transientTimeByCalculName.get(calculName);
  }

  public ModeleResultatTimeKeyContent getTimeContent() {
    return timeContent;
  }

  public EMHScenario getScenario() {
    return scenario;
  }

  /**
   * @param emhNom le nom de l'emh
   * @param timeKey le temps demande
   * @param variableName la variable
   * @return valeur lue ou null si non trouvée.
   */
  public Double readValue(String emhNom, ResultatTimeKey timeKey, String variableName) {
    EMH emh = emhByName.get(emhNom);
    if (emh == null || emh.getResultatCalcul() == null) {
      return null;
    }

    try {
      final Map<String, Object> read = emh.getResultatCalcul().read(timeKey);
      return (Double) read.get(variableName);
    } catch (IOException e) {
      Logger.getLogger(getClass().getName()).log(Level.SEVERE, "error in read emh= " + emhNom + " for time= " + timeKey + " and var=" + variableName, e);
    }
    return null;
  }
}
