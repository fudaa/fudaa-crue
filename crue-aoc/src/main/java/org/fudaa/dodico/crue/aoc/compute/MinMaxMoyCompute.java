package org.fudaa.dodico.crue.aoc.compute;

/**
 * Permet de calcul un min/max et une moyenne d'une serie de valeur.
 * Attention, renvoie la valeur {@link #getMax()} pour {@link #getValue()}
 *
 * @author deniger
 */
public class MinMaxMoyCompute implements ComputeXYContrat {
  private double min;
  private double max;
  private int size;
  private double sum;

  public double getMin() {
    return min;
  }

  public double getMax() {
    return max;
  }

  @Override
  public int getNbValues() {
    return size;
  }

  /**
   * @return {@link #getValue()}
   */
  @Override
  public double getValue() {
    return getMax();
  }

  public boolean isEmpty() {
    return size == 0;
  }

  public boolean isNotEmpty() {
    return size > 0;
  }

  /**
   * fait le calcul sur y seulement
   *
   * @param x la valeur de l'abscisse
   * @param y la valeur de l'ordonnee
   */
  @Override
  public void addXY(double x, double y) {
    add(y);
  }

  public void add(double newValue) {
    if (size == 0) {
      min = newValue;
      max = newValue;
    } else {
      min = Math.min(newValue, min);
      max = Math.max(newValue, max);
    }

    sum = sum + newValue;
    size++;
  }

  public double getMoy() {
    if (size == 0) {
      return 0;
    }
    return sum / size;
  }
}
