package org.fudaa.dodico.crue.aoc.validation;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluDefaultLogFormatter;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculPermanent;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.validation.ValidatingItem;

import java.util.*;

/**
 * Classe permettant de valider une liste de  AocLoiCalculPermanent
 *
 * @author deniger
 */
public class AocLoiCalculPermanentValidator {
  public static HashSet<String> createHashSet(String[] initValues) {
    return new HashSet<>(initValues == null ? Collections.emptyList() : Arrays.asList(initValues));
  }

  public static HashSet<String> createHashSet(List<String> initValues) {
    return new HashSet<>(initValues == null ? Collections.emptyList() : initValues);
  }

  /**
   * @param nodes list des {@link AocLoiCalculPermanent}
   * @param availableCalculsArray les calculs disponibles dans le scnario
   * @param availableLoisArray les lois disponibles dans LHPT
   */
  public CtuluLog validate(List<ValidatingItem<AocLoiCalculPermanent>> nodes, String[] availableCalculsArray, String[] availableLoisArray) {
    return validate(nodes, Arrays.asList(availableCalculsArray), Arrays.asList(availableLoisArray));
  }

  /**
   * @param nodes list des {@link AocLoiCalculPermanent}
   * @param availableCalculsArray les calculs disponibles dans le scnario
   * @param availableLoisArray les lois disponibles dans LHPT
   */
  public CtuluLog validate(List<ValidatingItem<AocLoiCalculPermanent>> nodes, List<String> availableCalculsArray, List<String> availableLoisArray) {
    CtuluLog res = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    res.setDesc("aoc.validation.loiCalculPermanent");
    if (nodes.isEmpty()) {
      return res;
    }
    int idx = 0;
    Set<String> availableCalculs = createHashSet(availableCalculsArray);
    Set<String> availableLois = createHashSet(availableLoisArray);
    Set<String> usedLois = new HashSet<>();
    for (ValidatingItem<AocLoiCalculPermanent> node : nodes) {
      idx++;
      List<CtuluLogRecord> errorMsg = new ArrayList<>();

      //Validation du nom de calcul
      String calculRef = node.getObjectToValidate().getCalculRef();
      if (StringUtils.isBlank(calculRef)) {
        errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiCalculPermanent.calculBlank", idx));
      } else {
        if (!availableCalculs.contains(calculRef)) {
          errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiCalculPermanent.calculNotFound", idx, calculRef));
        }
      }

      //Validation de la loi
      String loiRef = node.getObjectToValidate().getLoiRef();
      if (StringUtils.isBlank(loiRef)) {
        errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiCalculPermanent.loiBlank", idx));
      } else {
        if (usedLois.contains(loiRef.toUpperCase())) {
          errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiCalculPermanent.loiRefAlreadyUsed", idx, loiRef));
        }
        if (!availableLois.contains(loiRef)) {
          errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiCalculPermanent.loiNotFound", idx, loiRef));
        }
        if (!loiRef.toUpperCase().startsWith(EnumTypeLoi.LoiSectionsZ.getNom().toUpperCase())) {
          errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiCalculPermanent.loiInvalid", idx, loiRef));
        }
        usedLois.add(loiRef.toUpperCase());
      }

      //ponderation
      if (node.getObjectToValidate().getPonderation() < 1) {
        errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.loiCalculPermanent.ponderationInvalid", idx));
      }

      if (!errorMsg.isEmpty()) {
        node.setErrorMessage(CtuluDefaultLogFormatter.asHtml(errorMsg, BusinessMessages.RESOURCE_BUNDLE));
      } else {
        node.clearMessage();
      }
    }
    res.updateLocalizedMessage(BusinessMessages.RESOURCE_BUNDLE);
    return res;
  }
}
