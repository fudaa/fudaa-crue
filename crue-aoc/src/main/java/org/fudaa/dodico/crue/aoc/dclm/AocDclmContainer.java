package org.fudaa.dodico.crue.aoc.dclm;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.dodico.crue.aoc.helper.AocParamCalcExtract;
import org.fudaa.dodico.crue.aoc.helper.AocParamCalcSorter;
import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.aoc.projet.AocParamCalc;
import org.fudaa.dodico.crue.aoc.projet.AocTempoCCM;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.PropertyEpsilon;
import org.fudaa.dodico.crue.metier.emh.CalcPseudoPermItem;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Permet de générer les valeurs dclm
 *
 * @author deniger
 */
public class AocDclmContainer {
  private boolean hasDeltaQ;
  private final List<AocDclmItem> items = new ArrayList<>();

  /**
   * @param aocCampagne la campagne
   * @param scenario le scenario
   * @param ccm le CrueConfigMetier
   * @return le builder
   */
  public static AocDclmContainer create(AocCampagne aocCampagne, EMHScenario scenario, CrueConfigMetier ccm) {
    AocDclmContainer res = new AocDclmContainer();
    final PropertyEpsilon epsilon = AocTempoCCM.getDeltaQ().getEpsilon();
    final Map<String, List<CalcPseudoPermItem>> extract = new AocParamCalcExtract().extract(aocCampagne, scenario);
    AocParamCalcSorter sortedValues = new AocParamCalcSorter(aocCampagne.getDonnees().getListeCalculs());
    for (Map.Entry<String, List<CalcPseudoPermItem>> entry : extract.entrySet()) {
      String nomCalcul = entry.getKey();
      final List<CalcPseudoPermItem> donCLimMs = entry.getValue();
      for (CalcPseudoPermItem donCLimM : donCLimMs) {
        final String nomEMH = donCLimM.getEmh().getNom();
        AocParamCalc value = sortedValues.getAndRemove(nomCalcul, nomEMH);
        if (value != null) {
          final boolean isZero = epsilon.isZero(value.getDeltaQ());
          res.items.add(new AocDclmItem(value, donCLimM.getValue(), isZero));
          if (!isZero) {
            res.hasDeltaQ = true;
          }
        }
      }
    }
    return res;
  }

  public boolean hasDeltaQ() {
    return hasDeltaQ;
  }

  /**
   * @return initial values
   */
  public List<AocDclmDataItem> generateNotModifiedValues() {
    return items.stream().map(AocDclmDataItem::new).collect(Collectors.toList());
  }

  /**
   * @return initial values
   */
  public List<AocDclmDataItem> generateRandomValues() {
    return items.stream().map(AocDclmItem::generateRandom).collect(Collectors.toList());
  }

  public List<AocDclmDataItem> generateMaxValues() {
    return items.stream().map(AocDclmItem::generateMax).collect(Collectors.toList());
  }

  public List<AocDclmDataItem> generateMinValues() {
    return items.stream().map(AocDclmItem::generateMin).collect(Collectors.toList());
  }

  /**
   * @return true si des elements de configuration sont présents.
   */
  public boolean isNotEmpty() {
    return CollectionUtils.isNotEmpty(items);
  }
}
