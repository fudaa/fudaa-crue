package org.fudaa.dodico.crue.aoc.projet;

import org.fudaa.dodico.crue.common.annotation.PropertyDesc;

/**
 * Created by deniger on 28/06/2017.
 */
public class AocLoiStrickler {
  private String loiRef;
  private int min;
  private int ini;
  private int max;

  public AocLoiStrickler() {

  }

  public AocLoiStrickler(String loiRef, int min, int ini, int max) {
    this.loiRef = loiRef;
    this.min = min;
    this.ini = ini;
    this.max = max;
  }

  public double getIniAsDouble() {
    return (double) ini;
  }

  public double getMinAsDouble() {
    return (double) getMin();
  }

  public double getMaxAsDouble() {
    return (double) getMax();
  }

  public String getLoiRef() {
    return loiRef;
  }

  public void setLoiRef(String loiRef) {
    this.loiRef = loiRef;
  }

  @PropertyDesc(i18n = "aoc.min.property")
  public int getMin() {
    return min;
  }

  public void setMin(int min) {
    this.min = min;
  }

  @PropertyDesc(i18n = "aoc.ini.property")
  public int getIni() {
    return ini;
  }

  public void setIni(int ini) {
    this.ini = ini;
  }

  @PropertyDesc(i18n = "aoc.max.property")
  public int getMax() {
    return max;
  }

  public void setMax(int max) {
    this.max = max;
  }

  public AocLoiStrickler clone() {
    return new AocLoiStrickler(getLoiRef(), getMin(), getIni(), getMax());
  }
}
