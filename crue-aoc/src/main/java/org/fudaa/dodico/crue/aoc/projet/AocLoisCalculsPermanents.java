package org.fudaa.dodico.crue.aoc.projet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deniger on 28/06/2017.
 */
public class AocLoisCalculsPermanents {
    private final List<AocLoiCalculPermanent> lois = new ArrayList<>();

    public List<AocLoiCalculPermanent> getLois() {
        return lois;
    }

    /**
     * @param loiCalculPermanents les nouvelles lois qui seront clonées
     */
    public void setClonedLois(List<AocLoiCalculPermanent> loiCalculPermanents) {
        this.lois.clear();
        if (loiCalculPermanents != null) {
            for (AocLoiCalculPermanent aocLoiCalculPermanent : loiCalculPermanents) {
                this.lois.add(aocLoiCalculPermanent.clone());
            }
        }
    }

    public void addLoiCalculPermanent(String calculRef, String loiRef, int ponderation) {
        lois.add(new AocLoiCalculPermanent(calculRef, loiRef, ponderation));
    }
}
