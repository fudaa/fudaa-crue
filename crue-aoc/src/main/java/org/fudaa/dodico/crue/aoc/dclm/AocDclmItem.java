package org.fudaa.dodico.crue.aoc.dclm;

import org.fudaa.dodico.crue.aoc.projet.AocParamCalc;

import java.util.concurrent.ThreadLocalRandom;

/**
 * @author deniger
 */
public class AocDclmItem {
    private final AocParamCalc paramCalc;
    private final double initValue;
    private final boolean isZero;

    public AocDclmItem(AocParamCalc paramCalc, double initValue, boolean isZero) {
        this.paramCalc = paramCalc;
        this.initValue = initValue;
        this.isZero = isZero;
    }

    public boolean isZero() {
        return isZero;
    }

    public AocDclmDataItem generateRandom() {
        final double deltaQ = paramCalc.getDeltaQ();
        if (isZero || deltaQ <= 0) {
            return new AocDclmDataItem(this);
        }
        double newValue = ThreadLocalRandom.current().nextDouble(initValue * (1 - deltaQ), initValue * (1 + deltaQ));
        return new AocDclmDataItem(paramCalc.getCalculRef(), paramCalc.getEmhRef(), newValue, deltaQ, isZero);
    }

    public double getInitValue() {
        return initValue;
    }

    public AocParamCalc getParamCalc() {
        return paramCalc;
    }

    /**
     * @return une valeur avec le min possible
     */
    public AocDclmDataItem generateMin() {
        return new AocDclmDataItem(paramCalc.getCalculRef(), paramCalc.getEmhRef(), initValue * (1 - paramCalc.getDeltaQ()), paramCalc.getDeltaQ(), isZero);
    }

    /**
     * @return une valeur avec le max possible
     */
    public AocDclmDataItem generateMax() {
        return new AocDclmDataItem(paramCalc.getCalculRef(), paramCalc.getEmhRef(), initValue * (1 + paramCalc.getDeltaQ()), paramCalc.getDeltaQ(), isZero);
    }
}
