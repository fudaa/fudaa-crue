package org.fudaa.dodico.crue.aoc.validation;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.aoc.helper.AocParamCalcExtract;
import org.fudaa.dodico.crue.aoc.projet.*;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.aoc.EnumAocTypeCalageDonnees;
import org.fudaa.dodico.crue.metier.aoc.LoiMesuresPost;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.metier.helper.LoiHelper;
import org.fudaa.dodico.crue.validation.ValidatingItem;
import org.fudaa.dodico.crue.validation.ValidationHelper;

import java.util.*;

/**
 * Permet de valider complètement une campagne
 *
 * @author deniger
 */
public class AocValidationHelper {
  private final LoiMesuresPost loiMesuresPost;
  private final CrueConfigMetier ccm;
  private final Map<String, String> commentByCalculId;
  private final AocCampagne campagne;
  private final EMHScenario scenario;
  private final List<String> availableCalculTransitoires;
  private final List<String> availableCalculPermanents;
  private final List<String> availableSections;
  private final List<String> availableSectionsLhpt;
  private final List<String> availableFrottements;

  /**
   * @param campagne la campgnes
   * @param scenario le scenario
   * @param loiMesuresPost les données LHPT
   * @param ccm CrueConfigMetier
   */
  public AocValidationHelper(AocCampagne campagne, EMHScenario scenario, LoiMesuresPost loiMesuresPost, CrueConfigMetier ccm) {
    this.campagne = campagne;
    this.ccm = ccm;
    this.scenario = scenario;
    this.loiMesuresPost = loiMesuresPost;
    final List<OrdCalc> ordCalc = scenario.getOrdCalcScenario().getOrdCalc();
    final List<CalcTrans> calcTransList = EMHHelper.collectCalcTrans(ordCalc);
    final List<CalcPseudoPerm> calcPseudoPermList = EMHHelper.collectCalcPseudoPerm(ordCalc);

    commentByCalculId = Collections.unmodifiableMap(createCommentMap(ordCalc));

    availableCalculTransitoires = TransformerHelper.toNom(calcTransList);
    availableCalculPermanents = TransformerHelper.toNom(calcPseudoPermList);
    final List<CatEMHSection> sections = scenario.getSections();
    availableSections = new ArrayList<>();
    availableSectionsLhpt = new ArrayList<>();
    for (CatEMHSection section : sections) {
      availableSectionsLhpt.add(section.getNom());
      if (section.getActuallyActive()) {
        availableSections.add(section.getNom());
      }
    }
    List<DonFrtStrickler> listFrt = EMHHelper.selectClass(EMHHelper.getAllFrottements(scenario).getListFrt(), DonFrtStrickler.class);
    availableFrottements = TransformerHelper.toNom(listFrt);
    availableFrottements.remove("FkSto_K0");
    Collections.sort(availableCalculPermanents);
    Collections.sort(availableCalculTransitoires);
    Collections.sort(availableSections);
    Collections.sort(availableFrottements);
  }

  /**
   * @param campagne la campagne
   * @return true si les lois TZ doivent être utilisées.
   */
  public static boolean useLoiTZForTransitoire(AocCampagne campagne) {
    return EnumAocTypeCalageDonnees.TRANSITOIRE_LIMNIGRAMME.equals(campagne.getTypeCalageDonnees());
  }

  /**
   * @param ordCalcs les ordres de calculs
   * @return map calcul id -> commentaire si commentaire non vide
   */
  private Map<String, String> createCommentMap(List<OrdCalc> ordCalcs) {
    HashMap commentsByCalculId = new HashMap<>();
    for (OrdCalc pseudoPerm : ordCalcs) {
      Calc calc = pseudoPerm.getCalc();
      if (StringUtils.isNotBlank(calc.getCommentaire())) {
        commentsByCalculId.put(calc.getNom(), calc.getCommentaire());
      }
    }
    return commentsByCalculId;
  }

  /**
   * @return map donnant pour les id de calcul le commentaire associé si dispo.
   */
  public Map<String, String> getCommentByCalculId() {
    return Collections.unmodifiableMap(commentByCalculId);
  }

  /**
   * @return les dclm utilisables dans la campagne AOC
   * @see AocParamCalcExtract#extract(AocCampagne, EMHScenario)
   */
  public Map<String, List<CalcPseudoPermItem>> getAvailableDclmByCalcul() {
    return new AocParamCalcExtract().extract(campagne, scenario.getDonCLimMScenario());
  }

  /**
   * @return les frottements utilisables dans la campagne AOC
   */
  public List<String> getAvailableFrottements() {
    return availableFrottements;
  }

  /**
   * @return les frottements utilisables dans la campagne AOC
   */
  public String[] getAvailableFrottementsArray() {
    return availableFrottements.toArray(new String[0]);
  }

  /**
   * @return les calculs permanents utilisables dans la campagne AOC
   */
  public String[] getAvailableCalculPermanentsArray() {
    return availableCalculPermanents.toArray(new String[0]);
  }

  /**
   * @return les calculs transitoires utilisables dans la campagne AOC
   */
  public String[] getAvailableCalculTransitoiresArray() {
    return availableCalculTransitoires.toArray(new String[0]);
  }

  /**
   * @return les sections utilisables dans la campagne AOC
   */
  public String[] getAvailableSectionsArray() {
    return availableSections.toArray(new String[0]);
  }

  public CtuluLogGroup validate() {
    return validate(true);
  }

  /**
   * @param includeLhpt si true, valide également les données LHPT
   * @return résultat de validation de la campagne + LHPT si demandé
   */
  public CtuluLogGroup validate(boolean includeLhpt) {
    CtuluLogGroup res = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    res.setDescription(includeLhpt ? "aoc.validation.campagne.all" : "aoc.validation.campagne.allButLhpt");

    res.addGroup(new AocContentValidator().valid(campagne));
    res.addLog(validateCalculPermanent());
    res.addLog(validateCalculTransitoire());
    res.addLog(validateLoiStricker());
    res.addLog(validateParamCalc());
    res.addLog(validateValidationCroisee());
    res.addLog(validateParametresNumeriques());
    if (includeLhpt) {
      res.addGroup(validateLhpt());
    }
    return res;
  }

  private CtuluLog validateParametresNumeriques() {
    CtuluLog res = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    res.setDesc("aoc.validation.parametresNumeriques");

    final AocParametreNumeriques parametreNumeriques = campagne.getParametreNumeriques();
    if (parametreNumeriques.getTemperatureFinale_Recuit() >= parametreNumeriques.getTemperatureInitiale_Recuit()) {
      res.addError("aoc.validation.temperatureFinalBiggerThanInitiale", parametreNumeriques.getTemperatureInitiale_Recuit(), parametreNumeriques.getTemperatureFinale_Recuit());
    } else if (parametreNumeriques.getTemperatureFinale_Recuit() >= parametreNumeriques.getTemperatureInitiale_Recuit() / 10) {
      res.addWarn("aoc.validation.temperatureFinalNotNormal", parametreNumeriques.getTemperatureInitiale_Recuit(), parametreNumeriques.getTemperatureFinale_Recuit());
    }
    ValidationHelper.validateObject("", res, parametreNumeriques, ccm);
    return res;
  }

  private CtuluLogGroup validateLhpt() {
    return new LHPTContentValidator().valid(loiMesuresPost, new HashSet<>(availableSectionsLhpt), scenario.getNom(), ccm);
  }

  private CtuluLog validateValidationCroisee() {
    return validateValidationCroisee(create(campagne.getDonnees().getValidationCroisee().getGroupes()));
  }

  public CtuluLog validateValidationCroisee(List<ValidatingItem<AocValidationGroupe>> validatingItems) {
    return new AocValidationCroiseeValidator().validate(validatingItems, getAvailableLoisValidationCroisee());
  }

  public String[] getAvailableLoisValidationCroisee() {
    return AocValidationCroiseeValidator.getAvailableLois(campagne);
  }

  private CtuluLog validateParamCalc() {
    return validateParamCalc(create(campagne.getDonnees().getListeCalculs().getParamCalcs()));
  }

  public CtuluLog validateParamCalc(List<ValidatingItem<AocParamCalc>> validatingItems) {
    return new AocParamCalcValidator().validate(validatingItems, getAvailableDclmByCalcul());
  }

  private CtuluLog validateLoiStricker() {
    return validateLoiStricker(create(campagne.getDonnees().getLoisStrickler().getLois()));
  }

  public CtuluLog validateLoiStricker(List<ValidatingItem<AocLoiStrickler>> validatingItems) {
    return new AocLoiStricklerValidator().validate(validatingItems, availableFrottements);
  }

  private CtuluLog validateCalculTransitoire() {
    return validateCalculTransitoire(create(campagne.getDonnees().getLoisCalculsTransitoires().getLois()));
  }

  private CtuluLog validateCalculPermanent() {
    return validateCalculPermanent(create(campagne.getDonnees().getLoisCalculsPermanents().getLois()));
  }

  public CtuluLog validateCalculPermanent(List<ValidatingItem<AocLoiCalculPermanent>> validatingItems) {

    return new AocLoiCalculPermanentValidator().validate(validatingItems, availableCalculPermanents, getAvailableLoisCalculPermanents());
  }

  /**
   * @return les lois utilisables pour les calculs permanents: les lois SectionsZ
   */
  private List<String> getAvailableLoisCalculPermanents() {
    final List<String> list = TransformerHelper.toNom(LoiHelper.filterLois(loiMesuresPost.getLois(), EnumTypeLoi.LoiSectionsZ));
    Collections.sort(list);
    return list;
  }

  /**
   * @return les lois utilisables pour les calculs permanents: les lois SectionsZ
   */
  public String[] getAvailableLoisCalculPermanentsArray() {
    List<String> res = getAvailableLoisCalculPermanents();
    return res.toArray(new String[0]);
  }

  public CtuluLog validateCalculTransitoire(List<ValidatingItem<AocLoiCalculTransitoire>> validatingItems) {
    return new AocLoiCalculTransitoireValidator()
        .validate(validatingItems, availableCalculTransitoires, getAvailableLoisCalculsTransitoires(), availableSections);
  }

  /**
   * @return les lois utilisables pour les calculs transitoires: les lois LoiTQ et LoiTZ
   */
  private List<String> getAvailableLoisCalculsTransitoires() {
    final List<String> list = TransformerHelper.toNom(LoiHelper.filterLois(loiMesuresPost.getLois(), EnumTypeLoi.LoiTQ, EnumTypeLoi.LoiTZ));
    Collections.sort(list);
    return list;
  }

  /**
   * @return les lois utilisables pour les calculs transitoires: les lois LoiTQ et LoiTZ
   */
  public String[] getAvailableLoisCalculsTransitoiresArray() {
    List<String> res = getAvailableLoisCalculsTransitoires();
    return res.toArray(new String[0]);
  }

  private <T> List<ValidatingItem<T>> create(List<T> in) {
    if (in == null) {
      return null;
    }
    List<ValidatingItem<T>> res = new ArrayList<>();
    for (T t : in) {
      res.add(new ValidatingItem<>(t, null));
    }
    return res;
  }
}
