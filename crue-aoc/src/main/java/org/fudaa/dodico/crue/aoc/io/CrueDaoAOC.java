package org.fudaa.dodico.crue.aoc.io;

import org.fudaa.dodico.crue.common.io.AbstractCrueDao;

/**
 * @author CANEL Christophe
 */
public class CrueDaoAOC extends AbstractCrueDao {
  public String AuteurCreation;
  public String DateCreation;
  public String AuteurDerniereModif;
  public String DateDerniereModif;
  public boolean LogAll;
  public CrueDaoStructureAOC.EtudeAssociee EtudeAssociee;
  public CrueDaoStructureAOC.TypeCalage TypeCalage;
  public CrueDaoStructureAOC.Calage Calage;
  public CrueDaoStructureAOC.DonneesCampagne DonneesCampagne;
  public CrueDaoStructureAOC.ParametresNumeriques ParametresNumeriques;
}
