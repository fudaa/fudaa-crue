package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.crue.common.CrueOperationResult;

/**
 * @author deniger
 */
public interface AocExecIteratorContrat {
  CrueOperationResult<AocExecContainer> compute(CrueOperationResult<AocExecContainer> in, ProgressionUpdater progressionUpdater, String prefix, AocExecAllLogger logger);

  void stop();


}
