package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.aoc.validation.AocContentValidator;
import org.fudaa.dodico.crue.aoc.validation.AocValidationHelper;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.ConnexionInformation;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.dodico.crue.common.CrueOperationResult;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.CoeurManager;
import org.fudaa.dodico.crue.edition.EditionDeleteContainer;
import org.fudaa.dodico.crue.metier.CrueLevelType;
import org.fudaa.dodico.crue.metier.aoc.LoiMesuresPost;
import org.fudaa.dodico.crue.metier.etude.*;
import org.fudaa.dodico.crue.metier.factory.CruePrefix;
import org.fudaa.dodico.crue.projet.EMHProjetController;
import org.fudaa.dodico.crue.projet.ScenarioLoaderOperation;
import org.fudaa.dodico.crue.projet.copy.CopyInformations;
import org.fudaa.dodico.crue.projet.copy.ManagerCopyCallable;
import org.fudaa.dodico.crue.projet.rename.RenameManager;
import org.fudaa.dodico.crue.projet.select.SelectableFinder;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
public class AocExecContainerLoader {
  public static final String DATE_PATTERN = "yyyyMMdd HH-mm-ss";
  private final CoeurManager coeurManager;

  public AocExecContainerLoader(CoeurManager coeurManager) {
    this.coeurManager = coeurManager;
  }

  /**
   * Charge le projet AOC, l'étude et valide le tout.
   *
   * @param aocFile le fichier de la campagne
   */
  public CrueOperationResult<AocExecContainer> load(File aocFile) {

    AocContentLoader loader = new AocContentLoader(coeurManager);

    //on charge la campagne
    CtuluLogGroup logGroup = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    final CrueIOResu<AocCampagne> aocReadResult = loader.loadCampagne(logGroup, aocFile);
    //impossible de lire la campagne
    final AocCampagne aocCampagne = aocReadResult.getMetier();
    if (aocCampagne == null) {
      return new CrueOperationResult<>(null, logGroup);
    }

    //on valide la campagne
    final CtuluLogGroup valid = new AocContentValidator().valid(aocCampagne);
    logGroup.addGroup(valid);
    if (valid.containsError() || valid.containsFatalError()) {
      return new CrueOperationResult<>(null, logGroup);
    }

    //on charge l'etude
    String etu = aocCampagne.getEtudeChemin();

    final CrueOperationResult<EMHProjet> emhProjetCrueOperationResult = loader.loadProjet(new File(etu));
    logGroup.addGroup(emhProjetCrueOperationResult.getLogs());
    final EMHProjet emhProjet = emhProjetCrueOperationResult.getResult();
    if (emhProjetCrueOperationResult.getLogs().containsFatalError() || emhProjet == null) {
      return new CrueOperationResult<>(null, logGroup);
    }

    //on construit le résultat
    AocExecContainer container = new AocExecContainer();
    container.setAocFile(aocFile);
    container.setCampagne(aocCampagne);
    container.setProjet(emhProjet);
    emhProjet.setEtuFile(new File(etu));
    container.setManagerScenarioReference(emhProjet.getScenario(aocCampagne.getNomScenario()));

    //on charge le scenario
    final ManagerEMHScenario managerScenarioReference = container.getManagerScenarioReference();
    if (managerScenarioReference == null) {
      //pas de scenario trouve -> erreur
      logGroup.createLog().addSevereError("aoc.exec.scenarioNotFound", aocCampagne.getNomScenario());
    } else {
      final ScenarioLoaderOperation scenarioLoaderOperation = loader.loadScenario(emhProjet, managerScenarioReference);
      logGroup.addGroup(scenarioLoaderOperation.getLogs());
      //impossible de charge le scenario -> erreur
      if (scenarioLoaderOperation.getResult() == null || scenarioLoaderOperation.getLogs().containsError()) {
        return new CrueOperationResult<>(null, logGroup);
      }

      container.setScenarioReference(scenarioLoaderOperation.getResult());

      //on charge les données LHPT
      final CrueIOResu<LoiMesuresPost> loiMesuresPostCrueIOResu = AocContentLoader.loadLHPT(emhProjet, managerScenarioReference);
      logGroup.addLog(loiMesuresPostCrueIOResu.getAnalyse());
      if (loiMesuresPostCrueIOResu.getAnalyse().containsErrorOrSevereError() || loiMesuresPostCrueIOResu.getMetier() == null) {
        return new CrueOperationResult<>(null, logGroup);
      }
      container.setLhpt(loiMesuresPostCrueIOResu.getMetier());

      //on valide la campagne entièrement
      AocValidationHelper helper = new AocValidationHelper(aocCampagne, container.getScenarioReference(), container.getLhpt(), emhProjet.getPropDefinition());
      final CtuluLogGroup validate = helper.validate(true);
      logGroup.addGroup(validate);
      if (validate.containsError()) {
        return new CrueOperationResult<>(null, logGroup);
      }
    }

    return new CrueOperationResult<>(container, logGroup);
  }

  /**
   * Copie le scenario et initialise le répertoire de reporting
   *
   * @param loaded les données chargeés
   * @param connexionInformation les infos de connexions
   * @return loader
   */
  public CrueOperationResult<AocExecContainer> build(CrueOperationResult<AocExecContainer> loaded, ConnexionInformation connexionInformation) {
    //on créé le répertoire qui va contenir les résultats du calage
    final AocExecContainer aocExecContainer = loaded.getResult();
    final LocalDateTime currentDate = connexionInformation.getCurrentDate();

    File parentDir = aocExecContainer.getAocFile().getParentFile();
    DateTimeFormatter fmt = DateTimeFormat.forPattern(DATE_PATTERN);
    String identifiant = AocExecHelper.generateCalageIdentifiant(aocExecContainer);
    //copie profonde:
    final CrueOperationResult<AocExecContainer> result = deepClone(loaded, connexionInformation, aocExecContainer, identifiant);
    //la copie profonde a echoué: on arrete
    if (result.getLogs().containsError()) {
      return result;
    }

    String repName = identifiant + "_" + fmt.print(currentDate);

    File dirToCreate = new File(parentDir, repName);
    if (!dirToCreate.mkdir()) {
      loaded.getLogs().createLog().addSevereError("aoc.exec.cantCreateDir", dirToCreate.getAbsolutePath());
      return loaded;
    }
    aocExecContainer.setReportDirectory(dirToCreate);

    //on copie les fichier aoc et lhpt dans le répertoire pour avoir une trace des donnees utilisées.
    final File lhptFile = FichierLHPTSupport.getLHPTFile(aocExecContainer.getProjet(), aocExecContainer.getManagerScenarioReference());
    final File lhptCopyFile = new File(dirToCreate, lhptFile.getName());
    CtuluLibFile.copyFile(lhptFile, lhptCopyFile);

    File aocFile = aocExecContainer.getAocFile();
    final File aocFileCopy = new File(dirToCreate, aocFile.getName());
    CtuluLibFile.copyFile(aocFile, aocFileCopy);

    CrueFileHelper.setLastModified(lhptCopyFile, lhptFile, getClass());
    CrueFileHelper.setLastModified(aocFileCopy, aocFile, getClass());

    return result;
  }

  public CrueOperationResult<AocExecContainer> deepClone(CrueOperationResult<AocExecContainer> loaded, ConnexionInformation connexionInformation,
                                                         AocExecContainer aocExecContainer, String identifiant) {

    final ManagerEMHScenario scenarioReference = aocExecContainer.getManagerScenarioReference();
    String newRadical = CruePrefix.changePrefix(scenarioReference.getNom(), CruePrefix.P_SCENARIO, "") + "_" + identifiant;

    final ManagerEMHScenario existingScenario = (ManagerEMHScenario) aocExecContainer.getProjet().getContainer(CruePrefix.P_SCENARIO + newRadical, CrueLevelType.SCENARIO);

    //on supprime le scenario existant si trouve
    if (existingScenario != null) {
      EditionDeleteContainer deleteContainer = new EditionDeleteContainer(new EMHProjetController(aocExecContainer.getProjet(), connexionInformation));
      final CtuluLogGroup deleteResult = deleteContainer.delete(existingScenario, true);
      loaded.getLogs().addGroup(deleteResult);
      if (deleteResult.containsError()) {
        return loaded;
      }
    }

    RenameManager renameManager = new RenameManager();
    renameManager.setProject(aocExecContainer.getProjet());

    SelectableFinder finder = new SelectableFinder();
    finder.setProject(aocExecContainer.getProjet());
    boolean deep = true;
    final RenameManager.NewNameInformations newNames = renameManager
        .getNewNames(scenarioReference, finder.getItems(Collections.singletonList(scenarioReference), deep), newRadical, deep);

    CopyInformations copyInfos = new CopyInformations(scenarioReference, newNames, deep, aocExecContainer.getProjet());
    List<String> overwrittenFiles = copyInfos.getOverwrittenFiles();

    String scenarioCible = null;
    for (Map.Entry<String, ManagerEMHContainerBase> containerByNewName : newNames.newNameContainers.entrySet()) {
      if (containerByNewName.getValue().getLevel() == CrueLevelType.SCENARIO) {
        scenarioCible = containerByNewName.getKey();
        break;
      }
    }

    //si des fichiers sont ecrasés on arrete
    final CtuluLog copyLogs = loaded.getLogs().createLog();
    copyLogs.setDesc("aoc.exec.deepCopy");
    copyLogs.setDescriptionArgs(scenarioReference.getNom(), scenarioCible);
    if (!overwrittenFiles.isEmpty()) {
      for (String overwrittenFile : overwrittenFiles) {
        if (!overwrittenFile.endsWith(".lhpt.xml")) {
          copyLogs.addSevereError("aoc.exec.filesWillBeOverwritten", overwrittenFile);
        }
      }
      return loaded;
    }

    //on effectue la copie
    ManagerCopyCallable copier = new ManagerCopyCallable(copyInfos, false, connexionInformation);
    boolean copySuccess = true;
    try {
      final Boolean call = copier.call();
      if (Boolean.FALSE.equals(call)) {
        copyLogs.addSevereError("aoc.exec.errorInDeepCopy", scenarioReference.getNom());
        copyLogs.addAllLogRecord(copier.getOperationLog());
        copySuccess = false;
      }
    } catch (Exception e) {
      Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
      copyLogs.addSevereError("aoc.exec.errorInDeepCopy", scenarioReference.getNom(), e.getMessage());
      copySuccess = false;
    }
    if (!copySuccess) {
      final Map<FichierCrue, String> filesToCopy = copyInfos.getFilesToCopy();
      for (Map.Entry<FichierCrue, String> fichierCrueNewName : filesToCopy.entrySet()) {
        FichierCrue destFichier = new FichierCrue(fichierCrueNewName.getValue(), fichierCrueNewName.getKey().getPath(), fichierCrueNewName.getKey().getType());
        final File createdFile = destFichier.getProjectFile(aocExecContainer.getProjet());
        CrueFileHelper.deleteFile(createdFile, getClass());
      }
      return loaded;
    }

    //on recharge le tout
    EMHProjetController projetController = new EMHProjetController(aocExecContainer.getProjet(), connexionInformation);
    final File etuFile = aocExecContainer.getProjet().getInfos().getEtuFile();
    projetController.saveProjet(etuFile, loaded.getLogs());
    if (loaded.getLogs().containsError()) {
      return loaded;
    }

    final CrueOperationResult<EMHProjet> emhProjetResult = EMHProjetController
        .readProjet(etuFile, aocExecContainer.getProjet().getCoeurConfig(), true);
    loaded.getLogs().addGroup(emhProjetResult.getLogs());
    final EMHProjet emhProjet = emhProjetResult.getResult();
    if (emhProjet == null) {
      return loaded;
    }
    aocExecContainer.setProjet(emhProjet);

    aocExecContainer.setManagerScenarioReference(aocExecContainer.getProjet().getScenario(scenarioReference.getId()));
    aocExecContainer.setManagerScenarioCible(aocExecContainer.getProjet().getScenario(scenarioCible));
    if (aocExecContainer.getManagerScenarioReference() == null) {
      copyLogs.addSevereError("aoc.exec.scenarioNotFound", aocExecContainer.getCampagne().getNomScenario());
    }
    if (aocExecContainer.getManagerScenarioCible() == null) {
      copyLogs.addSevereError("aoc.exec.scenarioCibleNotFound", scenarioCible);
    }

    return loaded;
  }
}
