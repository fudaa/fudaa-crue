package org.fudaa.dodico.crue.aoc.projet;

/**
 * Created by deniger on 28/06/2017.
 */
public class AocLoiCalculPermanent extends AbstractLoiCalcul implements Cloneable{



    public AocLoiCalculPermanent() {

    }

    public AocLoiCalculPermanent(String calculRef, String loiRef, int ponderation) {
        super(calculRef, loiRef, ponderation);
    }


    public AocLoiCalculPermanent clone(){
        return new AocLoiCalculPermanent(getCalculRef(),getLoiRef(),getPonderation());
    }
}
