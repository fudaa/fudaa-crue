/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.aoc.io;

import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.CrueFileFormatBuilder;
import org.fudaa.dodico.crue.io.common.CrueDataXmlReaderWriterImpl;
import org.fudaa.dodico.crue.metier.CrueFileType;

public class CrueFileFormatBuilderAOC implements CrueFileFormatBuilder<AocCampagne> {
  @Override
  public Crue10FileFormat<AocCampagne> getFileFormat(final CoeurConfigContrat coeurConfig) {
    return new Crue10FileFormat<>(new CrueDataXmlReaderWriterImpl<>(CrueFileType.AOC,
        coeurConfig, new CrueConverterAOC(coeurConfig.getCrueConfigMetier()), new CrueDaoStructureAOC()));
  }
}
