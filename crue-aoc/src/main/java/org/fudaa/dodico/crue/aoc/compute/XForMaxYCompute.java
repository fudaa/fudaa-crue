package org.fudaa.dodico.crue.aoc.compute;

/**
 * Permet de calcul le x tel que x=le temps d’arrivée du premier maxima absolu
 *
 * @author deniger
 */
public class XForMaxYCompute implements ComputeXYContrat {
  private int size;
  private double yMaxAbsolu;
  private double xMax;

  @Override
  public int getNbValues() {
    return size;
  }

  public boolean isEmpty() {
    return size == 0;
  }

  public boolean isNotEmpty() {
    return size > 0;
  }

  @Override
  public double getValue() {
    return xMax;
  }

  /**
   * fait le calcul sur y seulement
   *
   * @param x la valeur de l'abscisse
   * @param y la valeur de l'ordonnee
   */
  @Override
  public void addXY(double x, double y) {
    if (size == 0) {
      yMaxAbsolu = Math.abs(y);
      xMax = x;
    }
    if (Math.abs(y) > yMaxAbsolu) {
      yMaxAbsolu = Math.abs(y);
      xMax = x;
    }
    size++;
  }
}
