package org.fudaa.dodico.crue.aoc.validation;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluDefaultLogFormatter;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.aoc.projet.AocCampagneDonnees;
import org.fudaa.dodico.crue.aoc.projet.AocLoiCalculPermanent;
import org.fudaa.dodico.crue.aoc.projet.AocValidationGroupe;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.TransformerHelper;
import org.fudaa.dodico.crue.validation.ValidatingItem;

import java.util.*;

/**
 * Classe permettant de valider une liste de  AocValidationGroupe
 *
 * @author deniger
 */
public class AocValidationCroiseeValidator {
  public static String[] getAvailableLois(AocCampagne aocCampagne) {
    return getAvailableLois(aocCampagne.getDonnees());
  }

  public static String[] getAvailableLois(AocCampagneDonnees donnees) {
    final List<AocLoiCalculPermanent> aocLoiCalculPermanents = donnees.getLoisCalculsPermanents().getLois();
    List<String> lois = new ArrayList<>();
    for (AocLoiCalculPermanent aocLoiCalculPermanent : aocLoiCalculPermanents) {
      lois.add(aocLoiCalculPermanent.getLoiRef());
    }
    Collections.sort(lois);
    return lois.toArray(new String[0]);
  }

  /**
   * @param nodes list des {@link AocValidationGroupe}
   * @param availableLoisArray les lois disponibles dans le fichier AOC
   */
  public CtuluLog validate(List<ValidatingItem<AocValidationGroupe>> nodes, String[] availableLoisArray) {
    CtuluLog res = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    res.setDesc("aoc.validation.aocValidationGroupe");
    int idx = 0;
    if (nodes.isEmpty()) {
      return res;
    }
    Set<String> availableLois = AocLoiCalculPermanentValidator.createHashSet(availableLoisArray);
    Map<String, String> usedLois = new HashMap<>();
    Set<String> usedGroupName = new HashSet<>();
    for (ValidatingItem<AocValidationGroupe> node : nodes) {
      idx++;
      List<CtuluLogRecord> errorMsg = new ArrayList<>();

      //Validation du nom de calcul
      final AocValidationGroupe aocValidationGroupe = node.getObjectToValidate();
      String nomGoupe = aocValidationGroupe.getNom();
      if (StringUtils.isBlank(nomGoupe)) {
        errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.aocValidationGroupe.nomBlank", idx));
      } else if (usedGroupName.contains(nomGoupe)) {
        errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.aocValidationGroupe.nomAlreadyUsed", idx, nomGoupe));
      }
      usedGroupName.add(nomGoupe);

//            Validation des lois
      String[] lois = TransformerHelper.toArrayOfNom(aocValidationGroupe.getLois());
      if (ArrayUtils.isEmpty(lois)) {
        errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.aocValidationGroupe.noLois", idx, nomGoupe));
      } else {
        boolean blankDone = false;
        for (String loi : lois) {
          if (StringUtils.isBlank(loi)) {
            if (!blankDone) {
              errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.aocValidationGroupe.emptyLoi", idx, nomGoupe));
            }
            blankDone = true;
          } else if (!availableLois.contains(loi)) {
            errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.aocValidationGroupe.loiNotFound", idx, nomGoupe, loi));
          } else if (usedLois.containsKey(loi)) {
            errorMsg.add(
                res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.aocValidationGroupe.loiAlreadyUsed", idx, nomGoupe, loi,
                    usedLois.get(loi)));
          } else {
            usedLois.put(loi, nomGoupe);
          }
        }

        final int effectifApprentissage = aocValidationGroupe.getEffectifApprentissage();
        if (effectifApprentissage < 1 || effectifApprentissage > lois.length) {
          errorMsg.add(res.addRecord(CtuluLogLevel.ERROR, "aoc.validation.aocValidationGroupe.effectifApprentissageInvalid", idx, nomGoupe));
        }
      }
//

      if (!errorMsg.isEmpty()) {
        node.setErrorMessage(CtuluDefaultLogFormatter.asHtml(errorMsg, BusinessMessages.RESOURCE_BUNDLE));
      } else {
        node.clearMessage();
      }
    }
    res.updateLocalizedMessage(BusinessMessages.RESOURCE_BUNDLE);
    return res;
  }
}
