package org.fudaa.dodico.crue.aoc.io;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.batch.AocCampagneResult;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.io.common.CrueConverterCommonLog;
import org.fudaa.dodico.crue.io.common.CrueDaoStructureLog;
import org.fudaa.dodico.crue.io.common.CrueDataConverter;
import org.fudaa.dodico.crue.metier.CrueData;

/**
 * @author Fred Deniger
 */
public class CrueConverterRAOC implements CrueDataConverter<CrueDaoRAOC, AocCampagneResult> {
  private static void convertMetierToDaoHeader(final CrueDaoRAOC target, final AocCampagneResult metier, final CtuluLog analyser) {
    target.Header = new CrueDaoStructureLog.Header();
    target.setCommentaire(metier.getCommentaire());
    target.Header.Date = DateDurationConverter.dateToXsd(metier.getDate());
    target.Header.LoggerGroup = CrueConverterCommonLog.convertMetierToDao(metier.getGlobalValidation());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public AocCampagneResult convertDaoToMetier(final CrueDaoRAOC dao, final CrueData dataLinked, final CtuluLog ctuluLog) {
    final AocCampagneResult metier = new AocCampagneResult();

    metier.setCommentaire(dao.getCommentaire());
    if (dao.Header != null) {
      metier.setDate(DateDurationConverter.getDate(dao.Header.Date));
      metier.setGlobalValidation(CrueConverterCommonLog.convertDaoToMetier(dao.Header.LoggerGroup));
    }
    return metier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public AocCampagneResult getConverterData(final CrueData in) {
    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public CrueDaoRAOC convertMetierToDao(final AocCampagneResult metier, final CtuluLog ctuluLog) {
    final CrueDaoRAOC dao = new CrueDaoRAOC();
    convertMetierToDaoHeader(dao, metier, ctuluLog);
    return dao;
  }
}
