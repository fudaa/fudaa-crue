package org.fudaa.dodico.crue.aoc.exec.writer;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.aoc.exec.*;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.metier.emh.LoiDF;

import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author deniger
 */
public class AocExecResultWriterMinMax {
  private boolean isAccepted(AocExecResultIteration iteration) {
    return isSensibiliteMin(iteration) || AocEnumIterationType.SENSIBILITE_MAX.equals(iteration.getIterationType());
  }

  private boolean isSensibiliteMin(AocExecResultIteration iteration) {
    return AocEnumIterationType.SENSIBILITE_MIN.equals(iteration.getIterationType());
  }

  /**
   * Ecrit tous les fichiers
   *
   * @param result le resultats
   * @return le log
   */
  public CtuluLog write(AocExecContainer result) {
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("aoc.exec.write.result");
    //on ecrit pour min et max uniquement
    result.getResultContainer().getResultIterations().stream().filter(this::isAccepted).forEach(resultIteration -> {

      //le fichier de destination
      File resultFile = getFile(result, isSensibiliteMin(resultIteration) ? "Min" : "Max");
      //les lignes
      final List<String> lines = generateFileContent(resultIteration, result);
      AocExecWriterHelper.writeToFiles(log, lines, resultFile);
    });

    //pas demandé...par les specs
    final AocExecResultIteration min = AocExecHelper.getWithType(result.getResultContainer().getResultIterations(), AocEnumIterationType.SENSIBILITE_MIN);
    final AocExecResultIteration max = AocExecHelper.getWithType(result.getResultContainer().getResultIterations(), AocEnumIterationType.SENSIBILITE_MAX);

    final List<String> lines = generateFileContentMinMax(min, max, result);
    File resultFile = getFile(result, "MinMax");
    AocExecWriterHelper.writeToFiles(log, lines, resultFile);
    AocExecWriterHelper.writeCrue10Failures(result, log, getFileForFailures(result, "_crue10_failure"));

    writeBilan(result, log);
    return log;
  }

  /**
   * Ecrit le bilan du calage dans le fichier resultat_XXX.csv
   *
   * @param result les resultat
   * @param log les logs
   */
  private void writeBilan(AocExecContainer result, CtuluLog log) {
    List<String> lines = AocExecWriterHelper.createBilanHeader(result);
    //the result line to write:

    String fileName = result.getCampagne().getNomScenario() + "_bilan.csv";
    AocExecWriterHelper.writeToFiles(log, lines, new File(result.getReportDirectory(), fileName));
  }

  /**
   * Ecrit le bilan du calage dans le fichier resultat_XXX.csv
   *
   * @param result les resultat
   */
  private List<String> generateFileContent(final AocExecResultIteration iteration, final AocExecContainer result) {
    if (result.getCampagne().isPermanent()) {
      return generateFileContentPermanent(iteration, result);
    }
    return generateFileContentTransitoire(iteration, result);
  }

  private List<String> generateFileContentPermanent(AocExecResultIteration iteration, AocExecContainer result) {
    List<String> lines = new ArrayList<>();
    //resultats aux points de controles:
    List<AocExecResult> allResults = iteration.getAllResults();

    //Construction des lignes titre
    StringJoiner enteteJoiner = new StringJoiner(AocExecWriterHelper.DELIMITER);
    enteteJoiner.add("Section");
    allResults.forEach(aocExecResult -> enteteJoiner.add(aocExecResult.getNomComplet()));
    lines.add(enteteJoiner.toString());

    result.getLhpt().getEchellesSections().getEchellesSectionList().forEach(aocProfilSection -> {
      StringJoiner sectionJoiner = new StringJoiner(AocExecWriterHelper.DELIMITER);
      sectionJoiner.add(aocProfilSection.getSectionRef());
      allResults.forEach(calcul -> sectionJoiner.add(getValueToAdd(calcul, aocProfilSection.getSectionRef())));
      lines.add(sectionJoiner.toString());
    });
    return lines;
  }

  private List<String> generateFileContentTransitoire(AocExecResultIteration iteration, AocExecContainer aocExecContainer) {
    //les calculs
    List<AocExecResult> calculs = iteration.getAllResults();
    List<String> lines = new ArrayList<>();
    //pour chaque calcul
    for (AocExecResult calcul : calculs) {
      //on ecrit le nom du calcul
      lines.add("Calcul;" + calcul.getNomCalcul() + ";Loi;" + calcul.getNomLoi());
      //la loi a ecrire
      final LoiDF loiComputed = calcul.getHydroOrLimniCompute();
      AocExecWriterHelper.writeLoi(aocExecContainer, lines, loiComputed, "", "");
      //ligne blanche pour aerer.
      lines.add("");
    }
    return lines;
  }

  /**
   * Ecrit le bilan du calage dans le fichier resultat_XXX.csv
   *
   * @param result les resultat
   */
  private List<String> generateFileContentMinMax(final AocExecResultIteration iterationMin, final AocExecResultIteration iterationMax, final AocExecContainer result) {
    if (result.getCampagne().isPermanent()) {
      return generateFileContentMinMaxPermanent(iterationMin, iterationMax, result);
    }
    return generateFileContentMinMaxTransitoire(iterationMin, iterationMax, result);
  }

  /**
   * @param iterationMin l'iteration avec les valeurs min
   * @param iterationMax l'iteration avec les valeurs max
   * @param aocExecContainer le conteneur
   * @return les lignes a écrire
   */
  private List<String> generateFileContentMinMaxTransitoire(AocExecResultIteration iterationMin, AocExecResultIteration iterationMax, AocExecContainer aocExecContainer) {
    //les lignes
    List<String> lines = new ArrayList<>();

    //les calculs
    List<AocExecResult> calculs = iterationMin.getAllResults();
    List<AocExecResult> allResultsMax = iterationMax.getAllResults();
    Map<String, AocExecResult> maxByNomComplet = allResultsMax.stream().collect(Collectors.toMap(AocExecResult::getNomComplet, Function.identity()));

    //pour chaque calcul
    for (AocExecResult calcul : calculs) {
      //on ecrit le nom du calcul
      lines.add("Calcul;" + calcul.getNomCalcul() + ";Loi;" + calcul.getNomLoi());
      //on construit le writer de LoiFF/LoiDF
      AocExecTransitoireControlPointWriter controlPointWriter = AocExecTransitoireControlPointWriter
          .create(calcul.getHydroOrLimniCompute(), aocExecContainer);

      //on ajoute les points. Attention on detourne un peu la classe avec min=oberve et max=computed.
      controlPointWriter.addValuesObserved(calcul.getHydroOrLimniCompute());
      AocExecResult maxResult = maxByNomComplet.get(calcul.getNomComplet());
      controlPointWriter.addValuesComputed(maxResult.getHydroOrLimniCompute());

      //le nom de la variable en ordonnée
      final String ordonneeName = AocExecFormatterHelper.getVariableOrdonnee(aocExecContainer, calcul.getHydroOrLimniCompute()).getDisplayNom();

      //on ajoute finalement les lignes
      controlPointWriter.addToLines(
          ";" + AocExecFormatterHelper.getVariableAbscisse(aocExecContainer, calcul.getHydroOrLimniCompute()).getDisplayNom(),
          ";" + ordonneeName + " Min",
          ";" + ordonneeName + " Max",
          ";" + ordonneeName + " Delta",
          lines
      );
      //ligne blanche pour aerer.
      lines.add("");
    }
    return lines;
  }

  private List<String> generateFileContentMinMaxPermanent(AocExecResultIteration iterationMin, AocExecResultIteration iterationMax, AocExecContainer result) {
    List<String> lines = new ArrayList<>();

    //les calculs
    List<AocExecResult> allResultsMin = iterationMin.getAllResults();
    List<String> allNomComplet = allResultsMin.stream().map(AocExecResult::getNomComplet).collect(Collectors.toList());

    List<AocExecResult> allResultsMax = iterationMax.getAllResults();
    //Construction des lignes titre
    StringJoiner enteteJoiner = new StringJoiner(AocExecWriterHelper.DELIMITER);
    enteteJoiner.add("Section");
    allNomComplet.forEach(enteteJoiner::add);
    lines.add(enteteJoiner.toString());

    Map<String, AocExecResult> maxByNomComplet = allResultsMax.stream().collect(Collectors.toMap(AocExecResult::getNomComplet, Function.identity()));

    //le formater pour z
    final NumberFormat zFormater = AocExecFormatterHelper.getZFormater(result);

    //pour chaque section defini dans le lhpt:
    result.getLhpt().getEchellesSections().getEchellesSectionList().forEach(aocProfilSection -> {
      StringJoiner sectionJoiner = new StringJoiner(AocExecWriterHelper.DELIMITER);
      sectionJoiner.add(aocProfilSection.getSectionRef() + " Min");
      allResultsMin.forEach(calcul -> sectionJoiner.add(getValueToAdd(calcul, aocProfilSection.getSectionRef())));
      lines.add(sectionJoiner.toString());

      StringJoiner sectionJoinerMax = new StringJoiner(AocExecWriterHelper.DELIMITER);
      sectionJoinerMax.add(aocProfilSection.getSectionRef() + " Max");

      allResultsMax.forEach(calcul -> sectionJoinerMax.add(getValueToAdd(calcul.getNomComplet(), aocProfilSection.getSectionRef(), maxByNomComplet)));
      lines.add(sectionJoinerMax.toString());

      StringJoiner sectionJoinerDiff = new StringJoiner(AocExecWriterHelper.DELIMITER);
      sectionJoinerDiff.add(aocProfilSection.getSectionRef() + " Diff");

      allResultsMin.forEach(calcul -> sectionJoinerDiff.add(getDiffValueToAdd(calcul, maxByNomComplet, zFormater, aocProfilSection.getSectionRef())));
      lines.add(sectionJoinerDiff.toString());
    });
    return lines;
  }

  private String getValueToAdd(AocExecResult calcul, String section) {
    return calcul.getResultByVariable(AocExecVariableHelper.PROP_Z).getComputedValueAsString(section);
  }

  private String getValueToAdd(String nomComplet, String section, Map<String, AocExecResult> map) {
    final AocExecResult aocExecResult = map.get(nomComplet);
    if (aocExecResult == null) {
      return StringUtils.EMPTY;
    }
    return aocExecResult.getResultByVariable(AocExecVariableHelper.PROP_Z).getComputedValueAsString(section);
  }

  private String getDiffValueToAdd(AocExecResult iteration1, Map<String, AocExecResult> iteration2, final NumberFormat zFormater, String section) {
    final Double value1 = iteration1.getResultByVariable(AocExecVariableHelper.PROP_Z).getComputedValue(section);
    Double value2 = null;
    if (iteration2.containsKey(iteration1.getNomComplet())) {
      value2 = iteration2.get(iteration1.getNomComplet()).getResultByVariable(AocExecVariableHelper.PROP_Z).getComputedValue(section);
    }
    if (value1 != null && value2 != null) {
      return zFormater.format(value1 - value2);
    }
    return "";
  }

  private File getFile(AocExecContainer aocExecContainer, String suffix) {

    String fileName = aocExecContainer.getCampagne().getNomScenario() + "_EnvZ" + suffix + ".csv";
    return new File(aocExecContainer.getReportDirectory(), fileName);
  }

  private File getFileForFailures(AocExecContainer aocExecContainer, String suffix) {

    String fileName = aocExecContainer.getCampagne().getNomScenario() + suffix + ".csv";
    return new File(aocExecContainer.getReportDirectory(), fileName);
  }
}
