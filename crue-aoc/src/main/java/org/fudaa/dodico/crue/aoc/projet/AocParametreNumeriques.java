package org.fudaa.dodico.crue.aoc.projet;

import org.fudaa.dodico.crue.aoc.validation.AocValidationHelper;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.aoc.EnumAocTypeCalageCritere;

/**
 * @author deniger
 */
public class AocParametreNumeriques {
  public static final String PROP_AMPLITUDE = "coefAmplitude_Recuit";
  public static final String PROP_TEMPERATURE_INITIALE = "temperatureInitiale_Recuit";
  public static final String PROP_TEMPERATURE_FINALE = "temperatureFinale_Recuit";
  public static final String PROP_SEUIL_RMSEZ = "seuilRMSEZ_CriterePermTransAOC";
  public static final String PROP_SEUIL_RMSEQ = "seuilRMSEQ_CritereTransAOC";
  public static final String PROP_SEUIL_T = "seuilTarriveeMax_CritereTransAOC";
  public static final String PROP_SEUIL_ZMAX = "seuilZmax_CritereTransAOC";
  public static final String PROP_SEUIL_QMAX = "seuilQmax_CritereTransAOC";
  public static final String PROP_SEUIL_VOL = "seuilVol_CritereTransAOC";
  private double temperatureInitiale_Recuit;
  private double temperatureFinale_Recuit;
  private double coefAmplitude_Recuit;
  private double seuilRMSEZ_CriterePermTransAOC;
  private double seuilRMSEQ_CritereTransAOC;
  private double seuilTarriveeMax_CritereTransAOC;
  private double seuilZmax_CritereTransAOC;
  private double seuilQmax_CritereTransAOC;
  private double seuilVol_CritereTransAOC;

  public AocParametreNumeriques(CrueConfigMetier ccm) {
    coefAmplitude_Recuit = getDefaultValue(ccm, PROP_AMPLITUDE, 1);
    temperatureInitiale_Recuit = getDefaultValue(ccm, PROP_TEMPERATURE_INITIALE, 0.5);
    temperatureFinale_Recuit = getDefaultValue(ccm, PROP_TEMPERATURE_FINALE, 0.001);
    seuilRMSEZ_CriterePermTransAOC = getDefaultValue(ccm, PROP_SEUIL_RMSEZ, 0.00001);
    seuilRMSEQ_CritereTransAOC = getDefaultValue(ccm, PROP_SEUIL_RMSEQ, 0.00001);
    seuilTarriveeMax_CritereTransAOC = getDefaultValue(ccm, PROP_SEUIL_T, 0.0001);
    seuilZmax_CritereTransAOC = getDefaultValue(ccm, PROP_SEUIL_ZMAX, 0.00001);
    seuilVol_CritereTransAOC = getDefaultValue(ccm, PROP_SEUIL_VOL, 0.00001);
    seuilQmax_CritereTransAOC = getDefaultValue(ccm, PROP_SEUIL_QMAX, 0.00001);
  }

  @PropertyDesc(i18n = "aoc.temperature.initiale.property")
  public double getTemperatureInitiale_Recuit() {
    return temperatureInitiale_Recuit;
  }

  public void setTemperatureInitiale_Recuit(double temperatureInitiale_Recuit) {
    this.temperatureInitiale_Recuit = temperatureInitiale_Recuit;
  }

  @PropertyDesc(i18n = "aoc.temperature.finale.property")
  public double getTemperatureFinale_Recuit() {
    return temperatureFinale_Recuit;
  }

  public void setTemperatureFinale_Recuit(double tFinal) {
    this.temperatureFinale_Recuit = tFinal;
  }

  @PropertyDesc(i18n = "aoc.amplitude.property")
  public double getCoefAmplitude_Recuit() {
    return coefAmplitude_Recuit;
  }

  public void setCoefAmplitude_Recuit(double coefAmplitude_Recuit) {
    this.coefAmplitude_Recuit = coefAmplitude_Recuit;
  }

  @PropertyDesc(i18n = "aoc.seuil.rmseZ.property")
  public double getSeuilRMSEZ_CriterePermTransAOC() {
    return seuilRMSEZ_CriterePermTransAOC;
  }

  public void setSeuilRMSEZ_CriterePermTransAOC(double seuilRMSEZ_CriterePermTransAOC) {
    this.seuilRMSEZ_CriterePermTransAOC = seuilRMSEZ_CriterePermTransAOC;
  }

  @PropertyDesc(i18n = "aoc.seuil.rmseQ.property")
  public double getSeuilRMSEQ_CritereTransAOC() {
    return seuilRMSEQ_CritereTransAOC;
  }

  public void setSeuilRMSEQ_CritereTransAOC(double seuilRMSEQ_CritereTransAOC) {
    this.seuilRMSEQ_CritereTransAOC = seuilRMSEQ_CritereTransAOC;
  }

  @PropertyDesc(i18n = "aoc.seuil.t.property")
  public double getSeuilTarriveeMax_CritereTransAOC() {
    return seuilTarriveeMax_CritereTransAOC;
  }

  public void setSeuilTarriveeMax_CritereTransAOC(double seuilTarriveeMax_CritereTransAOC) {
    this.seuilTarriveeMax_CritereTransAOC = seuilTarriveeMax_CritereTransAOC;
  }

  @PropertyDesc(i18n = "aoc.seuil.Zmax.property")
  public double getSeuilZmax_CritereTransAOC() {
    return seuilZmax_CritereTransAOC;
  }

  public void setSeuilZmax_CritereTransAOC(double seuilZmax_CritereTransAOC) {
    this.seuilZmax_CritereTransAOC = seuilZmax_CritereTransAOC;
  }

  @PropertyDesc(i18n = "aoc.seuil.Qmax.property")
  public double getSeuilQmax_CritereTransAOC() {
    return seuilQmax_CritereTransAOC;
  }

  public void setSeuilQmax_CritereTransAOC(double seuilQmax_CritereTransAOC) {
    this.seuilQmax_CritereTransAOC = seuilQmax_CritereTransAOC;
  }

  @PropertyDesc(i18n = "aoc.seuil.vol.property")
  public double getSeuilVol_CritereTransAOC() {
    return seuilVol_CritereTransAOC;
  }

  public void setSeuilVol_CritereTransAOC(double seuilVol_CritereTransAOC) {
    this.seuilVol_CritereTransAOC = seuilVol_CritereTransAOC;
  }

  public double getSeuilToUse(AocCampagne campagne) {
    if (campagne.isPermanent()) {
      return seuilRMSEZ_CriterePermTransAOC;
    }
    if (EnumAocTypeCalageCritere.ECART_TEMPS_ARRIVEE_MAX.equals(campagne.getTypeCalageCritere())) {
      return seuilTarriveeMax_CritereTransAOC;
    }
    if (EnumAocTypeCalageCritere.ECART_VOLUMES.equals(campagne.getTypeCalageCritere())) {
      return seuilVol_CritereTransAOC;
    }
    boolean isZ = AocValidationHelper.useLoiTZForTransitoire(campagne);
    if (EnumAocTypeCalageCritere.ECART_NIVEAUX_MAX.equals(campagne.getTypeCalageCritere())) {
      return isZ ? seuilZmax_CritereTransAOC : seuilQmax_CritereTransAOC;
    }
    return isZ ? seuilRMSEZ_CriterePermTransAOC : seuilRMSEQ_CritereTransAOC;
  }

  private double getDefaultValue(CrueConfigMetier crueConfigMetier, String prop, double defaultValue) {
    if (crueConfigMetier.isPropertyDefined(prop)) {
      return crueConfigMetier.getDefaultDoubleValue(prop);
    }
    return defaultValue;
  }
}
