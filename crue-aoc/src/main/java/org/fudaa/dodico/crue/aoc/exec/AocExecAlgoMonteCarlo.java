package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.dodico.crue.aoc.dclm.AocDclmDataItem;
import org.fudaa.dodico.crue.aoc.dclm.AocDclmContainer;

import java.util.List;
import java.util.Map;

/**
 * @author deniger
 */
public class AocExecAlgoMonteCarlo implements AocExecAlgoContrat {
  @Override
  public AocExecAlgoResult generateForNextIteration(AocExecContainer aocExecContainer, AocDclmContainer dclmBuilder, AocExecAllLogger logger, boolean doIterate) {

    Map<String, Double> dfrtValues = AOCStricklerBuilder.buildValues(aocExecContainer.getCampagne().getDonnees().getLoisStrickler());
    List<AocDclmDataItem> dclmdValues = null;
    if (AocExecHelper.debitCouldBeModified(dclmBuilder, aocExecContainer.getCampagne())) {
      logger.addLogFormated(";Algo MonteCarlo;generation valeurs aléatoires pour dfrt et dclm");
      if(!dclmBuilder.hasDeltaQ()){
        logger.addLogFormated(";Algo MonteCarlo;les valeurs dclm ne varient pas et sont toujours egales aux valeurs initiales");
      }
      dclmdValues = dclmBuilder.generateRandomValues();
    } else {
      logger.addLogFormated(";Algo MonteCarlo;generation valeurs aléatoires pour dfrt uniquement");
    }
    return new AocExecAlgoResult(dfrtValues, dclmdValues);
  }

  @Override
  public void fillIterationResultWithParamater(AocExecResultIteration resultIteration) {
    //RIEN a faire dans ce cas
  }
}
