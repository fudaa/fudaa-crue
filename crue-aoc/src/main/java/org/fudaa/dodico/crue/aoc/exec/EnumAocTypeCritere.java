package org.fudaa.dodico.crue.aoc.exec;

/**
 * @author deniger
 */
public enum EnumAocTypeCritere {

  /**
   * Le critere de l'iteration est celui calculé sur les lois d'apprentissage. Utilise pour les validations croisées dans
   * les tirs car le calage se base sur cette valeur
   */
  VALIDATION_CROISEE_APPRENTISSAGE,
  /**
   * A la fin de la validation croisee, le critere est recalcule en fonction des ponderations apprentissage/validation
   */
  VALIDATION_CROISEE_ERREUR_APPRENTISSAGE,
  /**
   * Cas commun, le critere est calcule sur toutes les lois
   */
  GLOBAL
}
