package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.CrueFileFormatBuilderDFRT;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.DonFrt;
import org.fudaa.dodico.crue.metier.emh.DonFrtConteneur;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.FichierCrue;
import org.fudaa.dodico.crue.metier.etude.FichierCrueParModele;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Modifie l'ensemble des fichiers dfrt d'un scenario avec les valeurs generees par l'algo
 *
 * @author deniger
 */
public class AOCStricklerModifier {


  /**
   * @param projet le projet parent
   * @param scenario le scenario a modifier
   * @param loisStrickler les nouvelles valeurs pour les stricklers
   * @return les log
   */
  public CtuluLogGroup applyFrottement(final EMHProjet projet, final ManagerEMHScenario scenario, final Map<String, Double> loisStrickler) {
    final CtuluLogGroup logs = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    if (loisStrickler == null || loisStrickler.isEmpty()) {
      return logs;
    }
    final Map<String, FichierCrueParModele> allFileUsed = scenario.getAllFileUsed();
    final List<FichierCrue> dfrtFiles = allFileUsed.values().stream().filter(line -> CrueFileType.DFRT.equals(line.getFichier().getType())).map(FichierCrueParModele::getFichier)
        .collect(Collectors.toList());

    final Crue10FileFormat<List<DonFrt>> dfrtFileFormat = new CrueFileFormatBuilderDFRT().getFileFormat(projet.getCoeurConfig());
    //on le fait pas en // sachant que plus tard il n'y aura qu'un seul fichier dfrt

    for (final FichierCrue dfrtFile : dfrtFiles) {

      final File file = dfrtFile.getProjectFile(projet);
      final CtuluLog res = applyFrottements(projet.getPropDefinition(), loisStrickler, dfrtFileFormat, file);
      logs.addLog(res);
    }
    return logs;
  }

  CtuluLog applyFrottements(final CrueConfigMetier ccm, final Map<String, Double> newValuesForStricker, final Crue10FileFormat<List<DonFrt>> dfrtFileFormat, final File file) {
    final CrueDataImpl crueData = new CrueDataImpl(ccm);
    final CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    dfrtFileFormat.read(file, log, crueData);
    if (log.containsErrorOrSevereError()) {
      return log;
    }
    final DonFrtConteneur donFrtConteneur = crueData.getFrottements();
    final List<DonFrt> listFrt = donFrtConteneur.getListFrt();
    for (final DonFrt donFrt : listFrt) {
      if (newValuesForStricker.containsKey(donFrt.getNom())) {
        final double newValue = Math.max(newValuesForStricker.get(donFrt.getNom()).doubleValue(), 1);
        donFrt.getLoi().getEvolutionFF().getPtEvolutionFF().forEach(line -> line.setOrdonnee(newValue));
      }
    }
    dfrtFileFormat.write(crueData, file, log);
    return log;
  }
}
