package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.dodico.crue.aoc.projet.AocLoiStrickler;
import org.fudaa.dodico.crue.aoc.projet.AocLoisStrickler;

import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

/**
 * @author deniger
 */
public class AOCStricklerBuilder {

  private AOCStricklerBuilder(){}
  /**
   * @param lois les lois aoc
   * @return une map avec loiRef->getIni
   */
  static Map<String, Double> buildInitValue(AocLoisStrickler lois) {
    return lois.getLois().stream().collect(
        Collectors.toMap(AocLoiStrickler::getLoiRef, AocLoiStrickler::getIniAsDouble));
  }

  /**
   * @param lois les lois aoc
   * @return une map avec loiRef->getMin
   */
  static Map<String, Double> buildMinValue(AocLoisStrickler lois) {
    return lois.getLois().stream().collect(
        Collectors.toMap(AocLoiStrickler::getLoiRef, AocLoiStrickler::getMinAsDouble));
  }

  /**
   * @param lois les lois aoc
   * @return une map avec loiRef->getMin
   */
  static Map<String, Double> buildMaxValue(AocLoisStrickler lois) {
    return lois.getLois().stream().collect(
        Collectors.toMap(AocLoiStrickler::getLoiRef, AocLoiStrickler::getMaxAsDouble));
  }

  /**
   * @param lois les lois initiales
   * @return une map avec loiRef->random ( min, max)
   */
  static Map<String, Double> buildValues(final AocLoisStrickler lois) {
    return lois.getLois().stream().collect(
        Collectors.toMap(AocLoiStrickler::getLoiRef, loi -> (ThreadLocalRandom.current().nextDouble((double) loi.getMin(), (double) loi.getMax()))));
  }

  /**
   * @param lois les lois initiales
   * @return une map avec loiRef-> valeur centree sur ancienne valeur.
   */
  static Map<String, Double> buildValuesCentered(final AocLoisStrickler lois, final Map<String, Double> reference, final double coefficientAmplitude) {
    return lois.getLois().stream().collect(
        Collectors.toMap(AocLoiStrickler::getLoiRef, loi -> generate(reference.get(loi.getLoiRef()), (double) loi.getMin(), (double) loi.getMax(), coefficientAmplitude)));
  }

  /**
   * @param lois les lois initiales
   * @return l'amplitude maximale obtenues
   */
  static double getMaxAmplitude(final AocLoisStrickler lois, final double coefficientAmplitude) {
    if (lois.getLois().isEmpty()) {
      return 0;
    }
    return lois.getLois().stream().mapToDouble(loi -> getAmplitude((double) loi.getMin(), (double) loi.getMax(), coefficientAmplitude)).max().getAsDouble();
  }

  /**
   * @param reference la valeur initiale de référence
   * @param min le min des valeurs
   * @param max le max des valeurs
   * @param coefficientAmplitude le coefficient a applique au delta initial
   * @return la valeur générée.
   */
  private static double generate(final double reference, final double min, final double max, final double coefficientAmplitude) {
    double delta = Math.abs(max - min) * coefficientAmplitude;
    if (delta <= 0) {
      return reference;
    }
    double newValue = ThreadLocalRandom.current().nextDouble(reference - delta / 2d, reference + delta / 2d);
    return Math.max(min, Math.min(newValue, max));
  }

  /**
   * @param min le min des valeurs
   * @param max le max des valeurs
   * @param coefficientAmplitude le coefficient
   * @return l'amplitude.
   */
  public static double getAmplitude(final double min, final double max, final double coefficientAmplitude) {
    return Math.max(0, Math.abs(max - min) * coefficientAmplitude);
  }
}
