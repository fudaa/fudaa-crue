package org.fudaa.dodico.crue.aoc.projet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deniger on 28/06/2017.
 */
public class AocListeCalculs {
    List<AocParamCalc> paramCalcs = new ArrayList<>();

    public List<AocParamCalc> getParamCalcs() {
        return paramCalcs;
    }

    public void setParamCalcs(List<AocParamCalc> paramCalcs) {
        this.paramCalcs = paramCalcs;
    }

    public void addParamCalcul(String calculRef, String emhRef, double deltaQ) {
        paramCalcs.add(new AocParamCalc(calculRef, emhRef, deltaQ));
    }

    public void setClonedParams(List<AocParamCalc> clonedParams) {
        this.paramCalcs = new ArrayList<>();
        if (clonedParams != null) {
            for (AocParamCalc aocLoiCalculPermanent : clonedParams) {
                this.paramCalcs.add(new AocParamCalc(aocLoiCalculPermanent));
            }
        }
    }
}
