package org.fudaa.dodico.crue.aoc.validation;

import org.fudaa.dodico.crue.config.loi.EnumTypeLoi;
import org.fudaa.dodico.crue.metier.aoc.LoiMesuresPost;
import org.fudaa.dodico.crue.metier.emh.AbstractLoi;
import org.fudaa.dodico.crue.metier.emh.LoiTF;
import org.fudaa.dodico.crue.metier.emh.PtEvolutionTF;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Met a jour le contenu des lois SectionsZ en fonction des profil disponibles.
 *
 * @author deniger
 */
public class LHPTLoiUpdater {
    public boolean updateLoi(LoiMesuresPost loiMesurePost) {
        final Set<String> definedProfils = loiMesurePost.getEchellesSections().getDefinedEchelles();
        final List<AbstractLoi> lois = loiMesurePost.getLois();
        boolean modified = false;
        for (AbstractLoi abstractLoi : lois) {
            if (EnumTypeLoi.LoiSectionsZ.equals(abstractLoi.getType())) {
                LoiTF loiTF = (LoiTF) abstractLoi;
                List<PtEvolutionTF> newValues = new ArrayList<>();
                for (PtEvolutionTF ptEvolutionTF : loiTF.getEvolutionTF().getPtEvolutionTF()) {
                    if (!definedProfils.contains(ptEvolutionTF.getAbscisse())) {
                        modified = true;
                    } else {
                        newValues.add(ptEvolutionTF);
                    }
                }
                loiTF.getEvolutionTF().setPtEvolutionTF(newValues);
            }
        }
        return modified;
    }
}
