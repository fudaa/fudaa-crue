package org.fudaa.dodico.crue.aoc.projet;

import com.memoire.fu.FuEmptyArrays;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.crue.common.annotation.PropertyDesc;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.common.transformer.ToStringHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by deniger on 30/06/2017.
 */
public class AocValidationGroupe implements ObjetNomme {
    public static final int EFFECTIF_APPRENTISSAGE_DEFAULT_VALUE = 1;
    List<AocValidationLoi> lois = new ArrayList<>();
    private String id;
    private String nom;
    private int effectifApprentissage = EFFECTIF_APPRENTISSAGE_DEFAULT_VALUE;
    private String commentaire;

    public AocValidationGroupe() {
    }

    public AocValidationGroupe(String id, String nom, int effectifApprentissage, String commentaire) {
        this.effectifApprentissage = effectifApprentissage;
        this.id = id;
        this.nom = nom;
        this.commentaire = commentaire;
    }

    public static String[] stringToLois(String in) {
        if (StringUtils.isBlank(in)) {
            return FuEmptyArrays.STRING0;
        }
        String[] loisToAdd = CtuluLibString.parseString(in, ";");
        if ((loisToAdd == null || loisToAdd.length == 0) && StringUtils.contains(in, ',')) {
            loisToAdd = CtuluLibString.parseString(in, ",");
        }
        return loisToAdd;
    }

    public static String loisArrayToString(Object[] in) {
        return StringUtils.join(in, ';');
    }

    @Override
    public AocValidationGroupe clone() {
        AocValidationGroupe res = new AocValidationGroupe(id, nom, effectifApprentissage, commentaire);
        res.setClonedLois(lois);
        return res;
    }

    /**
     * @return string sous forme loi1;loi2;...
     */
    public String getLoisAsString() {
        return ToStringHelper.transformToNom(lois);
    }

    public void setLoisAsString(String in) {
        if (in == null) {
            return;
        }

        String[] loisToAdd = stringToLois(in);
        Arrays.sort(loisToAdd);
        this.lois.clear();
        for (String loi : loisToAdd) {
            this.lois.add(new AocValidationLoi(loi));
        }
    }

    public void addLoi(String loiRef) {
        lois.add(new AocValidationLoi(loiRef));
    }

    @PropertyDesc(i18n = "aoc.commentaire.shortName")
    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @PropertyDesc(i18n = "aoc.groupe.shortName")
    @Override
    public String getNom() {
        return nom;
    }

    @Override
    public void setNom(String nom) {
        this.nom = nom;
    }

    @PropertyDesc(i18n = "aoc.effectifApprentissage.shortName")
    public int getEffectifApprentissage() {
        return effectifApprentissage;
    }

    public void setEffectifApprentissage(int effectifApprentissage) {
        this.effectifApprentissage = effectifApprentissage;
    }

    @PropertyDesc(i18n = "aoc.lois.shortName")
    public List<AocValidationLoi> getLois() {
        return lois;
    }

    public void setLois(List<AocValidationLoi> lois) {
        this.lois = lois;
    }

    public void setClonedLois(List<AocValidationLoi> loisToCloned) {
        lois = new ArrayList<>();
        for (AocValidationLoi aocValidationLoi : loisToCloned) {
            lois.add(new AocValidationLoi(aocValidationLoi));
        }
    }
}
