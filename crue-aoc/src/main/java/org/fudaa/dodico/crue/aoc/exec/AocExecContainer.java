package org.fudaa.dodico.crue.aoc.exec;

import org.fudaa.dodico.crue.aoc.projet.AocCampagne;
import org.fudaa.dodico.crue.metier.aoc.LoiMesuresPost;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;

import java.io.File;

/**
 * Le conteneur global pour l'exécution d'une campagne aoc.
 *
 * @author deniger
 */
public class AocExecContainer {
  private File reportDirectory;
  private File aocFile;
  private ManagerEMHScenario managerScenarioReference;
  private EMHScenario scenarioReference;
  private ManagerEMHScenario managerScenarioCible;
  private AocCampagne campagne;
  private EMHProjet projet;
  private LoiMesuresPost lhpt;
  private AocExecResultContainer resultContainer = new AocExecResultContainer();

  /**
   * @return les resultat. Toujours non null
   */
  public AocExecResultContainer getResultContainer() {
    return resultContainer;
  }

  /**
   * A utiliser dans le cas de plusieurs lancements successifs pour reinitialiser les résultats.
   *
   * @param parentMainTimeInfo l'info sur les durations du parent
   */
  public void clearResult(AocExecTimeInfo parentMainTimeInfo) {
    resultContainer = new AocExecResultContainer();
    resultContainer.getTimeInfo().setParentTimeInfo(parentMainTimeInfo);
  }

  public EMHScenario getScenarioReference() {
    return scenarioReference;
  }

  public void setScenarioReference(EMHScenario scenarioReference) {
    this.scenarioReference = scenarioReference;
  }

  public ManagerEMHScenario getManagerScenarioCible() {
    return managerScenarioCible;
  }

  public void setManagerScenarioCible(ManagerEMHScenario managerScenarioCible) {
    this.managerScenarioCible = managerScenarioCible;
  }

  public File getAocFile() {
    return aocFile;
  }

  public void setAocFile(File aocFile) {
    this.aocFile = aocFile;
  }

  public File getReportDirectory() {
    return reportDirectory;
  }

  public void setReportDirectory(File reportDirectory) {
    this.reportDirectory = reportDirectory;
  }

  public ManagerEMHScenario getManagerScenarioReference() {
    return managerScenarioReference;
  }

  public void setManagerScenarioReference(ManagerEMHScenario managerScenarioReference) {
    this.managerScenarioReference = managerScenarioReference;
  }

  public AocCampagne getCampagne() {
    return campagne;
  }

  public void setCampagne(AocCampagne campagne) {
    this.campagne = campagne;
  }

  public EMHProjet getProjet() {
    return projet;
  }

  public void setProjet(EMHProjet projet) {
    this.projet = projet;
  }

  public LoiMesuresPost getLhpt() {
    return lhpt;
  }

  public void setLhpt(LoiMesuresPost lhpt) {
    this.lhpt = lhpt;
  }


}
