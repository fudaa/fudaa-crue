package org.fudaa.fudaa.crue.post.action;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.fudaa.crue.comparison.ScenarioComparaisonLauncher;
import org.fudaa.fudaa.crue.loader.LoaderService;
import org.fudaa.fudaa.crue.loader.ProjectLoadContainer;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View",
id = "org.fudaa.fudaa.crue.post.action.PostComparisonOtherEtuAction")
@ActionRegistration(displayName = "#CTL_PostComparisonOtherEtuAction")
@ActionReferences({
  @ActionReference(path = "Actions/Post", position = 2, separatorAfter = 3)
})
public final class PostComparisonOtherEtuAction extends AbstractPostAction {

  private final LoaderService loaderService = Lookup.getDefault().lookup(LoaderService.class);
  final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public PostComparisonOtherEtuAction() {
    super(false);
    putValue(Action.NAME, NbBundle.getMessage(AbstractPostAction.class, "CTL_PostComparisonOtherEtuAction"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new PostComparisonOtherEtuAction();
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    ProjectLoadContainer otherProjet = loaderService.loadProject();
    if (otherProjet != null) {
      boolean isSame = otherProjet.getEtuFile().equals(projetService.getEtuFile());
      CrueConfigMetier propDefinition = projetService.getSelectedProject().getPropDefinition();
      new ScenarioComparaisonLauncher(postService.getManagerScenarioLoaded(), postService.getScenarioLoaded(), propDefinition, postService.getRunLoaded(),
              otherProjet.getProjet(), isSame).compute();
    }
  }
}
