package org.fudaa.fudaa.crue.post.action;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.action.AbstractHelpAction;
import org.openide.awt.*;

@ActionID(category = "View",
        id = "org.fudaa.fudaa.crue.post.action.PostHelpAction")
//iconBase ne semble pas fonctionner !
@ActionRegistration(displayName = "#CTL_PostHelpAction", iconBase = AbstractHelpAction.HELP_ICON)
@ActionReference(path = "Actions/Post", position = 100, separatorBefore = 99)
public final class PostHelpAction extends AbstractHelpAction {

  public PostHelpAction() {
    super(PerspectiveEnum.POST);
  }
}
