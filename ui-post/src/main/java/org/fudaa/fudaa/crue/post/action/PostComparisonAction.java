package org.fudaa.fudaa.crue.post.action;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.fudaa.crue.comparison.ScenarioComparaisonLauncher;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View",
id = "org.fudaa.fudaa.crue.post.action.PostComparisonAction")
@ActionRegistration(displayName = "#CTL_PostComparison")
@ActionReferences({
  @ActionReference(path = "Actions/Post", position = 1)
})
public final class PostComparisonAction extends AbstractPostAction {

  final EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);

  public PostComparisonAction() {
    super(false);
    putValue(Action.NAME, NbBundle.getMessage(AbstractPostAction.class, "CTL_PostComparison"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new PostComparisonAction();
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    CrueConfigMetier propDefinition = projetService.getSelectedProject().getPropDefinition();
    new ScenarioComparaisonLauncher(postService.getManagerScenarioLoaded(), postService.getScenarioLoaded(), propDefinition, postService.getRunLoaded(),
            projetService.getSelectedProject(), true).compute();
  }
}
