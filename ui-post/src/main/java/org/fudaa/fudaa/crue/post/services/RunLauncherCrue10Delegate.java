package org.fudaa.fudaa.crue.post.services;

import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.io.avct.AvctReader;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.CompteRenduAvancement;
import org.fudaa.dodico.crue.projet.calcul.CalculCrueContrat;
import org.fudaa.dodico.crue.projet.calcul.Crue10OptionBuilder;
import org.fudaa.dodico.crue.projet.calcul.ExecConfigurer;
import org.fudaa.dodico.crue.projet.calcul.ExecInputDefault;
import org.fudaa.dodico.crue.projet.conf.Option;
import org.fudaa.dodico.crue.projet.conf.OptionsEnum;
import org.fudaa.dodico.crue.projet.create.RunCalculOption;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.post.PostAvctTopComponent;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import java.io.File;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author deniger
 */
public class RunLauncherCrue10Delegate extends AbstractRunLauncherCrueDelegate {
    private final Map<CrueFileType, RunCalculOption> options;
    private final PostAvctTopComponent avtComponent;
    final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);
    Timer avctReader;

    public RunLauncherCrue10Delegate(ExecInputDefault execInputDefault, PostServiceImpl.RunAfter after, CrueRunLauncher runLauncher,
                                     Map<CrueFileType, RunCalculOption> options, PostAvctTopComponent avtComponent) {
        super(execInputDefault, after, runLauncher);
        this.options = options;
        this.avtComponent = avtComponent;
    }

    @Override
    protected void clean() {
        try {
            if (avctReader != null) {
                avctReader.cancel();
            }
        } catch (Exception e) {
        }
        super.clean();
    }

    @Override
    public void run() {
        Option option = configurationManagerService.getOptionsManager().getOption(OptionsEnum.UI_UPDATE_FREQ);
        String valeur = option.getValeur();
        double freq = -1;
        try {
            freq = Double.parseDouble(valeur);
        } catch (NumberFormatException numberFormatException) {
            freq = Double.parseDouble(OptionsEnum.UI_UPDATE_FREQ.getDefaultValue());
        }
        if (freq > 0) {
            avctReader = new Timer("avct");
            long period = (long) (freq * 1000);
            avctReader.schedule(new ReadAvctTask(new File(execInputDefault.getRunDir(),
                            AvctReader.getAvctName(execInputDefault.getScenario()))),
                    new Date(System.currentTimeMillis() + period), period);
        }
        super.run();
        if (!isCalculFinishedCorrectly()) {

        }
    }

    @Override
    protected CalculCrueContrat createExec() {
        Crue10OptionBuilder optionsBuilder = new Crue10OptionBuilder(runLauncher.getExecOptions());
        ExecConfigurer createConfigurer = optionsBuilder.createConfigurer(execInputDefault, options);
        final CalculCrueContrat runner = runLauncher.getCrue10Runner(execInputDefault.getProjet().getCoeurConfig()).createRunner(execInputDefault,
                createConfigurer);
        runner.setUI(new ExecCtuluUI());
        return runner;
    }

    private static class ExecCtuluUI extends CtuluUIForNetbeans {
        @Override
        public void error(String _titre, String _msg, boolean _tempo) {
            super.error(
                    NbBundle.getMessage(RunLauncherCrue10Delegate.class, "crue10Exe.exec.errorTitle"),
                    NbBundle.getMessage(RunLauncherCrue10Delegate.class, "crue10Exe.exec.errorMessage"),
                    false
                       );
        }
    }

    private class ReadAvctTask extends TimerTask {
        private final File avct;

        public ReadAvctTask(File avct) {
            this.avct = avct;
        }

        @Override
        public void run() {
            if (avct != null && avct.exists() && avtComponent != null) {
                AvctReader reader = new AvctReader();
                CrueIOResu<CompteRenduAvancement> read = reader.read(avct);
                if (read.getMetier() != null) {
                    avtComponent.updateUi(read.getMetier());
                }
            }
        }
    }
}
