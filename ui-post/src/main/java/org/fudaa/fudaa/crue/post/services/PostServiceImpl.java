package org.fudaa.fudaa.crue.post.services;

import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.calcul.CalculCrueRunnerManager;
import org.fudaa.dodico.crue.projet.calcul.ExecInputDefault;
import org.fudaa.dodico.crue.projet.create.RunCalculOption;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.PostRunService;
import org.fudaa.fudaa.crue.common.services.SelectedPerspectiveService;
import org.fudaa.fudaa.crue.loader.ScenarioLoaderServiceAbstract;
import org.fudaa.fudaa.crue.study.services.CrueService;
import org.fudaa.fudaa.crue.study.services.EMHProjetServiceImpl;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.lookup.ServiceProvider;

import java.awt.*;
import java.util.Map;

/**
 * Service utilise pour charger des runs. Pas de lookup dans ce service.
 *
 * @author deniger
 */
@ServiceProvider(service = PostRunService.class)
public class PostServiceImpl extends ScenarioLoaderServiceAbstract implements PostRunService {
  private CrueService crueService;
  private LookupListener lookupListener;
  private CrueRunLauncher crueRunLauncherService;

  public PostServiceImpl() {
    super(PerspectiveEnum.POST);
  }

  private CrueRunLauncher getRunLauncher() {
    if (crueRunLauncherService == null) {
      crueRunLauncherService = new CrueRunLauncher();
    }
    return crueRunLauncherService;
  }

  /**
   * Attention: a utiliser pour les tests uniquement !
   *
   * @param scenario
   * @param projet
   */
  public void setForTest(ManagerEMHScenario manager, EMHScenario scenario, EMHRun run, EMHProjet projet) {
    assert scenario != null;
    crueService = Lookup.getDefault().lookup(CrueService.class);
    EMHProjetServiceImpl projetService = Lookup.getDefault().lookup(EMHProjetServiceImpl.class);
    projetService.setForTest(projet);
    super.dynamicContent.add(run);
    super.dynamicContent.add(manager);
    super.dynamicContent.add(scenario);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void run(ExecInputDefault execInputDefault,
                  Map<CrueFileType, RunCalculOption> options) {
    unloadRun();
    getRunLauncher().run(execInputDefault, options, new RunAfter(execInputDefault));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean validCrue9() {
    return getRunLauncher().validCrue9();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public CalculCrueRunnerManager getCalculCrueRunnerManagerOtfa() {
    final CalculCrueRunnerManager createCalculCrueRunnerManager = getRunLauncher().createCalculCrueRunnerManager();
    return createCalculCrueRunnerManager;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean validConfigurationForRun(ManagerEMHScenario scenario, CoeurConfigContrat coeurConfig) {
    return getRunLauncher().validConfiguration(scenario, coeurConfig);
  }

  private void activeListener() {
    crueService = Lookup.getDefault().lookup(CrueService.class);
    lookupListener = new LookupListener() {
      @Override
      public void resultChanged(LookupEvent ev) {
        updateState();
      }
    };
    crueService.addEMHProjetStateLookupListener(lookupListener);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public EMHProjet getSelectedProjet() {
    if (crueService == null) {
      crueService = Lookup.getDefault().lookup(CrueService.class);
    }
    return crueService.getProjectLoaded();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public CrueConfigMetier getCcm() {
    return getSelectedProjet().getPropDefinition();
  }

  /**
   * Met à jour l'état du scenario/run chargé en fonction de l'état global de l'application
   *
   * @see CrueService
   * @see CrueService#getProjectLoaded()
   */
  public void updateState() {
    if (isRunLoaded()) {
      EMHProjet projectLoaded = crueService.getProjectLoaded();
      if (projectLoaded != null) {
        ManagerEMHScenario managerScenarioLoaded = getManagerScenarioLoaded();
        ManagerEMHScenario scenario = projectLoaded.getScenario(managerScenarioLoaded.getNom());
        if (scenario == null) {
          unloadRun();
        } else {
          EMHRun runFromScenar = scenario.getRunFromScenar(getRunLoaded().getId());
          if (runFromScenar == null) {
            unloadRun();
          }
        }
      }
    }
  }

  @Override
  public boolean isRunLoaded() {
    return super.isRunLoaded();
  }

  /**
   * Chargement du run
   *
   * @param projet
   * @param scenario
   * @param run
   * @return true si le chargement c'est bien déroulé. <code>false</code> si erreur fatale
   */
  @Override
  public boolean loadRun(EMHProjet projet, ManagerEMHScenario scenario, EMHRun run) {
    activeListener();
    return super.loadScenario(projet, scenario, run) != null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void unloadRun() {
    super.unloadScenario();
    if (crueService != null) {
      crueService.removeEMHProjetStateLookupListener(lookupListener);
    }
    crueService = null;
  }

  public class RunAfter {
    final ExecInputDefault input;
    private PerspectiveEnum initPerspective;

    public RunAfter(ExecInputDefault input) {
      this.input = input;
    }

    public void runAfter(final AbstractRunLauncherCrueDelegate runLauncherCrueDelegate) {
      if (!EventQueue.isDispatchThread()) {
        EventQueue.invokeLater(() -> RunAfter.this.runAfter(runLauncherCrueDelegate));
      } else {
        if (runLauncherCrueDelegate.hasCalculNoOptions()) {
          if (crueService == null) {
            crueService = Lookup.getDefault().lookup(CrueService.class);
          }
          crueService.getProjetService().deepModification(() -> {
            //attention les scenarios sont clonés par securité
            //on doit recuperer les references initiales
            final ManagerEMHScenario scenario = crueService.getProjetService().getController().getProjet().getScenario(input.getScenario().getId());
            if (input.getRunReference() != null) {
              scenario.setRunCourant(scenario.getRunFromScenar(input.getRunReference().getId()));
            }
            crueService.getProjetService().getController().deleteRun(scenario, input.getRun(), null);
            return Boolean.TRUE;
          }, "deleteRun");
          if (input.getRunReference() != null) {
            String open = NbBundle.getMessage(PostServiceImpl.class, "crue10Exe.noServiceLaunched.openRun");
            final Object result = DialogHelper.showQuestion(
                NbBundle.getMessage(PostServiceImpl.class, "crue10Exe.noServiceLaunched.title"),
                NbBundle.getMessage(PostServiceImpl.class, "crue10Exe.noServiceLaunched.message", input.getRunReference().getNom()),
                new String[]{
                    open,
                    NbBundle.getMessage(PostServiceImpl.class, "crue10Exe.noServiceLaunched.cancel")
                }
            );
            if (open.equals(result)) {
              loadRun(input.getProjet(), input.getScenario(), input.getRunReference());
            } else if (initPerspective != null) {
              SelectedPerspectiveService selectedPerspectiveService = Lookup.getDefault().lookup(SelectedPerspectiveService.class);
              selectedPerspectiveService.activePerspective(initPerspective);
            }
          }
        } else {
          loadRun(input.getProjet(), input.getScenario(), input.getRun());
        }
      }
    }

    public PerspectiveEnum getInitPerspective() {
      return initPerspective;
    }

    public void setInitPerspective(PerspectiveEnum initPerspective) {
      this.initPerspective = initPerspective;
    }
  }
}
