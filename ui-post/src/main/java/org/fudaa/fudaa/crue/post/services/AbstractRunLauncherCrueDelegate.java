package org.fudaa.fudaa.crue.post.services;

import org.fudaa.dodico.crue.projet.calcul.CalculCrueContrat;
import org.fudaa.dodico.crue.projet.calcul.ExecInputDefault;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.util.Cancellable;
import org.openide.util.NbBundle;

import java.awt.*;

/**
 * @author deniger
 */
public abstract class AbstractRunLauncherCrueDelegate implements Runnable, Cancellable {
  final ExecInputDefault execInputDefault;
  final PostServiceImpl.RunAfter after;
  final CrueRunLauncher runLauncher;
  ProgressHandle progressHandle;
  CalculCrueContrat calculCrue;

  public AbstractRunLauncherCrueDelegate(ExecInputDefault execInputDefault, PostServiceImpl.RunAfter after, CrueRunLauncher runLauncher) {
    this.execInputDefault = execInputDefault;
    this.after = after;
    this.runLauncher = runLauncher;
  }

  @Override
  public boolean cancel() {
    if (calculCrue != null) {
      if (calculCrue.isStopFileWritten()) {
        String stopFile = NbBundle.getMessage(AbstractRunLauncherCrueDelegate.class, "cru10Exec.AlreadyStopped.KillOption");
        String wait = NbBundle.getMessage(AbstractRunLauncherCrueDelegate.class, "cru10Exec.AlreadyStopped.WaitOption");
        Object userAnswer = DialogHelper.showQuestion(NbBundle.getMessage(AbstractRunLauncherCrueDelegate.class,
            "crue10Exec.AlreadyStopped.DialogTitle"),
            NbBundle.getMessage(AbstractRunLauncherCrueDelegate.class,
                "crue10Exec.AlreadyStopped.DialogMessage"),
            new Object[]{stopFile, wait});
        if (wait.equals(userAnswer)) {
          return false;
        }
      }
      calculCrue.stop();
    }
    return true;
  }

  public boolean isCalculFinishedCorrectly() {
    return calculCrue != null && calculCrue.isComputeFinishedCorrectly();
  }

  /**
   * @return true si le calcul n'a pas d'options et ne peut pas être lancé
   */
  public boolean hasCalculNoOptions() {
    return calculCrue != null && calculCrue.hasNoOptions();
  }

  protected void clean() {
    EventQueue.invokeLater(() -> runLauncher.perspectiveServicePost.setDirty(false));
    progressHandle.finish();
    if (after != null) {
      after.runAfter(this);
    }
  }

  @Override
  public void run() {
    try {
      calculCrue = createExec();
      if (!calculCrue.hasNoOptions()) {
        calculCrue.run();
      }
    } finally {
      clean();
    }
  }

  protected abstract CalculCrueContrat createExec();

  void setProgressHandle(ProgressHandle ph) {
    this.progressHandle = ph;
  }
}
