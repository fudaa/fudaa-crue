package org.fudaa.fudaa.crue.post.services;

import org.fudaa.dodico.crue.projet.calcul.CalculCrueContrat;
import org.fudaa.dodico.crue.projet.calcul.ExecInputDefault;

/**
 *
 * @author deniger
 */
public class RunLauncherCrue9Delegate extends AbstractRunLauncherCrueDelegate {

  public RunLauncherCrue9Delegate(ExecInputDefault execInputDefault, PostServiceImpl.RunAfter after, CrueRunLauncher runLauncher) {
    super(execInputDefault, after, runLauncher);
  }

  @Override
  protected CalculCrueContrat createExec() {
    return runLauncher.getCrue9Runner().createRunner(execInputDefault, null);
  }
}
