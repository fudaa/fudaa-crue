package org.fudaa.fudaa.crue.post;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.fudaa.fudaa.crue.common.view.NbSheetCustom;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//org.fudaa.fudaa.crue.post//PostPropertiesTopComponent//EN",
autostore = false)
@TopComponent.Description(preferredID = PostPropertiesTopComponent.TOPCOMPONENT_ID,
iconBase = "org/fudaa/fudaa/crue/post/carre-vert_16.png",
persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "post-bottomLeft", openAtStartup = false, position = 2)
@ActionID(category = "Window", id = "org.fudaa.fudaa.crue.post.PostPropertiesTopComponent")
@ActionReference(path = "Menu/Window/Post", position = 7, separatorBefore = 6)
@TopComponent.OpenActionRegistration(displayName = PostPropertiesTopComponent.TOPCOMPONENT_ACTION_DISPLAYNAME,
preferredID = PostPropertiesTopComponent.TOPCOMPONENT_ID)
public final class PostPropertiesTopComponent extends NbSheetCustom {

  public static final String TOPCOMPONENT_ID = "PostPropertiesTopComponent";
  public static final String TOPCOMPONENT_ACTION = "CTL_" + TOPCOMPONENT_ID;
  public static final String TOPCOMPONENT_ACTION_DISPLAYNAME = "#" + TOPCOMPONENT_ACTION;

  public PostPropertiesTopComponent() {
    super(true, PerspectiveEnum.POST);
    setName(NbBundle.getMessage(PostPropertiesTopComponent.class, TOPCOMPONENT_ACTION));
    setToolTipText(NbBundle.getMessage(PostPropertiesTopComponent.class, "HINT_" + TOPCOMPONENT_ID));
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(SysdocUrlBuilder.getTopComponentHelpCtxId("vueProprietes", PerspectiveEnum.POST));
  }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
  void writeProperties(java.util.Properties p) {
    // better to version settings since initial version as advocated at
    // http://wiki.apidesign.org/wiki/PropertyFiles
    p.setProperty("version", "1.0");
  }

  void readProperties(java.util.Properties p) {
  }
}
