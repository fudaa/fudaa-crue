/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.post.action;

import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.action.AbstractPerspectiveAwareAction;
import org.fudaa.fudaa.crue.common.services.PostRunService;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;

public abstract class AbstractPostAction extends AbstractPerspectiveAwareAction {

  protected final PostRunService postService = Lookup.getDefault().lookup(PostRunService.class);
  private final Result<EMHScenario> resultat;

  public AbstractPostAction(boolean enableInEditMode) {
    super(PerspectiveEnum.POST,enableInEditMode);
    resultat = postService.getLookup().lookupResult(EMHScenario.class);
    resultat.addLookupListener(this);
    resultChanged(null);
  }

  @Override
  protected boolean getEnableState() {
    return super.getEnableState() && postService.isRunLoaded();
  }
}
