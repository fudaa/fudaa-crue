package org.fudaa.fudaa.crue.post.perspective;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.TopComponentHelper;
import org.fudaa.fudaa.crue.common.services.AbstractPerspectiveService;
import org.fudaa.fudaa.crue.common.services.PerspectiveService;
import org.fudaa.fudaa.crue.common.services.PerspectiveState;
import org.fudaa.fudaa.crue.post.PostAvctTopComponent;
import org.fudaa.fudaa.crue.post.PostCtuluLogTopComponent;
import org.fudaa.fudaa.crue.post.PostEmhExplorerTopComponent;
import org.fudaa.fudaa.crue.post.PostReportsTopComponent;
import org.fudaa.fudaa.crue.study.services.CrueService;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;
import org.openide.windows.TopComponent;

/**
 * Le service perspective de Compte-Rendu.
 *
 * @author Fred Deniger
 */
@ServiceProviders(value = {
  @ServiceProvider(service = PerspectiveServicePost.class)
  ,
  @ServiceProvider(service = PerspectiveService.class)})
public class PerspectiveServicePost extends AbstractPerspectiveService {

  private final Set<String> components = Collections.unmodifiableSet(new LinkedHashSet<>(Arrays.asList(
          PostReportsTopComponent.TOPCOMPONENT_ID,
          PostEmhExplorerTopComponent.TOPCOMPONENT_ID,
          PostCtuluLogTopComponent.TOPCOMPONENT_ID,
          PostAvctTopComponent.TOPCOMPONENT_ID)));
  /**
   * contient l'état ouvert ou non des TopComponent de cette perspective. Permet de persister la liste des TopComponent ouverts afin de les réouvrir
   * lors des (dés)activation des perspectives.
   */
  private final Map<String, Boolean> openedStateByTopComponentKey = new HashMap<>();
  private final CrueService crueService = Lookup.getDefault().lookup(CrueService.class);

  /**
   * Ajoute des listener sur le service {@link CrueService} pour suivre le (de)chargement de l'étude et des runs.
   */
  public PerspectiveServicePost() {
    super(PerspectiveEnum.POST);
    openedStateByTopComponentKey.put("PostReportStdoutTopComponent", Boolean.FALSE);
    crueService.addEMHProjetStateLookupListener(new LookupListener() {
      @Override
      public void resultChanged(LookupEvent ev) {
        projectLoadedChange();
      }
    });
    crueService.addPostLookupListener(new LookupListener() {
      @Override
      public void resultChanged(LookupEvent ev) {
        postLoadedChanged();
      }
    });
  }

  /**
   * Appelé par le listener a chaque fois qu'un run est (dé)chargé. Gère les états (read/read only) de la perspective.
   * Cette perspective ne passe jamais en mode edit.
   *
   * @see PerspectiveState
   */
  public void postLoadedChanged() {
    if (crueService.isProjetReadOnly()) {
      setState(PerspectiveState.MODE_READ_ONLY_ALWAYS);
    } else {
      setState(crueService.isAScenarioLoadedInPostPerspective() ? PerspectiveState.MODE_READ : PerspectiveState.MODE_READ_ONLY_TEMP);
    }
  }

  /**
   * Appelé par le listener a chaque fois qu'un projet est (dé)chargé. Gère les états (read/read only) de la perspective.
   * Cette perspective ne passe jamais en mode edit.
   *
   * @see PerspectiveState
   */
  public void projectLoadedChange() {
    if (crueService.getProjectLoaded() == null) {
      resetStateToReadOnlyTemp();
    } else {
      setState(crueService.isProjetReadOnly() ? PerspectiveState.MODE_READ_ONLY_ALWAYS : PerspectiveState.MODE_READ_ONLY_TEMP);
    }

  }

  /**
   * @return false: pas d'edition
   */
  @Override
  public boolean supportEdition() {
    return false;
  }

  /**
   * @return alwas true car pas d'edition dans cette perspective
   */
  @Override
  public boolean closing() {
    return true;
  }

  /**
   *
   * Persiste la liste des TopComponent ouvert pour les reactiver avec la perspective.
   *
   * @return true en général.
   */
  @Override
  public boolean deactivate() {
    boolean ok = super.deactivate();
    if (ok) {
      //persiste tous les TopComponent ouverts:
      Map<String, Collection<TopComponent>> openedTopComponentById = TopComponentHelper.getOpenedTopComponentById();
      for (Entry<String, Boolean> entry : openedStateByTopComponentKey.entrySet()) {
        Collection<TopComponent> listOftopComponent = openedTopComponentById.get(entry.getKey());
        if (listOftopComponent != null && listOftopComponent.size() == 1) {
          TopComponent tc = listOftopComponent.iterator().next();
          entry.setValue(tc != null && tc.isOpened());
        }
      }
    }
    return ok;
  }

  /**
   *
   * @param state
   * @return always true: pas d'édition.
   */
  @Override
  protected boolean canStateBeModifiedTo(PerspectiveState state) {
    return true;
  }

  /**
   *
   * @return always true
   */
  @Override
  public boolean activate() {
    return true;
  }

  /**
   *
   * @return la liste des TopComponents par defaut plus ceux ouverts en plus et persistés par la méthode {@link #deactivate()}.
   *         Il est possible d'ouvrir plusieurs instances du meme TopComponent.
   */
  @Override
  public Set<String> getDefaultTopComponents() {
    Set<String> opened = null;
    for (Entry<String, Boolean> entry : openedStateByTopComponentKey.entrySet()) {
      if (Boolean.TRUE.equals(entry.getValue())) {
        if (opened == null) {
          opened = new HashSet<>();
        }
        opened.add(entry.getKey());
      }
    }
    if (opened != null) {
      opened.addAll(components);
      return opened;
    }
    return components;
  }
}
