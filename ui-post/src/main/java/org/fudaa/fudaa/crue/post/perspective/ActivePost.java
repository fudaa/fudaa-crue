package org.fudaa.fudaa.crue.post.perspective;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.action.AbstractPerspectiveAction;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
/**
 * Permet d'activer une perspective. Action utilisée dans la toobar pour changer de perspective. Pour la gestion des ordres des menus, voir le fichier
 * layer.xml
 */
@ActionID(category = "File", id = "org.fudaa.fudaa.crue.post.perspective.ActivePost")
@ActionRegistration(displayName = "#CTL_ActivePost")
@ActionReference(path = "Menu/Window" , position = 4)
public final class ActivePost extends AbstractPerspectiveAction {

  public ActivePost() {
    super("CTL_ActivePost", PerspectiveServicePost.class.getName(),PerspectiveEnum.POST);
    setBooleanState(false);
  }
  
  @Override
  protected String getFolderAction() {
    return "Actions/Post";
  }
}
