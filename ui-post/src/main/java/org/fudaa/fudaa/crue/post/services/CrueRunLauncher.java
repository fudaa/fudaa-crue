package org.fudaa.fudaa.crue.post.services;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.calcul.*;
import org.fudaa.dodico.crue.projet.conf.GlobalOptionsManager;
import org.fudaa.dodico.crue.projet.conf.Option;
import org.fudaa.dodico.crue.projet.conf.OptionsEnum;
import org.fudaa.dodico.crue.projet.create.RunCalculOption;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.SelectedPerspectiveService;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.fudaa.fudaa.crue.post.MessagesPost;
import org.fudaa.fudaa.crue.post.PostAvctTopComponent;
import org.fudaa.fudaa.crue.post.perspective.PerspectiveServicePost;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

import java.io.File;
import java.util.EnumMap;
import java.util.Map;

/**
 * @author deniger
 */
public class CrueRunLauncher {
  final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);
  final PerspectiveServicePost perspectiveServicePost = Lookup.getDefault().lookup(PerspectiveServicePost.class);
  final SelectedPerspectiveService selectedPerspectiveService = Lookup.getDefault().lookup(SelectedPerspectiveService.class);

  public boolean validCrue9() {
    final Option option = configurationManagerService.getOptionsManager().getOption(OptionsEnum.CRUE9_EXE);
    if (option == null || StringUtils.isEmpty(option.getValeur())) {
      DialogHelper.showError(NbBundle.getMessage(MessagesPost.class, "LaunchCrue9.ExecNotDefined.DialogTitle"),
          NbBundle.getMessage(MessagesPost.class, "LaunchCrue9.ExecNotDefined.DialogMessage"));
      return false;
    }
    return true;
  }

  public CalculCrueRunnerManager createCalculCrueRunnerManager() {
    CalculCrueRunnerManagerImpl manager = new CalculCrueRunnerManagerImpl();
    final Option option = configurationManagerService.getOptionsManager().getOption(OptionsEnum.CRUE9_EXE);
    if (option != null) {
      manager.setCrue9Exec(option.getValeur());
    }
    manager.setExecOptions(getExecOptions());
    return manager;
  }

  //  @Override
  public Map<CrueFileType, String> getExecOptions() {
    Map<CrueFileType, String> res = new EnumMap<>(CrueFileType.class);
    final GlobalOptionsManager optionsManager = configurationManagerService.getOptionsManager();
    res.put(CrueFileType.RPTR, optionsManager.getOption(OptionsEnum.RPTR_EXE_OPTION).getValeur());
    res.put(CrueFileType.RPTG, optionsManager.getOption(OptionsEnum.RPTG_EXE_OPTION).getValeur());
    res.put(CrueFileType.RPTI, optionsManager.getOption(OptionsEnum.RPTI_EXE_OPTION).getValeur());
    res.put(CrueFileType.RCAL, optionsManager.getOption(OptionsEnum.RCAL_EXE_OPTION).getValeur());
    return res;
  }

  public CalculCrueRunner getCrue10Runner(CoeurConfigContrat coeur) {
    return createCalculCrueRunnerManager().getCrue10Runner(coeur);
  }

  public void run(final ExecInputDefault execInputDefault, Map<CrueFileType, RunCalculOption> options, PostServiceImpl.RunAfter runAfter) {

    final PerspectiveEnum currentPerspective = selectedPerspectiveService.getCurrentPerspectiveService().getPerspective();
    if(runAfter!=null){
      runAfter.setInitPerspective(currentPerspective);
    }
    selectedPerspectiveService.activePerspective(PerspectiveEnum.POST);

    ManagerEMHScenario scenario = execInputDefault.getScenario();
    AbstractRunLauncherCrueDelegate launcher;
    String message;
    if (scenario.isCrue9()) {
      if (!validCrue9()) {
        return;
      }
      message = NbBundle.getMessage(CrueRunLauncher.class,
          "Progression.Crue9.Name");
      launcher = new RunLauncherCrue9Delegate(execInputDefault, runAfter, this);
    } else {
      if (!validCrue10(execInputDefault.getProjet().getCoeurConfig())) {
        return;
      }
      message = NbBundle.getMessage(CrueRunLauncher.class,
          "Progression.Crue10.Name");
      PostAvctTopComponent avct = (PostAvctTopComponent) WindowManager.getDefault().findTopComponent(PostAvctTopComponent.TOPCOMPONENT_ID);
      launcher = new RunLauncherCrue10Delegate(execInputDefault, runAfter, this, options, avct);
    }
    final ProgressHandle ph = ProgressHandleFactory.createHandle(message, launcher);
    ph.start();
    launcher.setProgressHandle(ph);
    perspectiveServicePost.setDirty(true);
    new Thread(launcher).start();
  }

  /**
   * @param coeurConfig
   * @return true si l'excecutable existe.
   */
  private boolean validCrue10(final CoeurConfigContrat coeurConfig) {
    File execFile = new File(coeurConfig.getExecFile());

    if (!execFile.exists()) {
      DialogHelper.showError(NbBundle.getMessage(CrueRunLauncher.class, "crue10Exe.notExist"),
          NbBundle.getMessage(CrueRunLauncher.class, "crue10Exe.notExist.Details",
              coeurConfig.getXsdVersion(), execFile.getAbsolutePath()));
      return false;
    }
    return true;
  }

  public boolean validConfiguration(ManagerEMHScenario scenario, CoeurConfigContrat coeurConfig) {
    if (scenario.isCrue9()) {
      return validCrue9();
    }
    return validCrue10(coeurConfig);
  }

  public CalculCrueRunner getCrue9Runner() {
    validCrue9();
    return new CalculCrue9RunnerWrapper(createCalculCrueRunnerManager().getCrue9Runner());
  }

  /**
   * Pour afficher un message si non configure.
   */
  private class CalculCrue9RunnerWrapper implements CalculCrueRunner {
    private final CalculCrueRunner initial;

    public CalculCrue9RunnerWrapper(CalculCrueRunner initial) {
      this.initial = initial;
    }

    @Override
    public CalculCrueContrat createRunner(ExecInput execInput, ExecConfigurer configurer) {
      if (!validCrue9()) {
        return null;
      }
      return initial.createRunner(execInput, configurer);
    }
  }
}
