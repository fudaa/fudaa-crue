package org.fudaa.fudaa.crue.post.node;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.io.CrueIOResu;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.config.cr.CRReader;
import org.fudaa.dodico.crue.config.cr.CrueLogTranslator;
import org.fudaa.dodico.crue.config.cr.CrueTranslationReaderWriter;
import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.helper.EMHHelper;
import org.fudaa.dodico.crue.projet.conf.OptionsEnum;
import org.fudaa.fudaa.crue.options.services.ConfigurationManagerService;
import org.openide.util.Lookup;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Construit les noms des compte-rendus à partir des fichiers de compte-rendu et du fichier messages.
 *
 * @author deniger
 */
public class ReportNodesBuilder {
    int maxLignesToRead = -1;
    final ConfigurationManagerService configurationManagerService = Lookup.getDefault().lookup(ConfigurationManagerService.class);

    public int getMaxNumberToRead() {
        if (maxLignesToRead < 0) {
            Integer integerValue = configurationManagerService.getOptionsManager().getIntegerValue(OptionsEnum.MAX_LOG_ITEM);
            if (integerValue != null) {
                maxLignesToRead = integerValue.intValue();
            } else {
                maxLignesToRead = 10000;
            }
        }
        return maxLignesToRead;
    }

    private CtuluLogGroup createLogGroups(EMH modele) {
        CompteRendusInfoEMH reportEMH = (CompteRendusInfoEMH) EMHHelper.selectInfoEMH(modele, EnumInfosEMH.LOGS);
        if (reportEMH == null) {
            return null;
        }
        Collection<CompteRendu> compteRendus = reportEMH.getCompteRendus();
        if (compteRendus == null) {
            return null;
        }
        CtuluLogGroup gr = new CtuluLogGroup(null);
        gr.setDescription(modele.getNom());
        for (CompteRendu compteRendu : compteRendus) {
            CRReader reader = new CRReader(getMaxNumberToRead());
            CrueIOResu<CtuluLog> read = reader.read(compteRendu.getLogFile());
            if (read.getAnalyse().isNotEmpty()) {
                gr.addLog(read.getAnalyse());
            }
            final CtuluLog metier = read.getMetier();
            if (metier != null) {
                metier.setDesc(compteRendu.getLogFile().getName());
                gr.addLog(metier);
            }
        }

        //(CPTR, CPTG, CPTI, CCAL
        return gr;
    }

    public List<CtuluLogGroup> buildGroups(EMHScenario scenarioLoaded, CoeurConfigContrat coeur) {
        return buildGroups(scenarioLoaded, coeur, false);
    }

    public List<CtuluLogGroup> buildGroups(EMHScenario scenarioLoaded, CoeurConfigContrat coeur, boolean onlyScenario) {

        List<CtuluLogGroup> groups = new ArrayList<>();
        String currentLocale = configurationManagerService.getCurrentLocale();
        String messageURL = coeur.getMessageURL(currentLocale);
        CrueLogTranslator translator = null;
        CtuluLog translationLog = null;
        if (messageURL != null) {
            CrueTranslationReaderWriter reader = new CrueTranslationReaderWriter();
            translationLog = new CtuluLog();
            CrueIOResu<CrueLogTranslator> res = reader.readXML(new File(messageURL), translationLog);
            translator = res.getMetier();
        }
        if (onlyScenario) {
            CtuluLogGroup scenarioLogs = createLogGroups(scenarioLoaded);
            if (scenarioLogs == null) {
                return groups;
            }
            if (translationLog != null && translationLog.isNotEmpty()) {
                scenarioLogs.addLogFirstPosition(translationLog);
            }
            if (translator != null) {
                translator.translateGroup(scenarioLogs);
            }
            groups.add(scenarioLogs);
            return groups;
        }
        //on recherche les erreur:
        CompteRendusInfoEMH reportEMH = (CompteRendusInfoEMH) EMHHelper.selectInfoEMH(scenarioLoaded, EnumInfosEMH.LOGS);

        CtuluLog logMainError = null;
        if (reportEMH != null && reportEMH.getError() != null) {
            logMainError = new CtuluLog();
            logMainError.setDesc(BusinessMessages.getString("calcul.failure.ui.error.title"));
            logMainError.addError(BusinessMessages.getString("calcul.failure.ui.error"));
            logMainError.updateLocalizedMessage(null);
        }
        boolean first = true;
        List<EMHModeleBase> modeles = scenarioLoaded.getModeles();
        for (EMHModeleBase modele : modeles) {
            CtuluLogGroup logGroup = createLogGroups(modele);
            if (logGroup == null) {
                continue;
            }
            if (logMainError != null) {
                logGroup.addLogFirstPosition(logMainError);
            }
            if (first && translationLog != null && translationLog.isNotEmpty()) {
                logGroup.addLogFirstPosition(translationLog);
            }
            first = false;
            if (translator != null) {
                translator.translateGroup(logGroup);
            }
            groups.add(logGroup);
        }
        if (modeles.isEmpty() && logMainError != null) {
            CtuluLogGroup group = new CtuluLogGroup(null);
            group.addLog(logMainError);
            groups.add(group);
        }
        return groups;
    }

    /**
     * Methode qui relit les logs et renvoie le premier.
     *
     * @param scenarioLoaded
     * @param coeurConfig
     * @return null si pas de logs.
     */
    public CtuluLogGroup buildStdoutGroups(EMHScenario scenarioLoaded, CoeurConfigContrat coeurConfig) {
        final List<CtuluLogGroup> logsGroups = buildGroups(scenarioLoaded, coeurConfig, true);
        return logsGroups.isEmpty() ? null : logsGroups.get(0);
    }
}
