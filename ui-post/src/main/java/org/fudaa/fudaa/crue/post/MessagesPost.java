package org.fudaa.fudaa.crue.post;

import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class MessagesPost {
  private MessagesPost() {
  }

  public static String getMessage(String code) {
    return NbBundle.getMessage(MessagesPost.class, code);
  }

}
