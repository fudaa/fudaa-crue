package org.fudaa.fudaa.crue.post.action;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.services.SelectedPerspectiveService;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

@ActionID(category = "View",
id = "org.fudaa.fudaa.crue.post.action.PostCloseAction")
@ActionRegistration(displayName = "#CTL_PostCloseAction")
@ActionReferences({
  @ActionReference(path = "Actions/Post", position = 4)
})
public final class PostCloseAction extends AbstractPostAction {
  final SelectedPerspectiveService selectedPerspectiveService = Lookup.getDefault().lookup(SelectedPerspectiveService.class);

  public PostCloseAction() {
    super(false);
    putValue(Action.NAME, NbBundle.getMessage(PostCloseAction.class, "CTL_PostCloseAction"));
  }

  @Override
  public Action createContextAwareInstance(Lookup actionContext) {
    return new PostCloseAction();
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    postService.unloadRun();
    selectedPerspectiveService.activePerspective(PerspectiveEnum.STUDY);

  }
}
