/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLogLevel;
import static org.junit.Assert.*;
import org.junit.Test;

public class FileIntegrityManagerTest {

  public List<File> create() {
    final List<File> res = new ArrayList<>();
    try {
      res.add(File.createTempFile("test", ".txt"));
      res.add(File.createTempFile("test1", ".txt"));
    } catch (final IOException e) {
      Logger.getLogger(FileIntegrityManagerTest.class.getName()).log(Level.INFO, "message {0}", e);
    }
    assertEquals(2, res.size());
    for (final File file : res) {
      assertTrue(file.exists());
    }
    return res;
  }

  public void delete(final Collection<File> files) {
    for (final File file : files) {
      if (file.exists()) {
        assertTrue(file.delete());
      }
    }
  }

  @Test
  public void testWithoutBackupSucceed() {
    final List<File> files = create();
    // on efface les fichiers pour ne pas faire de backup
    delete(files);
    final FileIntegrityManager manager = new FileIntegrityManager(files);
    manager.start();
    assertNull(manager.getBackupFileFor(files.get(0)));
    assertNull(manager.getBackupFileFor(files.get(1)));

    doOperation(files);
    manager.operationSucceed();
    assertTrue(files.get(0).exists());
    assertTrue(files.get(1).exists());
    delete(files);
  }

  private void doOperation(final List<File> files) {
    try {
      if (!files.get(0).exists()) {
        assertTrue(files.get(0).createNewFile());
      }
      if (!files.get(1).exists()) {
        assertTrue(files.get(1).createNewFile());
      }
    } catch (final IOException e) {
      fail(e.getMessage());
    }
  }

  @Test
  public void testWithBackupSucceed() {
    final List<File> files = create();
    // on efface le premier fichier. Il y aura un backup sur le deuxieme fichier
    assertTrue(files.get(0).delete());
    final FileIntegrityManager manager = new FileIntegrityManager(files);
    manager.start();
    assertNull(manager.getBackupFileFor(files.get(0)));
    final File backupFileFor1 = manager.getBackupFileFor(files.get(1));
    assertNotNull(backupFileFor1);
    doOperation(files);
    manager.operationSucceed();
    assertTrue(files.get(0).exists());
    assertTrue(files.get(1).exists());
    assertFalse(backupFileFor1.exists());
    delete(files);
  }

  @Test
  public void testWithBackupFailed() {
    final List<File> files = create();
    // on efface le premier fichier. Il y aura un backup sur le deuxieme fichier
    assertTrue(files.get(0).delete());

    final File fichier1 = files.get(1);
    try {
      final FileWriter writer = new FileWriter(fichier1);
      writer.append("Test");
      writer.close();
    } catch (final IOException e) {
      fail(e.getMessage());
    }
    assertTrue(fichier1.length() > 0);
    final FileIntegrityManager manager = new FileIntegrityManager(files);
    fichier1.setLastModified(1000000L);
    assertEquals(1000000L, fichier1.lastModified());
    manager.start();
    //pour simuler une opération sur le fichier 1:
    fichier1.delete();
    doOperation(files);
    //l'operation a cree un fichier vide
    assertTrue(fichier1.length() == 0);
    assertNull(manager.getBackupFileFor(files.get(0)));
    final File backupFileFor1 = manager.getBackupFileFor(files.get(1));
    assertNotNull(backupFileFor1);
    manager.operationFailed();
    assertFalse(files.get(0).exists());
    assertTrue(fichier1.exists());
    assertEquals(1000000L, fichier1.lastModified());
    //le backup a bien recupere l'ancien fichier:
    try {
      final String texte = CtuluLibFile.litFichierTexte(fichier1, fichier1.length(), true, false);
      assertEquals("Test", texte);
    } catch (final IOException e) {
      e.printStackTrace();
      fail(e.getMessage());
    }
    assertFalse(backupFileFor1.exists());
    delete(files);
    assertFalse(manager.getBackupLog().isEmpty());
    assertEquals(2, manager.getBackupLog().getNbOccurence(CtuluLogLevel.INFO));
  }

  @Test
  public void testWithoutBackupFailed() {
    final List<File> files = create();
    // on efface les fichiers pour ne pas faire de backup
    delete(files);
    final FileIntegrityManager manager = new FileIntegrityManager(files);
    manager.start();
    assertNull(manager.getBackupFileFor(files.get(0)));
    assertNull(manager.getBackupFileFor(files.get(1)));
    doOperation(files);
    files.get(1).delete();
    manager.operationFailed();

    assertFalse(files.get(0).exists());
    assertFalse(files.get(1).exists());

    delete(files);
    assertFalse(manager.getBackupLog().isEmpty());
  }
}
