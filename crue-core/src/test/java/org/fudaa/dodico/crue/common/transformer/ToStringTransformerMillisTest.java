/*
 GPL 2
 */
package org.fudaa.dodico.crue.common.transformer;

import org.fudaa.dodico.crue.common.time.ToStringTransformerMillis;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Frederic Deniger
 */
public class ToStringTransformerMillisTest {

  public ToStringTransformerMillisTest() {
  }

  @Test
  public void testParse() {
    ToStringTransformerMillis option = new ToStringTransformerMillis();
    Double parse = option.parseInSec("00:00:01:00");
    assertNotNull(parse);
    assertEquals(60, parse.intValue());
    parse = option.parseInSec("00:01:00.001");
    assertNotNull(parse);
    assertEquals(60.001, parse.doubleValue(), 1e-4);
  }

  @Test
  public void testFormat() {
    ToStringTransformerMillis option = new ToStringTransformerMillis();
    Long oneSecond = 1000L;
    Long oneMin = oneSecond * 60;
    String print = option.format(oneMin);
    assertEquals("00:01:00", print);
    Long onHour = oneMin * 60;
    print = option.format(oneMin + onHour);
    assertEquals("01:01:00", print);
    print = option.format(oneMin + onHour + oneSecond + 1);
    assertEquals("01:01:01.001", print);
    Long twoDay = onHour * 24 * 2;
    print = option.format(twoDay + oneMin + onHour + oneSecond + 1);
    assertEquals("2:01:01:01.001", print);
    Long lotOfDays = onHour * 24 * 400;
    print = option.format(lotOfDays + oneMin + onHour + oneSecond + 1);
    assertEquals("400:01:01:01.001", print);


  }
}