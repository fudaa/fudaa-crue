/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common;

import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

/**
 * @author deniger
 */
public class CrueFileHelperTest extends AbstractTestParent {
  @Test
  public void testSupprimeQuotes() {
    Assert.assertNull(CrueFileHelper.clearCsvString(null));
    assertEquals("\"ee", CrueFileHelper.clearCsvString("\"ee"));
    assertEquals("", CrueFileHelper.clearCsvString("\"\""));
    assertEquals("coucou", CrueFileHelper.clearCsvString("\"coucou\""));
    assertEquals("test", CrueFileHelper.clearCsvString("'test'"));
  }

  @Test
  public void testCopyDir() throws IOException {
    final File init = createTempDir();
    final File file1 = new File(init, "file");
    file1.createNewFile();
    final File work1 = new File(init, "work1");
    work1.mkdirs();
    final File work1File = new File(work1, "test1");
    work1File.createNewFile();
    final File dest = createTempDir();
    CrueFileHelper.copyDirWithSameLastDate(init, dest);
    final File[] listFiles = dest.listFiles();
    assertEquals(2, listFiles.length);
    for (final File file : listFiles) {
      if (file.isFile()) {
        assertEquals(file1.getName(), file.getName());
        assertEquals(file1.lastModified(), file.lastModified());
      } else if (file.isDirectory()) {
        assertEquals(work1.getName(), file.getName());
        assertEquals(work1.lastModified(), file.lastModified());
        final File[] filesInWork1 = file.listFiles();
        assertEquals(1, filesInWork1.length);
        final File work1FileDest = filesInWork1[0];
        assertEquals(work1File.getName(), work1FileDest.getName());
        assertEquals(work1File.lastModified(), work1FileDest.lastModified());
      }
    }
  }

  @Test
  public void testGetAbsoluteFile() {
    final File absoluteDir = createTempDir();
    final File absoluteDir2 = createTempDir();
    File canonicalBaseDir = CrueFileHelper.getCanonicalBaseDir(absoluteDir, null);
    assertEquals(absoluteDir, canonicalBaseDir);
    canonicalBaseDir = CrueFileHelper.getCanonicalBaseDir(absoluteDir, "  ");
    assertEquals(absoluteDir, canonicalBaseDir);
    canonicalBaseDir = CrueFileHelper.getCanonicalBaseDir(absoluteDir, ".\\");
    assertEquals(absoluteDir, canonicalBaseDir);
    canonicalBaseDir = CrueFileHelper.getCanonicalBaseDir(absoluteDir, ".");
    assertEquals(absoluteDir, canonicalBaseDir);
    canonicalBaseDir = CrueFileHelper.getCanonicalBaseDir(absoluteDir, "./");
    assertEquals(absoluteDir, canonicalBaseDir);
    canonicalBaseDir = CrueFileHelper.getCanonicalBaseDir(absoluteDir, "\\.");
    assertEquals(absoluteDir, canonicalBaseDir);

    //2 eme is an absolute path
    canonicalBaseDir = CrueFileHelper.getCanonicalBaseDir(absoluteDir, absoluteDir2.getAbsolutePath());
    assertEquals(absoluteDir2, canonicalBaseDir);

    canonicalBaseDir = CrueFileHelper.getCanonicalBaseDir(absoluteDir, "test");
    assertEquals(new File(absoluteDir, "test"), canonicalBaseDir);
    canonicalBaseDir = CrueFileHelper.getCanonicalBaseDir(absoluteDir, "./test");
    assertEquals(new File(absoluteDir, "test"), canonicalBaseDir);
    canonicalBaseDir = CrueFileHelper.getCanonicalBaseDir(absoluteDir, ".\\test");
    assertEquals(new File(absoluteDir, "test"), canonicalBaseDir);
    canonicalBaseDir = CrueFileHelper.getCanonicalBaseDir(absoluteDir, "./test/");
    assertEquals(new File(absoluteDir, "test"), canonicalBaseDir);
  }
}
