/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common;

import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.joda.time.Duration;
import org.joda.time.LocalDateTime;
import org.joda.time.Period;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author deniger
 */
public class DateDurationConverterTest {
  private static final long NB_MILLIS_BY_HOUR = 3600000;

  @Test
  public void testFormatter() {
    Duration d = DateDurationConverter.getDuration(1, 2, 3, 64);
    Assert.assertEquals("01:02:04:04",
        DateDurationConverter.durationToDDHHMMSS(d));
    Assert.assertEquals("26:04:04",
        DateDurationConverter.durationToHHMMSS(d));
    d = DateDurationConverter.getDuration(3, 0, 0, 1);
    Assert.assertEquals("72:00:01",
        DateDurationConverter.durationToHHMMSS(d));
    Assert.assertEquals("03:00:00:01",
        DateDurationConverter.durationToDDHHMMSS(d));
  }

  /**
   * Test la conversion {@link DateDurationConverter#getPeriod(int, int, int, int)}.
   */
  @Test
  public void testPeriodDurationConvertFromDayHourMinuteSec() {
    final Period period = DateDurationConverter.getPeriod(1, 25, 3, 62);
    Assert.assertEquals(0, period.getWeeks());
    Assert.assertEquals(0, period.getYears());
    Assert.assertEquals(0, period.getMonths());
    Assert.assertEquals(1, period.getDays());
    Assert.assertEquals(25, period.getHours());
    Assert.assertEquals(3, period.getMinutes());
    Assert.assertEquals(62, period.getSeconds());
    final long expected = 1 * 24 * NB_MILLIS_BY_HOUR + 25 * NB_MILLIS_BY_HOUR + 3 * 60000 + 62000;
    Assert.assertEquals(expected, DateDurationConverter.getDuration(1, 25, 3, 62).getMillis());
  }

  @Test
  public void testMillis() {
    final String res = DateDurationConverter.durationToXsd(new Duration(123L));
    Assert.assertEquals("P0Y0M0DT0H0M0.123S", res);
  }

  /**
   * Test la conversion depuis une chaine xsd.
   */
  @Test
  public void testPeriodDurationConvertFromXsd() {
    final String xsd = "P5Y2M10DT15H2.5S";
    final Period period = DateDurationConverter.getDaysHourMinSecPeriod(xsd);
    final int nbDaysExpected = 5 * 365 + 2 * 30 + 10;
    Assert.assertEquals(0, period.getWeeks());
    Assert.assertEquals(0, period.getYears());
    Assert.assertEquals(0, period.getMonths());
    Assert.assertEquals(nbDaysExpected, period.getDays());
    Assert.assertEquals(15, period.getHours());
    Assert.assertEquals(2, period.getSeconds());
    Assert.assertEquals(500, period.getMillis());
    final long expected = nbDaysExpected * 24 * NB_MILLIS_BY_HOUR + 15 * NB_MILLIS_BY_HOUR + 2000 + 500;
    Assert.assertEquals(expected, DateDurationConverter.getDuration(xsd).getMillis());
  }

  /**
   * Test les conversions de date.
   */
  @Test
  public void testDate() {
    Assert.assertEquals("0001-01-01T00:00:00.000", DateDurationConverter.dateToXsd(DateDurationConverter.getDateFromZeroDate(
        0, 0, 0, 0)));
    final LocalDateTime t = DateDurationConverter.getDateFromZeroDate(1, 13, 0, 0);
    Assert.assertEquals("0001-01-02T13:00:00.000", DateDurationConverter.dateToXsd(t));
    Assert.assertEquals("0001-01-02 13:00:00", DateDurationConverter.dateToXsdUI(t));
    Assert.assertEquals("1 13 0 0", DateDurationConverter.dateToCrueFormat(t));
    final LocalDateTime t2 = DateDurationConverter.getDate("0001-01-02T13:00:00.000");
    Assert.assertEquals(t, t2);
  }

  /**
   * Test les conversion vers les string
   */
  @Test
  public void testDurationToString() {
    final String xsd = "P5Y2M10DT15H2S";
    final String crueString = DateDurationConverter.durationToCrueFormat(DateDurationConverter.getDuration(xsd));
    final int nbDays = 5 * 365 + 2 * 30 + 10;
    Assert.assertEquals(nbDays + " 15 0 2", crueString);
    final String xsdString = DateDurationConverter.durationToXsd(DateDurationConverter.getDuration(xsd));
    Assert.assertEquals("P0Y0M" + nbDays + "DT15H0M2S", xsdString);
  }
}
