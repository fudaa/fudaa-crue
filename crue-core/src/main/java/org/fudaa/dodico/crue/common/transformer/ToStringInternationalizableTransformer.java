/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.transformer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author deniger
 */
public class ToStringInternationalizableTransformer {

  public static final Transformer TO_18N = new Toi18n();
  public static final Transformer TO_18N_LONG = new Toi18nLong();

  public static List<String> toi18n(List<? extends ToStringInternationalizable> in) {
    List<String> res = new ArrayList<>();
    CollectionUtils.collect(in, TO_18N, res);
    return res;
  }

  public static String toi18nString(List<? extends ToStringInternationalizable> in, String sep) {
    if (CollectionUtils.isEmpty(in)) {
      return StringUtils.EMPTY;
    }
    StringBuilder builder = new StringBuilder();
    int nb = in.size();
    for (int i = 0; i < nb; i++) {
      if (i > 0) {
        builder.append(sep);
      }
      builder.append(TO_18N.transform(in.get(i)));
    }
    return builder.toString();
  }

  public static <T extends ToStringInternationalizable> Map<String, T> toi18nMap(List<T> ins) {
    Map<String, T> res = new HashMap<>();
    for (T in : ins) {
      res.put(in.geti18n(), in);

    }
    return res;
  }

  public static List<String> toi18nLong(List<? extends ToStringInternationalizable> in) {
    List<String> res = new ArrayList<>();
    CollectionUtils.collect(in, TO_18N_LONG, res);
    return res;
  }

  private static class Toi18n implements Transformer {

    @Override
    public Object transform(Object o) {
      return ((ToStringInternationalizable) o).geti18n();
    }
  }

  private static class Toi18nLong implements Transformer {

    @Override
    public Object transform(Object o) {
      return ((ToStringInternationalizable) o).geti18nLongName();
    }
  }
}
