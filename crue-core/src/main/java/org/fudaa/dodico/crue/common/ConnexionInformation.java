/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common;

import org.joda.time.LocalDateTime;

/**
 * Information de connexion: le nom du user {@link #getCurrentUser()} et la date locale {@link #getCurrentDate() }.
 *
 * @author deniger
 */
public interface ConnexionInformation {

  /**
   * @return la date courante
   */
  LocalDateTime getCurrentDate();

  /**
   *
   * @return l'identifiant de l'utilisateur en cours.
   */
  String getCurrentUser();
}
