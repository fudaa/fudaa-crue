/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.io;

import org.fudaa.ctulu.CtuluLog;

/**
 * Cette classe sera utilise pour les resultats/entree des operations de lecture/ecriture.
 *
 * @author deniger
 * @param <M> l'objet métier
 */
public class CrueIOResu<M> {

  private M metier;
  private String crueCommentaire;
  private CtuluLog analyse;

  /**
   * @return the analyse
   */
  public CtuluLog getAnalyse() {
    return analyse;
  }

  /**
   * @param analyse the analyse to set
   */
  public void setAnalyse(final CtuluLog analyse) {
    this.analyse = analyse;
  }

  /**
   * @return the metier
   */
  public M getMetier() {
    return metier;
  }

  /**
   * @param metier l'objet metier porte par cet objet
   */
  public CrueIOResu(final M metier) {
    super();
    this.metier = metier;
  }

  public CrueIOResu(final M metier, final CtuluLog log) {
    super();
    this.metier = metier;
    this.analyse = log;
  }

  /**
   * Constructeur par defaut
   */
  public CrueIOResu() {
    super();
  }

  /**
   * @param metier the metier to set
   */
  public void setMetier(final M metier) {
    this.metier = metier;
  }

  /**
   * @return the crueCommentaire
   */
  public String getCrueCommentaire() {
    return crueCommentaire;
  }

  /**
   * @param crueCommentaire the crueCommentaire to set
   */
  public void setCrueCommentaire(final String crueCommentaire) {
    this.crueCommentaire = crueCommentaire;
  }
}
