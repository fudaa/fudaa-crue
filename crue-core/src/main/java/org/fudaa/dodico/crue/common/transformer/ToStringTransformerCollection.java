/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.transformer;

import java.util.Collection;
import org.apache.commons.lang3.StringUtils;

public class ToStringTransformerCollection implements ToStringTransformer {

  private final ToStringTransformerDelegate delegate;

  /**
   * @param delegate permet de transformer un element en string
   */
  public ToStringTransformerCollection(ToStringTransformerDelegate delegate) {
    super();
    this.delegate = delegate;
  }

  @SuppressWarnings("unchecked")
  @Override
  public String transform(Object in) {
    if (!(in instanceof Collection)) {
      return null;
    }
    Collection list = (Collection) in;
    if (list.isEmpty()) {
      return StringUtils.EMPTY;
    }
    Object first = list.iterator().next();
    return ToStringHelper.transformTo(list, delegate.getTransformer(first));
  }
}
