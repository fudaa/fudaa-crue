/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.contrat;

public interface ObjetWithID {

  /**
   * @return doit renvoyer le nom en majuscule
   */
  String getId();

  /**
   * @return initialName
   */
  String getNom();
}