/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.transformer;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;

import java.util.Collection;

/**
 * Methodes utilitaires pour transformer des listes, objets en string.
 *
 * @author deniger
 */
@SuppressWarnings("unchecked")
public class ToStringHelper {
  public static final String SEPARATOR = ";";

  private static Transformer getTransformer(ToStringTransformer in) {
    return new TransformerAdapter(in);
  }

  public static String join(Collection<String> in) {
    return StringUtils.join(in, "; ");
  }

  /**
   * @param objetNommes les objets nommés
   * @return les noms séparés par des virgules.
   */
  public static String transformToNom(Collection<? extends ObjetWithID> objetNommes) {
    return transformTo(objetNommes, new TransformerHelper.TransformerObjetNommeToNom());
  }

  public static String typeToi18n(String type) {
    if (type == null) {
      return null;
    }
    return BusinessMessages.getStringOrDefault(type + ".shortName", type);
  }

  public static String typeToi18nLong(String type) {
    if (type == null) {
      return null;
    }
    try {
      return BusinessMessages.getStringOrDefault(type + ".longName", type);
    } catch (Exception ignored) {
    }
    return type;
  }

  /**
   * @param collection a transformer
   * @param transformer le transformer
   * @return la chaine des objets séparés par ;
   */
  public static String transformTo(Collection collection, Transformer transformer) {
    return transformTo(collection, transformer, SEPARATOR);
  }

  /**
   * @param collection a transformer
   * @param transformer le transformer
   * @return la chaine des objets séparés par ;
   */
  public static String transformTo(Collection collection, ToStringTransformer transformer) {
    return transformTo(collection, transformer, SEPARATOR);
  }

  private static String transformTo(Collection in, Transformer transformer, String separator) {
    return StringUtils.join(CollectionUtils.collect(in, transformer), separator);
  }

  private static String transformTo(Collection in, ToStringTransformer transformer, String separator) {
    return StringUtils.join(CollectionUtils.collect(in, getTransformer(transformer)), separator);
  }
}
