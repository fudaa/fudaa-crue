/*
 GPL 2
 */
package org.fudaa.dodico.crue.common.io;

import org.apache.commons.collections.map.IdentityMap;
import org.fudaa.ctulu.CtuluLibFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Frederic Deniger
 */
public class CacheFileInputStream {
  private static final CacheFileInputStream INSTANCE = new CacheFileInputStream();
  public static final Logger LOGGER = Logger.getLogger(CacheFileInputStream.class.getName());

  public static final CacheFileInputStream getINSTANCE() {
    return INSTANCE;
  }

  private Thread thread;
  private final IdentityMap inputByFile = new IdentityMap();
  private volatile boolean active;

  public boolean isActive() {
    return active;
  }

  public FileInputStream open(File file) throws FileNotFoundException {
    if (active) {
      testThread();
      FileInputStream res = (FileInputStream) inputByFile.get(file);
      if (res == null) {
        res = new FileInputStream(file);
        inputByFile.put(file, res);
      }
      return res;
    } else {
      return new FileInputStream(file);
    }
  }

  public void close(File file, FileInputStream input) {
    if (active) {
      testThread();
    } else {
      CtuluLibFile.close(input);
    }
  }

  public void setActive(boolean active) {
    if (active != this.active) {
      this.active = active;
      if (active) {
        thread = Thread.currentThread();
      } else {
        thread = null;
        Collection<FileInputStream> values = new ArrayList<>(inputByFile.values());
        for (FileInputStream fileInputStream : values) {
          if (fileInputStream != null) {
            try {
              fileInputStream.close();
            } catch (IOException ex) {
              Logger.getLogger(CacheFileInputStream.class.getName()).log(Level.WARNING, null, ex);
            }
          }
        }
        inputByFile.clear();
      }
    }
  }

  private void testThread() {
    if (Thread.currentThread() != thread && LOGGER.isLoggable(Level.FINEST)) {
      LOGGER.log(Level.FINEST, "message {0}", this);
    }
  }
}
