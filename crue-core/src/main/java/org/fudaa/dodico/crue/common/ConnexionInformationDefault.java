/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common;

import org.joda.time.LocalDateTime;

/**
 * @author deniger
 */
public class ConnexionInformationDefault implements ConnexionInformation {
  private final static ConnexionInformation INSTANCE = new ConnexionInformationDefault();

  public static ConnexionInformation getDefault() {
    return INSTANCE;
  }


  @Override
  public LocalDateTime getCurrentDate() {
    return new LocalDateTime();
  }

  @Override
  public String getCurrentUser() {
    return System.getProperty("user.name");
  }
}
