/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common;

import com.memoire.fu.FuLib;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.attribute.FileTime;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class CrueFileHelper {
  private static final Logger LOGGER = Logger.getLogger(CrueFileHelper.class.getName());
  private static final Set<String> EMPTY_PATH = new HashSet<>(Arrays.asList(".", "./", ".\\"));

  private CrueFileHelper() {
  }

  /**
   * Supprime un fichier et log erreur si necessaire.
   *
   * @param fileToDelete fichier a supprimer
   * @param classForLog  la classe pour le log. Peut etre null
   */
  public static void deleteFile(File fileToDelete, Class classForLog) {
    if (fileToDelete != null && fileToDelete.exists()) {
      try {
        Files.delete(fileToDelete.toPath());
      } catch (IOException ex) {
        getLogger(classForLog).log(Level.WARNING, "Can't delete file " + fileToDelete.getAbsolutePath() + ": " + ex.getMessage());
      }
    }
  }

  private static Logger getLogger(Class classForLog) {
    Logger logger = LOGGER;
    if (classForLog != null) {
      logger = Logger.getLogger(classForLog.getName());
    }
    return logger;
  }

  public static void setLastModified(File toChange, File fromFile, Class classForLog) {
    setLastModified(toChange, fromFile.lastModified(), classForLog);
  }

  public static void setLastModified(File toChange, long newLastModified, Class classForLog) {
    if (toChange != null) {
      try {
        Files.setLastModifiedTime(toChange.toPath(), FileTime.fromMillis(newLastModified));
      } catch (IOException ex) {
        getLogger(classForLog).log(Level.WARNING, "Can't change last modified time " + toChange.getAbsolutePath() + ": " + ex.getMessage());
      }
    }
  }

  /**
   * @param savedFile le fichier a comparer
   * @param destFile  le fichier a comparer
   * @return true si les fichiers sont différents.
   */
  public static boolean isDifferentFiles(File savedFile, File destFile) {
    boolean res = true;
    FileReader savedReader = null;
    FileReader destReader = null;
    try {
      savedReader = new FileReader(savedFile);
      destReader = new FileReader(destFile);
      res = !IOUtils.contentEqualsIgnoreEOL(savedReader, destReader);
    } catch (IOException iOException) {
      Logger.getLogger(CrueFileHelper.class.getName()).log(Level.SEVERE, "error while comparing files {0}", iOException);
    } finally {
      CtuluLibFile.close(savedReader);
      CtuluLibFile.close(destReader);
    }
    return res;
  }

  /**
   * @param absoluteOrRelativeFile le path a tester
   * @return true si le path est un path vide
   */
  public static boolean isEmpty(String absoluteOrRelativeFile) {
    return StringUtils.isBlank(absoluteOrRelativeFile) || EMPTY_PATH.contains(absoluteOrRelativeFile);
  }

  public static Map<String, URL> createFrom(final Map<String, File> files) {
    if (MapUtils.isEmpty(files)) {
      return Collections.emptyMap();
    }
    final Map<String, URL> res = new HashMap<>();
    try {
      for (final Map.Entry<String, File> it : files.entrySet()) {
        res.put(it.getKey(), it.getValue().toURI().toURL());
      }
    } catch (final MalformedURLException e) {
      LOGGER.log(Level.SEVERE, "createFrom", e);
    }
    return res;
  }

  public static boolean isRelative(String absoluteOrRelativeFile) {
    if (isEmpty(absoluteOrRelativeFile)) {
      return true;
    }
    return !new File(absoluteOrRelativeFile).isAbsolute();
  }

  public static File getCanonicalBaseDir(File baseDir, String absoluteOrRelativeFile) {
    if (isEmpty(absoluteOrRelativeFile)) {
      return getCanonicalFile(baseDir);
    }
    File dir = new File(absoluteOrRelativeFile);
    if (dir.isAbsolute()) {
      return getCanonicalFile(dir);
    }
    return getCanonicalFile(new File(baseDir, absoluteOrRelativeFile));
  }

  public static File getCanonicalFile(File initFile) {
    if (initFile == null) {
      return null;
    }

    File resFile = initFile.getAbsoluteFile();
    if (FuLib.isWindows() && resFile.getAbsolutePath().indexOf('/') != -1) {
      String newPath = StringUtils.replaceChars(resFile.getAbsolutePath(), '/', '\\');
      resFile = new File(newPath);
    } else if ((FuLib.isLinux() || FuLib.isUnix()) && resFile.getAbsolutePath().indexOf('\\') != -1) {
      String newPath = StringUtils.replaceChars(resFile.getAbsolutePath(), '\\', '/');
      resFile = new File(newPath);
    }
    try {
      resFile = resFile.getCanonicalFile();
    } catch (IOException ex) {
      Logger.getLogger(CrueFileHelper.class.getName()).log(Level.WARNING, null, ex);
    }
    return resFile;
  }

  public static List<String[]> readFile(final String file, final CtuluLog analyze) {
    InputStream in = null;
    List<String[]> lines = null;

    try {
      in = CrueFileHelper.class.getResourceAsStream(file);
      lines = readFile(new InputStreamReader(in), analyze);
    } catch (final Exception e) {
      analyze.manageException(e);
    } finally {
      CtuluLibFile.close(in);
    }
    return lines;
  }

  public static List<String[]> readFile(final Reader in, final CtuluLog ctuluLog) throws IOException {
    final List<String[]> lines = new ArrayList<>(300);
    if (in == null) {
      return lines;
    }
    final LineNumberReader reader = new LineNumberReader(in);
    String line = reader.readLine();
    while (line != null) {
      if (line.length() > 0 && line.charAt(0) != '#') {
        String[] allTokens = StringUtils.splitPreserveAllTokens(line, ';');
        if (allTokens != null) {
          for (int i = 0; i < allTokens.length; i++) {
            allTokens[i] = clearCsvString(allTokens[i]);
          }
        }
        lines.add(allTokens);
      }

      line = reader.readLine();
    }
    return lines;
  }

  /**
   * @param in la chaine a traiter
   * @return enlever les quotes au debut et fin si necessaire.
   */
  public static String clearCsvString(String in) {
    if (in == null || in.length() < 2) {
      return in;
    }
    char zero = in.charAt(0);
    char last = in.charAt(in.length() - 1);
    if ((zero == '"' && last == '"') || (zero == '\'' && last == '\'')) {
      return in.substring(1, in.length() - 1);
    }
    return in;
  }

  public static boolean copyFileWithSameLastModifiedDate(File src, File dest) {
    if (CtuluLibFile.copyFile(src, dest)) {
      setLastModified(dest, src, null);
      return true;
    }
    return false;
  }

  public static boolean copyDirWithSameLastDate(File initDir, File destDir) {
    if (initDir.isFile()) {
      return copyDirWithSameLastDate(initDir, destDir);
    }
    final boolean mkdirs = destDir.mkdirs();
    if (!mkdirs) {
      LOGGER.log(Level.WARNING, "can't create dir " + destDir.getAbsolutePath());
    }

    File[] listFiles = initDir.listFiles();
    boolean res = true;
    for (File file : listFiles) {
      File destFile = new File(destDir, file.getName());
      if (file.isFile()) {
        res &= copyFileWithSameLastModifiedDate(file, destFile);
      } else {
        copyDirWithSameLastDate(file, destFile);
      }
    }
    setLastModified(destDir, initDir, null);
    return res;
  }

  public static File changeExtension(File file, String initExt, String finalExt) {
    if (file == null) {
      return null;
    }
    File dir = file.getParentFile();
    String name = file.getName().replace(initExt, finalExt);
    if (dir == null) {
      return new File(name);
    }
    return new File(dir, name);
  }

  /**
   *
   * @param initFileURL non null, la source a copier
   * @param dregFileProjectFile non null, le fichier de destination
   * @return true si l'opération s'est déroulée correctement.
   */
  public static boolean copyFile(URL initFileURL, File dregFileProjectFile) {
    try {
      FileUtils.copyInputStreamToFile(initFileURL.openStream(), dregFileProjectFile);
      return true;
    } catch (IOException e) {
      LOGGER.log(Level.WARNING, "can't copy file from " + initFileURL.toString() + " to " + dregFileProjectFile.getAbsolutePath(), e);
    }
    return false;
  }
}
