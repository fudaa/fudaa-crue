/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.io;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.ConversionException;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import com.thoughtworks.xstream.mapper.CannotResolveClassException;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.xml.ErrorHandlerDefault;
import org.fudaa.ctulu.xml.UnicodeInputStream;
import org.fudaa.ctulu.xml.XmlVersionFinder;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.xml.sax.InputSource;

import javax.xml.XMLConstants;
import javax.xml.transform.sax.SAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * File format customisé pour Crue. Indique si le fichier est de type crue 9 ou 10. Contient un validator xsd pour le fichier donné. Contient une
 * méthode statique très pratique qui permet de retourner automatiquement le bon FileFormat en fonction du type renvoyé. T correspond à la structure
 * métier associée au format du fichier
 *
 * @param <D> Represente la structure DAO
 * @param <M> Represente le modele Metier
 * @author Adrien Hadoux
 */
public class CrueXmlReaderWriterImpl<D extends AbstractCrueDao, M> {
    private static final  Logger LOGGER = Logger.getLogger(CrueXmlReaderWriterImpl.class.getName());
    protected final CrueConverter<D, M> converter;
    private final CrueDaoStructure daoConfigurer;
    /**
     * La version du fichier
     */
    private final String version;
    /**
     * le nom du fichier xsd a utiliser
     */
    private final String xsdId;
    /**
     * La path complet du fichier xsd
     */
    private final String xsdPath;
    private final String xsdFile;

    /**
     * Utilise par des lecteurs/ecrivain qui ne sont pas des fichiers xml de Crue10.
     *
     * @param fileType      type de fichier
     * @param version       la vesion
     * @param converter     le converter
     * @param daoConfigurer le configurer de structure
     */
    public CrueXmlReaderWriterImpl(final String fileType, final String version, final CrueConverter<D, M> converter,
                                   final CrueDaoStructure daoConfigurer) {
        this.daoConfigurer = daoConfigurer;
        this.converter = converter;
        this.version = version;
        this.xsdId = fileType;
        this.xsdFile = xsdId.toLowerCase() + "-" + version + ".xsd";
        xsdPath = "/xsd/" + xsdFile;
    }

    public static boolean isValide(final URL xml, final String xsd, final CtuluLog res) {
        res.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
        res.setDesc("valid.xml");
        if (xml == null) {
            res.addSevereError("io.fileNotFound");
            return false;
        }
        res.setDescriptionArgs(xml.toString());
        final ErrorHandlerDefault handler = new ErrorHandlerDefault(res);
        InputStream xmlStream = null;
        UnicodeInputStream unicodeStream=null;
        try {
            final SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            URL xsdURL;
            File xsdFile = new File(xsd);
            if (xsdFile.exists()) {
                xsdURL = xsdFile.toURI().toURL();
            } else {
                xsdURL = CrueXmlReaderWriterImpl.class.getResource(xsd);
            }
            if (xsdURL == null) {
                res.addSevereError("xsdNotFound.error", xsd);
                return false;
            }
            final Schema schema = schemaFactory.newSchema(xsdURL);
            final Validator validator = schema.newValidator();
            validator.setErrorHandler(handler);
            xmlStream = xml.openStream();
            unicodeStream = new UnicodeInputStream(xmlStream, XmlVersionFinder.ENCODING);
            unicodeStream.init();
            validator.validate(new SAXSource(new InputSource(unicodeStream)));
        } catch (final Exception e) {
            res.manageException(e);
            return false;
        } finally {
            CtuluLibFile.close(xmlStream);
            CtuluLibFile.close(unicodeStream);
        }
        return !handler.isHasError();
    }

    public static void initXstreamSecurity(XStream xstream) {
      XStream.setupDefaultSecurity(xstream);
      xstream.allowTypesByWildcard(new String[] {
        "org.fudaa.**"
      });
    }

    protected final void configureXStream(final XStream xstream, final CtuluLog analyse) {
        daoConfigurer.configureXStream(xstream, analyse);
    }

    /**
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    private XStream initXmlParser(final CtuluLog analyse) {
        final XmlFriendlyNameCoder replacer = createReplacer();
        final StaxDriver staxDriver = new StaxDriver(replacer);
        final XStream xstream = new XStream(staxDriver);
        CrueXmlReaderWriterImpl.initXstreamSecurity(xstream);
        xstream.setMode(XStream.NO_REFERENCES);
        // -- creation des alias pour que ce soit + parlant dans le xml file --//

        // -- alias pour les entete xsd --//

        xstream.aliasAttribute("xmlns:xsi", "xmlnsxsi");
        xstream.aliasAttribute("xsi:schemaLocation", "xsischemaLocation");

        xstream.useAttributeFor(AbstractCrueDao.class, "xmlns");
        xstream.useAttributeFor(AbstractCrueDao.class, "xmlnsxsi");
        xstream.useAttributeFor(AbstractCrueDao.class, "xsischemaLocation");

        configureXStream(xstream, analyse);
        return xstream;
    }

    private XmlFriendlyNameCoder createReplacer() {
        return new XmlFriendlyNameCoder("#", "_");
    }

    public boolean isValide(final File xml, final CtuluLog res) {
        try {
            final boolean valide = isValide(xml.toURI().toURL(), res);
            res.setDesc("valid.xml");
            res.setDescriptionArgs(xml.getName());
            return valide;
        } catch (final MalformedURLException e) {
            res.manageException(e);
            LOGGER.log(Level.SEVERE, "isValide", e);
            return false;
        }
    }

    public boolean isValide(final String xml, final CtuluLog res) {
        return isValide(getClass().getResource(xml), res);
    }

    public boolean isValide(final URL xml, final CtuluLog res) {
        return isValide(xml, xsdPath, res);
    }

    /**
     * Lit les données dans le fichier f avec les données liées.
     */
    public final CrueIOResu<M> read(final URL f, final CtuluLog analyzer) {
        analyzer.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
        analyzer.setDesc("read.file");
        analyzer.setDescriptionArgs(f.getFile());
        final D d = readDao(f, analyzer);
        if (d != null) {
            return createResu(d, converter.convertDaoToMetier(d, analyzer), analyzer);
        }
        return null;
    }

    private CrueIOResu<M> createResu(final D d, final M m, final CtuluLog analyze) {
        final CrueIOResu<M> res = new CrueIOResu<>();
        res.setMetier(m);
        if (d != null) {
            res.setCrueCommentaire(d.getCommentaire());
        }
        res.setAnalyse(analyze);
        return res;
    }

    /**
     * @param fichier  le fichier a lire
     * @param ctuluLog les logs
     * @return le dao lu
     */
    public D readDao(final File fichier, final CtuluLog ctuluLog) {
        FileInputStream in = null;
        D newData = null;
        try {
            in = new FileInputStream(fichier);
            newData = readDao(in, ctuluLog);
        } catch (final FileNotFoundException e) {
            LOGGER.log(Level.FINE, "readDao", e);
            final String path = fichier == null ? "null" : fichier.getAbsolutePath();
            ctuluLog.addSevereError("io.FileNotFoundException.error", path);
        } finally {
            CtuluLibFile.close(in);
        }
        return newData;
    }

    /**
     * @param inputStream le stream a lire
     * @return le dao
     */
    @SuppressWarnings("unchecked")
    protected D readDao(final InputStream inputStream, final CtuluLog analyser) {
        analyser.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
        D newData = null;
        BufferedReader contentRead = null;
        try {
            final XStream parser = initXmlParser(analyser);

            // The unicodeInputStream is used to eliminate the BOM bug from java:
            UnicodeInputStream unicodeStream = new UnicodeInputStream(inputStream, XmlVersionFinder.ENCODING);
            unicodeStream.init();
            contentRead = new BufferedReader(new InputStreamReader(unicodeStream, XmlVersionFinder.ENCODING));

            newData = (D) parser.fromXML(contentRead);// we not that is a D object.
        } catch (ConversionException conversionException) {
            LOGGER.log(Level.SEVERE, "io.unknown.balise", conversionException);
            analyser.addSevereError("io.unknown.balise",
                    StringUtils.substringAfterLast(conversionException.getShortMessage(), "."));
        } catch (CannotResolveClassException cannotResolveException) {
            LOGGER.log(Level.FINE, "io.unknown.balise", cannotResolveException);
            analyser.addSevereError("io.unknown.balise", StringUtils.substringAfterLast(cannotResolveException.getMessage(), "."));
        } catch (final Exception e) {
            LOGGER.log(Level.SEVERE, "io.xml.error", e);
            analyser.addSevereError("io.xml.error", e.getMessage());
        } finally {
            CtuluLibFile.close(contentRead);
        }
        return newData;
    }

    /**
     * @param pathToResource l'adresse du fichier a charger commencant par /
     * @param ctuluLog       les logs de l'operation
     * @return le DAO lu
     */
    protected D readDao(final String pathToResource, final CtuluLog ctuluLog) {
        return readDao(getClass().getResource(pathToResource), ctuluLog);
    }

    public D readDao(final URL url, final CtuluLog analyser) {
        if (url == null) {
            analyser.addSevereError("file.url.null.error");
            return null;
        }

        InputStream in = null;
        D newData = null;
        try {
            in = url.openStream();
            newData = readDao(in, analyser);
        } catch (final IOException e) {
            LOGGER.log(Level.FINE, e.getMessage(), e);
            analyser.addSevereError("io.xml.error", e.getMessage());
        } finally {
            CtuluLibFile.close(in);
        }
        return newData;
    }

    /**
     * Lit les données dans le fichier f avec les données liées.
     *
     * @param file     fichier a lire
     * @param ctuluLog les logs.
     * @return le resultat de l'opération
     */
    public final CrueIOResu<M> readXML(final File file, final CtuluLog ctuluLog) {
        ctuluLog.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
        ctuluLog.setDesc("read.file");
        ctuluLog.setDescriptionArgs(file.getName());
        final D d = readDao(file, ctuluLog);
        if (d != null) {
            return createResu(d, converter.convertDaoToMetier(d, ctuluLog), ctuluLog);
        }
        return createResu(null, null, ctuluLog);
    }

    /**
     * Lit les données dans le fichier f avec les données liées.
     */
    public final CrueIOResu<M> readXML(final String pathToResource, final CtuluLog analyzer) {
        analyzer.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
        analyzer.setDesc("read.file");
        analyzer.setDescriptionArgs(pathToResource);
        final D d = readDao(pathToResource, analyzer);
        if (d != null) {
            return createResu(d, converter.convertDaoToMetier(d, analyzer), analyzer);
        }
        final CrueIOResu<M> res = new CrueIOResu<>();
        res.setAnalyse(analyzer);
        return res;
    }

    private boolean writeDAO(final File file, final D dao, final CtuluLog analyser) {
        FileOutputStream out = null;
        boolean ok;
        try {
            out = new FileOutputStream(file);
            out.write(UnicodeInputStream.getBOM(XmlVersionFinder.ENCODING));
            out.flush();
            ok = writeDAO(out, dao, analyser);
        } catch (final IOException e) {
            LOGGER.log(Level.SEVERE, "writeDAO " + file.getName(), e);
            ok = false;
        } finally {
            CtuluLibFile.close(out);
        }
        return ok;
    }

    /**
     * @param out      le flux de sortie
     * @param dao      le dao a persister
     * @param analyser le receveur d'information
     */
    @SuppressWarnings("deprecation")
    private boolean writeDAO(final OutputStream out, final D dao, final CtuluLog analyser) {
        boolean isOk = true;
        try {
            final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, XmlVersionFinder.ENCODING), 8192 * 4);
            writer.write(XmlVersionFinder.ENTETE_XML + CtuluLibString.LINE_SEP);
            if (dao.getCommentaire() == null) {
                dao.setCommentaire(StringUtils.EMPTY);
            }
            final XStream parser = initXmlParser(analyser);
            parser.marshal(dao, new CustomPrettyPrintWriter(writer, new char[]{' ', ' '},
                    createReplacer()));
        } catch (final IOException e) {
            LOGGER.log(Level.SEVERE, "writeDAO", e);
            analyser.addSevereError("file.write.error");
            isOk = false;
        } finally {
            CtuluLibFile.close(out);
        }
        return isOk;
    }

    /**
     * Methode qui permet d'ecrire les datas dans le fichier f specifie.
     *
     * @param metier   le metier a écrire
     * @param file     le fichier a ecrire
     * @param ctuluLog les logs
     * @return true si écriture ok
     */
    public final boolean writeXMLMetier(final CrueIOResu<M> metier, final File file, final CtuluLog ctuluLog) {
        file.getParentFile().mkdirs();
        ctuluLog.setDesc("write.file");
        ctuluLog.setDescriptionArgs(file.getName());
        ctuluLog.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
        final D d = converter.convertMetierToDao(metier.getMetier(), ctuluLog);
        if (d != null) {
            if (StringUtils.isEmpty(d.getCommentaire())) {
                d.setCommentaire(metier.getCrueCommentaire());
            }
            d.setXsdName(xsdFile);
            return writeDAO(file, d, ctuluLog);
        }
        return false;
    }

    /**
     * @param metier   l'objet metier
     * @param out      le flux de sortie qui ne sera pas ferme
     * @param ctuluLog les logs
     * @return true si reussite
     */
    public boolean writeXMLMetier(final CrueIOResu<M> metier, final OutputStream out, final CtuluLog ctuluLog) {
        ctuluLog.setDesc("write.file");
        ctuluLog.setDesc(xsdId);
        ctuluLog.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
        final D d = converter.convertMetierToDao(metier.getMetier(), ctuluLog);
        if (d != null) {
            d.setCommentaire(metier.getCrueCommentaire());
            return writeDAO(out, d, ctuluLog);
        }
        return false;
    }
}
