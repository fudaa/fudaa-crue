/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.io;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluLog;

/**
 * Interface pour les structures des DAO
 *
 * @author deniger
 */
public interface CrueDaoStructure {

  /**
   * @param xstream le flux a configurer
   * @param analyze l'anayze recevant les erreurs
   * @param props les propriétés utilisées pour récuperer les types de loi.
   */
  void configureXStream(XStream xstream, CtuluLog analyze);
}
