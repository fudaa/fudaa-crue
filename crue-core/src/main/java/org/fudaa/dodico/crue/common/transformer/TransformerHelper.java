/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.transformer;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Transformer;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;

import java.util.*;

/**
 * @author Frederic Deniger
 */
public class TransformerHelper {
    public static <T extends ObjetWithID> Map<String, T> toMapOfNom(Collection<T> in) {
        HashMap<String, T> res = new HashMap<>();
        if (in == null) {
            return res;
        }
        for (T t : in) {
            res.put(t.getNom(), t);
        }
        return res;
    }

    public static <T extends ObjetWithID> Map<String, T> toMapOfId(Collection<T> in) {
        HashMap<String, T> res = new HashMap<>();
        if (in == null) {
            return res;
        }
        for (T t : in) {
            res.put(t.getId(), t);
        }
        return res;
    }

    public static final Collection<String> toId(Collection<? extends ObjetWithID> in) {
        if (in == null) {
            return null;
        }
        List<String> res = new ArrayList<>(in.size());
        CollectionUtils.collect(in, new TransformerObjetWithIdToId(), res);
        return res;
    }

    public static final List<String> toNom(Collection<? extends ObjetNomme> in) {
        if (in == null) {
            return null;
        }
        List<String> res = new ArrayList<>(in.size());
        CollectionUtils.collect(in, new TransformerObjetNommeToNom(), res);
        return res;
    }

    public static final String[] toArrayOfNom(Collection<? extends ObjetNomme> in) {
        List<String> res = toNom(in);
        if (res == null) {
            return null;
        }
        return res.toArray(new String[0]);
    }

    public static final Set<String> toSetOfId(Collection<? extends ObjetWithID> in) {
        if (in == null) {
            return null;
        }
        Set<String> res = new HashSet<>(in.size());
        CollectionUtils.collect(in, new TransformerObjetWithIdToId(), res);
        return res;
    }

    public static final Set<String> toSetNom(Collection<? extends ObjetNomme> in) {
        if (in == null) {
            return null;
        }
        Set<String> res = new HashSet<>(in.size());
        CollectionUtils.collect(in, new TransformerObjetNommeToNom(), res);
        return res;
    }

    public static final class TransformerObjetNommeToNom implements Transformer {
        @Override
        public Object transform(final Object input) {
            if (input == null) {
                return null;
            }
            return ((ObjetWithID) input).getNom();
        }
    }

    /**
     * @author deniger
     */
    public static final class TransformerObjetWithIdToId implements Transformer {
        @Override
        public Object transform(final Object input) {
            return ((ObjetWithID) input).getId();
        }
    }
}
