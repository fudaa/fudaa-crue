/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.transformer;

/**
 * Utiliser pour transformer des objets (EMH, Infos,....) en String.
 *
 * @author deniger
 */
public interface ToStringTransformer {

  /**
   * @param in si null renvoie chaine vide.
   * @return la vision sous forme de String de l'objet en question
   */
  String transform(Object in);
}
