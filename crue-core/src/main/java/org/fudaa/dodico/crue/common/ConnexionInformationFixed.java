/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common;

import org.joda.time.LocalDateTime;

/**
 * Des informations de connexion fixes dans le temps
 *
 * @author Frederic Deniger
 */
public class ConnexionInformationFixed implements ConnexionInformation {

  private final LocalDateTime currentDate;
  private final String user;

    public ConnexionInformationFixed(final LocalDateTime currentDate, final String user) {
        this.currentDate = currentDate;
        this.user = user;
    }

    /**
   *
   * @param from ne doit pas etre null. Permet d'initialiser les données
   */
  public ConnexionInformationFixed(final ConnexionInformation from) {
    currentDate = from.getCurrentDate();
    user = from.getCurrentUser();
  }

  @Override
  public LocalDateTime getCurrentDate() {
    return currentDate;
  }

  @Override
  public String getCurrentUser() {
    return user;
  }
}
