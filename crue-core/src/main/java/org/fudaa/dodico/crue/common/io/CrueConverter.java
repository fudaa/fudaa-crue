/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.io;

import org.fudaa.ctulu.CtuluLog;

/**
 * @author deniger Interface pour les converter entre les objet dao et les objet metier
 * @param <D> Represente la structure DAO
 * @param <M> Represente le modele Metier
 */
public interface CrueConverter<D extends AbstractCrueDao, M> {

  /**
   * Remplit les infos de la classe persistantes avec les données métier appropriées. Cette méthode est appelée dans le constructeur.
   *
   * @param dao la dao a convertir
   * @param ctuluLog les logs
   * @return l'objet metier
   */
  M convertDaoToMetier(D dao, CtuluLog ctuluLog);

  /**
   * Crée une structure métier à partir des informations persistantes. Utiliser pour le remplissage de la structure métier apres lecture du fichier
   * xml. Utilise en plus des données liées.
   *
   * @param metier le metier a convertir
   * @param ctuluLog les logs
   * @return l'objet DAO
   */
  D convertMetierToDao(M metier, CtuluLog ctuluLog);
}
