/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;

/**
 * Simple result of an operation,
 *
 * @param <R>
 * @author deniger
 */
public class CrueOperationResult<R> {
    private final CtuluLogGroup logs;
    private final R result;

    /**
     * @param result le resultat
     * @param log    les logs
     */
    public CrueOperationResult(R result, CtuluLogGroup log) {
        super();
        this.result = result;
        this.logs = log;
    }

    /**
     * @param result le resultat
     * @param log    les logs
     */
    public CrueOperationResult(R result, CtuluLog log) {
        this(result, log == null ? new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE) : new CtuluLogGroup(log.getDefaultResourceBundle()));
        if (log != null) {
            this.logs.setDescription(log.getDesc());
            this.logs.setDescriptionArgs(log.getDescriptionArgs());
            this.logs.addLog(log);
        }
    }

    /**
     *
     * @return true si le resultat est null ou le log contient des erreur
     */
    public boolean isErrorOrEmpty(){
        if(getResult()==null){
            return true;
        }
        return logs!=null && (logs.containsError() || logs.containsFatalError());
    }

    /**
     * @return the log
     */
    public CtuluLogGroup getLogs() {
        return logs;
    }

    /**
     * @return the result
     */
    public R getResult() {
        return result;
    }
}
