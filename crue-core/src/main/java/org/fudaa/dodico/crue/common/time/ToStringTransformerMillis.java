/*
 GPL 2
 */
package org.fudaa.dodico.crue.common.time;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

/**
 *
 * @author Frederic Deniger
 */
public class ToStringTransformerMillis implements ToStringTransformer, SecondParser {

  final PeriodFormatter dureeFormater = new PeriodFormatterBuilder()
          .appendDays().appendSeparatorIfFieldsBefore(":")
          .printZeroAlways().minimumPrintedDigits(2).
          appendHours().printZeroAlways().appendSuffix(":")
          .appendMinutes().minimumPrintedDigits(2).appendSuffix(":")
          .appendSecondsWithOptionalMillis().minimumPrintedDigits(2).toFormatter();

  @Override
  public String transform(Object in) {
    if (in == null) {
      return StringUtils.EMPTY;
    }
    Long value = (Long) in;
    return dureeFormater.print(createPeriodFromMillis(value.longValue()));
  }

  @Override
  public Double parseInSec(String in) {
    try {
      if (in.indexOf('.') < 0) {
        in = in + ".000";
      }
      int countMatches = StringUtils.countMatches(in, ":");
      if (countMatches == 2) {
        in = "00:" + in;
      }
      final Period parsePeriod = dureeFormater.parsePeriod(in);
      return parsePeriod == null ? null : Double.valueOf(parsePeriod.toStandardDuration().getMillis() / 1000d);
    } catch (Exception ex) {
      Logger.getLogger(ToStringTransformerMillis.class.getName()).log(Level.FINE, "message {0}", ex);
    }
    return null;
  }

  public String format(long millis) {
    return dureeFormater.print(createPeriodFromMillis(millis));
  }

  public static Period createPeriodFromMillis(long millis) {
    return new Period(millis).normalizedStandard(PeriodType.dayTime());
  }
}
