/*
 GPL 2
 */
package org.fudaa.dodico.crue.common.time;

/**
 *
 * @author Frederic Deniger
 */
public interface SecondParser {

  Double parseInSec(String in);
}
