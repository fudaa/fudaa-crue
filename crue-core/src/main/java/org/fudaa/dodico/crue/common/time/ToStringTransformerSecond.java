/*
 GPL 2
 */
package org.fudaa.dodico.crue.common.time;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;

/**
 *
 * @author Frederic Deniger
 */
public class ToStringTransformerSecond implements ToStringTransformer, CtuluNumberFormatI, SecondParser {

  final ToStringTransformerMillis millis = new ToStringTransformerMillis();

  @Override
  public String transform(Object in) {
    if (in == null) {
      return StringUtils.EMPTY;
    }
    Number value = (Number) in;
    return millis.format((long) (value.doubleValue() * 1000L));
  }

  @Override
  public CtuluNumberFormatI getCopy() {
    return this;
  }

  @Override
  public String toLocalizedPattern() {
    return null;
  }

  @Override
  public boolean isDecimal() {
    return true;
  }

  @Override
  public String format(double sec) {
    return millis.format((long) (sec * 1000L));
  }

  @Override
  public Double parseInSec(String in) {
    return millis.parseInSec(in);
  }
}
