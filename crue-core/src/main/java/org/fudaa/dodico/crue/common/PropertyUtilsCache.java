/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.common;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.LazyDynaMap;
import org.apache.commons.beanutils.PropertyUtils;

/**
 *
 * @author Fred Deniger
 */
public class PropertyUtilsCache {

  static {
    CustomPropertyUtilsBeanInstaller.install();
  }

  private boolean isComplexProperty(String property) {
    return property.indexOf('.') > 0 || property.indexOf('[') > 0 || property.indexOf('(') > 0;
  }

  private static class PropertyDescriptorByName {

    final Map<String, PropertyDescriptor> propertyByName = new HashMap<>();

    void init(Class c) throws IntrospectionException {
      PropertyDescriptor[] propertyDescriptors = BeanUtilsBean.getInstance().getPropertyUtils().getPropertyDescriptors(c);
      for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
        propertyByName.put(propertyDescriptor.getName(), propertyDescriptor);
      }
      if (Collection.class.isAssignableFrom(c)) {
        propertyByName.put("size", new PropertyDescriptor("size", c, "size", null));
      }
    }

    PropertyDescriptor getDescriptor(String propertyName) {
      return propertyByName.get(propertyName);
    }
  }
  boolean active;
  final Map<Class, PropertyDescriptorByName> descriptors = new HashMap<>();

  public boolean isActive() {
    return active;
  }
  private final static PropertyUtilsCache CACHE = new PropertyUtilsCache();

  public static PropertyUtilsCache getInstance() {
    return CACHE;
  }

  public void setActive(boolean active) {
    this.active = active;
    if (!active) {
      descriptors.clear();
    }
  }
  private static final Object[] EMPTY_OBJECT_ARRAY = new Object[0];

  public Object getProperty(Object bean, String property) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntrospectionException {
    if (!active || isComplexProperty(property) || LazyDynaMap.class.equals(bean.getClass())) {
      return PropertyUtils.getProperty(bean, property);
    }
    final Class<?> classToDescribe = bean.getClass();
    PropertyDescriptorByName descriptorByName = descriptors.get(classToDescribe);
    if (descriptorByName == null) {
      descriptorByName = new PropertyDescriptorByName();
      descriptorByName.init(classToDescribe);
      descriptors.put(classToDescribe, descriptorByName);
    }
    PropertyDescriptor descriptor = descriptorByName.getDescriptor(property);
    if (descriptor == null) {
      throw new IllegalAccessException("property " + property + " not found in " + classToDescribe);
    }
    return descriptor.getReadMethod().invoke(bean, EMPTY_OBJECT_ARRAY);
  }
}
