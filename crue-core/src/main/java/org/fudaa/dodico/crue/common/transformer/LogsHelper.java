package org.fudaa.dodico.crue.common.transformer;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;

import java.util.List;

/**
 * @author deniger
 */
public class LogsHelper {
  private LogsHelper() {
  }

  /**
   * @param log le log
   * @param firstLine la preemier ligne du texte
   * @return le log au format htnl
   */
  public static String toHtml(CtuluLog log, String firstLine) {
    StringBuilder res = new StringBuilder();
    res.append("<html><body>");
    if (firstLine != null) {
      res.append(firstLine).append("<br>");
    }
    appendToHtml(log, res, false);
    return res.append("</body><html>").toString();
  }

  public static String toText(CtuluLog log) {
    StringBuilder res = new StringBuilder();
    appendToText(log, res, false);
    return res.toString();
  }

  private static void appendToHtml(CtuluLog log, StringBuilder res, boolean onlyError) {
    List<CtuluLogRecord> records = log.getRecords();
    if (onlyError && !log.containsErrorOrSevereError()) {
      return;
    }
    res.append("<ul>");
    for (CtuluLogRecord ctuluLogRecord : records) {
      if (!onlyError || ctuluLogRecord.getLevel().isLessVerboseThan(CtuluLogLevel.ERROR, false)) {
        BusinessMessages.updateLocalizedMessage(ctuluLogRecord);
        res.append("<li>");
        res.append(ctuluLogRecord.getLocalizedMessage());
        res.append("</li>");
      }
    }
    res.append("</ul>");
  }

  private static void appendToText(CtuluLog log, StringBuilder res, boolean onlyError) {
    List<CtuluLogRecord> records = log.getRecords();
    if (onlyError && !log.containsErrorOrSevereError()) {
      return;
    }
    for (CtuluLogRecord ctuluLogRecord : records) {
      if (!onlyError || ctuluLogRecord.getLevel().isLessVerboseThan(CtuluLogLevel.ERROR, false)) {
        BusinessMessages.updateLocalizedMessage(ctuluLogRecord);
        res.append("\t");
        res.append(ctuluLogRecord.getLocalizedMessage());
      }
    }
  }

  /**
   * @param log les logs
   * @param firstLine la premiere ligne du texte
   * @return le log au format htnl
   */
  public static String toHtml(CtuluLogGroup log, String firstLine) {
    return toHtml(log, firstLine, false);
  }

  /**
   * @param log les logs
   * @return le log au format htnl
   */
  public static String toText(CtuluLogGroup log) {
    return toText(log, false);
  }

  /**
   * @param log les logs
   * @param firstLine la preemier ligne du texte
   * @return le log au format htnl
   */
  public static String toHtml(CtuluLogGroup log, String firstLine, boolean onlyError) {
    StringBuilder res = new StringBuilder();
    res.append("<html><body>");
    if (firstLine != null) {
      res.append(firstLine).append("<br>");
    }
    appendToHtml(log, res, onlyError);
    return res.append("</body><html>").toString();
  }

  public static String toText(CtuluLogGroup log, boolean onlyError) {
    StringBuilder res = new StringBuilder();
    appendToText(log, res, onlyError);
    return res.toString();
  }

  private static void appendToHtml(CtuluLogGroup logGroup, StringBuilder res, boolean onlyError) {
    if (onlyError && !logGroup.containsError()) {
      return;
    }
    String desc = logGroup.getDesci18n();
    if (StringUtils.isNotBlank(desc)) {
      res.append(res).append("<br>");
    }
    for (CtuluLog log : logGroup.getLogs()) {
      if (onlyError && !log.containsErrorOrSevereError()) {
        continue;
      }
      desc = log.getDesci18n();
      if (StringUtils.isNotBlank(desc)) {
        res.append(log.getDesci18n()).append("<br>");
      }
      appendToHtml(log, res, onlyError);
    }
    final List<CtuluLogGroup> groups = logGroup.getGroups();
    for (CtuluLogGroup group : groups) {
      res.append("<ul><li>");
      appendToHtml(group, res, onlyError);
      res.append("</li></ul>");
    }
  }

  private static void appendToText(CtuluLogGroup logGroup, StringBuilder res, boolean onlyError) {
    if (onlyError && !logGroup.containsError()) {
      return;
    }
    res.append("************************\n");
    String desc = logGroup.getDesci18n();
    if (StringUtils.isNotBlank(desc)) {
      res.append(res);
    }
    for (CtuluLog log : logGroup.getLogs()) {
      if (onlyError && !log.containsErrorOrSevereError()) {
        continue;
      }
      desc = log.getDesci18n();
      if (StringUtils.isNotBlank(desc)) {
        res.append(log.getDesci18n()).append('\n');
      }
      appendToText(log, res, onlyError);
    }
    final List<CtuluLogGroup> groups = logGroup.getGroups();
    for (CtuluLogGroup group : groups) {
      res.append("\n\n");
      appendToText(group, res, onlyError);
    }
  }

  public static String toHtml(CtuluLog log) {
    return toHtml(log, null);
  }
}
