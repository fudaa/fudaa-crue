/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.io;

import java.io.File;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.fortran.FileCharSimpleWriterAbstract;

/**
 * Classe permettant de setter correctement le ResourceBundle.
 *
 * @author deniger
 */
public abstract class CrueFileOpWriterCharSimpleAbstract<T> extends FileCharSimpleWriterAbstract<T> {

  /**
   * appelle la methode parent et set le RESOURCE_BUNDLE correctement
   */
  @Override
  public void setFile(final File _f) {
    super.setFile(_f);
    analyze_.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
  }
}
