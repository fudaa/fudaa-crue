/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConvertUtilsBean;

/**
 * @author deniger
 */
public class CustomPropertyUtilsBeanInstaller {

  private static final CustomPropertyUtilsBean CUSTOM_UTIL = new CustomPropertyUtilsBean();

  /**
   * Install un PropertyUtilsBean perso permettant de récuperer la taille d'une collection
   */
  public static void install() {
    if (BeanUtilsBean.getInstance().getPropertyUtils() != CUSTOM_UTIL) {
      BeanUtilsBean.setInstance(new BeanUtilsBean(new ConvertUtilsBean(), new CustomPropertyUtilsBean()));
    }

  }
}
