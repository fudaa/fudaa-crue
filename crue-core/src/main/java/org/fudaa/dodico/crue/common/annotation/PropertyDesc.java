/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface PropertyDesc {

  /**
   * la clé à utiliser pour traduire la propriete : doit se trouver dans BusinessMessages.properties
   */
  String i18n();

  /**
   * si non vide, sera utilise pour traduire la propriété
   */
  String i18nBundleBaseName() default "";

  /**
   * @return l'ordre. Si 2 égaux, sera trié par ordre alphabétique (i18n)
   */
  int order() default 2000;

  boolean readOnly() default false;
}
