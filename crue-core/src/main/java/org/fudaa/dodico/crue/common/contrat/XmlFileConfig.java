/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.contrat;

/**
 *
 * @author Frederic Deniger
 */
public interface XmlFileConfig {

  String getXsdUrl(String xsdFile);

  /**
   * La version de la grammaire.
   *
   * @return the xsdVersion
   */
  String getXsdVersion();
}
