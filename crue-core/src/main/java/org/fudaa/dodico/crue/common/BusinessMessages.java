/*
 * Licence GPL 2
 */
package org.fudaa.dodico.crue.common;

import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.ctulu.MessageFormatHelper;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author deniger
 */
public final class BusinessMessages {
    public static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("org.fudaa.dodico.crue.common.businessMessages");
    public static final String ENTITY_SEPARATOR = " - ";

    private BusinessMessages() {
    }

    public static String getString(String s) {
        return RESOURCE_BUNDLE.getString(s);
    }

    /**
     * recherche des traduction avec le suffixe shortName
     *
     * @param aClass la classe a traduire
     * @return la traduction
     */
    public static String geti18nForClass(Class aClass) {
        return getStringOrDefault(aClass.getSimpleName() + ".shortName", aClass.getSimpleName());
    }

    public static String getString(String s, Object... args) {
        return MessageFormatHelper.getS(RESOURCE_BUNDLE, s, args);
    }

    public static String getStringOrDefault(String s, String defaultString) {
        try {
            if (RESOURCE_BUNDLE.containsKey(s)) {
                return RESOURCE_BUNDLE.getString(s);
            }
        } catch (MissingResourceException e) {
        }
        return defaultString;
    }

    public static void updateLocalizedMessage(CtuluLogRecord record) {
        CtuluLogRecord.updateLocalizedMessage(record, RESOURCE_BUNDLE);
    }
}
