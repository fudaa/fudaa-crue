/*
 GPL 2
 */
package org.fudaa.dodico.crue.common.time;

/**
 *
 * @author Frederic Deniger
 */
public class TimeFormatOption implements Cloneable {

  

  public enum DureeFormatType {

    AS_SECOND,//les millisecondes seront transformées en sec et formatés selon le ccm
    FORMATTED//une chaine de formatage sera utilisée
  }

  public enum DateFormatType {

    AS_DATE,//si un temps de deb de scénario est 
    AS_DUREE
  }
  DureeFormatType dureeFormatType = DureeFormatType.FORMATTED;
  DateFormatType dateFormatType = DateFormatType.AS_DATE;

  public boolean useSeconds() {
    return dureeFormatType.equals(DureeFormatType.AS_SECOND);
  }

  public DureeFormatType getDureeFormatType() {
    return dureeFormatType;
  }

  @Override
  public TimeFormatOption clone() {
    try {
      return (TimeFormatOption) super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
    }
    throw new IllegalAccessError("why");
  }

  public void setUseSeconds(boolean newValue) {
    if (newValue) {
      dureeFormatType = DureeFormatType.AS_SECOND;
    } else {
      dureeFormatType = DureeFormatType.FORMATTED;
    }
  }
}
