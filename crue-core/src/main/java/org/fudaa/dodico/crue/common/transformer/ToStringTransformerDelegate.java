/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.transformer;

/**
 *
 * @author Frederic Deniger
 */
public interface ToStringTransformerDelegate {

  ToStringTransformer getTransformer(Object in);
}
