/*
 GPL 2
 */
package org.fudaa.dodico.crue.common.transformer;

import org.fudaa.ctulu.CtuluNumberFormat;
import org.fudaa.ctulu.CtuluNumberFormatI;

/**
 * @author Frederic Deniger
 */
public class CtuluNumberFormatAppender extends CtuluNumberFormat {
  private final transient CtuluNumberFormatI init;
  private String prefix;

  private CtuluNumberFormatAppender(final CtuluNumberFormatI init) {
    this.init = init;
  }

  public CtuluNumberFormatAppender(final CtuluNumberFormatI init, final String prefix) {
    this.init = init;
    this.prefix = prefix;
  }

  @Override
  public String format(final double _d) {
    final String res = init.format(_d);
    if (prefix == null) {
      return res;
    }
    return res + prefix;
  }

  @Override
  public CtuluNumberFormatI getCopy() {
    final CtuluNumberFormatAppender appender = new CtuluNumberFormatAppender(init.getCopy());
    appender.prefix = prefix;
    return appender;
  }

  @Override
  public String toLocalizedPattern() {
    return null;
  }
}
