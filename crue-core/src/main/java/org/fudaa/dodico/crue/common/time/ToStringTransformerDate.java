/*
 GPL 2
 */
package org.fudaa.dodico.crue.common.time;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormatter;

/**
 * @author Frederic Deniger
 */
public class ToStringTransformerDate implements ToStringTransformer, CtuluNumberFormatI, SecondParser {
    private static final DateTimeFormatter XSD_DATE_FORMATTER = DateDurationConverter.XSD_DATE_FORMATTER_UI;
    private LocalDateTime date;
    private final ToStringTransformerSecond initTransformer = new ToStringTransformerSecond();

    public ToStringTransformerDate(LocalDateTime date) {
        this.date = date;
    }

    public static String format(LocalDateTime dateTime) {
        String formatted = dateTime.toString(XSD_DATE_FORMATTER);
        return StringUtils.removeEnd(formatted, ".000");
    }

    @Override
    public Double parseInSec(String in) {
        //ce n'est pas une date
        if (in.indexOf(':') < 0) {
            return initTransformer.parseInSec(in);
        }
        if (in.indexOf('.') < 0) {
            return parseInSec(in + ".000");
        }
        try {
            DateTime parseDateTime = XSD_DATE_FORMATTER.parseDateTime(in);
            long millis = parseDateTime.getMillis() - date.toDateTime().getMillis();
            return millis / 1000d;
        } catch (Exception ignored) {
        }
        return null;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    /**
     * @param in in est en second.
     * @return l'objet transforme
     */
    @Override
    public String transform(Object in) {
        if (in == null) {
            return StringUtils.EMPTY;
        }
        if (date == null) {
            return initTransformer.transform(in);
        }
        Number value = (Number) in;
        LocalDateTime plusMillis = date.plusMillis((int) (value.doubleValue() * 1000));
        return format(plusMillis);
    }

    @Override
    public CtuluNumberFormatI getCopy() {
        return this;
    }

    @Override
    public String toLocalizedPattern() {
        return null;
    }

    @Override
    public boolean isDecimal() {
        return true;
    }

    @Override
    public String format(double sec) {
        if (date == null) {
            return initTransformer.format(sec);
        }
        LocalDateTime plusMillis = date.plusMillis((int) (sec * 1000));
        return format(plusMillis);
    }
}
