/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileIntegrityManager {
  private static final  Logger LOGGER = Logger.getLogger(FileIntegrityManager.class.getName());
  private final Set<File> fileSaved;
  private Map<File, File> backupFileByInitFile;
  private final CtuluLog backupLog = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);

  public FileIntegrityManager(final Collection<File> savedFile) {
    fileSaved = new HashSet<>(savedFile);
  }

  public void start() {
    if (backupFileByInitFile != null) {
      return;
    }
    backupFileByInitFile = new HashMap<>();
    backupLog.setDesc("io.saveFileBackup");
    final DateTime dt = new DateTime();
    final DateTimeFormatter fmt = ISODateTimeFormat.basicDate();
    final String str = fmt.print(dt);
    for (final File saveFile : fileSaved) {
      if (saveFile.exists()) {
        final File backupFile = createBackupFile(str, saveFile);
        if (backupFile == null) {
          backupLog.addWarn("cantCreateBackupFile.error", saveFile.getName());
        }
        if (LOGGER.isLoggable(Level.FINE)) {
          LOGGER.log(Level.FINE, "create backup file for{0} in {1}", new Object[]{saveFile, backupFile});
        }
        final boolean copyFile = CtuluLibFile.copyFile(saveFile, backupFile);
        if (!copyFile) {
          backupLog.addWarn("cantCopyFileToTheBackupFile.error", saveFile.getName());
        } else {
          backupFileByInitFile.put(saveFile, backupFile);
          if (backupFile != null) {
            CrueFileHelper.setLastModified(backupFile, saveFile, getClass());
          }
        }
      }
    }
  }

  public void finish(final boolean succeed) {
    if (succeed) {
      operationSucceed();
    } else {
      operationFailed();
    }
  }

  File getBackupFileFor(final File in) {
    return backupFileByInitFile.get(in);
  }

  /**
   * L'operation a réussi: on supprime les fichiers de backups.
   */
  void operationSucceed() {
    for (final File backupFile : backupFileByInitFile.values()) {
      deleteBackupFile(backupFile);
    }
    clearData();
  }

  /**
   * L'opération a échoué: on recupere les fichiers de backups si présents. Pour les fichiers nouvellement créé, on les efface.
   */
  void operationFailed() {
    int nbRestoredFile = 0;
    int nbDeletedFile = 0;
    for (final File saveFile : fileSaved) {
      final File backup = backupFileByInitFile.get(saveFile);
      if (backup != null) {
        final boolean copyFile = CtuluLibFile.copyFile(backup, saveFile);
        if (!copyFile) {
          backupLog.addWarn("operationFailedButCantRestoreFile.error", saveFile.getName());
        } else {
          CrueFileHelper.setLastModified(saveFile, backup, getClass());
          deleteBackupFile(backup);
          nbRestoredFile++;
        }
      } else if (saveFile.exists()) {
        try {
          Files.delete(saveFile.toPath());
          nbDeletedFile++;
        } catch (IOException e) {
          backupLog.addWarn("operationFailedButCantDeleteFile.error", saveFile.getName());
          LOGGER.log(Level.WARNING, "can delete file " + saveFile.getAbsolutePath(), e);
        }
      }
    }
    backupLog.addInfo("backupFileRestored.info", nbRestoredFile);
    backupLog.addInfo("backupFileDeleted.info", nbDeletedFile);
    clearData();
  }

  private void deleteBackupFile(File backup) {
    try {
      Files.delete(backup.toPath());
    } catch (IOException e) {
      backupLog.addWarn("CantDeleteBackup.error", backup.getName());
      LOGGER.log(Level.WARNING, "can delete file " + backup.getAbsolutePath(), e);
    }
  }

  private void clearData() {
    fileSaved.clear();
    backupFileByInitFile.clear();
  }

  private File createBackupFile(final String str, final File saveFile) {
    File backupFile = new File(saveFile + "-" + str + ".old");
    if (backupFile.exists()) {
      try {
        backupFile = File.createTempFile(saveFile.getName(), ".backup", saveFile.getParentFile());
      } catch (final IOException e) {
        LOGGER.log(Level.SEVERE, "createBackupFile", e);
        return null;
      }
    }
    return backupFile;
  }

  /**
   * @return the backupLog
   */
  public CtuluLog getBackupLog() {
    return backupLog;
  }
}
