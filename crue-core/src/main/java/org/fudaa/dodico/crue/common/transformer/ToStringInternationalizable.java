/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.transformer;

/**
 *
 * @author deniger
 */
public interface ToStringInternationalizable {

  /**
   * @return chaine traduite.
   */
  String geti18n();

  String geti18nLongName();
}
