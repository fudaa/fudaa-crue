/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.io;

/**
 * classe dao persist qui représente la structure xml à persister.
 *
 * @author Adrien Hadoux
 */
public class AbstractCrueDao {

  /**
   * En tete xsd
   */
  protected String xmlns = "http://www.fudaa.fr/xsd/crue";
  protected String xmlnsxsi = "http://www.w3.org/2001/XMLSchema-instance";
  protected String xsischemaLocation;
  private String Commentaire;

  public void setXsdName(final String xsdFile) {
    xsischemaLocation = "http://www.fudaa.fr/xsd/crue http://www.fudaa.fr/xsd/crue/" + xsdFile;
  }

  public void updateXmlns() {
    xmlns = "http://www.fudaa.fr/xsd/crue";
    xmlnsxsi = "http://www.w3.org/2001/XMLSchema-instance";
  }

  public void setCommentaire(final String commentaire) {
    Commentaire = commentaire;
  }

  public String getCommentaire() {
    return Commentaire;
  }


}
