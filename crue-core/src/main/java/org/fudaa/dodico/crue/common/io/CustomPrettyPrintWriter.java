/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.io;

import com.thoughtworks.xstream.io.naming.NameCoder;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import java.io.Writer;
import org.fudaa.ctulu.CtuluLibString;

/**
 *
 * @author Frederic Deniger
 */
public class CustomPrettyPrintWriter extends PrettyPrintWriter {

  public CustomPrettyPrintWriter(final Writer writer, final char[] lineIndenter, final NameCoder nameCoder) {
    super(writer, XML_QUIRKS, lineIndenter, nameCoder);
  }

  @Override
  protected String getNewLine() {
    return CtuluLibString.LINE_SEP;
  }
}
