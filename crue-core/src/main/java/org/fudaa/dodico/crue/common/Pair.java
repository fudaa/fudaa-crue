/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.common;

/**
 * @author deniger
 */
public class Pair<T, V> {
  public Pair() {
  }

  public Pair(T first, V second) {
    this.first = first;
    this.second = second;
  }

  public T first;
  public V second;
}
