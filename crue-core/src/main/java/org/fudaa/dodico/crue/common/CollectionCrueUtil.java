/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;

/**
 *
 * @author deniger
 */
public class CollectionCrueUtil {

  public static <T> ArrayList<T> select(final Collection<T> collection, final Predicate predicate) {
    final ArrayList<T> res = new ArrayList<>(collection.size());
    CollectionUtils.select(collection, predicate, res);
    return res;
  }

  public static void insertDataAtStart(final List list, final Object data) {
    if (list.isEmpty()) {
      list.add(data);
    } else {
      list.add(0, data);
    }
  }

  public static <T extends Comparable<? super T>> ArrayList<T> getSortedList(final Collection<T> collection) {
    final ArrayList<T> res = new ArrayList<>(collection);
    Collections.sort(res);
    return res;
  }

  public static <T> ArrayList<T> getSortedList(final Collection<T> collection, final Comparator comparator) {
    final ArrayList<T> res = new ArrayList<>(collection);
    Collections.sort(res, comparator);
    return res;
  }

  public static <T> void addInList(final List<T> dest, final T objectToAdd, final T old) {
    int idx = -1;
    if (old != null) {
      idx = dest.indexOf(old);
    }
    if (idx >= 0 && idx < dest.size()) {
      dest.set(idx, objectToAdd);
    } else {
      dest.add(objectToAdd);
    }

  }

  public static <O> void select(final Iterable<? extends O> inputCollection,
                                final Predicate<? super O> predicate, final Collection outputCollection) {

    if (inputCollection != null && predicate != null) {
      for (final O item : inputCollection) {
        if (predicate.evaluate(item)) {
          outputCollection.add(item);
        }
      }
    }
  }
}
