/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.transformer;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.joda.time.*;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.format.*;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
public final class DateDurationConverter {
    /**
     * La date null
     */
    public final static LocalDateTime ZERO_DATE = new LocalDateTime(01, 01, 01, 0, 0, 0, 0);
    public static final DateTimeFormatter XSD_DATE_FORMATTER_UI
            = new DateTimeFormatterBuilder()
            .append(ISODateTimeFormat.date())
            .appendLiteral(' ')
            .append(ISODateTimeFormat.hourMinuteSecondMillis())
            .toFormatter();
    public static final PeriodFormatter HMDS_DURATION_FORMATTER;
    /**
     * La valeur null pour le format crue
     */
    public final static String CRUE_ZERO = "0 0 0 0";
    private static final PeriodFormatter JJHHMMSS = new PeriodFormatterBuilder().printZeroAlways().minimumPrintedDigits(2).appendDays().
            appendLiteral(":").
            appendHours().appendLiteral(":").
            appendMinutes().appendLiteral(":")
            .appendSecondsWithOptionalMillis().toFormatter();
    private static final PeriodFormatter HHMMSS = new PeriodFormatterBuilder().printZeroAlways().minimumPrintedDigits(2).
            appendHours().appendLiteral(":").
            appendMinutes().appendLiteral(":")
            .appendSecondsWithOptionalMillis().toFormatter();
    private static final PeriodFormatter XSD_DURATION_FORMATTER = new PeriodFormatterBuilder().printZeroAlways().appendLiteral(
            "P0Y0M").appendDays().appendSuffix(
            "D").appendSeparatorIfFieldsAfter("T").appendHours().appendSuffix("H").appendMinutes().appendSuffix("M").
            appendSecondsWithOptionalMillis().
            appendSuffix(
                    "S").toFormatter();
    private static final PeriodFormatter CRUE_DURATION_FORMATTER = new PeriodFormatterBuilder().printZeroAlways().appendDays().appendLiteral(
            " ").appendHours().appendLiteral(" ").appendMinutes().appendLiteral(" ").appendSecondsWithOptionalMillis().toFormatter();
    private static final DateTimeFormatter XSD_DATE_FORMATTER = ISODateTimeFormat.dateHourMinuteSecondMillis();
    private static final DateTimeFormatter XSD_DATE_PARSER = ISODateTimeFormat.dateTimeParser();
    private static final PeriodFormatter XSD_PERIOD_PARSER = ISOPeriodFormat.standard();

    static {
        HMDS_DURATION_FORMATTER = new PeriodFormatterBuilder().printZeroRarelyFirst().
                appendDays().appendSuffix(BusinessMessages.getString("day.shortDesc")).appendSeparator(" ").
                appendHours().appendSuffix(BusinessMessages.getString("hour.shortDesc")).appendSeparator(" ").
                appendMinutes().appendSuffix(BusinessMessages.getString("min.shortDesc")).appendSeparator(" ").
                appendSecondsWithOptionalMillis().appendSuffix(BusinessMessages.getString("sec.shortDesc")).
                toFormatter();
    }

    private DateDurationConverter() {
        // classe utilitaire
    }

    /**
     * La valeur null pour le format crue
     */
    public static String dateToXsdUI(long longValue) {
        String formatted = XSD_DATE_FORMATTER_UI.print(longValue);
        return StringUtils.removeEnd(formatted, ".000");
    }

    public static long toMillis(double sec) {
        return Math.round(sec * 1000);
    }

    /**
     * @param days    le nombre de jour
     * @param hours   le nombre d'heures
     * @param minutes le nombre de minutes
     * @param seconds les secondes
     * @return la durée correspondante
     */
    public static Duration getDuration(final int days, final int hours, final int minutes, final int seconds) {
        return getPeriod(days, hours, minutes, seconds).toStandardDuration();
    }

    public static Duration getDuration(final int days, final int hours, final int minutes, final double seconds) {
        return getPeriodWithMillis(days, hours, minutes, seconds).toStandardDuration();
    }

    /**
     * @param days    le nombre de jour
     * @param hours   le nombre d'heures
     * @param minutes le nombre de minutes
     * @param seconds les secondes
     * @return la periode correspondante
     */
    public static Period getPeriod(final int days, final int hours, final int minutes, final int seconds) {
        return new Period(0, 0, 0, days, hours, minutes, seconds, 0);
    }

    private static Period getPeriodWithMillis(final int days, final int hours, final int minutes, final double seconds) {
        long totalMillis = (long) (seconds * 1000L);
        long millis = totalMillis % 1000;
        long sec = (totalMillis - millis) / 1000;
        return new Period(0, 0, 0, days, hours, minutes, (int) sec, (int) millis);
    }

    /**
     * @param days    le nombre de jour
     * @param hours   le nombre d'heures
     * @param minutes le nombre de minutes
     * @param seconds les secondes
     * @return la date à partir du 1/1/1
     */
    public static LocalDateTime getDateFromZeroDate(final int days, final int hours, final int minutes, final int seconds) {
        final Duration p = getDuration(days, hours, minutes, seconds);
        return getDateFromZeroDate(p);
    }

    /**
     * @param p la duration
     * @return la date en partant du zero de Crue.
     */
    private static LocalDateTime getDateFromZeroDate(final Duration p) {
        return DateDurationConverter.ZERO_DATE.plus(p);
    }

    /**
     * @param t la date a transformer
     * @return la représentation xsd
     */
    public static String dateToXsd(final LocalDateTime t) {
        return XSD_DATE_FORMATTER.print(t);
    }

    public static String dateToXsdUI(final LocalDateTime t) {
        String formatted = t.toString(XSD_DATE_FORMATTER_UI);
        return StringUtils.removeEnd(formatted, ".000");
    }

    /**
     * @param xsdDuration la chaine xsd:duration
     * @return la duration correspondante
     */
    public static Duration getDuration(final String xsdDuration) {
        if (xsdDuration == null) {
            return null;
        }
        return getDaysHourMinSecPeriod(xsdDuration.trim()).toStandardDuration();
    }

    /**
     * @param xsdDate la duration xsd
     * @return la date
     */
    public static LocalDateTime getDate(final String xsdDate) {
        if (StringUtils.isBlank(xsdDate)) {
            return null;
        }
        try {
            return XSD_DATE_PARSER.parseDateTime(xsdDate.trim()).toLocalDateTime();
        } catch (Exception e) {
            Logger.getLogger(DateDurationConverter.class.getName()).log(Level.INFO, "message {0}", e);
        }
        return null;
    }

    /**
     * @param xsdDuration la chaine xsd:duration
     * @return la period correspondante
     */
    public static Period getDaysHourMinSecPeriod(final String xsdDuration) {
        final MutablePeriod parseMutablePeriod = XSD_PERIOD_PARSER.parseMutablePeriod(xsdDuration);
        final int nbYear = parseMutablePeriod.getYears();
        int nbDaysToAdd = nbYear * 365;
        final int nbMonth = parseMutablePeriod.getMonths();
        nbDaysToAdd += nbMonth * 30;
        final int nbWeek = parseMutablePeriod.getWeeks();
        nbDaysToAdd += nbWeek * 7;
        parseMutablePeriod.setMonths(0);
        parseMutablePeriod.setYears(0);
        parseMutablePeriod.setWeeks(0);
        parseMutablePeriod.setDays(parseMutablePeriod.getDays() + nbDaysToAdd);
        return parseMutablePeriod.toPeriod();
    }

    /**
     * @param d la duration
     * @return le format Crue 'Hours Minutes Minutes Sec'
     */
    public static String durationToCrueFormat(final Duration d) {
        if (d == null) {
            return CRUE_ZERO;
        }
        return CRUE_DURATION_FORMATTER.print(durationToDaysHourMinSecPeriod(d));
    }

    public static boolean isGreaterThanDay(long millis) {
        return millis > 1000 * 3600 * 24;
    }

    /**
     * @param d la duration a traiter
     * @return la period avec les jour/heures/minutes/secondes
     */
    private static Period durationToDaysHourMinSecPeriod(final Duration d) {
        if (d == null) {
            return Period.ZERO;
        }
        return d.toPeriod(PeriodType.dayTime(), ISOChronology.getInstanceUTC());
    }

    public static String durationToDDHHMMSS(final Duration d, boolean useDay) {
        if (useDay) {
            return durationToDDHHMMSS(d);
        }
        return durationToHHMMSS(d);
    }

    public static String durationToDDHHMMSS(final Duration d) {
        return JJHHMMSS.print(durationToDaysHourMinSecPeriod(d));
    }

    public static String durationToHHMMSS(final Duration d) {
        return HHMMSS.print(d.toPeriod(PeriodType.time()));
    }

    /**
     * @param duration la duration a transformer
     * @return le format xsd en transformant les mois/années en nombre de jour
     */
    public static String durationToXsd(final Duration duration) {
        return XSD_DURATION_FORMATTER.print(durationToDaysHourMinSecPeriod(duration));
    }

    public static String durationToIso(final Duration d) {
        return HMDS_DURATION_FORMATTER.print(durationToDaysHourMinSecPeriod(d));
    }

    /**
     * @param date la date
     * @return la représentation Crue en partant du 1/1/1
     */
    public static String dateToCrueFormat(final LocalDateTime date) {
        if (date == null) {
            return CRUE_ZERO;
        }
        return durationToCrueFormat(new Duration(DateDurationConverter.ZERO_DATE.toDateTime(), date.toDateTime()));
    }

    public static Duration dateToDurationFromZero(final LocalDateTime date) {
        if (date == null) {
            return new Duration(0);
        }
        return new Duration(DateDurationConverter.ZERO_DATE.toDateTime(), date.toDateTime());
    }
}
