/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Visibility {

  /**
   * true if the method is visible in IHM
   */
  boolean ihm();
}
