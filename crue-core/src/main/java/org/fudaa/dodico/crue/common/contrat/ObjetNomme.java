/**
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.contrat;

/**
 * Objet commun a tous les objets possédant un nom/identifiant. Permet de généraliser les traitement de ces noms qui sont uniques sur l'étude
 *
 */
public interface ObjetNomme extends ObjetWithID {

  /**
   * @param newNom le nouveau nom
   */
  void setNom(String newNom);
}
