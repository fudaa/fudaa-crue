/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.transformer;

import org.apache.commons.collections4.Transformer;

/**
 * @author deniger.
 */
public final class TransformerAdapter implements Transformer {
    private final ToStringTransformer in;

    /**
     * @param toStringTransformer le tansformer
     */
    public TransformerAdapter(ToStringTransformer toStringTransformer) {
        super();
        this.in = toStringTransformer;
    }

    @Override
    public Object transform(Object input) {
        return in.transform(input);
    }
}
