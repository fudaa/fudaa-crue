/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common.contrat;

/**
 *
 * @author Frederic Deniger
 */
public interface DoubleValuable {

  double toDoubleValue();
}
