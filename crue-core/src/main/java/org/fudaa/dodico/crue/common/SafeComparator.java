/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common;

import java.util.Comparator;

/**
 *
 * @author deniger
 */
public abstract class SafeComparator<T> implements Comparator<T> {

  public static  int compareString(String o1, String o2) {
    return compareComparable(o1, o2);
  }

  public static <T extends Comparable> int compareComparable(T o1, T o2) {
    if (o1 == o2) {
      return 0;
    }
    if (o1 == null) {
      return -1;
    }
    if (o2 == null) {
      return 1;
    }
    return o1.compareTo(o2);
  }

  @Override
  public int compare(T o1, T o2) {
    if (o1 == o2) {
      return 0;
    }
    if (o1 == null) {
      return -1;
    }
    if (o2 == null) {
      return 1;
    }
    return compareSafe(o1, o2);
  }

  /**
   * Appellé si o1 et o2 ne sont pas null et si o1!=o2
   *
   * @param o1 not null
   * @param o2 not null
   * @return resultat de la comparaison
   */
  protected abstract int compareSafe(T o1, T o2);
}
