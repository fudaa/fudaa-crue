/*
 * License GPL v2
 */
package org.fudaa.dodico.crue.common;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.beanutils.PropertyUtilsBean;

/**
 * @author deniger
 */
public class CustomPropertyUtilsBean extends PropertyUtilsBean {

  private final static Logger LOGGER = Logger.getLogger(CustomPropertyUtilsBean.class.getName());

  @Override
  public PropertyDescriptor getPropertyDescriptor(final Object bean, final String name) throws IllegalAccessException,
          InvocationTargetException, NoSuchMethodException {
    if ("size".equals(name) && isCollection(bean)) {
      try {
        return new PropertyDescriptor("size", bean.getClass(), "size", null);
      } catch (final IntrospectionException e) {
        LOGGER.log(Level.SEVERE, "getPropertyDescriptor", e);
      }

    }
    return super.getPropertyDescriptor(bean, name);
  }

  private static boolean isCollection(final Object o) {
    if (o == null) {
      return false;
    }
    return o instanceof Collection;

  }
}
