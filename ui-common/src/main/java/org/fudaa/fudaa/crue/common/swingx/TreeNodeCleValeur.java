package org.fudaa.fudaa.crue.common.swingx;

import org.apache.commons.lang3.StringUtils;

/**
 *
 */
public class TreeNodeCleValeur extends AbstractTreeNode {
    private final String id;
    private final String valeur;
    private final String displayName;

    /**
     * @param id     l'identifiant du TreeNode
     * @param valeur sa valeur
     */
    public TreeNodeCleValeur(final String id, final String valeur) {
        super();
        this.id = id;
        this.valeur = valeur;
        displayName = "emh".equals(id) ? "EMH" : StringUtils.capitalize(id);
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(final int column) {
        if (column == 0) {
            return StringUtils.isNotBlank(displayName) ? displayName : id;
        }
        return valeur;
    }

    @Override
    public boolean isActive() {
        return true;
    }

    @Override
    public boolean isEditable(final int column) {
        return false;
    }
}
