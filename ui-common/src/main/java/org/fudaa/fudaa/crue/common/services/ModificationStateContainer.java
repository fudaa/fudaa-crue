package org.fudaa.fudaa.crue.common.services;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashSet;
import java.util.Set;

/**
 * Permet de maintenir des états de modifications par clés
 *
 * @author deniger
 */
public class ModificationStateContainer {
    /**
     * qqchose a été modifié
     */
    private static final String PROPERTY_MODIFIED = "modified";
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private boolean modified = false;
    private final Set<String> paramModified = new HashSet<>();

    public ModificationStateContainer() {
    }

    /**
     * @param listener ecoute la propriété MODIFIED. Ne sera avertit que s'il y a un changement d'état de "non modifié"
     *                 à "modifié" ou inversement
     */
    public void addModifiedListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(PROPERTY_MODIFIED, listener);
    }

    /**
     * @param property la propritété à écouter
     * @param listener le listener
     */
    public void addPropertyListener(String property, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(property, listener);
    }

    public void removePropertyListener(String property, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(property, listener);
    }

    public void clearModifiedState(String id) {
        paramModified.remove(id);
        updatModified();
    }

    public boolean isModified(String id) {
        return paramModified.contains(id);
    }

    public void removeModifiedSateListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(PROPERTY_MODIFIED, listener);
    }

    /**
     * envoi un evt avec la propriété property
     *
     * @param property la propriété utilise dans l'event
     */
    public void fireModification(String property) {
        pcs.firePropertyChange(property, Boolean.FALSE, Boolean.TRUE);
    }

    private void updatModified() {
        boolean old = modified;
        modified = !paramModified.isEmpty();
        if (old != modified) {
            pcs.firePropertyChange(PROPERTY_MODIFIED, !modified, modified);
        }
    }

    /**
     * appele par save.
     */
    public void clear() {
        paramModified.clear();
        updatModified();
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(String id) {
        paramModified.add(id);
        updatModified();
    }
}
