package org.fudaa.fudaa.crue.common.view;

import java.awt.BorderLayout;
import java.beans.PropertyVetoException;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.propertysheet.PropertySheetView;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

/**
 *
 * @author deniger
 */
public class DefaultConfigurationPanel extends JPanel implements ExplorerManager.Provider {

  private final ExplorerManager mgr = new ExplorerManager();
  protected OutlineView listView;
  protected PropertySheetView sheetView;

  public DefaultConfigurationPanel() {
    initComponents();
  }

  public void validEdition() {
  }

  public PropertySheetView getSheetView() {
    return sheetView;
  }

  public void stopEdition() {
    List<JTable> tables = DialogHelper.findComponent(getSheetView(), JTable.class);
    if (!tables.isEmpty()) {
      JTable table = tables.get(0);
      if (table.isEditing()) {
        table.getCellEditor().stopCellEditing();
      }
    }
  }

  public void initComponents() {
    setLayout(new BorderLayout(5, 5));
    listView = new OutlineView();
    listView.getOutline().setRootVisible(false);
    add(new JScrollPane(listView), BorderLayout.WEST);
    sheetView = new PropertySheetView();
    add(sheetView);
  }

  public void setNode(Node... nodes) {
    Children.Array children = new Children.Array();
    children.add(nodes);
    getExplorerManager().setRootContext(new AbstractNode(children));
  }

  public void setSelectedNode(Node node) {
    try {
      getExplorerManager().setSelectedNodes(new Node[]{node});
    } catch (PropertyVetoException propertyVetoException) {
    }
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return mgr;
  }
}
