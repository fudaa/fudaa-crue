package org.fudaa.fudaa.crue.common.log.filter;

import java.util.Map;
import org.openide.nodes.Node;
import org.openide.nodes.Node.Property;
import org.openide.util.Exceptions;

/**
 *
 * @author deniger
 */
public abstract class AbstractNodeQuickFilter implements NodeQuickFilter {

  final String propertyId;
  String description;
  String name;

    public AbstractNodeQuickFilter(final String propertyId) {
    this.propertyId = propertyId;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public boolean accept(final Node node) {
    if (node == null) {
      return false;
    }
    final Map<String, Node.Property> value = (Map<String, Node.Property>) node.getValue(NodeQuickFilterProcess.PROPERTY_BY_ID_KEY);
    if (value == null) {
      return true;
    }
    final Property property = value.get(propertyId);
    if (property == null) {
      return true;
    }
    try {
      return acceptValue(property.getValue());
    } catch (final Exception ex) {
      Exceptions.printStackTrace(ex);
    }
    return true;
  }

  protected abstract boolean acceptValue(Object value);
}
