package org.fudaa.fudaa.crue.common.log;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluPair;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.edition.EditionChangeSousModele.MoveToSousModeleResult;
import org.fudaa.dodico.crue.edition.EditionDelete;
import org.fudaa.fudaa.crue.common.UserPreferencesSaver;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Chris
 */
public class LogsDisplayer {
  private LogsDisplayer() {
  }

  /**
   * Création d'un JDialog à partir d'un CtuluLogsTopComponent, avec bouton "OK"
   *
   * @param table la table contenant les données
   * @param title le titre du dialogue
   * @param persistencePrefixForDialog le préfixe utilisé pour persister les données
   * @param versionForPersistence version pour la persistence et prendre en charge les versions
   * @return la dialogue utilisée pour afficher les messages d'erreurs/avertissements/info
   */
  public static JDialog createDialog(final CtuluLogsTopComponent table, String title, String persistencePrefixForDialog, String versionForPersistence) {
    return createDialog(table, title, persistencePrefixForDialog, JOptionPane.CLOSED_OPTION, DialogDescriptor.CLOSED_OPTION, versionForPersistence);
  }

  /**
   * Création d'un JDialog à partir d'un CtuluLogsTopComponent
   *
   * @param table la table contenant les données
   * @param title le titre du dialogue
   * @param persistencePrefixForDialog le préfixe utilisé pour persister les données et pour l'aide
   * @param versionForPersistence version pour la persistence et prendre en charge les versions
   * @return la dialogue utilisée pour afficher les messages d'erreurs/avertissements/info
   */
  private static JDialog createDialog(final CtuluLogsTopComponent table, String title, String persistencePrefixForDialog, int optionType,
                                      Object initialValue, final String versionForPersistence) {
    DialogDescriptor nd = new DialogDescriptor(
      table,
      title,
      true,
      optionType,
      initialValue,
      null);

    // forcer l'option, car sinon, c'est 2 boutons Oui/Non qui apparaissent
    nd.setOptions(new Object[]{initialValue});

    // installation des raccourcis d'aide
    SysdocUrlBuilder.installDialogHelpCtx(nd, "bdlBilanOperations", null, false);

    // création du dialog
    JDialog dialog = (JDialog) DialogDisplayer.getDefault().createDialog(nd);
    dialog.setName(persistencePrefixForDialog);

    // sauvegarde taille et position: pas de version
    UserPreferencesSaver.loadDialogLocationAndDimension(dialog, versionForPersistence);

    // lecture asynchrone des préférences
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        table.readPreferences(versionForPersistence);
      }
    });
    return dialog;
  }

  /**
   * @param table le top component a afficher
   * @param title le titre du dialog
   * @param persistencePrefixForDialog le prefixe pour l'aide et la persistences des infos (dimension et position)
   * @param versionForPersistence version pour la persistence et prendre en charge les versions
   */
  protected static void displayError(final CtuluLogsTopComponent table, String title, String persistencePrefixForDialog, String versionForPersistence) {
    if (GraphicsEnvironment.isHeadless()) {
      return;
    }
    JDialog dialog = createDialog(table, title, persistencePrefixForDialog, JOptionPane.OK_OPTION, DialogDescriptor.OK_OPTION, versionForPersistence);
    dialog.setVisible(true);
    table.writePreferences(table.getClass(), versionForPersistence);
    UserPreferencesSaver.saveLocationAndDimension(dialog, versionForPersistence);
  }

  /**
   * @param table le top component a afficher
   * @param title le titre du dialog
   * @param versionForPersistence version pour la persistence et prendre en charge les versions
   */
  protected static void displayError(final CtuluLogsTopComponent table, String title, String versionForPersistence) {
    displayError(table, title, table.getClass().getName(), versionForPersistence);
  }

  public static boolean displayErrorWithQuestion(final CtuluLog group, final String title, final String msgTop,
                                                 final String msgEnd, final String versionForPersistence) {
    if (!EventQueue.isDispatchThread()) {
      throw new IllegalAccessError("must be in EDT");
    }
    final CtuluLogsTopComponent table = new CtuluLogsTopComponent();
    table.setLog(group);
    return displayLogWithQuestion(title, msgTop, msgEnd, versionForPersistence, table);
  }

  public static boolean displayErrorWithQuestion(final CtuluLogGroup group, final String title, final String msgTop,
                                                 final String msgEnd, final String versionForPersistence) {
    if (!EventQueue.isDispatchThread()) {
      throw new IllegalAccessError("must be in EDT");
    }
    final CtuluLogsTopComponent table = new CtuluLogsTopComponent();
    table.setLogGroup(group);
    return displayLogWithQuestion(title, msgTop, msgEnd, versionForPersistence, table);
  }

  public static boolean displayLogWithQuestion(String title, String msgTop, String msgEnd, String versionForPersistence, CtuluLogsTopComponent table) {
    JPanel pn = new JPanel(new BorderLayout(0, 4));
    if (msgTop != null) {
      pn.add(new JLabel(msgTop), BorderLayout.NORTH);
    }
    if (msgEnd != null) {
      pn.add(new JLabel(msgEnd, SwingConstants.RIGHT), BorderLayout.SOUTH);
    }
    pn.add(table);
    DialogDescriptor nd = new DialogDescriptor(
      pn,
      title,
      true,
      JOptionPane.YES_NO_OPTION,
      DialogDescriptor.YES_OPTION,
      null);
    SysdocUrlBuilder.installDialogHelpCtx(nd, "bdlBilanOperations", null, false);
    JDialog dialog = (JDialog) DialogDisplayer.getDefault().createDialog(nd);
    dialog.setModal(true);
    dialog.setName("question" + table.getClass().getName());
    UserPreferencesSaver.loadDialogLocationAndDimension(dialog, DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        table.readPreferences(versionForPersistence);
      }
    });
    dialog.setVisible(true);
    table.writePreferences(versionForPersistence);
    UserPreferencesSaver.saveLocationAndDimension(dialog, DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    return NotifyDescriptor.YES_OPTION.equals(nd.getValue());
  }

  public static void displayError(final CtuluLogGroup group, final String title) {
    if (!EventQueue.isDispatchThread()) {
      EventQueue.invokeLater(new Runnable() {
        @Override
        public void run() {
          displayError(group, title);
        }
      });
      return;
    }
    final CtuluLogsTopComponent table = new CtuluLogsTopComponent();
    table.setLogGroup(group);
    displayError(table, title, DialogHelper.NO_VERSION_FOR_PERSISTENCE);
  }

  public static void displayError(final CtuluLog group, final String title) {
    if (!EventQueue.isDispatchThread()) {
      EventQueue.invokeLater(new Runnable() {
        @Override
        public void run() {
          displayError(group, title);
        }
      });
      return;
    }
    final CtuluLogsTopComponent table = new CtuluLogsTopComponent();
    table.setLog(group);
    displayError(table, title, DialogHelper.NO_VERSION_FOR_PERSISTENCE);
  }

  public static void displayBilan(EditionDelete.RemoveBilan deleted) {
    CtuluLogGroup bilan = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    boolean display = create(bilan, deleted.sectionNotRemovedUsedByBranche, "deleteEMH.SectionUsedByBranche.Title",
      "deleteEMH.SectionUsedByBranche.Item", CtuluLogLevel.INFO);
    display |= create(bilan, deleted.sectionNotRemovedReference, "deleteEMH.SectionUsedBySectionIdem.Title",
      "deleteEMH.SectionUsedBySectionIdem.Item", CtuluLogLevel.INFO);
    display |= create(bilan, deleted.sectionNotRemovedPilote, "deleteEMH.SectionPilote.Title",
      "deleteEMH.SectionPilote.Item", CtuluLogLevel.INFO);
    display |= create(bilan, deleted.noeudNotRemovedBranche, "deleteEMH.NoeudUsedByBranche.Title",
      "deleteEMH.NoeudUsedByBranche.Item", CtuluLogLevel.INFO);
    display |= create(bilan, deleted.noeudNotRemovedCasier, "deleteEMH.NoeudUsedByCasier.Title",
      "deleteEMH.NoeudUsedByCasier.Item", CtuluLogLevel.INFO);
    if(!deleted.noeudRemovedSousModele.isEmpty()){
      display=true;
      final CtuluLog newLog = bilan.createNewLog("deleteEMH.noeudRemovedFromSousModele.Title");
      deleted.noeudRemovedSousModele.forEach(noeudSmodele ->
        newLog.addRecord(CtuluLogLevel.INFO,"deleteEMH.noeudRemovedFromSousModele.Item", noeudSmodele.firstValue, noeudSmodele.secondValue));
    }
    if (display) {
      LogsDisplayer.displayError(bilan, BusinessMessages.getString("deleteEMH.Title"));
    }
  }

  private static boolean create(CtuluLogGroup group, List<String> noms, String title, String item, CtuluLogLevel level) {
    if (!noms.isEmpty()) {
      Collections.sort(noms);
      CtuluLog log = group.createNewLog(title);
      for (String nom : noms) {
        log.addRecord(level, item, nom);
      }
      return true;
    }
    return false;
  }

  private static boolean createFromPair(CtuluLogGroup group, List<CtuluPair<String>> noms, String title, String item, CtuluLogLevel level) {
    if (!noms.isEmpty()) {
      Collections.sort(noms);
      CtuluLog log = group.createNewLog(title);
      for (CtuluPair<String> nom : noms) {
        log.addRecord(level, item, nom.getKey(),nom.getObject());
      }
      return true;
    }
    return false;
  }

  public static void displayBilan(MoveToSousModeleResult changeSousModeleParent) {
    CtuluLogGroup bilan = new CtuluLogGroup(BusinessMessages.RESOURCE_BUNDLE);
    boolean display = createFromPair(bilan, changeSousModeleParent.brancheNotMovedSectionPilote,
      "changeSousModeleBrancheNotMovedSectionPilote.Title",
      "changeSousModeleBrancheNotMovedSectionPilote.Item", CtuluLogLevel.WARNING);
    display |= createFromPair(bilan, changeSousModeleParent.sectionNotMovedUsedByBranche,
      "changeSousModeleParentSectionUsedByBranche.Title",
      "changeSousModeleParentSectionUsedByBranche.Item", CtuluLogLevel.WARNING);
    display |= createFromPair(bilan, changeSousModeleParent.sectionIdemNotMoved,
      "changeSousModeleParentSectionIdemNotMoved.Title",
      "changeSousModeleParentSectionIdemNotMoved.Item", CtuluLogLevel.WARNING);
    display |= createFromPair(bilan, changeSousModeleParent.sectionDeReferenceNotMoved,
      "changeSousModeleParentSectionDeReferenceNotMoved.Title",
      "changeSousModeleParentSectionDeReferenceNotMoved.Item", CtuluLogLevel.WARNING);
    display |= createFromPair(bilan, changeSousModeleParent.sectionPiloteNotMoved,
      "changeSousModeleParentSectionPiloteNotMoved.Title",
      "changeSousModeleParentSectionPiloteNotMoved.Item", CtuluLogLevel.WARNING);
    display |= create(bilan, changeSousModeleParent.donFrtCloned,
      "changeSousModeleParentDonFrtCloned.Title",
      "changeSousModeleParentDonFrtCloned.Item", CtuluLogLevel.WARNING);
    display |= create(bilan, new ArrayList<>(changeSousModeleParent.noeudsDuplicatedInTarget),
      "changeSousModeleParentNoeudCloned.Title",
      "changeSousModeleParentNoeudCloned.Item", CtuluLogLevel.INFO);

    final String title = BusinessMessages.getString("changeSousModeleParent.Title", Integer.toString(
      changeSousModeleParent.getNbEMHsModified()));
    if (display) {
      create(bilan, new ArrayList<>(changeSousModeleParent.emhsMoved),
        "changeSousModeleParentEMHMoved.Title",
        "changeSousModeleParentEMHMoved.Item", CtuluLogLevel.INFO);
      LogsDisplayer.displayError(bilan, title);
    } else {
      DialogHelper.showNotifyOperationTermine(title, StringUtils.abbreviate(StringUtils.join(changeSousModeleParent.emhsMoved, ", "), 100));
    }
  }
}
