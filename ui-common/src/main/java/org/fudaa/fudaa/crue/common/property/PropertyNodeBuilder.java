/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.property;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizableTransformer;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.metier.emh.Pdt;
import org.fudaa.dodico.crue.metier.helper.PropertyFinder;
import org.fudaa.dodico.crue.metier.helper.PropertyFinder.Description;
import org.fudaa.fudaa.crue.common.services.EMHProjetService;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

/**
 *
 * @author Frédéric Deniger
 */
public class PropertyNodeBuilder {

  final EMHProjetService projetService = Lookup.getDefault().lookup(EMHProjetService.class);

  public PropertyNodeBuilder() {
  }
  CrueConfigMetier ccm;

  CrueConfigMetier getCcm() {
    if (ccm == null) {
      ccm = projetService.getCcm();
    }
    return ccm;

  }

  public static void configurePropertySupport(ItemVariable property, PropertySupportReflection propertySupportReflection, AbstractNodeFirable node) {
    //les enums sont traités différemments:
    if (property != null && !property.getNature().isEnum()) {
      addValidateListener(propertySupportReflection, property, node);
      return;
    }
    //prise en compte des enums:
    Class valueType = propertySupportReflection.getValueType();
    if (valueType.isEnum() && ToStringInternationalizable.class.isAssignableFrom(valueType)) {
      Object[] enumConstants = valueType.getEnumConstants();
      List<ToStringInternationalizable> enumTranslated = new ArrayList<>();
      for (Object enumValue : enumConstants) {
        enumTranslated.add((ToStringInternationalizable) enumValue);
      }
      Map<String, ToStringInternationalizable> toi18nMap = ToStringInternationalizableTransformer.toi18nMap(enumTranslated);
      List<String> tags = new ArrayList<>(toi18nMap.keySet());
      Collections.sort(tags);
      propertySupportReflection.setTags(tags.toArray(new String[0]));
      propertySupportReflection.setTranslationForObjects(toi18nMap);
    }
  }

  public static void addValidateListener(Node.Property propertySupportReflection, ItemVariable property, AbstractNodeFirable node) {
    propertySupportReflection.setShortDescription(property.getValidator().getHtmlDesc());
    ValidityValueListener listener = new ValidityValueListener(property, propertySupportReflection, node);
    listener.updateState();
  }

  public List<PropertySupportReflection> createFromPropertyDesc(DecimalFormatEpsilonEnum formatType, Object in, AbstractNodeFirable node) {
    PropertyFinder finder = new PropertyFinder();
    List<Description> availablePropertiesSorted = finder.getAvailablePropertiesSorted(in.getClass());
    List<PropertySupportReflection> res = new ArrayList<>();
    for (Description description : availablePropertiesSorted) {
      PropertySupportReflection prop = createPropertySupport(formatType, description, node, in);
      if (prop != null) {
        res.add(prop);
      }
    }
    return res;
  }

  public PropertySupportReflection createFromPropertyDesc(DecimalFormatEpsilonEnum formatType, Object in, AbstractNodeFirable node, String property) {
    PropertyFinder finder = new PropertyFinder();
    List<Description> availablePropertiesSorted = finder.getAvailablePropertiesSorted(in.getClass());
    for (Description description : availablePropertiesSorted) {
      if (description.property.equals(property)) {
        return createPropertySupport(formatType, description, node, in);
      }
    }
    return null;
  }

  public PropertySupportReflection createPropertySupport(DecimalFormatEpsilonEnum formatType, Description description, AbstractNodeFirable node,
          Object in) {
    PropertySupportReflection prop = null;
    CrueConfigMetier ccm = getCcm();
    assert ccm != null;
    try {
      if (Pdt.class.isAssignableFrom(description.propertyClass)) {
        prop = new PropertySupportReflectionPdt(node, in, description.property, ccm);
      } else if (Double.TYPE.equals(description.propertyClass)) {
        prop = new PropertySupportReflectionDouble(formatType, node, in, description.property);

        ((PropertySupportReflectionDouble) prop).setItemVariable(ccm.getProperty(description.property));
        PropertyCrueUtils.configureNoCustomEditor(prop);
        PropertyCrueUtils.installForColorFont(prop);
      } else {
        if (description.readOnly) {
          String prefix = "get";
          if (Boolean.class.equals(description.propertyClass) || Boolean.TYPE.equals(description.propertyClass)) {
            prefix = "is";
          }
          prop = new PropertySupportReflection(node, in, description.propertyClass, prefix + StringUtils.capitalize(description.property), null);
        } else {
          prop = new PropertySupportReflection(node, in, description.propertyClass, description.property);
        }
        PropertyCrueUtils.configureNoCustomEditor(prop);
        PropertyCrueUtils.installForColorFont(prop);
      }
      prop.setDisplayName(description.propertyDisplay);
    } catch (NoSuchMethodException exception) {
      Exceptions.printStackTrace(exception);
    }
    return prop;
  }
}
