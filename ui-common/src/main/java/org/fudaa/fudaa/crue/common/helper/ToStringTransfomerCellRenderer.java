package org.fudaa.fudaa.crue.common.helper;

import javax.swing.ListCellRenderer;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;

@SuppressWarnings("serial")
public class ToStringTransfomerCellRenderer extends CtuluCellTextRenderer implements ListCellRenderer{

  private final ToStringTransformer transfomer;

  public ToStringTransfomerCellRenderer(ToStringTransformer transfomer) {
    this.transfomer = transfomer;
  }

  @Override
  protected void setValue(Object value) {
    super.setValue(transfomer.transform(value));
  }
}
