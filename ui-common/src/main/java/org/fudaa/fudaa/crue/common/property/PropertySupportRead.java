package org.fudaa.fudaa.crue.common.property;

import org.openide.nodes.PropertySupport;
import org.openide.util.Exceptions;

import java.beans.PropertyEditor;

/**
 * I instance de l'objet T type de retour
 *
 * @author Fred Deniger
 */
public abstract class PropertySupportRead<I, T> extends PropertySupport.ReadOnly<T> {
    private final I instance;
    private Class<? extends PropertyEditor> propertyEditorClass;

    public PropertySupportRead(I instance, Class<T> valueType, String name, String displayName,
                               String description) {
        super(name, valueType, displayName, description);
        this.instance = instance;
    }

    protected PropertySupportRead(I instance, Class<T> valueType, String name, String displayName) {
        this(instance, valueType, name, displayName, null);
    }

    protected I getInstance() {
        return instance;
    }

    @Override
    public boolean canRead() {
        return super.canRead();
    }

    @Override
    public boolean canWrite() {
        return false;
    }

    public PropertyEditor getPropertyEditor() {
        if (propertyEditorClass != null) {
            try {
                return propertyEditorClass.newInstance();
            } catch (InstantiationException ex) {
                Exceptions.printStackTrace(ex);
            } catch (IllegalAccessException iex) {
                Exceptions.printStackTrace(iex);
            }
        }

        return super.getPropertyEditor();
    }

    /**
     * Set the property editor explicitly.
     *
     * @param clazz class type of the property editor
     */
    public void setPropertyEditorClass(Class<? extends PropertyEditor> clazz) {
        propertyEditorClass = clazz;
    }
}
