package org.fudaa.fudaa.crue.common.editor;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.beans.PropertyEditor;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import org.fudaa.ebli.trace.TypeIconCellRenderer;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;

/**
 *
 * @author deniger
 */
public class TypeIconJcomboBoxInplaceEditor implements InplaceEditor {

  private JComboBox combobox;
  private PropertyEditor editor = null;
  private PropertyModel model;
  final List<Integer> iconTypeToUse;

  public TypeIconJcomboBoxInplaceEditor(List<Integer> iconTypeToUse) {
    this.iconTypeToUse = iconTypeToUse;
  }

  private JComboBox getCombobox() {
    if (combobox == null) {
      this.combobox = new JComboBox(iconTypeToUse.toArray(new Integer[0]));
      combobox.setRenderer(new TypeIconCellRenderer());
    }
    return combobox;
  }

  @Override
  public void connect(PropertyEditor propertyEditor, PropertyEnv env) {
    editor = propertyEditor;
    reset();
  }

  @Override
  public JComponent getComponent() {
    return getCombobox();
  }

  @Override
  public void clear() {
    //avoid memory leaks:
    editor = null;
    model = null;
  }

  @Override
  public Object getValue() {
    return getCombobox().getSelectedItem();
  }

  @Override
  public void setValue(Object object) {
    try {
      getCombobox().setSelectedItem(object);
    } catch (IllegalArgumentException e) {
      getCombobox().setSelectedItem(null);
    }
  }

  @Override
  public boolean supportsTextEntry() {
    return true;
  }

  @Override
  public void reset() {
    Object d = editor.getValue();
    if (d != null) {
      setValue(d);
    }
  }

  @Override
  public KeyStroke[] getKeyStrokes() {
    return new KeyStroke[0];
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    return editor;
  }

  @Override
  public PropertyModel getPropertyModel() {
    return model;
  }

  @Override
  public void setPropertyModel(PropertyModel propertyModel) {
    this.model = propertyModel;
  }

  @Override
  public boolean isKnownComponent(Component component) {
    return component == getCombobox() || getCombobox().isAncestorOf(component);
  }

  @Override
  public void addActionListener(ActionListener actionListener) {
    //do nothing - not needed for this component
  }

  @Override
  public void removeActionListener(ActionListener actionListener) {
    //do nothing - not needed for this component
  }
}
