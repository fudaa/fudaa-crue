package org.fudaa.fudaa.crue.common.helper;

import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 * Classe permettant d'ouvrir un TopCompoent par son id
 * @author f.deniger
 */
public class TopComponentOpener {
    protected final String topComponentId;
    private final String mode;

    public TopComponentOpener(String mode, String topComponentId) {
        this.mode = mode;
        this.topComponentId = topComponentId;
    }

    public TopComponent open() {
        TopComponent initTc = WindowManager.getDefault().findTopComponent(topComponentId);
        if (!initTc.isOpened()) {
            initTc.open();
        }
        initTc.requestActive();
        return initTc;
    }
}
