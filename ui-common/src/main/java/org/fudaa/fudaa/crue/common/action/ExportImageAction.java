/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.action;

import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.image.CtuluImageExportPanel;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ExportImageAction extends EbliActionSimple {

  private final CtuluImageProducer imageProducer;

  public ExportImageAction(CtuluImageProducer producer) {
    super(NbBundle.getMessage(ExportImageAction.class, "ExportImageAction.DisplayName"), BuResource.BU.getIcon("photographie"),
            CtuluLibImage.SNAPSHOT_COMMAND);
    this.imageProducer = producer;
    setDefaultToolTip(NbBundle.getMessage(ExportImageAction.class, "ExportImageAction.Tooltip"));
    updateTooltip();
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    CtuluUI ui = CtuluUIForNetbeans.DEFAULT;
    CtuluImageExportPanel panel = new CtuluImageExportPanel(imageProducer, ui);
    SysdocUrlBuilder.installHelpShortcut(panel, SysdocUrlBuilder.getDialogHelpCtxId("bdlEnregistrerImage", null));
    panel.afficheModale(ui.getParentComponent(), CtuluLib
            .getS("Enregistrement image"));
  }
}
