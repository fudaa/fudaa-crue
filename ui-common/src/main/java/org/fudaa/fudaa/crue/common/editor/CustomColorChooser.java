/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.editor;

import org.openide.explorer.propertysheet.editors.XMLPropertyEditor;
import org.openide.util.Exceptions;

import javax.swing.*;
import javax.swing.colorchooser.ColorSelectionModel;
import javax.swing.colorchooser.DefaultColorSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyEditor;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * A property editor for Color class. (Final only for performance, can be unfinaled if desired).
 *
 * @author Jan Jancura, Ian Formanek
 */
public final class CustomColorChooser implements PropertyEditor, XMLPropertyEditor {
  /**
   * Swing Palette mode.
   */
  public static final int SWING_PALETTE = 3;
  /**
   * AWT colors used in AWT Palette.
   */
  /**
   * Swing colors names and values are static and lazy initialized. They are also cleared when l&f changes.
   */
  private static String[] swingColorNames;
  /**
   * Swing colors used in Swing Palette.
   */
  private static Color[] swingColors;
  private static final boolean GTK = "GTK".equals(UIManager.getLookAndFeel().getID());//NOI18N
  private static final boolean AQUA = "Aqua".equals(UIManager.getLookAndFeel().getID());//NOI18N
  private static final boolean ANTIALIAS = Boolean.getBoolean("nb.cellrenderer.antialiasing") // NOI18N
      || Boolean.getBoolean("swing.aatext") // NOI18N
      || (GTK && gtkShouldAntialias()) // NOI18N
      || AQUA;
  private static Boolean gtkAA;
  private static Map hintsMap;

  // static initializer .........................................
  static {
    UIManager.addPropertyChangeListener(evt -> {
          swingColorNames = null;
          swingColors = null;
        }
    );

    swingColorNames = null;
    swingColors = null;
  }
  // variables ..................................................................................
  /**
   * Selected color.
   */
  private SuperColor superColor;
  /**
   * Property change support. Helper field.
   */
  private final PropertyChangeSupport support;

  /**
   * Gets
   * <code>staticChooser</code> instance.
   */
  private static JColorChooser getStaticChooser(final CustomColorChooser ce) {
    return new JColorChooser(new DefaultColorSelectionModel(Color.white) {
      @Override
      public void setSelectedColor(final Color color) {
        if (color instanceof SuperColor) {
          super.setSelectedColor(color);
        } else if (color instanceof Color) {
          super.setSelectedColor(new SuperColor(color));
        }
      }
    }) {
      @Override
      public void setColor(final Color c) {
        if (c == null) {
          return;
        }
        super.setColor(c);
      }
    };
  }

  // init .......................................................................................

  /**
   * Creates color editor.
   */
  public CustomColorChooser() {
    support = new PropertyChangeSupport(this);
  }

  // main methods .......................................................................................

  /**
   * Gets value. Implements
   * <code>PropertyEditor</code> interface.
   *
   * @return <code>Color</code> value or <code>null</code>
   */
  @Override
  public Object getValue() {
    if (superColor != null) {
      if (superColor.getID() != null) {
        return superColor;
      } else {
        return superColor.getColor();
      }
    } else {
      return null;
    }
  }

  /**
   * Sets value. Implements
   * <code>PropertyEditor</code> interface.
   *
   * @param object object to set, accepts <code>Color</code> or <code>SuperColor<code> types
   */
  @Override
  public void setValue(final Object object) {
    if (object != null) {
      if (object instanceof SuperColor) {
        superColor = (SuperColor) object;
      } else if (object instanceof Color) {
        superColor = new SuperColor((Color) object);
      }
    } else {
      superColor = null;
    }

    support.firePropertyChange("", null, null); // NOI18N
  }

  /**
   * Gets value as text. Implements
   * <code>PropertyEditor</code> interface.
   */
  @Override
  public String getAsText() {
    if (superColor == null) {
      return "null"; // NOI18N
    }
    return superColor.getAsText();
  }

  /**
   * Sets value ad text. Implements
   * <code>PropertyEditor</code> interface.
   */
  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    if (text == null) {
      throw new IllegalArgumentException("null parameter"); // NOI18N
    }

    text = text.trim();

    if ("null".equals(text)) { // NOI18N
      setValue(null);
      return;
    }

    try { // try to extract RGB values - represented as [r,g,b] or r,g,b
      final int len = text.length();
      if (len > 0) {
        int start = -1;
        int end = -1;

        final char c1 = text.charAt(0);
        final char c2 = text.charAt(len - 1);
        if (c1 == '[' && c2 == ']') {
          start = 1;
          end = len - 1;
        } else if (c1 >= '0' && c1 <= '9' && c2 >= '0' && c2 <= '9') {
          start = 0;
          end = len;
        }

        if (start >= 0) {
          final int index1 = text.indexOf(',');
          final int index2 = index1 < 0 ? -1 : text.indexOf(',', index1 + 1);

          if (index1 >= 0 && index2 >= 0) {
            final int red = Integer.parseInt(text.substring(
                start, index1).trim());
            final int green = Integer.parseInt(text.substring(
                index1 + 1, index2).trim());
            final int blue = Integer.parseInt(text.substring(
                index2 + 1, end).trim());

            try {
              setValue(new SuperColor(null,
                  0,
                  new Color(red, green, blue)));
              return;
            } catch (final IllegalArgumentException iaE) {
              Exceptions.printStackTrace(iaE);
              throw iaE;
            }
          }
        }
      }
    } catch (final NumberFormatException nfe) {
      // Ignore it and try out from palette's next.
    }

    int index = -1;
    int palette = 0;
    Color color = null;


    if (index < 0) {
      initSwingConstants();
      if ((index = getIndex(swingColorNames, text)) >= 0) {
        palette = SWING_PALETTE;
        color = swingColors[index];
      }
    }

    if (index < 0) {
      final IllegalArgumentException iae = new IllegalArgumentException(text);
      Exceptions.printStackTrace(iae);
      throw iae;
    }

    setValue(new SuperColor(text, palette, color));
  }

  /**
   * Gets java inititalization string. Implements
   * <code>PropertyEditor</code> interface.
   */
  @Override
  public String getJavaInitializationString() {
    if (superColor == null) {
      return "null"; // NOI18N
    }
    if (superColor.getID() == null) {
      return "new java.awt.Color(" + superColor.getRed() + ", " + superColor.getGreen() + // NOI18N
          ", " + superColor.getBlue() + ")"; // NOI18N
    }
    switch (superColor.getPalette()) {
      default:

      case SWING_PALETTE:
        if (superColor.getID() == null) {
          return "new java.awt.Color(" + superColor.getRed() + ", " + superColor.getGreen() + // NOI18N
              ", " + superColor.getBlue() + ")"; // NOI18N
        }
        return "javax.swing.UIManager.getDefaults().getColor(\"" + // NOI18N
            superColor.getID() + "\")"; // NOI18N
    }
  }

  /**
   * Get tags possible for choosing value. Implements
   * <code>PropertyEditor</code> interface.
   */
  @Override
  public String[] getTags() {
    switch (superColor.getPalette()) {
      case SWING_PALETTE:
        initSwingConstants();
        return swingColorNames;
      default:
        return null;
    }
  }

  /**
   * Insicates whether this editor is paintable. Implements
   * <code>PropertyEditor</code> interface.
   *
   * @return <code>true</code>
   */
  @Override
  public boolean isPaintable() {
    return true;
  }

  /**
   * Paints the current value. Implements
   * <code>ProepertyEditor</code> interface.
   */
  @Override
  public void paintValue(final Graphics g, final Rectangle rectangle) {
    final int px;

    ((Graphics2D) g).setRenderingHints(getHints());

    if (this.superColor != null) {
      final Color color = g.getColor();
      g.drawRect(rectangle.x, rectangle.y + rectangle.height / 2 - 5, 10, 10);
      g.setColor(this.superColor);
      g.fillRect(rectangle.x + 1, rectangle.y + rectangle.height / 2 - 4, 9, 9);
      g.setColor(color);
      px = 18;
    } else {
      px = 0;
    }

    final FontMetrics fm = g.getFontMetrics();
    g.drawString(getAsText(), rectangle.x + px, rectangle.y
        + (rectangle.height - fm.getHeight()) / 2 + fm.getAscent());
  }

  /**
   * Indicates whether this editor supports custom editing. Implements
   * <code>PropertyEditor</code> interface.
   *
   * @return <code>true</code>
   */
  @Override
  public boolean supportsCustomEditor() {
    return true;
  }

  /**
   * Gets custom editor. Implements
   * <code>PropertyEditor</code> interface. *return
   * <code>NbColorChooser</code> instance
   */
  @Override
  public Component getCustomEditor() {
    return new NbColorChooser(this, getStaticChooser(this));
  }

  /**
   * Adds property change listener.
   */
  @Override
  public void addPropertyChangeListener(final PropertyChangeListener propertyChangeListener) {
    support.addPropertyChangeListener(propertyChangeListener);
  }

  /**
   * Removes property change listner.
   */
  @Override
  public void removePropertyChangeListener(final PropertyChangeListener propertyChangeListener) {
    support.removePropertyChangeListener(propertyChangeListener);
  }

  /**
   * Gets index of name from array.
   */
  private static int getIndex(final Object[] names, final Object name) {
    for (int i = 0; i < names.length; i++) {
      if (name.equals(names[i])) {
        return i;
      }
    }

    return -1;
  }

  /**
   * Initialized fields used in Swing Palette.
   */
  private static void initSwingConstants() {
    if (swingColorNames != null) {
      return;
    }

    final UIDefaults def = UIManager.getDefaults();
    final Enumeration e = def.keys();

    final java.util.TreeSet<String> names = new java.util.TreeSet<>();

    while (e.hasMoreElements()) {
      final Object k = e.nextElement();
      if (!(k instanceof String)) {
        continue;
      }
      final Object v = def.get(k);
      if (!(v instanceof Color)) {
        continue;
      }
      names.add((String) k);
    }

    swingColorNames = new String[names.size()];
    names.toArray(swingColorNames);
    swingColors = new Color[swingColorNames.length];

    int i;
    final int k = swingColorNames.length;
    for (i = 0; i < k; i++) {
      swingColors[i] = (Color) def.get(swingColorNames[i]);
    }
  }

  private SuperColor getSuperColor() {
    return superColor;
  }

  // innerclasses ............................................................................................

  /**
   * Panel used as custom property editor.
   */
  private static class NbColorChooser extends JPanel implements ChangeListener {
    /**
     * Color property editor
     */
    private final CustomColorChooser editor;
    /**
     * Reference to model which holds the color selected in the color chooser
     */
    private final ColorSelectionModel selectionModel;
    static final long serialVersionUID = -6230228701104365037L;

    /**
     * Creates new
     * <code>NbColorChooser</code>.
     */
    public NbColorChooser(final CustomColorChooser editor,
                          final JColorChooser chooser) {
      this.editor = editor;
      selectionModel = chooser.getSelectionModel();
      setLayout(new BorderLayout());
      add(chooser, BorderLayout.CENTER);
      chooser.setColor((Color) editor.getValue());
      selectionModel.addChangeListener(this);

    }

    /**
     * Overrides superclass method. Adds removing of change listener.
     */
    @Override
    public void removeNotify() {
      super.removeNotify();
      selectionModel.removeChangeListener(this);
    }

    /**
     * Overrides superclass method. Adds 50 pixels to each side.
     */
    @Override
    public Dimension getPreferredSize() {
      final Dimension s = super.getPreferredSize();
      return new Dimension(s.width + 50, s.height + 10);
    }

    /**
     * Implementats
     * <code>ChangeListener</code> interface
     */
    @Override
    public void stateChanged(final ChangeEvent evt) {
      editor.setValue(selectionModel.getSelectedColor());
    }
  } // End of class NbColorChooser.

  /**
   * Color belonging to palette and keeping its ID.
   */
  static class SuperColor extends Color {
    /**
     * generated Serialized Version UID
     */
    static final long serialVersionUID = 6147637669184334151L;
    /**
     * ID of this color.
     */
    private String id = null;
    /**
     * Palette where it belongs.
     */
    private int palette = 0;
    private final Color color;

    SuperColor(final Color color) {
      super(color.getRed(), color.getGreen(), color.getBlue());
      this.color = color;
    }

    SuperColor(final String id, final int palette, final Color color) {
      super(color.getRed(), color.getGreen(), color.getBlue());
      this.color = color;
      this.id = id;
      this.palette = palette;
    }

    /**
     * Overrides the equals(Object obj) method of java.awt.Color
     */
    @Override
    public boolean equals(final Object obj) {
      final boolean superEquals = super.equals(obj);
      String objID;
      int objPalette ;

      if (obj instanceof SuperColor) {
        objID = ((SuperColor) obj).getID();
        objPalette = ((SuperColor) obj).getPalette();
      } else {
        return superEquals;
      }

      if (objID != null) {
        return superEquals && objID.equals(getID()) && (objPalette == getPalette());
      } else {
        return superEquals && (null == getID()) && (objPalette == getPalette());
      }
    }

    /**
     * Gets ID of this color.
     */
    private String getID() {
      return id;
    }

    /**
     * Gets palette of this color.
     */
    private int getPalette() {
      return palette;
    }

    /**
     * Returns original color object
     */
    private Color getColor() {
      return this.color;
    }

    /**
     * Gets as text this color value.
     */
    private String getAsText() {
      if (id != null) {
        return id;
      }
      return "[" + getRed() + "," + getGreen() + "," + getBlue() + "]"; // NOI18N
    }
  } // End of class SuperColor.

  /**
   * Name of color element.
   */
  private static final String XML_COLOR = "Color"; // NOI18N
  /**
   * Name of type attribute.
   */
  private static final String ATTR_TYPE = "type"; // NOI18N
  /**
   * Name of red attribute.
   */
  private static final String ATTR_RED = "red"; // NOI18N
  /**
   * Name of green attribute.
   */
  private static final String ATTR_GREEN = "green"; // NOI18N
  /**
   * Name of blue attribute.
   */
  private static final String ATTR_BLUE = "blue"; // NOI18N
  /**
   * Name of id attribute.
   */
  private static final String ATTR_ID = "id"; // NOI18N
  /**
   * Name of palette attribute.
   */
  private static final String ATTR_PALETTE = "palette"; // NOI18N
  /**
   * Value of palette.
   */
  private static final String VALUE_PALETTE = "palette"; // NOI18N
  /**
   * Value of rgb.
   */
  private static final String VALUE_RGB = "rgb"; // NOI18N
  /**
   * Null value.
   */
  private static final String VALUE_NULL = "null"; // NOI18N

  /**
   * Called to loadLHPT property value from specified XML subtree. If succesfully loaded, Implements
   * <code>XMLPropertyEditor</code> interface. the value should be available via the getValue method. An IOException should be thrown when the value
   * cannot be restored from the specified XML element
   *
   * @param element the XML DOM element representing a subtree of XML from which the value should be loaded
   * @throws java.io.IOException thrown when the value cannot be restored from the specified XML element
   */
  @Override
  public void readFromXML(final org.w3c.dom.Node element) throws java.io.IOException {
    if (!XML_COLOR.equals(element.getNodeName())) {
      throw new java.io.IOException();
    }
    final org.w3c.dom.NamedNodeMap attributes = element.getAttributes();
    try {
      final String type = attributes.getNamedItem(ATTR_TYPE).getNodeValue();
      if (VALUE_NULL.equals(type)) {
        setValue(null);
      } else {
        final String red = attributes.getNamedItem(ATTR_RED).getNodeValue();
        final String green = attributes.getNamedItem(ATTR_GREEN).getNodeValue();
        final String blue = attributes.getNamedItem(ATTR_BLUE).getNodeValue();
        if (VALUE_PALETTE.equals(type)) {
          final int palette = Integer.parseInt(attributes.getNamedItem(ATTR_PALETTE).getNodeValue());
          final String id = attributes.getNamedItem(ATTR_ID).getNodeValue();

          setValue(new SuperColor(id, palette, new Color(Integer.parseInt(red, 16), Integer.parseInt(green, 16), Integer.parseInt(blue, 16))));
        } else {
          setValue(new SuperColor(new Color(Integer.parseInt(red, 16), Integer.parseInt(green, 16), Integer.parseInt(blue, 16))));
        }
      }
    } catch (final NullPointerException e) {
      throw new java.io.IOException();
    }
  }

  /**
   * Called to store current property value into XML subtree. The property value should be set using the Implemtns
   * <code>XMLPropertyEdtitor</code> interface. setValue method prior to calling this method.
   *
   * @param doc The XML document to store the XML in - should be used for creating nodes only
   * @return the XML DOM element representing a subtree of XML from which the value should be loaded
   */
  @Override
  public org.w3c.dom.Node storeToXML(final org.w3c.dom.Document doc) {
    final org.w3c.dom.Element el = doc.createElement(XML_COLOR);
    el.setAttribute(ATTR_TYPE, (superColor == null) ? VALUE_NULL : ((superColor.getID() == null) ? VALUE_RGB : VALUE_PALETTE));
    if (superColor != null) {
      el.setAttribute(ATTR_RED, Integer.toHexString(superColor.getRed()));
      el.setAttribute(ATTR_GREEN, Integer.toHexString(superColor.getGreen()));
      el.setAttribute(ATTR_BLUE, Integer.toHexString(superColor.getBlue()));
      if (superColor.getID() != null) {
        el.setAttribute(ATTR_ID, superColor.getID());
        el.setAttribute(ATTR_PALETTE, Integer.toString(superColor.getPalette()));
      }
    }
    return el;
  }

  private static boolean gtkShouldAntialias() {
    if (gtkAA == null) {
      final Object o = Toolkit.getDefaultToolkit().getDesktopProperty("gnome.Xft/Antialias"); //NOI18N
      gtkAA = Boolean.valueOf(Integer.valueOf(1).equals(o));
    }

    return gtkAA.booleanValue();
  }

  // copied from openide/awt/HtmlLabelUI
  @SuppressWarnings("unchecked") // need to use reflective access, no idea of type
  private static Map getHints() {
    if (hintsMap == null) {
      hintsMap = (Map) (Toolkit.getDefaultToolkit().getDesktopProperty("awt.font.desktophints")); //NOI18N
      if (hintsMap == null) {
        hintsMap = new HashMap();
        if (ANTIALIAS) {
          hintsMap.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        }
      }
    }
    return hintsMap;
  }
}
