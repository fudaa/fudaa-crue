package org.fudaa.fudaa.crue.common.swing;

import com.Ostermiller.util.CSVParser;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.crue.common.UserPreferencesSaver;
import org.jdesktop.swingx.JXTable;
import org.openide.util.ImageUtilities;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.KeyEvent;
import java.io.IOException;

/**
 * librairie commune fournit des methodes utiles.
 *
 * @author Adrien Hadoux
 */
public class CommonGuiLib {
  private CommonGuiLib() {
  }


  public static String getAcceleratorText(final KeyStroke _accelerator) {
    String acceleratorDelimiter = UIManager.getString("MenuItem.acceleratorDelimiter");
    if (acceleratorDelimiter == null) {
      acceleratorDelimiter = "+";
    }

    final StringBuilder acceleratorText = new StringBuilder(50);
    if (_accelerator != null) {
      final int modifiers = _accelerator.getModifiers();
      if (modifiers > 0) {
        acceleratorText.append(KeyEvent.getKeyModifiersText(modifiers)).append(acceleratorDelimiter);
      }

      final int keyCode = _accelerator.getKeyCode();
      if (keyCode == 0) {
        acceleratorText.append(_accelerator.getKeyChar());
      } else {
        acceleratorText.append(KeyEvent.getKeyText(keyCode));
      }
    }
    return acceleratorText.toString();
  }

  /**
   * Affiche une dialogue et persiste les dimensions et la position
   *
   * @param pn le panel
   * @param table la table
   * @param dialogName le nom du dialogue utilise pour la persistence et l'aide
   * @param dialogTitle le titre du dialogue
   * @param versionForPersistence la version a utiliser pour la persistence. Peut etre vide
   */
  public static void showDialogAndTable(final CtuluDialogPanel pn, final JXTable table,
                                        final String dialogName, final String dialogTitle, final String versionForPersistence) {
    showDialogAndTable(pn, CtuluDialog.QUIT_OPTION, table, dialogName, dialogTitle, versionForPersistence);
  }

  /**
   * Affiche une dialogue et persiste les dimensions et la position
   *
   * @param pn le panel
   * @param dialogOption voir les options de CtuluDialog (CtuluDialog.QUIT_OPTION,....)
   * @param table la table
   * @param dialogName le nom du dialogue utilise pour la persistence et l'aide
   * @param dialogTitle le titre du dialogue
   * @param versionForPersistence la version a utiliser pour la persistence. Peut etre vide
   */
  private static void showDialogAndTable(final CtuluDialogPanel pn, final int dialogOption,
                                         final JXTable table, final String dialogName, final String dialogTitle, final String versionForPersistence) {
    final JFrame mainWindow = (JFrame) WindowManager.getDefault().getMainWindow();
    final CtuluDialog s = pn.createDialog(mainWindow);
    s.setInitParent(mainWindow);
    s.setOption(dialogOption);
    s.setTitle(dialogTitle);
    s.setName(dialogName);

    s.afficheDialogModal(() -> {
      UserPreferencesSaver.loadDialogLocationAndDimension(s, versionForPersistence);
      loadTablePreferencesLater(table);
    });
    UserPreferencesSaver.saveLocationAndDimension(s, versionForPersistence);
    UserPreferencesSaver.saveTablePreferences(table);
  }

  /**
   * @return tableau a 2 dimensions extrait du clipboard
   */
  public static String[][] getStringsFromClipboard() throws UnsupportedFlavorException, IOException {
    //on récupère les données du clipboard
    final String toCopy = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
    //on les parse
    String[][] parse = CSVParser.parse(toCopy, '\t');
    //il se peut que les données copiées dans le clipboard soient séparées par des ;
    if (toCopy.indexOf(';') > 0 && parse.length > 0 && parse[0].length == 0) {
      parse = CSVParser.parse(toCopy, ';');
    }
    return parse;
  }

  private static void loadTablePreferencesLater(final JXTable table) {
    EventQueue.invokeLater(() -> UserPreferencesSaver.loadTablePreferences(table));
  }

  /**
   * Utilise un icon d'aide et l'align en haut.
   * @param jLabelMessage le label a modifier
   */
  public static void initilalizeHelpLabel(final JLabel jLabelMessage) {
    jLabelMessage.setIcon(ImageUtilities.loadImageIcon("org/openide/resources/propertysheet/propertySheetHelp.png", false));
    jLabelMessage.setVerticalAlignment(JLabel.TOP);
    jLabelMessage.setVerticalTextPosition(JLabel.TOP);
    jLabelMessage.setAlignmentY(Component.TOP_ALIGNMENT);
  }
}
