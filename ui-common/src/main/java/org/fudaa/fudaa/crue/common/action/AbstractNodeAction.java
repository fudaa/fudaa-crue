package org.fudaa.fudaa.crue.common.action;

import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.actions.NodeAction;

/**
 * Une NodeAction qui est synchrone et qui permet de tester si les noeuds sélectionnés sont vides ou non.
 *
 * @author deniger
 */
public abstract class AbstractNodeAction extends NodeAction {
    private final String name;

    public AbstractNodeAction(final String name) {
        this.name = name;
    }

    @Override
    protected boolean asynchronous() {
        return false;
    }

    @Override
    public String getName() {
        return name;
    }

    protected boolean isEnableWithoutActivatedNodes() {
        return false;
    }

    protected boolean preEnable(final Node[] activatedNodes) {
        if (!isEnableWithoutActivatedNodes()) {
            return activatedNodes == null || activatedNodes.length != 0;
        }
        return true;
    }

    @Override
    protected final boolean enable(final Node[] activatedNodes) {
        if (preEnable(activatedNodes)) {
            return isEnable(activatedNodes);
        }
        return false;
    }

    /**
     * @param activatedNodes non null et de taille &gt;=1
     * @return true is enabled
     */
    protected abstract boolean isEnable(Node[] activatedNodes);

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }
}
