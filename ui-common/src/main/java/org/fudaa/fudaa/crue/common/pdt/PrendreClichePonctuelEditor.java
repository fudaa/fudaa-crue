/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.pdt;

import java.awt.Component;
import java.beans.*;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.metier.emh.PrendreClichePonctuel;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.nodes.Node;

/**
 *
 * @author Frédéric Deniger
 */
public class PrendreClichePonctuelEditor extends PropertyEditorSupport implements ExPropertyEditor, PropertyChangeListener {

  static void registerEditor() {
    PropertyEditorManager.registerEditor(PrendreClichePonctuel.class, PrendreClichePonctuelEditor.class);
  }
  PropertyEnv env;

  @Override
  public void attachEnv(PropertyEnv env) {
    this.env = env;
    env.addPropertyChangeListener(this);
    env.setState(PropertyEnv.STATE_NEEDS_VALIDATION);
    FeatureDescriptor featureDescriptor = env.getFeatureDescriptor();
    featureDescriptor.setValue("suppressCustomEditor", Boolean.FALSE);
    PropertyCrueUtils.configureNoEditAsText(featureDescriptor);
  }
  PrendreClichePonctuelPanel prendreClichePonctuelPanel;

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (PropertyEnv.PROP_STATE.equals(evt.getPropertyName())
            && evt.getNewValue() == PropertyEnv.STATE_VALID) {
      setValue(prendreClichePonctuelPanel.getCliche());
    }
  }

  @Override
  public boolean supportsCustomEditor() {
    return true;
  }

  @Override
  public Component getCustomEditor() {
    if (prendreClichePonctuelPanel == null) {
      prendreClichePonctuelPanel = new PrendreClichePonctuelPanel();
    }
    Node.Property featureDescriptor = (Node.Property) env.getFeatureDescriptor();
    prendreClichePonctuelPanel.setCliche((PrendreClichePonctuel) getValue());
    prendreClichePonctuelPanel.setEditable(featureDescriptor.canWrite());
    return prendreClichePonctuelPanel;
  }

  @Override
  public String getAsText() {
    PrendreClichePonctuel value = (PrendreClichePonctuel) getValue();
    if (value == null) {
      return StringUtils.EMPTY;
    }
    return value.getNomFic() + " " + DateDurationConverter.durationToIso(value.getTempsSimu());
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
  }
}
