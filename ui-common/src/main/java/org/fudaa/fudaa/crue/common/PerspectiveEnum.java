package org.fudaa.fudaa.crue.common;

/**
 * @author deniger
 */
public enum PerspectiveEnum {

    STUDY("etude"),
    MODELLING("modelisation"),
    POST("compteRendu"),
    REPORT("rapport"),
    TEST("otfa"),
    AOC("aoc");

    private final String helpId;

    PerspectiveEnum(String helpId) {
        this.helpId = helpId;
    }

    public String getHelpId() {
        return helpId;
    }
}
