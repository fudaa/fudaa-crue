package org.fudaa.fudaa.crue.common.config;

import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.ctulu.converter.ToStringTransformerFactory;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.openide.nodes.Node.Property;
import org.openide.nodes.PropertySupport;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class ConfigurationInfoHelper {

  public static String getGetterMethod(String propName, Class type) {
    if (Boolean.TYPE.equals(type)) {
      return "is" + StringUtils.capitalize(propName);
    }
    return "get" + StringUtils.capitalize(propName);
  }

  public static String getSetterMethod(String propName, Class type) {
    return "set" + StringUtils.capitalize(propName);
  }

  public static PropertySupport.Reflection create(String prop, Class type, Object in, Class i18nClass) throws NoSuchMethodException {
    return create(prop, type, in, prop, i18nClass);
  }

  public static PropertySupport.Reflection create(String prop, Class type, Object in, String i18nCode, Class i18nClass) throws NoSuchMethodException {

    PropertySupport.Reflection res = new PropertySupport.Reflection(in, type, getGetterMethod(prop, type), getSetterMethod(prop,
            type));
    res.setDisplayName(NbBundle.getMessage(i18nClass, i18nCode + ".DisplayName"));
    res.setShortDescription(NbBundle.getMessage(i18nClass, i18nCode + ".ShortDescription"));
    PropertyCrueUtils.installForColorFont(res);
    return res;
  }

  public static CtuluLog setAndValidReadProperties(Map<String, String> properties, LinkedHashMap<String, Property> initProperties, CtuluLogLevel level) {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    log.setDesc("configEtu.load");
    Map<Class, AbstractPropertyToStringTransformer> createTransformers = ToStringTransformerFactory.createTransformers();
    for (Map.Entry<String, String> readProperties : properties.entrySet()) {
      String key = readProperties.getKey();
      String value = readProperties.getValue();
      Property property = initProperties.get(key);
      if (property == null) {
        log.addRecord(level, "configEtu.unknownProperty.Error", key);
      } else {
        AbstractPropertyToStringTransformer transformer = createTransformers.get(property.getValueType());
        if (transformer == null) {
          log.addRecord(level, "configEtu.unknownType.Error", property.getValueType());
        } else if (!transformer.isValid(value)) {
          log.addRecord(level, "configEtu.badValue.Error", key, value);
        } else {
          try {
            property.setValue(transformer.fromString(value));
          } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
            log.addRecord(level, "configEtu.valueCantBeSet.Error", key, value);
          }
        }
      }
    }
    return log;
  }
}
