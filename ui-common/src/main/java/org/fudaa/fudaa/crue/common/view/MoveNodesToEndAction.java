/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.view;

import java.awt.event.ActionEvent;
import java.util.Arrays;
import javax.swing.AbstractAction;
import org.apache.commons.lang3.ArrayUtils;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 *
 * @author Frédéric Deniger
 */
public class MoveNodesToEndAction extends AbstractAction {

  final Node[] activatedNodes;

  public MoveNodesToEndAction(Node[] activatedNodes) {
    super(NbBundle.getMessage(MoveNodesToEndAction.class, "MoveNodeToEnd.DisplayName"));
    this.activatedNodes = activatedNodes;
    setEnabled(ArrayUtils.isNotEmpty(activatedNodes));
    if (isEnabled()) {
      final Node node = activatedNodes[0];
      if (node instanceof AbstractNodeFirable) {
        setEnabled(((AbstractNodeFirable) node).isEditMode());
      }
    }
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    DefaultNodePasteType.nodeToEnd(Arrays.asList(activatedNodes), DefaultNodePasteType.isIndexUsedAsDisplayName(activatedNodes));
  }
}
