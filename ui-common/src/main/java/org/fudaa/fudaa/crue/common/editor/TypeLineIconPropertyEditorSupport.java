package org.fudaa.fudaa.crue.common.editor;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.beans.PropertyEditorSupport;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * Editor tracant les lignes.
 * @author deniger
 */
public class TypeLineIconPropertyEditorSupport extends PropertyEditorSupport {

  final TraceLigne ligne;

  public TypeLineIconPropertyEditorSupport() {
    TraceLigneModel icon = new TraceLigneModel(TraceLigne.INVISIBLE, 1, Color.BLACK);
    ligne = new TraceLigne(icon);
  }

  @Override
  public boolean isPaintable() {
    return true;
  }

  @Override
  public void paintValue(Graphics gfx, Rectangle box) {
    Integer value = (Integer) getValue();
    ligne.setTypeTrait(value);
    final int middle = box.y + box.height / 2;
    ligne.dessineTrait((Graphics2D) gfx, box.x, middle, box.x + box.width, middle);
  }
}
