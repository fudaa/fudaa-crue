package org.fudaa.fudaa.crue.common.log.property;

import java.awt.Component;
import java.util.MissingResourceException;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import org.fudaa.fudaa.crue.common.log.CtuluLogsTopComponent;
import org.fudaa.fudaa.crue.common.log.filter.NodeQuickFilterEmptyValue;
import org.fudaa.fudaa.crue.common.log.filter.NodeQuickFilterEqualsTo;
import org.fudaa.fudaa.crue.common.log.filter.NodeQuickFilterNotEmptyValue;
import org.netbeans.swing.etable.ETable;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class PropertyColumnFilterable extends PropertyColumnDefault {

  public void createFilterMenu(JPopupMenu res, Component component, int row, int column) throws MissingResourceException {
    JMenu menu = new JMenu(NbBundle.getMessage(PropertyColumnFilterable.class, "filter.mainMenuName", displayName));
    res.add(menu);
    ETable et = (ETable) component;
    if (et.getRowCount() > 0) {
      CtuluLogsTopComponent logs = (CtuluLogsTopComponent) ((JComponent) component).getClientProperty(
              CtuluLogsTopComponent.class.getSimpleName());
      menu.add(create(new NodeQuickFilterEmptyValue(columnId, displayName), logs));
      menu.add(create(new NodeQuickFilterNotEmptyValue(columnId, displayName), logs));
      Object valueAt = et.getValueAt(row, column);
      if (valueAt instanceof Node.Property) {
        try {
          valueAt = ((Node.Property) valueAt).getValue();
        } catch (Exception ex) {
          Exceptions.printStackTrace(ex);
        }
      }
      menu.add(create(new NodeQuickFilterEqualsTo(columnId, displayName, valueAt), logs));
    }
  }

  @Override
  public JPopupMenu createPopupMenu(int row, int column, Node[] selectedNodes, Component component) {
    JPopupMenu res = super.createPopupMenu(row, column, selectedNodes, component);
    res.addSeparator();
    createFilterMenu(res, component, row, column);
    return res;
  }
}
