package org.fudaa.fudaa.crue.common.property;

import java.awt.Color;
import java.awt.Font;
import java.beans.FeatureDescriptor;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.fudaa.crue.common.CommonMessage;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;
import org.openide.nodes.Node.Property;
import org.openide.util.NbBundle;

/**
 *
 * @author Fred Deniger
 */
public class PropertyCrueUtils {

  public static final String TYPE_NAME = "Type";
  public static final String COMMENTAIRE_NAME = "Commentaire";

  protected static void setNameAndDisplayName(Node.Property property, String name, String displayName) {
    property.setName(name);
    property.setDisplayName(displayName);
  }

  public static void setNullValueEmpty(Node.Property node) {
    node.setValue("nullValue", StringUtils.EMPTY);
  }

  public static void setNullValue(Node.Property node, String value) {
    node.setValue("nullValue", value);
  }

  protected static void setNameAndDisplayName(Node.Property property, String name) {
    setNameAndDisplayName(property, name, CommonMessage.class);
  }

  protected static void setNameAndDisplayName(Node.Property property, String name, Class bundleClass) {
    property.setName(name);
    property.setDisplayName(NbBundle.getMessage(bundleClass, name));
  }

  public static void addProperty(OutlineView view, String property) {
    view.addPropertyColumn(property, NbBundle.getMessage(CommonMessage.class, property));
  }

  public static void addProperty(OutlineView view, String property, Class bundleClass) {
    addProperty(view, property, bundleClass, property);
  }

  public static void addProperty(OutlineView view, String property, Class bundleClass, String bundleKey) {
    view.addPropertyColumn(property, NbBundle.getMessage(bundleClass, bundleKey));
  }

  public static void configureNoCustomEditor(FeatureDescriptor typeProp) {
    typeProp.setValue("suppressCustomEditor", Boolean.TRUE);
  }

  public static void configureCustomEditor(FeatureDescriptor typeProp) {
    typeProp.setValue("suppressCustomEditor", Boolean.FALSE);
  }

  public static void configureNotSortable(FeatureDescriptor typeProp) {
    typeProp.setValue("SortableColumn", Boolean.FALSE);
  }

  public static void configureNoEditAsText(FeatureDescriptor typeProp) {
    typeProp.setValue("canEditAsText", Boolean.FALSE);
  }

  public static final void installForColorFont(Property prop) {
    if (prop.getValueType().equals(Font.class) || (prop.getValueType().equals(Color.class))) {
      PropertyCrueUtils.configureNoEditAsText(prop);
      PropertyCrueUtils.configureCustomEditor(prop);
    }
  }
}
