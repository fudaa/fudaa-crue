/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.log.property;

import java.lang.reflect.InvocationTargetException;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.openide.nodes.PropertySupport;
import org.openide.util.NbBundle;

/**
 *
 * @author Fred Deniger
 */
public class FileProperty extends PropertySupport.ReadOnly<String> {

  public static final String ID = "File";
  private final CtuluLogRecord record;

  public FileProperty(CtuluLogRecord record) {
    super(ID, String.class, getDefaultDisplayName(),
          getDescription());
    this.record = record;
    PropertyCrueUtils.configureNoCustomEditor(this);
  }

  public static String getDescription() {
    return NbBundle.getMessage(FileProperty.class, "FilePropertyDescription");
  }

  public static String getDefaultDisplayName() {
    return NbBundle.getMessage(FileProperty.class, "FilePropertyName");
  }
  
   public static PropertyColumnFilterable createColumn() {
    PropertyColumnFilterable res = new PropertyColumnFilterable();
    res.setColumnId(ID);
    res.setDescription(getDescription());
    res.setDisplayName(getDefaultDisplayName());
    res.setVisibleByDefault(false);
    return res;
  }

  @Override
  public String toString() {
    return record.getRessource();
  }

  @Override
  public String getValue() throws IllegalAccessException, InvocationTargetException {
    return toString();
  }
}
