/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.view;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.tree.TreePath;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.netbeans.swing.outline.OutlineModel;

/**
 *
 * @author Frederic Deniger
 */
public class ExpandedNodesManager {

  private final OutlineModel model;
  final Set<String> savedPath = new HashSet<>();

  public ExpandedNodesManager(OutlineModel model) {
    this.model = model;
  }

  public void saveState() {
    savedPath.clear();
    final TreePath pathForRow = new TreePath(model.getRoot());
    TreePath[] expandedDescendants = model.getTreePathSupport().getExpandedDescendants(pathForRow);
    for (TreePath treePath : expandedDescendants) {
      final String toString = toString(treePath);
      savedPath.add(toString);
    }
  }
  Set<String> donePath;

  private void restoreState(TreePath parentPath) {
    final String toStringReload = toString(parentPath);
    if (donePath.contains(toStringReload)) {
      return;
    }
    donePath.add(toStringReload);
    if (savedPath.contains(toStringReload)) {
      model.getTreePathSupport().expandPath(parentPath);
    }
    Enumeration<TreePath> visiblePathsFrom = model.getLayout().getVisiblePathsFrom(parentPath);
    List<TreePath> paths = new ArrayList<>();
    if (visiblePathsFrom != null) {
      CollectionUtils.addAll(paths, visiblePathsFrom);
    }

    for (TreePath path : paths) {
      if (parentPath != path) {
        restoreState(path);
      }
    }
  }

  public void restoreState() {
    donePath = new HashSet<>();
    TreePath pathForRow = model.getLayout().getPathForRow(0);
    restoreState(pathForRow);
    savedPath.clear();
    donePath = null;
  }

  private String toString(TreePath treePath) {
    if (treePath == null) {
      return StringUtils.EMPTY;
    }
    Object[] path = treePath.getPath();
    if (path == null || path.length == 0) {
      return StringUtils.EMPTY;
    }
    StringBuilder builder = new StringBuilder();
    for (Object object : path) {
      builder.append('~').append(ObjectUtils.toString(object, "<NULL>"));
    }
    return builder.toString();
  }

  public boolean isNotEmpty() {
    return !savedPath.isEmpty();
  }
}
