package org.fudaa.fudaa.crue.common;

import com.jidesoft.swing.JideButton;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.action.ActionsInstaller;
import org.fudaa.ctulu.action.SwitchMinMaxDialogAction;
import org.fudaa.ctulu.border.BorderWithIconInstaller;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.UserSaveAnswer;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.fudaa.fudaa.crue.common.services.TopComponentOpenCloseHelper;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.awt.*;
import java.util.MissingResourceException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author deniger
 */
public abstract class AbstractTopComponent extends TopComponent implements TopComponentOpenCloseHelper.Target {
  protected JButton buttonSave;
  protected JButton buttonCancel;
  /**
   * indique si ce TopComponent doit être persisté.
   */
  private boolean isMainTopComponent = true;
  private JPanel cancelSavePanel;

  public AbstractTopComponent() {
    setFocusable(true);
    if (!GraphicsEnvironment.isHeadless()) {
      final SwitchMinMaxDialogAction installAndCreateSwitchAction = ActionsInstaller.installAndCreateSwitchAction(this);
      BorderWithIconInstaller.install(this, installAndCreateSwitchAction, true);
    }
  }

  public static void updateValidState(final JLabel labelDisplayingErrors, final String nonValidMessage) {
    if (nonValidMessage == null) {
      labelDisplayingErrors.setText(StringUtils.EMPTY);
      labelDisplayingErrors.setIcon(null);
    } else {
      labelDisplayingErrors.setText(nonValidMessage);
      labelDisplayingErrors.setIcon(LogIconTranslationProvider.getIcon(CtuluLogLevel.FATAL));
    }
  }

  protected abstract String getHelpCtxId();

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(getHelpCtxId());
  }

  @Override
  public void open() {
    super.open();
    final Description info = getClass().getAnnotation(Description.class);
    final String findTopComponentID = WindowManager.getDefault().findTopComponentID(this);
    if (info != null) {
      isMainTopComponent = findTopComponentID.equals(info.preferredID());
    } else {
      Logger.getLogger(AbstractTopComponent.class.getName()).log(Level.SEVERE, "no description for {0}", findTopComponentID);
    }
  }

  public void forceSave() {
    if (isModified()) {
      final String title = NbBundle.getMessage(AbstractTopComponent.class, "ValidationVue.DialogTitle");
      final String message = NbBundle.getMessage(AbstractTopComponent.class, "ValidationVue.DialogMessage");
      final String save = NbBundle.getMessage(AbstractTopComponent.class, "ValidationVue.ValidAction");
      final String dontSave = NbBundle.getMessage(AbstractTopComponent.class, "ValidationVue.CancelAction");
      final Object showQuestion = DialogHelper.showQuestion(title, message, new String[]{save, dontSave});
      if (save.equals(showQuestion)) {
        valideModification();
      } else {
        cancelModification();
      }
    }
  }

  /**
   * si modifié, demande à l'utilisateur une sauvegarde.
   *
   * @return null si non modifié
   * @throws MissingResourceException si pas de traduction trouvée.
   */
  protected UserSaveAnswer askForSave() {
    return askForSave(NbBundle.getMessage(AbstractTopComponent.class, "ValidationVue.DontChange"));
  }

  /**
   * si modifié, demande à l'utilisateur une sauvegarde.
   *
   * @return null si non modifié
   * @throws MissingResourceException si pas de traduction trouvée.
   */
  protected UserSaveAnswer askForSave(final String cancel) {
    UserSaveAnswer confirmSaveOrNot = null;
    if (isModified()) {
      final String title = NbBundle.getMessage(AbstractTopComponent.class, "ValidationVue.DialogTitle");
      final String save = NbBundle.getMessage(AbstractTopComponent.class, "ValidationVue.ValidAction");
      final String dontSave = NbBundle.getMessage(AbstractTopComponent.class, "ValidationVue.CancelAction");
      final String message = NbBundle.getMessage(AbstractTopComponent.class, "ValidationVue.DialogMessage");
      confirmSaveOrNot = DialogHelper.confirmSaveOrNot(
        title, message, save, dontSave, cancel);

      if (UserSaveAnswer.CANCEL.equals(confirmSaveOrNot)) {
        EventQueue.invokeLater(this::requestActive);
      } else if (UserSaveAnswer.SAVE.equals(confirmSaveOrNot)) {
        if (!valideModification()) {
          confirmSaveOrNot = UserSaveAnswer.CANCEL;
          EventQueue.invokeLater(this::requestActive);
        }
      } else if (UserSaveAnswer.DONT_SAVE.equals(confirmSaveOrNot)) {
        cancelModification();
      }
    }
    return confirmSaveOrNot;
  }

  /**
   * Mehode a redéfinir pour donner l'info sur l'état modifie ou pas de la fenetre
   *
   * @return true si modifié
   */
  protected boolean isModified() {
    return false;
  }

  /**
   * Methode a redéfinir pour annuler les moifications sur la fenetre
   */
  public void cancelModification() {
  }

  /**
   * Methode a redéfinir pour valider les moifications sur la fenetre
   */
  public boolean valideModification() {
    return true;
  }

  @Override
  public int getPersistenceType() {
    if (!isMainTopComponent) {
      return PERSISTENCE_NEVER;
    }
    return super.getPersistenceType();
  }

  protected JPanel createCancelSaveButtons(final JComponent first) {
    if (cancelSavePanel == null) {
      buttonSave = new JideButton();
      buttonSave.setIcon(ImageUtilities.image2Icon(ImageUtilities.loadImage("org/fudaa/fudaa/crue/common/icons/accepted.png")));
      buttonSave.setToolTipText(NbBundle.getMessage(AbstractTopComponent.class, "ValidationVue.ValidAction"));
      buttonSave.addActionListener(e -> valideModification());
      buttonCancel = new JideButton();
      buttonCancel.addActionListener(e -> cancelModification());
      buttonCancel.setIcon(ImageUtilities.image2Icon(ImageUtilities.loadImage("org/fudaa/fudaa/crue/common/icons/cancel.png")));
      buttonCancel.setToolTipText(NbBundle.getMessage(AbstractTopComponent.class, "ValidationVue.CancelAction"));
      cancelSavePanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 5));
      if (first != null) {
        cancelSavePanel.add(first);
      }
      cancelSavePanel.add(buttonSave);
      cancelSavePanel.add(buttonCancel);
      buttonCancel.setEnabled(isModified());
      buttonSave.setEnabled(isModified());
    }
    return cancelSavePanel;
  }

  @Override
  protected final void componentClosed() {
    new TopComponentOpenCloseHelper().componentClosedHandler(this);
  }
}
