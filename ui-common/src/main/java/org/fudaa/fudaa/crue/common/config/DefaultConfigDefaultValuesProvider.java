/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.config;

import org.fudaa.dodico.crue.edition.bean.CreationDefaultValue;

/**
 *
 * @author Frederic Deniger
 */
public class DefaultConfigDefaultValuesProvider implements ConfigDefaultValuesProvider {

  @Override
  public CreationDefaultValue getDefaultValues() {
    return new CreationDefaultValue();
  }
}
