/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.SwingUtilities;
import org.fudaa.fudaa.crue.common.services.SelectedPerspectiveService;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Item;
import org.openide.util.lookup.Lookups;

@ActionID(category = "Window",
id = "org.fudaa.fudaa.crue.common.ResetWindowsAction")
@ActionRegistration(displayName = "#CTL_ResetWindowsAction")
@ActionReferences({
  @ActionReference(path = "Menu/Window", position = 3333, separatorBefore = 3332)
})
public final class ResetWindowsAction implements ActionListener {

  final SelectedPerspectiveService service = Lookup.getDefault().lookup(SelectedPerspectiveService.class);

  @Override
  public void actionPerformed(ActionEvent e) {
    final String folder = "Actions/Window/";
    final String actionId = folder + "org-netbeans-core-windows-actions-ResetWindowsAction";
    Lookup.Template<ActionListener> template = new Lookup.Template<>(ActionListener.class, actionId, null);
    Item<ActionListener> lookupAll = Lookups.forPath(folder).lookupItem(template);
    if (lookupAll != null) {
      lookupAll.getInstance().actionPerformed(null);
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          service.rebuildCurrentPerspective();
        }
      });

    }
  }
}
