/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.services;

import org.openide.windows.TopComponent;

/**
 *
 * @author Frederic Deniger
 */
public class TopComponentActive {

  private final TopComponent topComponent;
  private final boolean active;

  public TopComponentActive(TopComponent topComponent, boolean active) {
    this.topComponent = topComponent;
    this.active = active;
  }

  public TopComponent getTopComponent() {
    return topComponent;
  }

  public boolean isActive() {
    return active;
  }
}
