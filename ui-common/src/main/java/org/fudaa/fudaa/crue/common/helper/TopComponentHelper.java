package org.fudaa.fudaa.crue.common.helper;

import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.swing.Action;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 * Classe des méthodes "Helpers" pour les TopComponent
 *
 * @author Yoan GRESSIER
 */
public class TopComponentHelper {

  /**
   * Fixe les propriétés (communes) des vues principales (Scénarios, Vue planimétrique, Compte-Rendus, Vue planimétrique, Tests)
   * de chaque perspectives (Études, Modélisation, Compte-rendu, Rapports, OTFA)
   *
   * @param tc référence vers la vue à laquelle il faut appliquer les propriétés
   */
  public static void setMainViewProperties(TopComponent tc) {
    tc.putClientProperty(TopComponent.PROP_CLOSING_DISABLED, Boolean.TRUE);
    tc.putClientProperty(TopComponent.PROP_DRAGGING_DISABLED, Boolean.TRUE);
    tc.putClientProperty(TopComponent.PROP_MAXIMIZATION_DISABLED, Boolean.TRUE);
    tc.putClientProperty(TopComponent.PROP_UNDOCKING_DISABLED, Boolean.TRUE);
  }

  /**
   * Retourne les actions communes des vues principales (Scénarios, Vue planimétrique, Compte-Rendus, Vue planimétrique, Tests)
   * de chaque perspectives (Études, Modélisation, Compte-rendu, Rapports, OTFA).
   *
   * @return les actions communes des vues principales
   */
  public static Action[] getMainViewActions() {
    return null;
  }

  /**
   *
   * @param topComponentToCheck le topComponent qui doit être un type {@link org.openide.explorer.ExplorerManager.Provider}.
   * @return les noeuds sélectionés dans le topComponent. null si pas de sélection
   * @see org.openide.explorer.ExplorerManager.Provider
   * @see org.openide.explorer.ExplorerManager.Provider#getExplorerManager()
   * @see org.openide.explorer.ExplorerManager#getSelectedNodes()
   */
  public static Node[] getSelectedNode(TopComponent topComponentToCheck) {
    if (topComponentToCheck instanceof ExplorerManager.Provider) {
      final ExplorerManager explorerManager = ((ExplorerManager.Provider) topComponentToCheck).getExplorerManager();
      if (explorerManager != null) {
        return explorerManager.getSelectedNodes();
      }
    }
    return null;
  }

  /**
   *
   * @return cle=TopComponent unique id et valeurs liste des TopComponent ouverts.
   * @see org.openide.windows.TopComponent#getRegistry()
   */
  public static Map<String, Collection<TopComponent>> getOpenedTopComponentById() {
    Set<TopComponent> opened = TopComponent.getRegistry().getOpened();
    Map<String, Collection<TopComponent>> res = new HashMap<>();
    for (TopComponent topComponent : opened) {
      String id = WindowManager.getDefault().findTopComponentID(topComponent);
      Collection<TopComponent> tcs = res.get(id);
      if (tcs == null) {
        tcs = new ArrayList<>();
        res.put(id, tcs);
      }
      tcs.add(topComponent);
    }
    return res;
  }

  /**
   *
   * @param topComponentToClear Le TopComponent dont la sélection doit être effacée.
   */
  public static void clearSelection(TopComponent topComponentToClear) {
    if (topComponentToClear instanceof ExplorerManager.Provider) {
      try {
        final ExplorerManager explorerManager = ((ExplorerManager.Provider) topComponentToClear).getExplorerManager();
        if (explorerManager != null) {
          explorerManager.setSelectedNodes(new Node[0]);
        }
      } catch (PropertyVetoException ex) {
        Exceptions.printStackTrace(ex);
      }
    }
  }

  /**
   *
   * @param findTopComponent (non null) le TopComponent à sélectionner et à initialiser avec la sélection <code>saved</code>
   * @param newNodesSelection la sélection de noeuds à appliquer
   */
  public static void activateTopComponentAndSetSelection(TopComponent findTopComponent, Node[] newNodesSelection) {
    findTopComponent.requestActive();
    if (findTopComponent instanceof ExplorerManager.Provider) {
      try {
        final ExplorerManager explorerManager = ((ExplorerManager.Provider) findTopComponent).getExplorerManager();
        if (explorerManager != null) {
          explorerManager.setSelectedNodes(newNodesSelection);
        }
      } catch (PropertyVetoException ex) {
        Exceptions.printStackTrace(ex);
      }
    }
  }

}
