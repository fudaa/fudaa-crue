/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.log.property;

import java.lang.reflect.InvocationTargetException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.openide.nodes.PropertySupport;
import org.openide.util.NbBundle;

/**
 *
 * @author Fred Deniger
 */
public class EntiteProperty extends PropertySupport.ReadOnly<String> {

  public static final String ID = "Entity";
  private final CtuluLogRecord record;

  public EntiteProperty(CtuluLogRecord record) {
    super(ID, String.class, getDefaultDisplayName(),
            getDescription());
    this.record = record;
    PropertyCrueUtils.configureNoCustomEditor(this);
  }

  public static String getDescription() {
    return NbBundle.getMessage(EntiteProperty.class, "EntityPropertyDescription");
  }

  public static String getDefaultDisplayName() {
    return NbBundle.getMessage(EntiteProperty.class, "EntityPropertyName");
  }

  public static PropertyColumnFilterable createColumn() {
    PropertyColumnFilterable res = new PropertyColumnFilterable();
    res.setColumnId(ID);
    res.setDescription(getDescription());
    res.setDisplayName(getDefaultDisplayName());
    return res;
  }

  @Override
  public String toString() {
    Object[] args = record.getArgs();
    if (ArrayUtils.isNotEmpty(args)) {
      return (String) args[0];
    }
    return StringUtils.EMPTY;
  }

  @Override
  public String getValue() throws IllegalAccessException, InvocationTargetException {
    return toString();
  }
}
