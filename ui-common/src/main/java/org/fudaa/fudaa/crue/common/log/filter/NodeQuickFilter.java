package org.fudaa.fudaa.crue.common.log.filter;

import org.openide.nodes.Node;

/**
 *
 * @author deniger
 */
public interface NodeQuickFilter {

  boolean accept(Node node);
  
  /**
   * 
   * @return  the long description
   */
  String getDescription();
  /**
   * 
   * @return  the name to be used in menu
   */
  String getName();
}
