package org.fudaa.fudaa.crue.common.services;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.openide.util.Lookup;

/**
 *
 * Interface simple pour la récupérer le CrueConfigMetier (CCM) et l'étude chargé.
 *
 * @author Frederic Deniger
 */
public interface EMHProjetService extends Lookup.Provider {

  /**
   *
   * @return le CCM en cours d'utilisation
   */
  CrueConfigMetier getCcm();

  /**
   *
   * @return le project chargé (null si aucun)
   */
  EMHProjet getSelectedProject();


  boolean isReloading();

  CtuluLogGroup getLastLogsGroup();
}
