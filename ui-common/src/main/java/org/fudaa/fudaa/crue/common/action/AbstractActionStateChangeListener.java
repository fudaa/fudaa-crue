/*
 * @creation 2 oct. 2003
 * @modification $Date: 2006-09-19 14:55:55 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.fudaa.crue.common.action;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.WeakReference;
import javax.swing.AbstractButton;
/**
 * @author deniger
 * @version $Id: EbliSelectedChangeListener.java,v 1.6 2006-09-19 14:55:55 deniger Exp $
 */
public class AbstractActionStateChangeListener implements PropertyChangeListener {

  final WeakReference button_;

  /**
   * @param _b le bouton concerne
   *
   */
  public AbstractActionStateChangeListener(final AbstractButton _b) {
    button_ = new WeakReference(_b);
  }

  @Override
  public void propertyChange(final PropertyChangeEvent _evt){
    final Object o = button_.get();
    if (o == null) {
      return;
    }
    final AbstractButton bt = (AbstractButton) o;
    if ("selected".equals(_evt.getPropertyName())) {
      final boolean newValue = ((Boolean) _evt.getNewValue()).booleanValue();
      if (newValue != bt.isSelected()) {
        bt.setSelected(newValue);
      }
    }
  }
}
