package org.fudaa.fudaa.crue.common.view;

import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluLibArray;
import org.openide.nodes.Node;
import org.openide.nodes.NodeTransfer;
import org.openide.util.datatransfer.PasteType;

import java.awt.datatransfer.Transferable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author deniger
 */
public class DefaultNodePasteType extends PasteType {
    public static final String PROP_USE_INDEX_AS_DISPLAY_NAME = "useIndexAsDisplayName";
    final Node[] dragged;
    final Node target;

    public DefaultNodePasteType(final Node dragged, final Node target) {
        this.dragged = new Node[]{dragged};
        this.target = target;
    }

    public DefaultNodePasteType(final Node[] dragged, final Node target) {
        this.dragged = dragged;
        this.target = target;
    }

    public static Node getMovedNodeInTransferable(final Transferable t) {
        Node dragged = NodeTransfer.node(t, NodeTransfer.DND_COPY_OR_MOVE);
        if (dragged == null) {
            dragged = NodeTransfer.node(t, NodeTransfer.CLIPBOARD_CUT);
        }
        return dragged;
    }

    public static Node[] getMovedNodesInTransferable(final Transferable t) {
        Node[] dragged = NodeTransfer.nodes(t, NodeTransfer.DND_COPY_OR_MOVE);
        if (dragged == null) {
            dragged = NodeTransfer.nodes(t, NodeTransfer.CLIPBOARD_CUT);
        }
        return dragged;
    }

    public static boolean isIndexUsedAsDisplayName(final Node[] activatedNodes) {
        return Boolean.TRUE.equals(activatedNodes[0].getValue(PROP_USE_INDEX_AS_DISPLAY_NAME));
    }

    public static boolean isIndexUsedAsDisplayName(final Node activatedNode) {
        return Boolean.TRUE.equals(activatedNode.getValue(PROP_USE_INDEX_AS_DISPLAY_NAME));
    }

    public static void setIndexUsedAsDisplayName(final Node activatedNodes) {
        activatedNodes.setValue(PROP_USE_INDEX_AS_DISPLAY_NAME, Boolean.TRUE);
    }

    public static void nodeToEnd(final List<Node> toEnd, final boolean useIndexAsDisplayName) {
        if (CollectionUtils.isEmpty(toEnd)) {
            return;
        }
        final Node parentNode = toEnd.get(0).getParentNode();
        final List<Node> children = new ArrayList(Arrays.asList(parentNode.getChildren().getNodes()));
        parentNode.getChildren().remove(children.toArray(new Node[0]));
        children.removeAll(toEnd);
        children.addAll(toEnd);
        parentNode.getChildren().add(children.toArray(new Node[0]));
        if (useIndexAsDisplayName) {
            updateIndexedDisplayName(parentNode);
        }
    }

    public static void updateIndexedDisplayName(final Node parentNode) {
        final Node[] children = parentNode.getChildren().getNodes();
        int ordre = 0;
        for (final Node node : children) {
            final String displayName = Integer.toString(++ordre);
            if (!displayName.equals(node.getDisplayName())) {
                node.setDisplayName(displayName);
            }
        }
    }

    public static void nodeToStart(final List<Node> toStart, final boolean useIndexAsDisplayName) {
        if (CollectionUtils.isEmpty(toStart)) {
            return;
        }
        final Node parentNode = toStart.get(0).getParentNode();
        final List<Node> children = new ArrayList(Arrays.asList(parentNode.getChildren().getNodes()));
        parentNode.getChildren().remove(children.toArray(new Node[0]));
        children.removeAll(toStart);
        final List<Node> newChildren = new ArrayList<>();
        newChildren.addAll(toStart);
        newChildren.addAll(children);
        parentNode.getChildren().add(newChildren.toArray(new Node[0]));
        if (useIndexAsDisplayName) {
            updateIndexedDisplayName(parentNode);
        }
    }

    protected static void nodeDragged(final Node[] dragged, final Node target) {
        if (dragged == null || target == null || dragged.length == 0 || dragged[0] == null) {
            return;
        }
        final boolean useIndexAsDisplayName = isIndexUsedAsDisplayName(target);
        final Node parentNode = target.getParentNode();
        final List<Node> children = new ArrayList(Arrays.asList(parentNode.getChildren().getNodes()));
        for (final Iterator<Node> it = children.iterator(); it.hasNext(); ) {
            final Node node = it.next();
            if (node.getParentNode() != parentNode) {
                it.remove();
            }
        }
        parentNode.getChildren().remove(children.toArray(new Node[0]));
        final int targetIdx = children.indexOf(target);
        if (targetIdx < children.indexOf(dragged[0])) {
            CtuluLibArray.invert(dragged, 0);
        }
        for (final Node node : dragged) {
            children.remove(node);
            if (targetIdx >= children.size()) {
                children.add(node);
            } else {
                children.add(targetIdx, node);
            }
        }

        parentNode.getChildren().add(children.toArray(new Node[0]));
        if (useIndexAsDisplayName) {
            updateIndexedDisplayName(parentNode);
        }
    }

    @Override
    public Transferable paste() throws IOException {
        nodeDragged(dragged, target);
        return null;
    }
}
