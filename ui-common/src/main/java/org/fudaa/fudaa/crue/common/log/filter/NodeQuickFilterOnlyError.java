package org.fudaa.fudaa.crue.common.log.filter;

import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class NodeQuickFilterOnlyError extends AbstractNodeQuickFilter {

  public NodeQuickFilterOnlyError() {
    super(null);
    name = NbBundle.getMessage(NodeQuickFilterOnlyError.class, "filter.onlyError.name");
    description = NbBundle.getMessage(NodeQuickFilterOnlyError.class, "filter.onlyError.description");
  }

  @Override
  public boolean accept(final Node node) {
    if (node == null) {
      return true;
    }
    final CtuluLogRecord lookup = node.getLookup().lookup(CtuluLogRecord.class);
    if (lookup == null) {
      return true;
    }
    return acceptValue(lookup);
  }

  @Override
  protected boolean acceptValue(final Object value) {
    final CtuluLogRecord lookup = (CtuluLogRecord) value;
    return lookup.getLevel().isLessVerboseThan(CtuluLogLevel.ERROR, false);
  }
}
