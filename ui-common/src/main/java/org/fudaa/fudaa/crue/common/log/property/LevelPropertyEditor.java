package org.fudaa.fudaa.crue.common.log.property;

import java.beans.PropertyEditorSupport;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;

/**
 *
 * @author deniger
 */
public class LevelPropertyEditor extends PropertyEditorSupport {

  String msg;

  @Override
  public void setValue(Object value) {
    super.setValue(value);
    msg = LogIconTranslationProvider.getTranslation((CtuluLogLevel) value);
  }

  @Override
  public String getAsText() {
    return msg;
  }
}
