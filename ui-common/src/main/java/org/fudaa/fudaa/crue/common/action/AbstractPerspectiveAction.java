package org.fudaa.fudaa.crue.common.action;

import com.jidesoft.swing.JideButton;
import com.memoire.bu.BuComposedIcon;
import com.memoire.bu.BuEmptyIcon;
import org.fudaa.ctulu.gui.CtuluPopupListener.PopupReceiver;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.services.SelectedPerspectiveService;
import org.openide.util.*;
import org.openide.util.actions.BooleanStateAction;
import org.openide.util.lookup.Lookups;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.util.Collection;

public abstract class AbstractPerspectiveAction extends BooleanStateAction implements LookupListener {
  
  private final Lookup.Result result;
  private final SelectedPerspectiveService service = Lookup.getDefault().lookup(SelectedPerspectiveService.class);
  private final PerspectiveEnum perspectiveId;
  
  public PerspectiveEnum getPerspectiveId() {
    return perspectiveId;
  }
  
  public AbstractPerspectiveAction(final String textCode, final String perspectiveId, final PerspectiveEnum perspective) {
    putValue(NAME, NbBundle.getMessage(getClass(), textCode));
    putValue(SHORT_DESCRIPTION, getValue(NAME));
    this.perspectiveId = perspective;
    setBooleanState(false);
    result = service.getLookup().lookupResult(PerspectiveEnum.class);
    result.allItems();
    result.addLookupListener(this);
    service.register(perspective, perspectiveId);
  }
  
  @Override
  public final void setBooleanState(final boolean value) {
    super.setBooleanState(value);
    if (res != null) {
      res.setSelected(value);
      res.setContentAreaFilled(value);
    }
    
  }
  
  @Override
  public void resultChanged(final LookupEvent lookupEvent) {
    final boolean isSelectedPerspective = result.allInstances().contains(perspectiveId);
    setBooleanState(isSelectedPerspective);
  }
  
  @Override
  public String getName() {
    return (String) getValue(NAME);
  }
  
  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(getClass());
  }
  
  @Override
  public void actionPerformed(final ActionEvent ev) {
    setBooleanState(service.activePerspective(perspectiveId));
  }
  
  protected abstract String getFolderAction();
  private JideButton res;
  
  @Override
  public Component getToolbarPresenter() {
    res = new JideButton(this);
    
    final Icon initIcon = CrueIconsProvider.getIconFromFilename("menuDown.gif");
    final BuEmptyIcon emptyIcon = new BuEmptyIcon(6, 12);
    final BuComposedIcon rollerOverIcon = new BuComposedIcon(emptyIcon, initIcon);
    rollerOverIcon.setVerticalAlignement(SwingConstants.CENTER);
    res.setRolloverSelectedIcon(rollerOverIcon);
    res.setRolloverEnabled(true);
    res.setIconTextGap(2);
    res.setHorizontalTextPosition(SwingConstants.LEFT);
    res.setVerticalAlignment(SwingConstants.BOTTOM);
    res.setIcon(emptyIcon);
    final MenuPopupReceiver menuPopupReceiver = new MenuPopupReceiver(res);
    res.addActionListener(e -> {
      if (res.isSelected()) {
        menuPopupReceiver.showMenu();
      }
    });
    return res;
  }
  
  protected void addSpecificMenuAtEnd(final JPopupMenu menu) {
    
  }
  
  class MenuPopupReceiver implements PopupReceiver {
    
    private JPopupMenu menu;
    private final Component cp;
    
    public MenuPopupReceiver(final Component res) {
      this.cp = res;
    }
    
    @Override
    public void popup(final MouseEvent mouseEvent) {
      final int x = mouseEvent.getX();
      final int y = mouseEvent.getY();
      showMenu(x, y);
    }
    
    public void showMenu() {
      showMenu(0, cp.getHeight());
    }
    
    public void showMenu(final int x, final int y) {
      if (menu == null) {
        menu = new JPopupMenu();
        final Collection all = Lookups.forPath(getFolderAction()).lookupAll(Object.class);
        for (final Object object : all) {
          if (object instanceof AbstractAction) {
            menu.add((AbstractAction) object);
          } else if (object instanceof JSeparator) {
            menu.addSeparator();
          }
        }
        addSpecificMenuAtEnd(menu);
      }
      menu.show(cp, x, y);
      
      
    }
  }
}
