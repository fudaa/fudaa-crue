/*
 *  @creation     2 oct. 2003
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.crue.common.action;

import java.awt.event.ActionEvent;
import javax.swing.*;

/**
 * @author deniger
 * @version $Id$
 */
public abstract class AbstractActionState extends AbstractAction {

  private boolean selected_;

  public AbstractActionState(final String _name, final Icon _ic, final String _action) {
    super(_name, _ic);
    putValue(Action.ACTION_COMMAND_KEY, _action);

  }

  /**
   * Change the selection state and call the method changeSelectedState.
   */
  @Override
  public final void actionPerformed(final ActionEvent _ae) {
    changeAll();
  }

  public void changeAll() {
    setSelected(!isSelected());
    changeAction();
  }

  /**
   * Change only the state of the component : do not call the methode changeSelectedState.
   */
  public void setSelected(final boolean _b) {
    if (_b != selected_) {
      selected_ = _b;
      firePropertyChange("selected", selected_ ? Boolean.FALSE : Boolean.TRUE, selected_ ? Boolean.TRUE : Boolean.FALSE);
    }
  }

  /**
   * Appele quand l'etat selectionne ou non de l'action est modifie
   */
  public abstract void changeAction();

  public boolean isSelected() {
    return selected_;
  }

  private void decoreStateButton(final AbstractButton _r) {
    _r.setAction(this);
    _r.setSelected(isSelected());
    addPropertyChangeListener(new AbstractActionStateChangeListener(_r));
  }

  public AbstractButton buildCheckBox() {
    final AbstractButton r = new JCheckBox();
    decoreStateButton(r);
    r.setRolloverEnabled(true);
    r.setBorderPainted(false);
    return r;
  }
}
