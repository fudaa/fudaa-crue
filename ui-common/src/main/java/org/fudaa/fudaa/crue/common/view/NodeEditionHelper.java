/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.view;

import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

/**
 *
 * @author deniger
 */
public class NodeEditionHelper {

  public static void addNodesFromClipboard(final ExplorerManager explorerManager, List<? extends Node> contents) {
    final Children children = explorerManager.getRootContext().getChildren();
    Node[] selectedNodes = explorerManager.getSelectedNodes();
    final Node[] nodes = children.getNodes();
    children.remove(nodes);
    List<Node> currentNodes = new ArrayList<>(Arrays.asList(nodes));
    int insertPoint = -1;
    if (selectedNodes != null && selectedNodes.length > 0) {
      insertPoint = currentNodes.indexOf(selectedNodes[selectedNodes.length - 1]);
    }
    if (insertPoint < 0 || insertPoint >= currentNodes.size() - 1) {
      currentNodes.addAll(contents);
    } else {
      currentNodes.addAll(insertPoint, contents);
    }
    children.add(currentNodes.toArray(new Node[0]));
    try {
      explorerManager.setSelectedNodes(contents.toArray(new Node[0]));
    } catch (PropertyVetoException ex) {
      Exceptions.printStackTrace(ex);
    }
  }
}
