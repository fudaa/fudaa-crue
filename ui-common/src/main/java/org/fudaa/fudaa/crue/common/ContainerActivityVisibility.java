package org.fudaa.fudaa.crue.common;

import org.fudaa.dodico.crue.metier.emh.CatEMHConteneur;
import org.fudaa.dodico.crue.metier.emh.EMHModeleBase;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.EMHSousModele;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Stocke la visibilité d'objet EMH par leur uid et garde la propriété du sous-modele actif.
 *
 * @author deniger
 */
public class ContainerActivityVisibility {
    /**
     * l'uid du sous-modele actif
     */
    private Long sousModeleActifUid;
    /**
     * les uid des conteneurs visible.
     */
    private final Set<Long> containerVisible = new HashSet<>();

    /**
     * @param conteneur le conteneur
     * @return true si visible (affiché dans la vue planimetrie)
     */
    public boolean isVisible(CatEMHConteneur conteneur) {
        return containerVisible.contains(conteneur.getUiId());
    }

    public boolean isSousModeleVisible(EMHSousModele emhSousModele) {
        return isVisible(emhSousModele) && isVisible(emhSousModele.getParent());
    }

    /**
     * @param conteneurUid uid du conteneur
     * @return true si visible
     */
    public boolean isVisible(Long conteneurUid) {
        return containerVisible.contains(conteneurUid);
    }

    public Long getSousModeleActifUid() {
        return sousModeleActifUid;
    }

    /**
     * Change le sous-modele acti
     *
     * @param sousModeleActifUid l'uid du nouveau sous-modele actif
     */
    public void setSousModeleActifUid(Long sousModeleActifUid) {
        this.sousModeleActifUid = sousModeleActifUid;
    }

    /**
     * @param sousModele le sous-modele a tester
     * @return true si actif
     */
    public boolean isSousModeleActif(EMHSousModele sousModele) {
        return sousModeleActifUid != null && sousModeleActifUid.equals(sousModele.getUiId());
    }

    /**
     * le premier sous-modele trouvé est activé par defaut.
     *
     * @param scenario le scenario
     */
    public void initialize(EMHScenario scenario) {
        List<EMHModeleBase> modeles = scenario.getModeles();
        for (EMHModeleBase modele : modeles) {
            containerVisible.add(modele.getUiId());
            List<EMHSousModele> sousModeles = modele.getSousModeles();
            for (EMHSousModele sousModele : sousModeles) {
                if (sousModeleActifUid == null) {
                    sousModeleActifUid = sousModele.getUiId();
                }
                containerVisible.add(sousModele.getUiId());
            }
        }
    }

    /**
     * @param uiId    uid du conteneur
     * @param visible true si visible
     */
    public void setVisible(final Long uiId, boolean visible) {
        if (visible) {
            containerVisible.add(uiId);
        } else {
            containerVisible.remove(uiId);
        }
    }
}
