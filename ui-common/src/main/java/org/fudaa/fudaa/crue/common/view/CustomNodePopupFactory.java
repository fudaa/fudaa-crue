/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.view;

import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.NodePopupFactory;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author Frédéric Deniger
 */
public class CustomNodePopupFactory extends NodePopupFactory {

  private boolean addCollapseAllNode;
  private OutlineView view;

  public CustomNodePopupFactory() {
  }

  public CustomNodePopupFactory(final boolean addCollapseAllNode, final OutlineView view) {
    this.addCollapseAllNode = addCollapseAllNode;
    this.view = view;
  }

  public void setAddCollapseAllNode(final boolean addCollapseAllNode) {
    this.addCollapseAllNode = addCollapseAllNode;
  }

  public void setView(final OutlineView view) {
    this.view = view;
  }

  public boolean canMoveEndBeAdded(final Node[] selectedNodes) {
    return true;
  }

  public void addSpecificMenuItems(final JPopupMenu popupMenu, final int row, final int column, final Node[] selectedNodes, final Component component) {
  }

  @Override
  public JPopupMenu createPopupMenu(final int row, final int column, final Node[] selectedNodes, final Component component) {
    final JPopupMenu res = super.createPopupMenu(row, column, selectedNodes, component);
    addSpecificMenuItems(res, row, column, selectedNodes, component);
    if (canMoveEndBeAdded(selectedNodes)) {
      addMoveEndStart(res, selectedNodes);
    }
    if (addCollapseAllNode) {
      addCollapseAll(res, component);
    }
    return res;
  }

  private void addCollapseAll(final JPopupMenu menu, final Component component) {
    menu.addSeparator();
    final JMenuItem expandAll = menu.add(NbBundle.getMessage(CustomNodePopupFactory.class, "ExpandAllNode.ActionNode"));
    expandAll.addActionListener(e -> {
      final ExplorerManager find = ExplorerManager.find(view);
      if (find != null) {
        NodeHelper.expandAll(find, view);
      }
    });
    final JMenuItem collapseAll = menu.add(NbBundle.getMessage(CustomNodePopupFactory.class, "CollapseAllNode.ActionNode"));
    collapseAll.addActionListener(e -> {
      final ExplorerManager find = ExplorerManager.find(view);
      if (find != null) {
        NodeHelper.collapseAll(find, view);
      }
    });

  }

  private static void addMoveEndStart(final JPopupMenu res, final Node[] selectedNodes) {
    if (res.getComponentCount() > 0) {
      res.addSeparator();
    }
    res.add(new MoveNodesToStartAction(selectedNodes));
    res.add(new MoveNodesToEndAction(selectedNodes));
  }
}
