package org.fudaa.fudaa.crue.common.helper;

import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.TreeTableNode;

import javax.swing.tree.TreePath;
import java.util.List;

/**
 * @author deniger
 */
@SuppressWarnings("serial")
public class JXTreeTableExtended extends JXTreeTable {
    /**
     * @param treeModel le modele
     */
    protected JXTreeTableExtended(DefaultTreeTableModel treeModel) {
        super(treeModel);
    }

    public JXTreeTableExtended() {
    }

    @Override
    public DefaultTreeTableModel getTreeTableModel() {
        return (DefaultTreeTableModel) super.getTreeTableModel();
    }

    public void selectAndShow(TreeTableNode node) {
        selectAndShow(node, true);
    }

    public void selectAndShow(TreeTableNode node, boolean expand) {
        if (node == null) {
            return;
        }
        final TreePath path = new TreePath(getTreeTableModel().getPathToRoot(node));
        getTreeSelectionModel().setSelectionPath(path);
        if (expand) {
            expandPath(path);
        }
        scrollPathToVisible(path);
    }

    public void selectAndShow(List<TreeTableNode> node, boolean expand) {
        if (node == null) {
            return;
        }
        final TreePath[] paths = new TreePath[node.size()];
        for (int i = 0; i < node.size(); i++) {
            paths[i] = new TreePath(getTreeTableModel().getPathToRoot(node.get(i)));
        }
        getTreeSelectionModel().setSelectionPaths(paths);
        if (expand) {
            for (TreePath treePath : paths) {
                expandPath(treePath);
            }
        }
        for (TreePath treePath : paths) {
            scrollPathToVisible(treePath);
        }
    }
}
