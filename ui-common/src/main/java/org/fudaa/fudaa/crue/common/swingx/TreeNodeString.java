/**
 * 
 */
package org.fudaa.fudaa.crue.common.swingx;

import org.apache.commons.lang3.StringUtils;

/**
 *
 */
public class TreeNodeString extends AbstractTreeNode {
  final String id;

  public TreeNodeString(final String id) {
    super(null);
    this.id = id;
  }

  @Override
  public Object getValueAt(final int column) {
    if (column == 0) { return id; }
    return StringUtils.EMPTY;
  }

  @Override
  public boolean isActive() {
    return true;
  }

  @Override
  public boolean isEditable(final int column) {
    return false;
  }
}
