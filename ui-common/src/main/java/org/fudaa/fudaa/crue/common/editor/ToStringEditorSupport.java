package org.fudaa.fudaa.crue.common.editor;

import java.beans.PropertyEditorSupport;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author deniger
 */
public class ToStringEditorSupport extends PropertyEditorSupport {

  final Map<Object, String> i18nByValues;
  final Map<String, Object> valuesByi18n;

  public ToStringEditorSupport(Map<Object, String> translationByObject) {
    i18nByValues = translationByObject;
    valuesByi18n = new HashMap<>();
    for (Map.Entry<Object, String> entry : translationByObject.entrySet()) {
      valuesByi18n.put(entry.getValue(), entry.getKey());
    }
  }

  @Override
  public String getAsText() {
    return i18nByValues.get(getValue());
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    Object get = valuesByi18n.get(text);
    if (get != null) {
      setValue(get);
    }
  }
}
