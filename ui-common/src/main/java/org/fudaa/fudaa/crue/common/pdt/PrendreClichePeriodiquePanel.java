/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.pdt;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuGridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.emh.PdtCst;
import org.fudaa.dodico.crue.metier.emh.PrendreClichePeriodique;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class PrendreClichePeriodiquePanel extends JPanel {

  final JTextField tfNom;
  final DurationPanelEditor pdtCstEditor;

  public PrendreClichePeriodiquePanel() {
    setLayout(new BuGridLayout(2, 5, 5));
    setBorder(BuBorders.EMPTY3333);
    add(new JLabel(NbBundle.getMessage(PrendreClichePeriodiquePanel.class, "PrendreCliche.NomFichier")));
    tfNom = new JTextField(20);
    add(tfNom);
    pdtCstEditor = new DurationPanelEditor();
    add(new JLabel(NbBundle.getMessage(PrendreClichePeriodiquePanel.class, "PrendreCliche.PdtRes")));
    add(pdtCstEditor);

  }

  PrendreClichePeriodique getCliche() {
    String nom = tfNom.getText();
    if (StringUtils.isBlank(nom)) {
      return null;
    }
    return new PrendreClichePeriodique(nom, new PdtCst(pdtCstEditor.getDuration()));
  }

  void setCliche(PrendreClichePeriodique prendreClichePeriodique) {
    if (prendreClichePeriodique == null) {
      tfNom.setText(StringUtils.EMPTY);
      pdtCstEditor.setPdt(null);
    } else {
      tfNom.setText(prendreClichePeriodique.getNomFic());
      pdtCstEditor.setPdt(((PdtCst) prendreClichePeriodique.getPdtRes()).getPdtCst());
    }
  }

  void setEditable(boolean canWrite) {
    tfNom.setEditable(canWrite);
    pdtCstEditor.setEditable(canWrite);
  }
}
