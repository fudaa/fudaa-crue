package org.fudaa.fudaa.crue.common.property.listener;

/**
 * Interface pour des listeners qui ont une methode a appeler à l'enregistrement et une au desenregistrement.
 *
 * @see ListenerManager
 * @author Fred Deniger
 */
public interface UnregisterableListener {

  void register();

  void unregister();

}
