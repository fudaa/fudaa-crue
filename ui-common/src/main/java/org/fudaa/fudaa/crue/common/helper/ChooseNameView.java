/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.helper;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.validation.ValidationPatternHelper;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Permet de choisir rapidement un nouveau nom.
 *
 * @author Frederic Deniger
 */
public class ChooseNameView {
  private final String labelText = org.openide.util.NbBundle.getMessage(ChooseNameView.class, "NewName.Label");
  private String dialogTitle = org.openide.util.NbBundle.getMessage(ChooseNameView.class, "NewName.DialogTitle");
  private final Set<String> usedName = new HashSet<>();
  private Pattern pattern;


  public void setPattern(final Pattern pattern) {
    this.pattern = pattern;
  }


  public void addUsedName(final String name) {
    usedName.add(name);
  }



  public void setDialogTitle(final String dialogTitle) {
    this.dialogTitle = dialogTitle;
  }

  public String chooseName(final String prefix, final String initName) {
    final RadicalInputNotifier input = new RadicalInputNotifier(prefix, initName);

    if (DialogDisplayer.getDefault().notify(input) == NotifyDescriptor.OK_OPTION) {
      return input.getInputText();
    }
    return null;
  }

  public class RadicalInputNotifier extends NotifyDescriptor.InputLine implements DocumentListener {
    final String prefix;
    final String initEntry;

    public RadicalInputNotifier(final String prefix, final String initEntry) {
      super(labelText, dialogTitle);
      this.prefix = prefix;
      this.initEntry = initEntry;
      super.textField.setText(initEntry);
      setValid(false);
      createNotificationLineSupport();
      setDefaultMessage();
      super.textField.getDocument().addDocumentListener(this);
    }

    @Override
    protected Component createDesign(final String text) {
      final JComponent res = (JComponent) super.createDesign(text);
      res.setPreferredSize(new Dimension(430, 45));
      return res;
    }

    public final void setDefaultMessage() {
      if (StringUtils.isBlank(prefix)) {
        getNotificationLineSupport().setInformationMessage(NbBundle.getMessage(ChooseNameView.class, "ChooseNameView.NoPrefix.Info"));
      } else {
        getNotificationLineSupport().setInformationMessage(NbBundle.getMessage(ChooseNameView.class, "ChooseNameView.Info", prefix));
      }
    }

    @Override
    public void changedUpdate(final DocumentEvent e) {
      updateValidState();
    }

    @Override
    public void insertUpdate(final DocumentEvent e) {
      updateValidState();
    }

    @Override
    public void removeUpdate(final DocumentEvent e) {
      updateValidState();
    }

    private void updateValidState() {
      final String newName = textField.getText().trim();
      if (StringUtils.isEmpty(newName)) {
        getNotificationLineSupport().setErrorMessage(NbBundle.getMessage(ChooseNameView.class, "ChangeName.Empty.Error"));
        setValid(false);
        return;
      }
      if (usedName.contains(newName)) {
        getNotificationLineSupport().setErrorMessage(NbBundle.getMessage(ChooseNameView.class, "ChangeName.AlreadyUsed.Error"));
        setValid(false);
        return;
      }
      final String radicalValideMsg = pattern == null ? ValidationPatternHelper.isNameValidei18nMessage(newName, prefix) : ValidationPatternHelper
          .isNameValidi18nErrorMessage(newName, prefix, pattern);
      final boolean isValid = radicalValideMsg == null;
      if (!isValid) {
        setValid(isValid);
        getNotificationLineSupport().setErrorMessage(radicalValideMsg);
        return;
      }
      getNotificationLineSupport().setErrorMessage(null);
      final boolean isNotSame = !StringUtils.equals(prefix, newName) && !StringUtils.equals(initEntry, newName);
      setValid(isNotSame);
      setDefaultMessage();
    }
  }
}
