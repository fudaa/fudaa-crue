package org.fudaa.fudaa.crue.common.log.property;

import java.awt.Component;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import org.fudaa.fudaa.crue.common.log.CtuluLogsTopComponent;
import org.fudaa.fudaa.crue.common.log.filter.NodeQuickFilter;
import org.fudaa.fudaa.crue.common.log.filter.NodeQuickFilterOnlyError;
import org.fudaa.fudaa.crue.common.log.filter.NodeQuickFilterOnlyWarningAndError;
import org.openide.explorer.view.NodePopupFactory;
import org.openide.nodes.Node;
import org.openide.nodes.NodeOp;
import org.openide.util.Utilities;

/**
 *
 * @author deniger
 */
public class PropertyColumnDefault extends NodePopupFactory {

  String columnId;
  String displayName;
  String description;
  boolean visibleByDefault = true;

  public boolean isVisibleByDefault() {
    return visibleByDefault;
  }

  public void setVisibleByDefault(boolean visibleByDefault) {
    this.visibleByDefault = visibleByDefault;
  }

  public String getColumnId() {
    return columnId;
  }

  public void setColumnId(String columnId) {
    this.columnId = columnId;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  private static class FilterAction extends AbstractAction {

    private final NodeQuickFilter filter;
    private final CtuluLogsTopComponent logComponent;

    public FilterAction(final NodeQuickFilter filter, CtuluLogsTopComponent logComponent) {
      putValue(NAME, filter.getName());
      this.filter = filter;
      this.logComponent = logComponent;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      logComponent.applyFilter(filter);
    }
  }

  protected AbstractAction create(NodeQuickFilter filter, CtuluLogsTopComponent logs) {
    return new FilterAction(filter, logs);
  }

  @Override
  public JPopupMenu createPopupMenu(int row, int column, Node[] selectedNodes, Component component) {
    Action[] actions = NodeOp.findActions(selectedNodes);
    JPopupMenu res = Utilities.actionsToPopup(actions, component);
    CtuluLogsTopComponent logs = (CtuluLogsTopComponent) ((JComponent) component).getClientProperty(
            CtuluLogsTopComponent.class.getSimpleName());
    if (logs != null) {
      logs.addCommonMenuItem(res);
      res.addSeparator();
      res.add(create(new NodeQuickFilterOnlyError(), logs));
      res.add(create(new NodeQuickFilterOnlyWarningAndError(), logs));
      res.add(logs.getNoFilterAction());
    }
    return res;
  }
}
