package org.fudaa.fudaa.crue.common.helper;

import org.netbeans.api.progress.BaseProgressUtils;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.api.progress.ProgressRunnable;

import java.awt.*;

/**
 * @author deniger
 */
public class CrueProgressUtils {
  public static <T> T showProgressDialogAndRun(final ProgressRunnable<T> operation, final String displayName) {
    return showProgressDialogAndRun(operation, displayName, false);
  }

  public static <T> T showProgressDialogAndRun(final ProgressRunnable<T> operation, final String displayName, boolean includeDetailLabel) {
    if (!EventQueue.isDispatchThread()) {
      ProgressHandle handle = ProgressHandleFactory.createHandle(displayName);
      T res = null;
      try {
        res = operation.run(handle);
      } finally {
        handle.finish();
      }
      return res;
    }
    T res = BaseProgressUtils.showProgressDialogAndRun(operation, displayName, includeDetailLabel);
    return res;
  }
}
