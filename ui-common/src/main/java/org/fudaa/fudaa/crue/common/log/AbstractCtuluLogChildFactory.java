package org.fudaa.fudaa.crue.common.log;

import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.openide.nodes.ChildFactory;

/**
 *
 * @author Fred Deniger
 */
public abstract class AbstractCtuluLogChildFactory extends ChildFactory<CtuluLogRecord> {

  final CtuluLog log;

  public AbstractCtuluLogChildFactory(CtuluLog log) {
    this.log = log;
  }

  @Override
  protected boolean createKeys(List<CtuluLogRecord> toPopulate) {
    toPopulate.addAll(log.getRecords());
    return true;
  }
  
  
  
}
