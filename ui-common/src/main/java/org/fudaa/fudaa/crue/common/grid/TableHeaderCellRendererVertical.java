/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.grid;

import com.jidesoft.swing.JideLabel;
import com.memoire.bu.BuBorders;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;
import org.apache.commons.lang3.ObjectUtils;

/**
 *
 * @author Frederic Deniger
 */
public class TableHeaderCellRendererVertical implements TableCellRenderer {

  private final JideLabel label = new JideLabel();

  public TableHeaderCellRendererVertical() {
    label.setOrientation(JideLabel.VERTICAL);
    label.setBorder(BuBorders.EMPTY1111);
    label.setClockwise(false);
    label.setBorder(UIManager.getBorder("TableHeader.cellBorder"));
  }

  public int getWidth() {
    return 16;
  }

  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    label.setText(ObjectUtils.toString(value));
    return label;
  }
}
