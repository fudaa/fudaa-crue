/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.property;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;
import org.fudaa.dodico.crue.validation.ValidatingIndicator;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

/**
 * @author Fred Deniger
 */
public abstract class AbstractNodeFirable extends AbstractNode implements ValidatingIndicator {
    public static final String DEFAULT_SHORT_DESCRIPTION = "defaultShortDescription";

    public AbstractNodeFirable(Children children, Lookup lookup) {
        super(children, lookup);
    }

    public AbstractNodeFirable(Children children) {
        super(children);
    }

    protected void fireObjectChange(String property, Object oldValue, Object newValue) {
        super.firePropertyChange(property, oldValue, newValue);
    }

    @Override
    public HelpCtx getHelpCtx() {
        EMH lookup = getLookup().lookup(EMH.class);
        if (lookup != null) {
            return new HelpCtx(SysdocUrlBuilder.getEMHHelpCtxId(lookup));
        }
        ManagerEMHContainerBase manager = getLookup().lookup(ManagerEMHContainerBase.class);
        if (manager != null) {
            return new HelpCtx(SysdocUrlBuilder.getEMHHelpCtxId(manager));
        }
        return super.getHelpCtx();
    }

    /**
     * pas d'image et la description par defaut issue de DEFAULT_SHORT_DESCRIPTION
     */
    public void restoreNoImageAndDefaultDescription() {
        setNoImage();
        String defaultShortDesc = (String) getValue(DEFAULT_SHORT_DESCRIPTION);
        setShortDescription(StringUtils.defaultString(defaultShortDesc));
    }

    public final void setNoImage() {
        setIconBaseWithExtension(LogIconTranslationProvider.NO_IMAGE_PATH);
    }

    public void setSevereIcon() {
        setIconBaseWithExtension(LogIconTranslationProvider.getIconBase(CtuluLogLevel.SEVERE));
    }

    public void setErrorIcon() {
        setIconBaseWithExtension(LogIconTranslationProvider.getIconBase(CtuluLogLevel.ERROR));
    }

    public void setWarningIcon() {
        setIconBaseWithExtension(LogIconTranslationProvider.getIconBase(CtuluLogLevel.WARNING));
    }

    public Node.Property find(String name) {
        PropertySet[] propertySets = getPropertySets();
        for (PropertySet propertySet : propertySets) {
            Property<?>[] properties = propertySet.getProperties();
            for (Property<?> property : properties) {
                if (name.equals(property.getName())) {
                    return property;
                }
            }
        }
        return null;
    }

    public abstract boolean isEditMode();

    /**
     * @return representation de la donnée en string pour export dans le clipboard.
     */
    public String getDataAsString() {
        throw new IllegalAccessError("Not implemented");
    }

    @Override
    public void clearMessage() {
        restoreNoImageAndDefaultDescription();
    }

    @Override
    public void setErrorMessage(String errorMessage) {
        setErrorIcon();
        setShortDescription(errorMessage);
    }

    @Override
    public void setWarningMessage(String errorMessage) {
        setWarningIcon();
        setShortDescription(errorMessage);
    }
}
