package org.fudaa.fudaa.crue.common.log.filter;

import gnu.trove.TObjectIntHashMap;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class NodeQuickFilterLevelDetailMoreVerbose extends AbstractNodeQuickFilter {

  private final int levelVerbosity;
  private final boolean strict;
  private final TObjectIntHashMap<String> verbosityByLevel;

  public NodeQuickFilterLevelDetailMoreVerbose(final String propertyId, final String propertyName, final String level, final boolean strict,
                                               final TObjectIntHashMap<String> verbosityByLevel) {
    super(propertyId);
    this.verbosityByLevel = verbosityByLevel;
    this.levelVerbosity = verbosityByLevel.get(level);
    this.strict = strict;
    final String translated = level;
    name = NbBundle.getMessage(NodeQuickFilterLevelDetailMoreVerbose.class,
                               strict ? "filter.moreVerboseStrict.name" : "filter.moreVerbose.name", propertyName, translated);
    description = NbBundle.getMessage(NodeQuickFilterLevelDetailMoreVerbose.class,
                                      strict ? "filter.moreVerboseStrict.description" : "filter.moreVerbose.description",
                                      propertyName, translated);
  }

  @Override
  protected boolean acceptValue(final Object value) {
    final int other = verbosityByLevel.get((String) value);
    if (strict) {
      return levelVerbosity < other;
    }
    return levelVerbosity <= other;

  }
}
