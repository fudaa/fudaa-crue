/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.swing;

import com.jidesoft.swing.JideButton;
import java.awt.event.KeyEvent;
import javax.swing.Action;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.fudaa.ebli.commun.EbliActionInterface;

/**
 *
 * @author Frederic Deniger
 */
public class JideToolButton extends JideButton {

  final KeyStroke key_;

  public JideToolButton(final EbliActionInterface _action) {
    super(null, null);
    setAction(_action);
    setFocusable(true);
    setButtonStyle(JideButton.TOOLBAR_STYLE);
    key_ = (KeyStroke) _action.getValue(Action.ACCELERATOR_KEY);
    if (key_ != null) {
      final KeyStroke accelerator = key_;
      final String acceleratorDelimiter
              = UIManager.getString("MenuItem.acceleratorDelimiter");
      String acceleratorText = "";
      if (accelerator != null) {
        final int modifiers = accelerator.getModifiers();
        if (modifiers > 0) {
          acceleratorText = KeyEvent.getKeyModifiersText(modifiers);
          //acceleratorText += "-";
          acceleratorText += acceleratorDelimiter;
        }

        final int keyCode = accelerator.getKeyCode();
        if (keyCode == 0) {
          acceleratorText += accelerator.getKeyChar();
        } else {
          acceleratorText += KeyEvent.getKeyText(keyCode);
        }
      }
      setToolTipText(super.getToolTipText() + " " + acceleratorText);
    }
  }

  @Override
  protected boolean processKeyBinding(final KeyStroke _ks, final KeyEvent _e, final int _condition, final boolean _pressed) {
    if (isEnabled() && key_ != null && key_.equals(_ks)) {
      return SwingUtilities.notifyAction(getAction(), _ks, _e, this, _e.getModifiers());
    }
    return super.processKeyBinding(_ks, _e, _condition, _pressed);
  }

}
