package org.fudaa.fudaa.crue.common.editor;

import java.beans.PropertyEditorSupport;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author deniger
 */
public class CodeTranslationEditorSupport extends PropertyEditorSupport{

  Map<Integer, String> i18nByValues = new HashMap<>();
  Map<String, Integer> valuesByi18n = new HashMap<>();

  public CodeTranslationEditorSupport(CodeTranslation in) {
    i18nByValues = in.geti18nByValues();
    valuesByi18n = in.getValuesByi18n();
  }

  @Override
  public String getAsText() {
    return i18nByValues.get(getValue());
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    Integer get = valuesByi18n.get(text);
    if (get != null) {
      setValue(get);
    }
  }
}
