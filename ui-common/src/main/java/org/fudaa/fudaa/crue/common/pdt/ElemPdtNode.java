/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.pdt;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.LogsHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.metier.emh.ElemPdt;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.PropertyNodeBuilder;
import org.fudaa.fudaa.crue.common.property.PropertySupportReflection;
import org.fudaa.fudaa.crue.common.view.DefaultNodePasteType;
import org.joda.time.Duration;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.Exceptions;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.Lookups;

import java.awt.datatransfer.Transferable;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

/**
 * @author Frédéric Deniger
 */
public class ElemPdtNode extends AbstractNodeFirable {
    boolean editable = true;
    private StateUpdater updater;

    public ElemPdtNode(ElemPdt elemPdt, CrueConfigMetier ccm) {
        super(Children.LEAF, Lookups.fixed(elemPdt, ccm));
        setDisplayName(StringUtils.EMPTY);
        super.setNoImage();
    }

    public CtuluLog validate() {
        return validate(getSheet().get(Sheet.PROPERTIES));
    }

    public CtuluLog validate(Set set) {
        CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
        try {
            PropertySupportReflection nbrPdt = (PropertySupportReflection) set.get(CrueConfigMetierConstants.PROP_NBR_PDT);
            CrueConfigMetier ccm = getLookup().lookup(CrueConfigMetier.class);
            ItemVariable nbrPdtProperty = ccm.getProperty(CrueConfigMetierConstants.PROP_NBR_PDT);

            nbrPdtProperty.getValidator().validateValue(StringUtils.EMPTY, nbrPdt.getValue(), log);
            PropertySupportReflection duree = (PropertySupportReflection) set.get(CrueConfigMetierConstants.PROP_DUREE_PDT);
            Duration duration = (Duration) duree.getValue();
            ItemVariable dureeProperty = ccm.getProperty(CrueConfigMetierConstants.PROP_DUREE_PDT);

            dureeProperty.getValidator().validateValue(StringUtils.EMPTY, duration.getMillis() / 1000d, log);
            restoreNoImageAndDefaultDescription();
        } catch (Exception exception) {
            Exceptions.printStackTrace(exception);
        }
        return log;
    }

    private void updateProperties() {
        Sheet.Set set = getSheet().get(Sheet.PROPERTIES);
        updateProperties(set);
    }

    private void updateProperties(Sheet.Set set) {
        try {
            CtuluLog log = validate(set);
            if (log.isNotEmpty()) {
                if (log.containsErrors()) {
                    setErrorIcon();
                } else {
                    setWarningIcon();
                }
                setShortDescription(LogsHelper.toHtml(log));
            }
        } catch (Exception exception) {
            Exceptions.printStackTrace(exception);
        }
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    @Override
    public boolean canCopy() {
        return isEditMode();
    }

    @Override
    public boolean isEditMode() {
        return editable;
    }

    @Override
    public boolean canDestroy() {
        return isEditMode();
    }

    @Override
    public PasteType getDropType(Transferable t, int action, int index) {
        if (isEditMode()) {
            Node dragged = DefaultNodePasteType.getMovedNodeInTransferable(t);
            return dragged == null ? null : new DefaultNodePasteType(dragged, this);
        }
        return null;
    }

    @Override
    protected Sheet createSheet() {
        ElemPdt elemPdt = getLookup().lookup(ElemPdt.class);
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        PropertyNodeBuilder nodeBuilder = new PropertyNodeBuilder();
        List<PropertySupportReflection> createFromPropertyDesc = nodeBuilder.createFromPropertyDesc(DecimalFormatEpsilonEnum.COMPARISON, elemPdt, this);
        for (PropertySupportReflection propertySupportReflection : createFromPropertyDesc) {
            set.put(propertySupportReflection);
        }

        sheet.put(set);

        updateProperties(set);
        updater = new StateUpdater();

        addPropertyChangeListener(updater);
        return sheet;
    }

    private class StateUpdater implements PropertyChangeListener {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (evt.getPropertyName().equals(CrueConfigMetierConstants.PROP_DUREE_PDT) || evt.getPropertyName()
                    .equals(CrueConfigMetierConstants.PROP_NBR_PDT)) {
                updateProperties();
            }
        }
    }
}
