/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.pdt;

import java.beans.*;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.emh.IniCalcCliche;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;

/**
 *
 * @author Frédéric Deniger
 */
public class IniCalcClicheEditor extends PropertyEditorSupport implements ExPropertyEditor, PropertyChangeListener {

  static void registerEditor() {
    PropertyEditorManager.registerEditor(IniCalcCliche.class, IniCalcClicheEditor.class);
  }
  PropertyEnv env;

  @Override
  public void attachEnv(PropertyEnv env) {
    this.env = env;
    env.addPropertyChangeListener(this);
    env.setState(PropertyEnv.STATE_NEEDS_VALIDATION);
    FeatureDescriptor featureDescriptor = env.getFeatureDescriptor();
    featureDescriptor.setValue("suppressCustomEditor", Boolean.TRUE);
    featureDescriptor.setValue("canEditAsText", Boolean.TRUE);
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
  }

  @Override
  public boolean supportsCustomEditor() {
    return false;
  }

  @Override
  public String getAsText() {
    IniCalcCliche value = (IniCalcCliche) getValue();
    if (value == null) {
      return StringUtils.EMPTY;
    }
    return value.getNomFic();
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    try {
      IniCalcCliche value = new IniCalcCliche();
      value.setNomFic(text);
      setValue(value);
    } catch (Exception e) {
    }
  }
}
