package org.fudaa.fudaa.crue.common.services;

import org.apache.commons.lang3.ObjectUtils;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ServiceProvider;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

import java.awt.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.reflect.Method;
import java.util.List;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Service permettant de connaitre la perspective activée et de récuperer le service de cet perspective.
 * * <table  border="1"  cellpadding="3" cellspacing="0"  summary="lookups">
 * <tr>
 * <th align="left">Lookup</th>
 * <th align="left">Commentaire</th>
 * <th align="left">Méthode utilisant le lookup</th>
 * </tr>
 * <tr>
 * <td>{@link PerspectiveEnum}</td>
 * <td>	L'enum representant la perspective active</td>
 * <td>
 * <code>{@link #getCurrentPerspectiveService()} </code>
 * <code>{@link #isActivated(PerspectiveEnum)} </code>
 * </td>
 * </tr>
 * <tr>
 * </table>
 *
 * @author Fred Deniger
 */
@ServiceProvider(service = SelectedPerspectiveService.class)
public class SelectedPerspectiveService implements Lookup.Provider {
    private final PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private final PropertyChangeListener editForwarder = evt -> changeSupport.firePropertyChange("currentPerspectiveInEditMode", PerspectiveState.MODE_EDIT.equals(evt.getOldValue()),
            PerspectiveState.MODE_EDIT.equals(evt.getNewValue()));
    /**
     * le contenu du lookup de ce service.
     */
    private final InstanceContent content = new InstanceContent();
    private final Lookup dynamicLookup = new AbstractLookup(content);
    private PerspectiveService currentPerspective;
    private final Map<PerspectiveEnum, String> serviceIdByPerspectiveEnum = new EnumMap<>(PerspectiveEnum.class);
    /**
     * Contient les TopComponent fermés pour une perspective. Si la liste est null, cela signifie que la perspective n'a pas encore ete ouverte.
     */
    private final Map<PerspectiveEnum, List<TopComponentActive>> closedTopComponents = new EnumMap<>(
            PerspectiveEnum.class);

    public SelectedPerspectiveService() {
    }

    /**
     * @param mode le mode
     * @return true si le mode en question est un mode separate ( fenetre non dockée).
     */
    private static boolean isSeparate(Mode mode) {
        if (mode == null) {
            return false;
        }
        Class<? extends Mode> aClass = mode.getClass();
        try {
            Method method = aClass.getMethod("getState");
            Object invoke = method.invoke(mode);
            return Integer.valueOf(1).equals(invoke);
        } catch (Exception noSuchMethodException) {
            Logger.getLogger(SelectedPerspectiveService.class.getName()).log(Level.WARNING, "can't find state", noSuchMethodException);
        }
        return false;
    }

    @Override
    public Lookup getLookup() {
        return dynamicLookup;
    }

    /**
     * @param perspectiveEnum l'enum de la perspective a tester
     * @return true si la <code>perspective</code> est activée
     */
    public boolean isActivated(PerspectiveEnum perspectiveEnum) {
        return ObjectUtils.equals(dynamicLookup.lookup(PerspectiveEnum.class), perspectiveEnum);
    }

    private void activeTopComponents(Collection<String> topComponents) {
        for (String id : topComponents) {
            TopComponent topComponent = WindowManager.getDefault().findTopComponent(id);
            if (topComponent != null && !topComponent.isOpened()) {
                topComponent.open();
                topComponent.requestVisible();
            }
        }
    }

    /**
     * @param perspective l'enum de la perspective. Attention enregistrement unique sinon exception
     * @param serviceId   l'id du service correspondant
     */
    public void register(PerspectiveEnum perspective, String serviceId) {
        if (serviceIdByPerspectiveEnum.containsKey(perspective)) {
            throw new IllegalAccessError("perspective is already set " + perspective);
        }
        serviceIdByPerspectiveEnum.put(perspective, serviceId);
    }

    /**
     * @return la perspective activée ( le service)
     */
    public PerspectiveService getCurrentPerspectiveService() {
        return this.currentPerspective;
    }

    /**
     * @param perspectiveEnum l'enum de la perspective
     * @return le service correspondant. Null si non trouvé.
     */
    public PerspectiveService getPerspectiveService(PerspectiveEnum perspectiveEnum) {
        String perspective = serviceIdByPerspectiveEnum.get(perspectiveEnum);
        Lookup.Template<PerspectiveService> template = new Lookup.Template<>(PerspectiveService.class, perspective,
                null);
        final Lookup.Item<PerspectiveService> selected = Lookup.getDefault().lookupItem(template);
        return selected == null ? null : selected.getInstance();
    }

    /**
     * @param listener un listener sur l'état "editMode" de la perspective courante.
     */
    public void addEditionChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener("currentPerspectiveInEditMode", listener);
    }

    public void removeEditionChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener("currentPerspectiveInEditMode", listener);
    }

    /**
     * @return true si la perspective active est en mode édition
     */
    public boolean isCurrentPerspectiveInEditMode() {
        return currentPerspective != null && PerspectiveState.MODE_EDIT.equals(currentPerspective.getState());
    }

    /**
     * @param newPerspectiveSelected la nouvelle perspective active
     * @return true si le changement a pu être effectué ( il se peut que des modifications en cours dans la perspective courante empeche le changement).
     */
    public boolean activePerspective(PerspectiveEnum newPerspectiveSelected) {
        final PerspectiveEnum currentPersectiveEnum = dynamicLookup.lookup(PerspectiveEnum.class);
        //pas de changement -> ok
        if (ObjectUtils.equals(currentPersectiveEnum, newPerspectiveSelected)) {
            return true;
        }
        // on ne peut pas désactiver la perspective en cours:
        if (currentPerspective != null && !currentPerspective.deactivate()) {
            return false;
        }
        //on enlève les listeners
        if (currentPerspective != null) {
            currentPerspective.removeStateListener(editForwarder);
        }
        //on  cherche le nouveau service perspective à activer
        String perspective = serviceIdByPerspectiveEnum.get(newPerspectiveSelected);
        Lookup.Template<PerspectiveService> template = new Lookup.Template<>(PerspectiveService.class, perspective,
                null);
        final Lookup.Item<PerspectiveService> selected = Lookup.getDefault().lookupItem(template);
        currentPerspective = selected == null ? null : selected.getInstance();
        //on met à jour les vues ( fermeture des anciennes et ouverture des fenetres par défaut)
        updateCurrentViews(currentPersectiveEnum, newPerspectiveSelected);
        if (currentPersectiveEnum != null) {
            content.remove(currentPersectiveEnum);
        }
        //enregistrement des listener et activation
        content.add(newPerspectiveSelected);
        if (currentPerspective != null) {
            currentPerspective.addStateListener(editForwarder);
            currentPerspective.activate();
        }
        return true;
    }

    /**
     * RAS des fenetres de la perspective courante
     */
    public void rebuildCurrentPerspective() {
        Set<TopComponent> opened = WindowManager.getDefault().getRegistry().getOpened();
        for (TopComponent topComponent : opened) {
            topComponent.close();
        }
        closedTopComponents.clear();
        activeTopComponents(currentPerspective == null
                ? Collections.emptySet()
                : currentPerspective.getDefaultTopComponents());
    }

    /**
     * Mise à jour interne
     *
     * @param oldPerspectiveEnum l'ancienne perspective active
     * @param newPerspectiveEnum la nouvelle perspective active
     */
    private void updateCurrentViews(PerspectiveEnum oldPerspectiveEnum, PerspectiveEnum newPerspectiveEnum) {
        //on ferme toutes les perspectives:
        Set<TopComponent> opened = WindowManager.getDefault().getRegistry().getOpened();
        if (oldPerspectiveEnum != null) {
            List<TopComponentActive> closed = closedTopComponents.get(oldPerspectiveEnum);
            if (closed != null) {
                closed.clear();
            } else {
                closed = new ArrayList<>();
                closedTopComponents.put(oldPerspectiveEnum, closed);
            }
            TopComponentOpenCloseHelper closeHelper = new TopComponentOpenCloseHelper();
            Map<TopComponent, Boolean> activeByTc = new HashMap<>();
            for (TopComponent topComponent : opened) {
                Mode mode = WindowManager.getDefault().findMode(topComponent);
                if (!isSeparate(mode)) {
                    boolean active = false;
                    if (mode != null) {
                        active = mode.getSelectedTopComponent() == topComponent;
                    }
                    activeByTc.put(topComponent, active);
                }
            }
            for (TopComponent topComponent : opened) {
                Mode mode = WindowManager.getDefault().findMode(topComponent);
                if (!isSeparate(mode)) {
                    closeHelper.closeTemporarily(topComponent);
                    closed.add(new TopComponentActive(topComponent, activeByTc.get(topComponent)));
                }
            }
        } else {
            //premier appel:on ferme tous les topComponent
            for (TopComponent topComponent : opened) {
                topComponent.close();
            }
        }
        //on reouvre les topcomponent déja ouvert
        final List<TopComponentActive> current = closedTopComponents.get(newPerspectiveEnum);
        if (current != null) {
            for (TopComponentActive topComponent : current) {
                topComponent.getTopComponent().open();
            }
            //on active les tc:
            final List<TopComponentActive> toActive = new ArrayList<>(current);
            Runnable runnable = () -> {
                for (TopComponentActive topComponent : toActive) {
                    if (topComponent.isActive()) {
                        topComponent.getTopComponent().requestActive();
                    }
                }
            };
            EventQueue.invokeLater(runnable);
            current.clear();
        } else {//premier appel:on ouvre les topComponents par défaut.
            activeTopComponents(currentPerspective == null
                    ? Collections.emptySet()
                    : currentPerspective.getDefaultTopComponents());
            closedTopComponents.put(newPerspectiveEnum, new ArrayList<>());
        }
    }
}
