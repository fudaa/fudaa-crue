/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.action;

import java.awt.event.ActionEvent;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class ExportImageToClipboardAction extends EbliActionSimple {

  private final CtuluImageProducer imageProducer;

  public ExportImageToClipboardAction(CtuluImageProducer producer) {
    super(NbBundle.getMessage(ExportImageToClipboardAction.class, "ExportImageToClipboardAction.DisplayName"), CtuluResource.CTULU.getIcon("copie-image"), CtuluLibImage.SNAPSHOT_CLIPBOARD_COMMAND);
    assert producer != null;
    this.imageProducer = producer;
    setDefaultToolTip(NbBundle.getMessage(ExportImageToClipboardAction.class, "ExportImageToClipboardAction.Tooltip"));
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    CtuluImageExport.exportImageInClipboard(imageProducer, CtuluUIForNetbeans.DEFAULT);
  }
}
