package org.fudaa.fudaa.crue.common.services;

/**
 * Etat de la perspective: en edition, en visu, en read-only permanent ( projet chargé en read-only et le mode edit est impossible) ou
 * en mode read-only temporaire (
 *
 * @author deniger
 */
public enum PerspectiveState {
  /**
   * utilise pour un mode visualisation: la perspective n'est pas modifiable mais l'utilisateur peut cliquer sur le bouton edit.
   */
  MODE_READ(true, "mode.visualization"),
  /**
   * utilise pour un mode readOnly temporaire.
   * Par exemple si un scenario est chargé, la perspective étude passe en readonly temporaire :( l'utilisateur ne peut pas modifier l'etude si
   * un scenario est chargé.
   */
  MODE_READ_ONLY_TEMP(false, "mode.readOnlyTemp"),
  /**
   * si une étude a été ouvertue en mode readonly. une perspective qui est dans ce mode ne doit jamais changer
   * de mode.
   */
  MODE_READ_ONLY_ALWAYS(false, "mode.readOnly"),
  MODE_EDIT(true, "mode.edit");

  private final boolean editActionEnable;
  private final String msg;

  /**
   *
   * @param editActionEnable true si le bouton "Mode edit" peut être activé.
   * @param msg              clé pour le tooltip qui sera affiché sur le bouton edit pour expliquer l'état actuel.
   */
  PerspectiveState(boolean editActionEnable, String msg) {
    this.editActionEnable = editActionEnable;
    this.msg = msg;
  }

  /**
   *
   * @return true si le bouton edit peut être activé
   */
  public boolean isEditActionEnable() {
    return editActionEnable;
  }

  /**
   *
   * @return la clé du message à afficher pour expliquer l'état.
   */
  public String getMsg() {
    return msg;
  }
}
