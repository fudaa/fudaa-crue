/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.grid;

import com.jidesoft.swing.JideLabel;
import javax.swing.JComponent;
import javax.swing.JLabel;

/**
 *
 * @author Frederic Deniger
 */
public class ColumnHeaderContent {

  private final String labelAfter;

  public ColumnHeaderContent(String labelAfter) {
    this.labelAfter = labelAfter;
  }

  JComponent getAfterComponent() {
    if (labelAfter == null) {
      return new JLabel();
    }
    JideLabel label = new JideLabel(labelAfter);
    label.setOrientation(JideLabel.VERTICAL);
    label.setClockwise(false);
    return label;
  }
}
