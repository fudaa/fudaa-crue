/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.view;

import java.awt.event.ActionEvent;
import java.util.Arrays;
import javax.swing.AbstractAction;
import org.apache.commons.lang3.ArrayUtils;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 *
 * @author Frédéric Deniger
 */
public class MoveNodesToStartAction extends AbstractAction {

  final Node[] activatedNodes;

  public MoveNodesToStartAction(Node[] activatedNodes) {
    super(NbBundle.getMessage(MoveNodesToStartAction.class, "MoveNodeToStart.DisplayName"));
    this.activatedNodes = activatedNodes;
    setEnabled(ArrayUtils.isNotEmpty(activatedNodes));
    if (isEnabled()) {
      final Node node = activatedNodes[0];
      if (node instanceof AbstractNodeFirable) {
        setEnabled(((AbstractNodeFirable) node).isEditMode());
      }
    }
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    DefaultNodePasteType.nodeToStart(Arrays.asList(activatedNodes), DefaultNodePasteType.isIndexUsedAsDisplayName(activatedNodes));
  }
}
