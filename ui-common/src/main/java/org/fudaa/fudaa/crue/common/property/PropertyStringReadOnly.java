package org.fudaa.fudaa.crue.common.property;

import java.lang.reflect.InvocationTargetException;
import org.openide.nodes.PropertySupport;

/**
 *
 * @author Fred Deniger
 */
public class PropertyStringReadOnly extends PropertySupport.ReadOnly<String> {

  private final String value;

  public PropertyStringReadOnly(String value, String name, String displayName, String shortDescription) {
    super(name, String.class, displayName, shortDescription);
    this.value = value;
  }

  @Override
  public String getValue() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    return value;
  }
  
}
