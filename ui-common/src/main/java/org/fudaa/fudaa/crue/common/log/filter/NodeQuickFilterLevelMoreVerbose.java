package org.fudaa.fudaa.crue.common.log.filter;

import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class NodeQuickFilterLevelMoreVerbose extends AbstractNodeQuickFilter {

  private final CtuluLogLevel level;
  private final boolean strict;

  public NodeQuickFilterLevelMoreVerbose(final String propertyId, final String propertyName, final CtuluLogLevel level, final boolean strict) {
    super(propertyId);
    this.level = level;
    this.strict = strict;
    final String translated = LogIconTranslationProvider.getTranslation(level);
    name = NbBundle.getMessage(NodeQuickFilterLevelMoreVerbose.class,
                               strict ? "filter.moreVerboseStrict.name" : "filter.moreVerbose.name", propertyName, translated);
    description = NbBundle.getMessage(NodeQuickFilterLevelMoreVerbose.class,
                                      strict ? "filter.moreVerboseStrict.description" : "filter.moreVerbose.description",
                                      propertyName, translated);
  }

  @Override
  public boolean accept(final Node node) {
    if (node == null) {
      return true;
    }
    final CtuluLogRecord lookup = node.getLookup().lookup(CtuluLogRecord.class);
    if (lookup == null) {
      return true;
    }
    return acceptValue(lookup);
  }

  @Override
  protected boolean acceptValue(final Object value) {
    final CtuluLogRecord lookup = (CtuluLogRecord) value;
    return lookup.getLevel().isMoreVerboseThan(level, strict);
  }
}
