/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import org.fudaa.fudaa.crue.common.helper.ComboBoxHelper;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;

/**
 *
 * @author Frederic Deniger
 */
public class ChooseInListHelper<T> {

  private String label;
  private String dialogTitle;
  private String dialogDescription;
  private ListCellRenderer renderer;

  public String getLabel() {
    return label;
  }


  public void setDialogDescription(final String dialogDescription) {
    this.dialogDescription = dialogDescription;
  }

  public void setLabel(final String label) {
    this.label = label;
  }

  public String getDialogTitle() {
    return dialogTitle;
  }

  public void setDialogTitle(final String dialogTitle) {
    this.dialogTitle = dialogTitle;
  }


  public ListCellRenderer getRenderer() {
    return renderer;
  }

  public void setRenderer(final ListCellRenderer renderer) {
    this.renderer = renderer;
  }

  public T choose(final List<T> in, final T selected) {
    final JComboBox cb = new JComboBox();
    ComboBoxHelper.setDefaultModel(cb, in);
    cb.setSelectedItem(selected);
    if (renderer != null) {
      cb.setRenderer(renderer);
    }
    JPanel pn = new JPanel(new FlowLayout(FlowLayout.LEFT));
    if (label != null) {
      pn.add(new JLabel(label));
    }
    pn.add(cb);
    if (dialogDescription != null) {
      final JPanel main = new JPanel(new BorderLayout());
      main.add(new JLabel(dialogDescription));
      main.add(pn, BorderLayout.CENTER);
      pn = main;
    }
    if (DialogHelper.showQuestionOkCancel(dialogTitle, pn)) {
      return (T) cb.getSelectedItem();
    }
    return null;
  }
}
