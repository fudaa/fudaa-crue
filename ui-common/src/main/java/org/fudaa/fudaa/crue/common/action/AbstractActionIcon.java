package org.fudaa.fudaa.crue.common.action;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;

/**
 *
 * @author deniger
 */
public abstract class AbstractActionIcon extends AbstractAction{

  /**
   * @param _name le nom de l'action
   * @param _icon l'icone
   * @param _command la commande
   */
  public AbstractActionIcon(final String _name, final Icon _icon, final String _command) {
    super(_name, _icon);
    putValue(Action.ACTION_COMMAND_KEY, _command);
  }
  
}
