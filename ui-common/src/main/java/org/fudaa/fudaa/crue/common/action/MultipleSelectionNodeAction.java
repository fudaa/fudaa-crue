package org.fudaa.fudaa.crue.common.action;

import org.fudaa.fudaa.crue.common.services.SelectedPerspectiveService;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.NodeAction;

/**
 *
 * @author deniger
 */
public abstract class MultipleSelectionNodeAction extends NodeAction {

  private final String name;
  private final boolean editAction;
  final SelectedPerspectiveService selectedPerspectiveService = Lookup.getDefault().lookup(SelectedPerspectiveService.class);

  public MultipleSelectionNodeAction(String name, boolean editAction) {
    this.name = name;
    this.editAction = editAction;
  }

  @Override
  protected boolean asynchronous() {
    return false;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  protected boolean enable(Node[] activatedNodes) {
    if (editAction && !selectedPerspectiveService.isCurrentPerspectiveInEditMode()) {
      return false;
    }
    if (activatedNodes != null && activatedNodes.length > 0) {
      for (Node activatedNode : activatedNodes) {
        if (activatedNode != null && enable(activatedNode))
        {
          return true;
        }
      }
    }
    return false;
  }

  protected abstract boolean enable(Node activatedNodes);

  protected abstract void performAction(Node activatedNodes);

  @Override
  public HelpCtx getHelpCtx() {
    return HelpCtx.DEFAULT_HELP;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    if (activatedNodes != null && activatedNodes.length > 0) {
      for (Node activatedNode : activatedNodes) {
        if (activatedNode != null) {
          performAction(activatedNode);
        }
      }
    }
  }
}
