/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.log.property;

import java.lang.reflect.InvocationTargetException;
import java.util.MissingResourceException;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.openide.nodes.PropertySupport;
import org.openide.util.NbBundle;

/**
 *
 * @author Fred Deniger
 */
public class LogIdProperty extends PropertySupport.ReadOnly<String> {

  public static final String ID = "IdMsg";
  private final CtuluLogRecord record;

  public LogIdProperty(CtuluLogRecord record) {
    super(ID, String.class, getDefaultDisplayName(),
            getDescription());
    this.record = record;
    PropertyCrueUtils.configureNoCustomEditor(this);
  }

  public static String getDescription() throws MissingResourceException {
    return NbBundle.getMessage(LogIdProperty.class, "LogIdPropertyDescription");
  }

  public static PropertyColumnFilterable createColumn() {
    PropertyColumnFilterable res = new PropertyColumnFilterable();
    res.setColumnId(ID);
    res.setDescription(getDescription());
    res.setDisplayName(getDefaultDisplayName());
    res.setVisibleByDefault(false);
    return res;
  }

  public static String getDefaultDisplayName() {
    return NbBundle.getMessage(LogIdProperty.class, "LogIdPropertyName");
  }

  @Override
  public String toString() {
    return record.getId();
  }

  @Override
  public String getValue() throws IllegalAccessException, InvocationTargetException {
    return record.getId();
  }
}
