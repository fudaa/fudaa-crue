/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.log;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.config.cr.CRReader;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

/**
 *
 * @author Fred Deniger
 */
public class LogIconTranslationProvider {

  public static final BufferedImage NO_IMAGE = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
  public static final Icon NO_ICON = new ImageIcon(NO_IMAGE);
  public static final String NO_IMAGE_PATH = "org/fudaa/fudaa/crue/common/icons/vide.png";
  private static final Map<CtuluLogLevel, Image> IMAGES = new EnumMap<>(CtuluLogLevel.class);
  private static final Map<CtuluLogLevel, String> RESOURCES = new EnumMap<>(CtuluLogLevel.class);
  private static final Map<CtuluLogLevel, String> TRANSLATION = new EnumMap<>(CtuluLogLevel.class);
  private static final Map<String, Image> IMAGES_BY_LEVEL_DETAIL = new HashMap<>();

  static {
    RESOURCES.put(CtuluLogLevel.DEBUG, "org/fudaa/fudaa/crue/common/icons/trace.png");
    RESOURCES.put(CtuluLogLevel.INFO, "org/fudaa/fudaa/crue/common/icons/information.png");
    RESOURCES.put(CtuluLogLevel.WARNING, "org/fudaa/fudaa/crue/common/icons/avertissement.png");
    RESOURCES.put(CtuluLogLevel.ERROR, "org/fudaa/fudaa/crue/common/icons/erreur-non-bloquante.png");
    RESOURCES.put(CtuluLogLevel.SEVERE, "org/fudaa/fudaa/crue/common/icons/erreur-bloquante.png");
    RESOURCES.put(CtuluLogLevel.FATAL, "org/fudaa/fudaa/crue/common/icons/erreur-fatale.png");
  }

  public static Image getNoImage() {
    return NO_IMAGE;
  }

  public static String getIconBase(CtuluLogLevel in) {
    return RESOURCES.get(in);
  }

  public static Image getImageFromDetail(String detail) {
    Image im = IMAGES_BY_LEVEL_DETAIL.get(detail);
    if (im == null) {
      im = getImage(CRReader.getLevel(detail));
      if (im == null) {
        im = getImage(CtuluLogLevel.WARNING);
      }
      IMAGES_BY_LEVEL_DETAIL.put(detail, im);
    }
    return im;
  }

  public static String getTranslation(CtuluLogLevel level) {
    if (level == null) {
      return StringUtils.EMPTY;
    }
    String translated = TRANSLATION.get(level);
    if (translated == null) {
      try {
        translated = NbBundle.getMessage(LogIconTranslationProvider.class, "MessageLevel." + level.name());
      } catch (MissingResourceException missingResourceException) {
        Exceptions.printStackTrace(missingResourceException);
        translated = level.name();
      }
      TRANSLATION.put(level, translated);
    }
    return translated;

  }

  public static Icon getIcon(CtuluLogLevel level) {
    return new ImageIcon(getImage(level));
  }

  public static Image getImage(CtuluLogLevel level) {
    Image res = IMAGES.get(level);
    if (res == null) {
      res = ImageUtilities.loadImage(RESOURCES.get(level));
      IMAGES.put(level, res);
    }
    return res;
  }
}
