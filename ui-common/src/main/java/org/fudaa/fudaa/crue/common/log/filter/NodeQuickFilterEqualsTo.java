package org.fudaa.fudaa.crue.common.log.filter;

import org.apache.commons.lang3.ObjectUtils;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class NodeQuickFilterEqualsTo extends AbstractNodeQuickFilter {

  private final Object value;

  public NodeQuickFilterEqualsTo(final String propertyId, final String propertyName, final Object value) {
    super(propertyId);
    this.value = value;
    description = NbBundle.getMessage(NodeQuickFilterEmptyValue.class, "filter.equalsTo.description", propertyName, value);
    name = NbBundle.getMessage(NodeQuickFilterEmptyValue.class, "filter.equalsTo.name", propertyName, value);
  }

  @Override
  protected boolean acceptValue(final Object value) {
    return ObjectUtils.equals(this.value, value);
  }
}
