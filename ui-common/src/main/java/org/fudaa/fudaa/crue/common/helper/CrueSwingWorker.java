/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.helper;

import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.util.Exceptions;

import javax.swing.*;

/**
 * @author Frederic Deniger
 */
public abstract class CrueSwingWorker<R> extends SwingWorker<R, Object> {
  private final ProgressHandle ph;

  public CrueSwingWorker(final String progessName) {
    this.ph = ProgressHandleFactory.createHandle(progessName);
  }

  @Override
  protected R doInBackground() throws Exception {
    ph.start();
    ph.switchToIndeterminate();
    return doProcess();
  }

  protected abstract R doProcess() throws Exception;

  @Override
  protected void done() {
    try {
      super.done();
      ph.finish();
      done(get());
    } catch (final Exception ex) {
      Exceptions.printStackTrace(ex);
    }
  }

  protected abstract void done(R result);
}
