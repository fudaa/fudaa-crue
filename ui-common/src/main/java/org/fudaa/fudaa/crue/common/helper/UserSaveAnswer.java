/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.helper;

/**
 *
 * @author deniger
 */
public enum UserSaveAnswer {

  CANCEL,
  DONT_SAVE,
  SAVE
}
