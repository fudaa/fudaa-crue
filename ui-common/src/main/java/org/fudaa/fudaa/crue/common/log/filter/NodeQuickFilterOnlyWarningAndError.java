package org.fudaa.fudaa.crue.common.log.filter;

import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class NodeQuickFilterOnlyWarningAndError extends AbstractNodeQuickFilter {

  public NodeQuickFilterOnlyWarningAndError() {
    super(null);
    name = NbBundle.getMessage(NodeQuickFilterOnlyWarningAndError.class, "filter.onlyWarningsAndError.name");
    description = NbBundle.getMessage(NodeQuickFilterOnlyWarningAndError.class, "filter.onlyWarningsAndError.description");
  }

  @Override
  public boolean accept(final Node node) {
    if (node == null) {
      return true;
    }
    final CtuluLogRecord lookup = node.getLookup().lookup(CtuluLogRecord.class);
    if (lookup == null) {
      return true;
    }
    return acceptValue(lookup);
  }

  @Override
  protected boolean acceptValue(final Object value) {
    final CtuluLogRecord lookup = (CtuluLogRecord) value;
    return lookup.getLevel().isLessVerboseThan(CtuluLogLevel.WARNING, false);
  }
}
