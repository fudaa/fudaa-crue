/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.log;

import java.awt.Image;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.Action;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.fudaa.crue.common.log.filter.NodeQuickFilterProcess;
import org.fudaa.fudaa.crue.common.log.property.LevelProperty;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Fred Deniger
 */
public final class CtuluLogRecordNode extends AbstractNode implements CtuluLogRecordContainer {

  public CtuluLogRecordNode(CtuluLogRecord record, ResourceBundle resourceBundle) {
    super(Children.LEAF, Lookups.singleton(record));
    setName(record.getMsg());
    setDisplayName(buildMessage(record, resourceBundle));
  }

  @Override
  public String toString() {
    return getDisplayName(); // NOI18N
  }

  @Override
  public Image getIcon(int type) {
    return LogIconTranslationProvider.getImage(getCtuluLogRecord().getLevel());
  }

  @Override
  public FilterNode createOnlyErrorNode() {
    if (isErrorNode(getCtuluLogRecord())) {
      return new FilterNode(this);
    }
    return null;
  }

  @Override
  public Action[] getActions(boolean context) {
    return new Action[0];
  }

  public static boolean isErrorNode(CtuluLogRecord logRecord) {
    final CtuluLogLevel level = logRecord.getLevel();
    return (CtuluLogLevel.ERROR.equals(level) || CtuluLogLevel.SEVERE.equals(level));
  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    Map<String, Node.Property> propertyById = new HashMap<>();
    final LevelProperty levelProperty = new LevelProperty(getCtuluLogRecord());
    PropertyCrueUtils.configureNoCustomEditor(levelProperty);
    set.put(CtuluLogRecordTraceNode.register(propertyById, levelProperty));
    setValue(NodeQuickFilterProcess.PROPERTY_BY_ID_KEY, Collections.unmodifiableMap(propertyById));
    sheet.put(set);
    return sheet;
  }

  protected String buildMessage(CtuluLogRecord record, ResourceBundle resourceBundle) {
    String msg = null;
    if (record.getLocalizedMessage() != null) {
      msg = record.getLocalizedMessage();
    } else {
      CtuluLogRecord.updateLocalizedMessage(record, resourceBundle==null?BusinessMessages.RESOURCE_BUNDLE:resourceBundle);
      msg = record.getLocalizedMessage();
    }
    return msg;
  }

  CtuluLogRecord getCtuluLogRecord() {
    return getLookup().lookup(CtuluLogRecord.class);
  }
}
