package org.fudaa.fudaa.crue.common.helper;

import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.openide.nodes.Node;

/**
 *
 * @author deniger
 */
public class NodeToStringTransformer implements ToStringTransformer {

  public NodeToStringTransformer() {
  }

  @Override
  public String transform(Object in) {
    return ((Node) in).getDisplayName();
  }
  
}
