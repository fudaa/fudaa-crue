package org.fudaa.fudaa.crue.common.editor;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyEditor;
import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.JSpinner;
import javax.swing.KeyStroke;
import javax.swing.SpinnerNumberModel;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;

/**
 *
 * @author deniger
 */
public class SpinnerInplaceEditor implements InplaceEditor, FocusListener {

  private final JSpinner spinner;
  private PropertyEditor editor = null;
  private PropertyModel model;

  public SpinnerInplaceEditor(SpinnerNumberModel model) {
    this.spinner = new JSpinner(model);
    spinner.getEditor().addFocusListener(this);

  }

  void componentEdited() {
  }

  @Override
  public void focusLost(FocusEvent e) {
    fire(COMMAND_SUCCESS);
  }

  @Override
  public void focusGained(FocusEvent e) {
  }

  @Override
  public void connect(PropertyEditor propertyEditor, PropertyEnv env) {
    editor = propertyEditor;
    reset();
  }

  @Override
  public JComponent getComponent() {
    return spinner;
  }

  @Override
  public void clear() {
    //avoid memory leaks:
    editor = null;
    model = null;
  }

  @Override
  public Object getValue() {
    try {
      spinner.commitEdit();
    } catch (ParseException parseException) {
    }
    return spinner.getValue();
  }

  @Override
  public void setValue(Object object) {
    try {
      spinner.setValue(object);
    } catch (IllegalArgumentException e) {
      spinner.setValue(null);
    }
  }

  @Override
  public boolean supportsTextEntry() {
    return true;
  }

  @Override
  public void reset() {
    Object d = editor.getValue();
    if (d != null) {
      setValue(d);
    }
  }

  @Override
  public KeyStroke[] getKeyStrokes() {
    return new KeyStroke[0];
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    return editor;
  }

  @Override
  public PropertyModel getPropertyModel() {
    return model;
  }

  @Override
  public void setPropertyModel(PropertyModel propertyModel) {
    this.model = propertyModel;
  }

  @Override
  public boolean isKnownComponent(Component component) {
    return component == spinner || spinner.isAncestorOf(component);
  }
  final Set<ActionListener> actionListeners = new HashSet<>();

  @Override
  public void addActionListener(ActionListener actionListener) {
    actionListeners.add(actionListener);
  }

  private void fire(String command) {
    ActionEvent evt = new ActionEvent(this, 0, command);
    for (ActionListener actionListener : actionListeners) {
      actionListener.actionPerformed(evt);

    }
  }

  @Override
  public void removeActionListener(ActionListener actionListener) {
    actionListeners.remove(actionListener);
  }
}
