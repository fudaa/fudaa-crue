 /*
  * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
  *
  * Copyright 1997-2010 Oracle and/or its affiliates. All rights reserved.
  *
  * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
  * Other names may be trademarks of their respective owners.
  *
  * The contents of this file are subject to the terms of either the GNU
  * General Public License Version 2 only ("GPL") or the Common
  * Development and Distribution License("CDDL") (collectively, the
  * "License"). You may not use this file except in compliance with the
  * License. You can obtain a copy of the License at
  * http://www.netbeans.org/cddl-gplv2.html
  * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
  * specific language governing permissions and limitations under the
  * License.  When distributing the software, include this License Header
  * Notice in each file and include the License file at
  * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
  * particular file as subject to the "Classpath" exception as provided
  * by Oracle in the GPL Version 2 section of the License file that
  * accompanied this code. If applicable, add the following below the
  * License Header, with the fields enclosed by brackets [] replaced by
  * your own identifying information:
  * "Portions Copyrighted [year] [name of copyright owner]"
  *
  * Contributor(s):
  *
  * The Original Software is NetBeans. The Initial Developer of the Original
  * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
  * Microsystems, Inc. All Rights Reserved.
  *
  * If you wish your version of this file to be governed by only the CDDL
  * or only the GPL Version 2, indicate your decision by adding
  * "[Contributor] elects to include this software in this distribution
  * under the [CDDL or GPL Version 2] license." If you do not indicate a
  * single choice of license, a recipient has the option to distribute
  * your version of this file under either the CDDL, the GPL Version 2 or
  * to extend the choice of license to its licensees as provided above.
  * However, if you add GPL Version 2 code and therefore, elected the GPL
  * Version 2 license, then the option applies only if the new code is
  * made subject to such option by the copyright holder.
  */
 package org.fudaa.fudaa.crue.common.view;

 import org.fudaa.fudaa.crue.common.PerspectiveEnum;
 import org.fudaa.fudaa.crue.common.services.PerspectiveService;
 import org.fudaa.fudaa.crue.common.services.SelectedPerspectiveService;
 import org.openide.explorer.ExplorerManager;
 import org.openide.explorer.propertysheet.PropertySheet;
 import org.openide.nodes.*;
 import org.openide.util.*;
 import org.openide.util.io.NbMarshalledObject;
 import org.openide.util.io.SafeException;
 import org.openide.windows.TopComponent;
 import org.openide.windows.WindowManager;

 import javax.swing.*;
 import java.awt.*;
 import java.beans.PropertyChangeEvent;
 import java.beans.PropertyChangeListener;
 import java.io.IOException;
 import java.io.ObjectInput;
 import java.io.ObjectOutput;
 import java.text.MessageFormat;
 import java.util.List;
 import java.util.*;
 import java.util.logging.Level;
 import java.util.logging.Logger;

 /**
  * @author netbeans
  */
 public class NbSheetCustom extends TopComponent {
   private static final Logger LOG = Logger.getLogger(NbSheetCustom.class.getName());
   /**
    * Name of a property that can be passed in a Node instance. The value of the property must be String and can be an alternative
    * to displayName.
    */
   private static final String PROP_LONGER_DISPLAY_NAME = "longerDisplayName"; // NOI18N
   /**
    * generated Serialized Version UID
    */
   static final long serialVersionUID = 7807519514644165460L;
   /**
    * listener to the property changes
    */
   private final transient Listener listener;
   /**
    * listener to the node changes, especially their destruction
    */
   private final transient SheetNodesListener snListener;
   /**
    * Should property sheet listen to the global changes ?
    */
   private boolean global;
   /**
    * the property sheet that is used to display nodes
    */
   private final PropertySheet propertySheet;
   /**
    * the nodes that are displayed in the property sheet
    */
   private transient Node[] nodes = new Node[0];
   private final PerspectiveEnum perspectiveEnum;
   private final transient SelectedPerspectiveService selectedPerspectiveService = Lookup.getDefault().lookup(SelectedPerspectiveService.class);

   /**
    * Constructor for new sheet. The sheet does not listen to global changes
    */
   public NbSheetCustom(final PerspectiveEnum perspectiveEnum) {
     this(false, perspectiveEnum);
   }

   /**
    * @param global should the content change when global properties changes?
    */
   public NbSheetCustom(final boolean global, final PerspectiveEnum perspectiveEnum) {
     this.global = global;
     this.propertySheet = new PropertySheet();
     this.perspectiveEnum = perspectiveEnum;

     // Instructs winsys to name this mode as single if only property sheet
     // is docked in this mode
     // it's workaround, should be solved throgh some Mode API in future
     // # 16888. Properties sheet is in single mode in SDI only.
//            putClientProperty(ModeImpl.NAMING_TYPE, ModeImpl.SDI_ONLY_COMP_NAME); // TEMP
     //Bugfix #36087: Fix naming type
     putClientProperty("NamingType", "BothOnlyCompName"); // NOI18N

     setLayout(new BorderLayout());
     add(propertySheet, BorderLayout.CENTER);

     setIcon(ImageUtilities.loadImage("org/netbeans/core/windows/resources/properties.gif", true)); // NOI18N

     // #36738 Component has to have a name from begining.
     updateTitle();
     // XXX - please rewrite to regular API when available - see issue #55955
     putClientProperty("SlidingName", org.openide.util.NbBundle.getMessage(NbSheetCustom.class, "CTL_PropertiesWindow")); //NOI18N

     // name listener and node listener
     listener = new Listener();

     snListener = new SheetNodesListener();

     setActivatedNodes(null);
   }

   /**
    * Transfer the focus to the property sheet.
    */
   @Override
   public void requestFocus() {
     super.requestFocus();
     propertySheet.requestFocus();
   }

   /**
    * Transfer the focus to the property sheet.
    */
   @Override
   public boolean requestFocusInWindow() {
     super.requestFocusInWindow();
     return propertySheet.requestFocusInWindow();
   }

   @Override
   public void open() {
     super.open();

     if (global) {
       // Set the nodes when opening.
       SwingUtilities.invokeLater(listener);
     }
   }

   /**
    * cache the title formatters, they are used frequently and are slow to construct
    */
   private static MessageFormat globalPropertiesFormat = null;

   /**
    * Changes name of the component to reflect currently displayed nodes. Called when set of displayed nodes has changed.
    */
   private void updateTitle() {
     // different naming for global and local sheets
     WindowManager.getDefault().findMode(this);
     String nodeTitle;

     // Fix a bug #12890, copy the nodes to prevent race condition.
     final List<Node> copyNodes = new ArrayList<>(Arrays.asList(nodes));

     Node node = null;

     if (!copyNodes.isEmpty()) {
       node = copyNodes.get(0);
     }

     if (node == null) {
       nodeTitle = "";  // NOI18N
     } else {
       nodeTitle = node.getDisplayName();
       final Object alternativeDisplayName = node.getValue(PROP_LONGER_DISPLAY_NAME);
       if (alternativeDisplayName instanceof String) {
         nodeTitle = (String) alternativeDisplayName;
       }
     }
     final Object[] titleParams = new Object[]{
         copyNodes.size(),
         nodeTitle
     };
     // different naming if docked in properties mode
     // NOI18N
     if (globalPropertiesFormat == null) {
       globalPropertiesFormat = new MessageFormat(NbBundle.getMessage(NbSheetCustom.class, "CTL_FMT_GlobalProperties"));
     }
     setName(globalPropertiesFormat.format(titleParams));
     setToolTipText(getName());
   }

   /**
    * Nodes to display.
    */
   public void setNodes(final Node[] nodes) {
     setNodes(nodes, true, "setNodes"); // NOI18N
   }

   private void setNodes(final Node[] initNodes, final boolean reattach, final String from) {
     final PerspectiveService currentPerspectiveService = selectedPerspectiveService.getCurrentPerspectiveService();
     if (currentPerspectiveService == null || currentPerspectiveService.getPerspective() != perspectiveEnum) {
       return;
     }
     LOG.log(
         Level.FINE, "setNodes({0}, {1}, {2})",
         new Object[]{Arrays.asList(initNodes), reattach, from});
     this.nodes = initNodes;
     propertySheet.setNodes(initNodes);
     SwingUtilities.invokeLater(this::updateTitle);
     if (reattach) {
       // re-attach to listen to new nodes
       snListener.detach();
       snListener.attach(initNodes);
     }
     LOG.log(
         Level.FINE, "finished setNodes({0}, {1}, {2})",
         new Object[]{Arrays.asList(initNodes), reattach, from});
   }

   final Node[] getNodes() {
     return nodes;
   }

   /**
    * Serialize this property sheet
    */
   @Override
   public void writeExternal(final ObjectOutput out)
       throws IOException {
     super.writeExternal(out);

     if (global) {
       // write dummy array
       out.writeObject(null);
     } else {
       final Node.Handle[] arr = NodeOp.toHandles(nodes);
       out.writeObject(arr);
     }

     out.writeBoolean(global);
   }

   /**
    * Deserialize this property sheet.
    */
   @Override
   public void readExternal(final ObjectInput in)
       throws IOException, ClassNotFoundException {
     try {
       super.readExternal(in);
     } catch (final SafeException se) {
       // ignore--we really do not care about the explorer manager that much
     }
     final Object obj = in.readObject();

     if (obj instanceof NbMarshalledObject || obj instanceof ExplorerManager) {
       // old version read the Boolean
       global = (Boolean) in.readObject();
     } else {
       Node[] ns;

       if (obj == null) {
         // handles can also be null for global
         // property sheet
         ns = TopComponent.getRegistry().getActivatedNodes();
       } else {
         // new version, first read the nodes and then the global boolean
         final Node.Handle[] arr = (Node.Handle[]) obj;

         try {
           ns = NodeOp.fromHandles(arr);
         } catch (final IOException ex) {
           Exceptions.printStackTrace(ex);
           ns = new Node[0];
         }
       }

       global = in.readBoolean();

       setNodes(ns, true, "readExternal"); // NOI18N
     }
   }

   /**
    * Helper, listener variable must be initialized before calling this
    */
   private void updateGlobalListening(final boolean listen) {
     if (global) {
       if (listen) {
         TopComponent.getRegistry().addPropertyChangeListener(
             listener);
       } else {
         TopComponent.getRegistry().removePropertyChangeListener(listener);
       }
     }
   }

   @Override
   protected void componentOpened() {
     updateGlobalListening(true);
   }

   @Override
   protected void componentClosed() {
     updateGlobalListening(false);
     setNodes(new Node[0], true, "componentClosed"); // NOI18N
   }

   @Override
   protected void componentDeactivated() {
     super.componentDeactivated();
     if (Utilities.isMac()) {
       propertySheet.firePropertyChange("MACOSX", true, false);
     }
   }

   /**
    * Change listener to changes in selected nodes. And also nodes listener to listen to global changes of the nodes.
    */
   private class Listener implements Runnable, PropertyChangeListener {
     Listener() {
     }

     @Override
     public void propertyChange(final PropertyChangeEvent ev) {
       if (TopComponent.Registry.PROP_ACTIVATED_NODES.equals(ev.getPropertyName())) {
         activate();
       }
     }

     @Override
     public void run() {
       activate();
     }

     public void activate() {
       final TopComponent tc = TopComponent.getRegistry().getActivated();
       final Node[] arr = TopComponent.getRegistry().getActivatedNodes();
       LOG.log(Level.FINE, "Active component {0}", tc);
       setNodes(arr, true, "activate"); // NOI18N
     }
   }

   /**
    * Change listener to changes in selected nodes. And also nodes listener to listen to global changes of the nodes.
    */
   private class SheetNodesListener extends NodeAdapter implements Runnable {
     /*
      * maps nodes to their listeners (Node, WeakListener)
      */
     private HashMap<Node, NodeListener> listenerMap;
     /*
      * maps nodes to their proeprty change listeners (Node, WeakListener)
      */
     private HashMap<Node, PropertyChangeListener> pListenerMap;

     SheetNodesListener() {
     }

     /**
      * Fired when the node is deleted.
      *
      * @param ev event describing the node
      */
     @Override
     public void nodeDestroyed(final NodeEvent ev) {
       Mutex.EVENT.readAccess(() -> handleNodeDestroyed(ev));
     }

     final void handleNodeDestroyed(final NodeEvent ev) {
       assert EventQueue.isDispatchThread();

       final Node destroyedNode = ev.getNode();
       final NodeListener listenerForDestroyedNode = listenerMap.get(destroyedNode);
       final PropertyChangeListener pListener = pListenerMap.get(destroyedNode);
       // stop to listen to destroyed node
       destroyedNode.removeNodeListener(listenerForDestroyedNode);
       destroyedNode.removePropertyChangeListener(pListener);
       listenerMap.remove(destroyedNode);
       pListenerMap.remove(destroyedNode);
       // close top component (our outer class) if last node was destroyed
       if (listenerMap.isEmpty() && !global) {
         //fix #39251 start - posting the closing of TC to awtevent thread
         close();
         //fix #39251 end
       } else {
         setNodes(
             (listenerMap.keySet().toArray(new Node[0])), false, "handleNodeDestroyed" // NOI18N
         );
       }
     }

     void attach(final Node[] nodes) {
       assert EventQueue.isDispatchThread();
       listenerMap = new HashMap<>(nodes.length * 2);
       pListenerMap = new HashMap<>(nodes.length * 2);
       NodeListener curListener ;
       PropertyChangeListener pListener;
       // start to listen to all given nodes and map nodes to
       // their listeners
       for (final Node n : nodes) {
         curListener = org.openide.nodes.NodeOp.weakNodeListener(this, n);
         pListener = org.openide.util.WeakListeners.propertyChange(this, n);
         listenerMap.put(n, curListener);
         pListenerMap.put(n, pListener);
         n.addNodeListener(curListener);
         n.addPropertyChangeListener(pListener);
       }
     }

     void detach() {
       assert EventQueue.isDispatchThread();
       if (listenerMap == null) {
         return;
       }
       // stop to listen to all nodes
       for (Map.Entry<Node, NodeListener> curEntry : listenerMap.entrySet()) {
         curEntry.getKey().removeNodeListener(curEntry.getValue());
       }
       for (Map.Entry<Node, PropertyChangeListener> curEntry : pListenerMap.entrySet()) {
         curEntry.getKey().removePropertyChangeListener(curEntry.getValue());
       }
       // destroy the map
       listenerMap = null;
       pListenerMap = null;
     }

     @Override
     public void propertyChange(final PropertyChangeEvent pce) {
       if (Node.PROP_DISPLAY_NAME.equals(pce.getPropertyName())) {
         SwingUtilities.invokeLater(this);
       }
     }

     @Override
     public void run() {
       assert EventQueue.isDispatchThread();
       updateTitle();
     }
   } // End of SheetNodesListener.
 }
