package org.fudaa.fudaa.crue.common;

import java.util.EnumMap;
import java.util.Map;
import javax.swing.Icon;
import org.fudaa.dodico.crue.metier.emh.EnumCatEMH;
import org.openide.util.ImageUtilities;

/**
 *
 * @author deniger
 */
public class CrueIconsProvider {

  private static final Map<EnumCatEMH, String> ICON_TYPE_EMH = new EnumMap<>(EnumCatEMH.class);
  private static final Map<EnumCatEMH, String> BASE_BY_TYPE_EMH = new EnumMap<>(EnumCatEMH.class);
  private static final Map<EnumCatEMH, Icon> ICON_BY_TYPE_EMH = new EnumMap<>(EnumCatEMH.class);

  static {
    ICON_TYPE_EMH.put(EnumCatEMH.SCENARIO, "scenario.png");
    ICON_TYPE_EMH.put(EnumCatEMH.MODELE, "modele.png");
    ICON_TYPE_EMH.put(EnumCatEMH.SOUS_MODELE, "sous-modele.png");
  }

  public static String getCompleteName(String fileName) {
    return "org/fudaa/fudaa/crue/common/icons/" + fileName;
  }
  private static Icon etuIcon = null;

  public static Icon getEtuIcon() {
    if (etuIcon == null) {
      final String name = "etude.png";
      etuIcon = getIconFromFilename(name);
    }
    return etuIcon;
  }

  public static String getIconBase(EnumCatEMH typeEMH) {
    String iconBase = BASE_BY_TYPE_EMH.get(typeEMH);
    if (iconBase == null && ICON_TYPE_EMH.containsKey(typeEMH)) {
      iconBase = getCompleteName(ICON_TYPE_EMH.get(typeEMH));
      BASE_BY_TYPE_EMH.put(typeEMH, iconBase);
    }
    return iconBase;
  }

  public static Icon getIcon(String baseName) {
    return ImageUtilities.image2Icon(ImageUtilities.loadImage(baseName));
  }

  public static Icon getIcon(EnumCatEMH typeEMH) {
    Icon iconBase = ICON_BY_TYPE_EMH.get(typeEMH);
    if (iconBase == null && ICON_TYPE_EMH.containsKey(typeEMH)) {
      iconBase = ImageUtilities.image2Icon(ImageUtilities.loadImage(getIconBase(typeEMH)));
      ICON_BY_TYPE_EMH.put(typeEMH, iconBase);
    }
    return iconBase;
  }

  public static String getIconBaseFile() {
    return getCompleteName("file.png");
  }

  public static String getIconBaseFileBroken() {
    return getCompleteName("file-broken.png");
  }

  public static String getIconBaseRun() {
    return getCompleteName("run.png");
  }

  public static String getIconBaseRunCourant() {
    return getCompleteName("element-courant-non-charge.png");
  }

  public static String getIconBaseScenarioCourant() {
    return getCompleteName("element-courant-non-charge.png");
  }


  public static Icon getIconRun() {
    return getIconFromFilename("run.png");
  }

  public static Icon getIconRunCourant() {
    return getIcon(getIconBaseRunCourant());
  }

  public static String getFileIconBase() {
    return getCompleteName("file.png");
  }

  public static String getDirIconBase() {
    return getCompleteName("folder.png");
  }

  public static Icon getIconFromFilename(final String name) {
    return getIcon(getCompleteName(name));
  }
}
