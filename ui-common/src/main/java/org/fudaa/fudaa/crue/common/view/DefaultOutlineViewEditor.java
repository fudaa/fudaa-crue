package org.fudaa.fudaa.crue.common.view;

import com.memoire.bu.BuGridLayout;
import org.apache.commons.lang3.ArrayUtils;
import org.fudaa.fudaa.crue.common.helper.DisableSortHelper;
import org.fudaa.fudaa.crue.common.node.NodeTreeListener;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * @author Frédéric Deniger
 */
public abstract class DefaultOutlineViewEditor extends JPanel implements ExplorerManager.Provider, Lookup.Provider {
  private final ExplorerManager em = new ExplorerManager();
  protected OutlineView view;
  protected JButton btAdd;
  protected JButton btPaste;
  protected boolean editable;
  protected boolean useAddButton = true;
  protected boolean useRemoveButton = true;
  private final ExplorerManagerChangeListener explorerManagerChangeListener = new ExplorerManagerChangeListener();
  private final NodeTreeListener treeListener = new NodeTreeListener() {
    @Override
    public void changeDoneInNode(String propertyName) {
      if (!Node.PROP_PARENT_NODE.equals(propertyName)) {
        changeInNode(propertyName);
      }
    }
  };
  private DisableSortHelper sortDisabler;
  private transient Lookup lookup;

  public DefaultOutlineViewEditor() {
    super(new BorderLayout(5, 5));
    initView();
    em.addPropertyChangeListener(explorerManagerChangeListener);
  }

  public boolean isUseRemoveButton() {
    return useRemoveButton;
  }

  public OutlineView getView() {
    return view;
  }

  protected void explorerChanged(Node old) {
    if (old != null) {
      treeListener.removeListener(old);
    }
    changeInNode("nodeChanged");
    treeListener.installListener(em.getRootContext());
  }

  protected void changeInNode(String propertyName) {
  }

  @Override
  public Lookup getLookup() {
    return lookup;
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return em;
  }

  protected abstract void addElement();

  protected void initActions() {
    ActionMap map = this.getActionMap();
    final Action actionDelete = ExplorerUtils.actionDelete(em, false);
    map.put("delete", actionDelete); //NOI18N
    InputMap keys = getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    keys.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "delete");
    JPanel addRemove = new JPanel(new BuGridLayout(1, 5, 5, true, true));
    addRemove.setOpaque(false);
    addRemove.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 3));
    if (useAddButton) {
      btAdd = new JButton(NbBundle.getMessage(DefaultOutlineViewEditor.class, "Add.Name"));
      btAdd.addActionListener(e -> addElement());
      addRemove.add(btAdd);
    }

    btPaste = createPasteComponent();
    if (btPaste != null) {
      addRemove.add(btPaste);
    }
    final JComponent[] otherButtons = createOtherButtons();
    if (ArrayUtils.isNotEmpty(otherButtons)) {
      for (JComponent otherButton : otherButtons) {
        addRemove.add(otherButton);
      }
    }
    if (useRemoveButton) {
      addRemove.add(new JSeparator());
      final JButton jButtonDelete = new JButton(actionDelete);
      jButtonDelete.setText(NbBundle.getMessage(DefaultOutlineViewEditor.class, "Delete.Name"));
      addRemove.add(jButtonDelete);
    }
    add(addRemove, BorderLayout.EAST);
    lookup = ExplorerUtils.createLookup(em, map);
    ExplorerUtils.activateActions(em, true);
    view.setNodePopupFactory(new CustomNodePopupFactory());
  }

  protected JButton createPasteComponent() {
    return null;
  }

  protected JComponent[] createOtherButtons() {
    return null;
  }

  protected abstract void createOutlineView();

  private void initView() {
    setOpaque(false);
    createOutlineView();
    add(view);
    initActions();
    sortDisabler = new DisableSortHelper(view);
  }

  public void setEditable(boolean editable) {
    this.editable = editable;
    sortDisabler.setDisableSort(editable);
    if (btAdd != null) {
      btAdd.setEnabled(editable);
    }
    if (btPaste != null) {
      btPaste.setEnabled(editable);
    }
  }

  private class ExplorerManagerChangeListener implements PropertyChangeListener {
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if (ExplorerManager.PROP_ROOT_CONTEXT.equals(evt.getPropertyName())) {
        explorerChanged((Node) evt.getOldValue());
      }
    }
  }
}
