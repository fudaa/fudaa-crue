package org.fudaa.fudaa.crue.common.helper;

import com.memoire.bu.BuValueValidator;

public class PositiveIntegerValidator extends BuValueValidator {
    @Override
    public boolean isValueValid(Object _value) {
        if (_value instanceof Integer) {
            return ((Integer) _value).intValue() > 0;
        }
        return false;
    }
}
