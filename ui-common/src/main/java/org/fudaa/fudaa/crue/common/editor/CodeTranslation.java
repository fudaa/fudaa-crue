package org.fudaa.fudaa.crue.common.editor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author deniger
 */
public class CodeTranslation {

  public final List<Integer> intValues = new ArrayList<>();
  public final List<String> strings = new ArrayList<>();

  public Map<Integer, String> geti18nByValues() {
    Map<Integer, String> i18nByValues = new HashMap<>();
    for (int i = 0; i < intValues.size(); i++) {
      i18nByValues.put(intValues.get(i), strings.get(i));
    }
    return i18nByValues;
  }

  public Integer[] getIntegers() {
    return intValues.toArray(new Integer[0]);
  }

  public Map<String, Integer> getValuesByi18n() {
    Map<String, Integer> i18nByValues = new HashMap<>();
    for (int i = 0; i < intValues.size(); i++) {
      i18nByValues.put(strings.get(i), intValues.get(i));
    }
    return i18nByValues;
  }
}
