package org.fudaa.fudaa.crue.common.editor;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.beans.PropertyEditor;
import java.util.Map;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;

/**
 *
 * @author deniger
 */
public class JcomboBoxIntegerInplaceEditor implements InplaceEditor {

  private final JComboBox combobox;
  private PropertyEditor editor = null;
  private PropertyModel model;
  final Map<Integer, String> i18nByValues;

  public JcomboBoxIntegerInplaceEditor(CodeTranslation codes) {
    this.combobox = new JComboBox(codes.getIntegers());
    i18nByValues = codes.geti18nByValues();
    combobox.setRenderer(new CtuluCellTextRenderer() {

      @Override
      protected void setValue(Object _value) {
        super.setValue(i18nByValues.get(_value));
      }
    });
  }

  @Override
  public void connect(PropertyEditor propertyEditor, PropertyEnv env) {
    editor = propertyEditor;
    reset();
  }

  @Override
  public JComponent getComponent() {
    return combobox;
  }

  @Override
  public void clear() {
    //avoid memory leaks:
    editor = null;
    model = null;
  }

  @Override
  public Object getValue() {
    return combobox.getSelectedItem();
  }

  @Override
  public void setValue(Object object) {
    try {
      combobox.setSelectedItem(object);
    } catch (IllegalArgumentException e) {
      combobox.setSelectedItem(null);
    }
  }

  @Override
  public boolean supportsTextEntry() {
    return true;
  }

  @Override
  public void reset() {
    Object d = editor.getValue();
    if (d != null) {
      setValue(d);
    }
  }

  @Override
  public KeyStroke[] getKeyStrokes() {
    return new KeyStroke[0];
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    return editor;
  }

  @Override
  public PropertyModel getPropertyModel() {
    return model;
  }

  @Override
  public void setPropertyModel(PropertyModel propertyModel) {
    this.model = propertyModel;
  }

  @Override
  public boolean isKnownComponent(Component component) {
    return component == combobox || combobox.isAncestorOf(component);
  }

  @Override
  public void addActionListener(ActionListener actionListener) {
    //do nothing - not needed for this component
  }

  @Override
  public void removeActionListener(ActionListener actionListener) {
    //do nothing - not needed for this component
  }
}
