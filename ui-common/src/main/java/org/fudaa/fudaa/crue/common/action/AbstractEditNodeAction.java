package org.fudaa.fudaa.crue.common.action;

import org.fudaa.fudaa.crue.common.services.SelectedPerspectiveService;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 * Une NodeAction active seulement si la perspective courante est en edition
 * @author deniger
 */
public abstract class AbstractEditNodeAction extends AbstractNodeAction {

  private final SelectedPerspectiveService selectedPerspectiveService = Lookup.getDefault().lookup(SelectedPerspectiveService.class);

  public AbstractEditNodeAction(String name) {
    super(name);
  }

  @Override
  protected boolean preEnable(Node[] activatedNodes) {
    return super.preEnable(activatedNodes) && selectedPerspectiveService.isCurrentPerspectiveInEditMode();
  }
}
