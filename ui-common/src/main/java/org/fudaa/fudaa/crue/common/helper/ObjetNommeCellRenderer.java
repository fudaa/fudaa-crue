package org.fudaa.fudaa.crue.common.helper;

import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.dodico.crue.common.contrat.ObjetNomme;

/**
 *
 * @author deniger
 */
public class ObjetNommeCellRenderer extends CtuluCellTextRenderer {

  @Override
  protected void setValue(Object _value) {
    if (_value == null) {
      super.setValue(" ");
    } else {
      final String nom = ((ObjetNomme) _value).getNom();
      super.setValue(nom);
      setToolTipText(nom);
    }
  }
}
