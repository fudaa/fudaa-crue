package org.fudaa.fudaa.crue.common.log.filter;

import org.apache.commons.lang3.StringUtils;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class NodeQuickFilterNotEmptyValue extends AbstractNodeQuickFilter {

  public NodeQuickFilterNotEmptyValue(final String propertyId, final String propertyName) {
    super(propertyId);
    description = NbBundle.getMessage(NodeQuickFilterEmptyValue.class, "filter.notEmptyValue.description", propertyName);
    name = NbBundle.getMessage(NodeQuickFilterEmptyValue.class, "filter.notEmptyValue.name", propertyName);
  }

  @Override
  protected boolean acceptValue(final Object value) {
    return value != null && !StringUtils.EMPTY.equals(value);
  }
}
