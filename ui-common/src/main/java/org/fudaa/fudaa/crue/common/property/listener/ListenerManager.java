package org.fudaa.fudaa.crue.common.property.listener;

import java.util.ArrayList;
import java.util.List;

/**
 * Gestion d'un ensemble de {@link UnregisterableListener}.
 *
 * @author Fred Deniger
 */
public class ListenerManager {

  private final List<UnregisterableListener> listeners = new ArrayList<>();

  /**
   * Enregistre le listener et appelle la methode {@link UnregisterableListener#register() }
   *
   * @param listener listener a jouter
   */
  public void registerAndaddListener(UnregisterableListener listener) {
    listener.register();
    listeners.add(listener);
  }

  /**
   * Efface la liste de listener et appelle la methode {@link UnregisterableListener#unregister() } sur chaque listener.
   */
  public void clean() {
    for (UnregisterableListener unregisterableListener : listeners) {
      unregisterableListener.unregister();
    }
    listeners.clear();
  }
}
