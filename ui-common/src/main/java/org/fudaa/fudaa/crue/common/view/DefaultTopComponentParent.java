package org.fudaa.fudaa.crue.common.view;

public interface DefaultTopComponentParent {
    boolean isEditable();


    void setModified(boolean b);
}
