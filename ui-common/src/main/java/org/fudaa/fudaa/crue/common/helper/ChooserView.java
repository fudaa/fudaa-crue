package org.fudaa.fudaa.crue.common.helper;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.ListView;
import org.openide.nodes.Node;

/**
 * Utiliser pour selectionner un noeud.
 *
 * @author deniger
 */
public class ChooserView extends JPanel implements ExplorerManager.Provider {

  private final ExplorerManager em = new ExplorerManager();

  public ChooserView(final Node rootNode) {
    final ListView view = new ListView();
    view.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    setLayout(new BorderLayout());
    add(view);
    em.setRootContext(rootNode);
  }


  public Node getSelectedNode() {
    final Node[] selectedNodes = em.getSelectedNodes();
    if (selectedNodes != null && selectedNodes.length == 1) {
      return selectedNodes[0];
    }
    return null;
  }

  @Override
  public ExplorerManager getExplorerManager() {
    return em;
  }
}
