package org.fudaa.fudaa.crue.common.action;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.services.SelectedPerspectiveService;
import org.openide.util.ContextAwareAction;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;

import javax.swing.*;
import java.awt.*;

/**
 * @author deniger
 */
public abstract class AbstractPerspectiveAwareAction extends AbstractAction implements ContextAwareAction, LookupListener {
    private final PerspectiveEnum perspective;
    private final boolean enableInEditModeOnly;
    private final Result<PerspectiveEnum> resultatPerspective;
    private final SelectedPerspectiveService perspectiveService = Lookup.getDefault().lookup(SelectedPerspectiveService.class);

    public AbstractPerspectiveAwareAction(PerspectiveEnum perspective, boolean enableInEditModeOnly) {
        this.perspective = perspective;
        resultatPerspective = perspectiveService.getLookup().lookupResult(PerspectiveEnum.class);
        resultatPerspective.addLookupListener(this);
        this.enableInEditModeOnly = enableInEditModeOnly;
        if (enableInEditModeOnly) {
            perspectiveService.addEditionChangeListener(evt -> resultChanged(null));
            setEnabled(perspectiveService.isCurrentPerspectiveInEditMode());
        }
    }

    public String getActionTitle() {
        return (String) getValue(NAME);
    }

    /**
     * Methode a implémenter pour modifier l'état de l'action
     *
     * @return true si enabled
     */
    protected boolean getEnableState() {
        final boolean perspectiveActivated = isMyPerspectiveActivated();
        if (perspectiveActivated && enableInEditModeOnly) {
            return perspectiveService.isCurrentPerspectiveInEditMode();
        }
        return perspectiveActivated;
    }

    protected boolean isMyPerspectiveActivated() {
        return perspectiveService != null && perspectiveService.isActivated(perspective);
    }

    /**
     * Redéfini #getEnableState pour modifier l'état.
     *
     * @param ev non utilise
     */
    @Override
    public final void resultChanged(LookupEvent ev) {
        updateEnableState();
    }

    protected void updateEnableState() {
        if (!EventQueue.isDispatchThread()) {
            EventQueue.invokeLater(this::updateEnableState);
            return;
        }
        setEnabled(getEnableState());
    }
}
