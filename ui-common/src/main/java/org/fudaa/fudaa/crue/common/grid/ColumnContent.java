/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.grid;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.fudaa.fudaa.crue.common.node.NodeHelper;

/**
 *
 * @author Frederic Deniger
 */
public class ColumnContent<T extends JComponent> {
  
  final List<T> components = new ArrayList<>();
  
  public static ColumnContent<JLabel> createLabelContent(String title) {
    ColumnContent<JLabel> res = new ColumnContent<>();
    res.components.add(new JLabel(title));
    return res;
  }

  public static ColumnContent<JTextField> createNonEditableTextContent(String title) {
    ColumnContent<JTextField> res = new ColumnContent<>();
    final JTextField jTextField = new JTextField(title);
    jTextField.setEditable(false);
    res.components.add(jTextField);
    return res;
  }
  
  public static ColumnContent<JLabel> createLabelContentBold(String title) {
    ColumnContent<JLabel> res = new ColumnContent<>();
    res.components.add(NodeHelper.useBoldFont(new JLabel(title)));
    return res;
  }
  
  public List<T> getComponents() {
    return components;
  }
  
  public void setEnabled(boolean b) {
    for (T t : components) {
      t.setEnabled(b);
    }
  }
  
  JComponent createComponent() {
    final int size = components.size();
    if (size == 0) {
      return new JLabel();
    } else if (size == 1) {
      return components.get(0);
    } else {
      JPanel res = new JPanel(new GridLayout(1, size));
      for (T t : components) {
        res.add(t);
      }
      return res;
    }
  }
}
