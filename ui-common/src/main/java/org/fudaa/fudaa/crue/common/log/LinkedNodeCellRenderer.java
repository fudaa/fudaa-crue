/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.log;

import java.awt.Component;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import org.apache.commons.lang3.StringUtils;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;

/**
 *
 * @author Frederic Deniger
 */
public class LinkedNodeCellRenderer extends DefaultTableCellRenderer {

  private final boolean multimedia;
  private static final Icon HELP_ICON = ImageUtilities.loadImageIcon("org/fudaa/fudaa/crue/common/icons/help.png", false);
  private static final Icon MULTIMEDIA_ICON = ImageUtilities.loadImageIcon("org/fudaa/fudaa/crue/common/icons/multimedia.png", false);

  public LinkedNodeCellRenderer(TableCellRenderer init, boolean multimedia) {
    this.multimedia = multimedia;
  }

  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    setIcon(null);
    setText(" ");
    setToolTipText(" ");
    
    if (value != null) {

      Node.Property property = (Node.Property) value;
      try {
        if (StringUtils.isNotBlank((String) property.getValue())) {
          String html = "<a href=\"" + property.getValue() + "\">" + property.getValue() + "</a>";
          setToolTipText(html);
          setIcon(multimedia ? MULTIMEDIA_ICON : HELP_ICON);

        }
      } catch (Exception illegalAccessException) {
        Logger.getLogger(LinkedNodeCellRenderer.class.getName()).log(Level.INFO, "message {0}", illegalAccessException);
      }
    }
    return this;
  }
}
