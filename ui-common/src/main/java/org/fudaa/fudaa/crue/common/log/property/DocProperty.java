/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.log.property;

import java.lang.reflect.InvocationTargetException;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.openide.nodes.PropertySupport;
import org.openide.util.NbBundle;

/**
 *
 * @author Fred Deniger
 */
public class DocProperty extends PropertySupport.ReadOnly<String> {

  public static final String ID = "Doc";
  private final CtuluLogRecord record;

  public DocProperty(CtuluLogRecord record) {
    super(ID, String.class, getDefaultDisplayName(),
          getDescription());
    this.record = record;
    PropertyCrueUtils.configureNoCustomEditor(this);
  }

  public static String getDescription() {
    return NbBundle.getMessage(DocProperty.class, "DocPropertyDescription");
  }

  public static String getDefaultDisplayName() {
    return NbBundle.getMessage(DocProperty.class, "DocPropertyName");
  }

  public static PropertyColumnFilterable createColumn() {
    PropertyColumnFilterable res = new PropertyColumnFilterable();
    res.setColumnId(ID);
    res.setDescription(getDescription());
    res.setDisplayName(getDefaultDisplayName());
    return res;
  }

  @Override
  public String toString() {
    return StringUtils.defaultString(record.getHelpUrl());
  }

  @Override
  public String getValue() throws IllegalAccessException, InvocationTargetException {
    return toString();
  }
}
