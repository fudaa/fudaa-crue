package org.fudaa.fudaa.crue.common.log.property;

import gnu.trove.TObjectIntHashMap;
import java.awt.Component;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.MissingResourceException;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import org.fudaa.fudaa.crue.common.log.CtuluLogsTopComponent;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.fudaa.fudaa.crue.common.log.filter.NodeQuickFilterEqualsTo;
import org.fudaa.fudaa.crue.common.log.filter.NodeQuickFilterLevelDetailLessVerbose;
import org.fudaa.fudaa.crue.common.log.filter.NodeQuickFilterLevelDetailMoreVerbose;
import org.netbeans.swing.etable.ETable;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class LevelDetailStringPropertyColumn extends PropertyColumnFilterable {

  @Override
  public void createFilterMenu(JPopupMenu res, Component component, int row, int column) throws MissingResourceException {
    JMenu menu = new JMenu(NbBundle.getMessage(PropertyColumnFilterable.class, "filter.mainMenuName", displayName));
    res.add(menu);
    ETable et = (ETable) component;
    CtuluLogsTopComponent logs = (CtuluLogsTopComponent) ((JComponent) component).getClientProperty(
            CtuluLogsTopComponent.class.getSimpleName());
    Object valueAt = et.getValueAt(row, column);
    if (valueAt instanceof Node.Property) {
      try {
        valueAt = ((Node.Property) valueAt).getValue();
      } catch (Exception ex) {
        Exceptions.printStackTrace(ex);
      }
    }
    menu.add(create(new NodeQuickFilterEqualsTo(columnId, displayName, valueAt), logs));
    TObjectIntHashMap<String> verbosityByLevel = logs.getVerbosityByLevel();
    List<String> verbosity = new ArrayList<>(Arrays.asList(verbosityByLevel.keys(new String[verbosityByLevel.size()])));
    Collections.sort(verbosity, new ComparatorWithMap(verbosityByLevel));
    menu.addSeparator();
    JMenu sousMenu = createSousMenu(verbosity.get(0));
    menu.add(sousMenu);
    sousMenu.add(create(new NodeQuickFilterEqualsTo(columnId, displayName, verbosity.get(0)), logs));
    for (int i = 1; i < verbosity.size() - 1; i++) {
      final String verbosityLevel = verbosity.get(i);
      sousMenu = createSousMenu(verbosityLevel);
      menu.add(sousMenu);
      sousMenu.add(create(new NodeQuickFilterEqualsTo(columnId, displayName, verbosityLevel), logs));
      sousMenu.add(create(new NodeQuickFilterLevelDetailLessVerbose(columnId, displayName, verbosityLevel, true, verbosityByLevel),
                          logs));
      sousMenu.add(create(
              new NodeQuickFilterLevelDetailLessVerbose(columnId, displayName, verbosityLevel, false, verbosityByLevel),
              logs));
      sousMenu.add(create(new NodeQuickFilterLevelDetailMoreVerbose(columnId, displayName, verbosityLevel, true, verbosityByLevel),
                          logs));
      sousMenu.add(create(
              new NodeQuickFilterLevelDetailMoreVerbose(columnId, displayName, verbosityLevel, false, verbosityByLevel),
              logs));
    }
    final String menuName = verbosity.get(verbosity.size() - 1);
    sousMenu = createSousMenu(menuName);
    menu.add(sousMenu);
    sousMenu.add(create(new NodeQuickFilterEqualsTo(columnId, displayName, menuName), logs));
  }

  private JMenu createSousMenu(final String verbosityLevel) {
    JMenu sousMenu;
    Image imageFromDetail = LogIconTranslationProvider.getImageFromDetail(verbosityLevel);
    sousMenu = new JMenu(verbosityLevel);
    if (imageFromDetail != null) {
      sousMenu.setIcon(new ImageIcon(imageFromDetail));
    }
    return sousMenu;
  }

  private static class ComparatorWithMap implements Comparator<String> {

    private final TObjectIntHashMap<String> verbosityByLevel;

    public ComparatorWithMap(TObjectIntHashMap<String> verbosityByLevel) {
      this.verbosityByLevel = verbosityByLevel;
    }

    @Override
    public int compare(String o1, String o2) {
      int i1 = verbosityByLevel.get(o1);
      int i2 = verbosityByLevel.get(o2);
      return i1 - i2;
    }
  }
}
