package org.fudaa.fudaa.crue.common.pdt;

import java.awt.Component;
import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditorSupport;
import org.apache.commons.lang3.ObjectUtils;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.Pdt;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.nodes.Node;

/**
 *
 * @author Frédéric Deniger
 */
public class PdtEditorSupport extends PropertyEditorSupport implements ExPropertyEditor, PropertyChangeListener {

  final PdtPanelEditor panelEditor = new PdtPanelEditor();
  private final CrueConfigMetier ccm;
  PropertyEnv env;

  public PdtEditorSupport(CrueConfigMetier ccm) {
    this.ccm = ccm;
  }

  @Override
  public void attachEnv(PropertyEnv env) {
    env.addPropertyChangeListener(this);
    env.setState(PropertyEnv.STATE_NEEDS_VALIDATION);
    FeatureDescriptor featureDescriptor = env.getFeatureDescriptor();
    PropertyCrueUtils.configureNoEditAsText(featureDescriptor);
    this.env = env;
  }

  @Override
  public String getAsText() {
    return ObjectUtils.toString(getValue());
  }
  
   @Override
  public void setAsText(String text) throws IllegalArgumentException {
  }
  
  

  @Override
  public boolean supportsCustomEditor() {
    return true;
  }

  @Override
  public Component getCustomEditor() {
    Node.Property featureDescriptor = (Node.Property) env.getFeatureDescriptor();
    panelEditor.setEditable(featureDescriptor.canWrite());
    panelEditor.setCcm(ccm);
    panelEditor.setPdt((Pdt) getValue());
    return panelEditor;
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (PropertyEnv.PROP_STATE.equals(evt.getPropertyName())
            && evt.getNewValue() == PropertyEnv.STATE_VALID) {
      setValue(panelEditor.getPdt());
    }
  }
}
