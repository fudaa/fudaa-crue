package org.fudaa.fudaa.crue.common.log;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogLevel;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public class CtuluLogGroupNode extends AbstractNode implements CtuluLogRecordContainer {

  public CtuluLogGroupNode(CtuluLogGroup group, CtuluLogGroupChildFactory.LogRecordBuilder childFactoryBuilder) {
    super(CtuluLogGroupChildFactory.createChildren(group, childFactoryBuilder), Lookups.singleton(group));
    setDisplayName(group.getDesci18n());
    setName(group.getDescription()==null?"LogsGroups":group.getDescription());
    CtuluLogLevel higherLevel = group.getHigherLevel();
    if (higherLevel != null) {
      setIconBaseWithExtension(LogIconTranslationProvider.getIconBase(higherLevel));
    }
  }

  @Override
  public FilterNode createOnlyErrorNode() {
    final Children onlyErrorChildren = CtuluLogGroupChildFactory.createOnlyErrorChildren(this);
    if (onlyErrorChildren == null || onlyErrorChildren.getNodesCount() == 0) {
      return null;
    }
    return new FilterNode(this, onlyErrorChildren);
  }
}
