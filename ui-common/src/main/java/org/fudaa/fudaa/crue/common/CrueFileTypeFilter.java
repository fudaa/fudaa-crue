/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common;

import org.fudaa.dodico.crue.metier.CrueFileType;
import org.openide.util.NbBundle;

import javax.swing.filechooser.FileFilter;
import java.io.File;
import java.util.List;

/**
 * @author Chris
 */
public class CrueFileTypeFilter extends FileFilter {
  private final List<CrueFileType> types;

  public CrueFileTypeFilter(List<CrueFileType> types) {
    this.types = types;
  }

  @Override
  public boolean accept(File f) {
    if ((f == null) || f.isHidden()) {
      return false;
    }
    //pour voir les répertoires !
    if (f.isDirectory()) {
      return true;
    }

    for (CrueFileType type : types) {
      if (type.hasExtension(f)) {
        return true;
      }
    }

    return false;
  }

  @Override
  public String getDescription() {
    if (types.size() == 1) {
      return NbBundle.getMessage(CrueFileTypeFilter.class, "CrueFileTypeFilter.Description") +" " +types.get(0).name()+" (*." + types.get(0).getExtension() + ")";
    }
    return NbBundle.getMessage(CrueFileTypeFilter.class, "CrueFileTypeFilter.Description");
  }
}
