package org.fudaa.fudaa.crue.common.log.filter;

import org.apache.commons.lang3.StringUtils;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class NodeQuickFilterEmptyValue extends AbstractNodeQuickFilter {

  public NodeQuickFilterEmptyValue(final String propertyId, final String propertyName) {
    super(propertyId);
    description = NbBundle.getMessage(NodeQuickFilterEmptyValue.class, "filter.emptyValue.description", propertyName);
    name = NbBundle.getMessage(NodeQuickFilterEmptyValue.class, "filter.emptyValue.name", propertyName);
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  protected boolean acceptValue(final Object value) {
    final boolean name = value == null || StringUtils.EMPTY.equals(value);
    return name;
  }
}
