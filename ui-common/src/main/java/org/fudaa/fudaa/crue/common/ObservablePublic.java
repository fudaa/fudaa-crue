/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common;

import java.util.Observable;

/**
 *
 * @author Frederic Deniger
 */
public class ObservablePublic extends Observable {

  @Override
  public synchronized void setChanged() {
    super.setChanged();
  }

  public void setChangedAndNotify() {
    setChanged();
    notifyObservers();
  }
}
