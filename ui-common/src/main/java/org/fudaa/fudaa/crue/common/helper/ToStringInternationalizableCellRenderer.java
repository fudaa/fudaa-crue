/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.helper;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.dodico.crue.common.transformer.ToStringInternationalizable;

/**
 *
 * @author Frédéric Deniger
 */
public class ToStringInternationalizableCellRenderer extends CtuluCellTextRenderer {

  @Override
  protected void setValue(Object _value) {
    if (_value == null) {
      super.setValue(StringUtils.EMPTY);
    } else {
      super.setValue(((ToStringInternationalizable) _value).geti18n());
    }
  }
}
