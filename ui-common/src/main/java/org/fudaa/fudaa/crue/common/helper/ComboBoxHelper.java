/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.helper;

import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

/**
 *
 * @author Frederic Deniger
 */
public class ComboBoxHelper {

  public static void setDefaultModel(JComboBox cb, List items) {
    cb.setModel(createComboboxModel(items));
  }

  public static DefaultComboBoxModel createComboboxModel(List items) {
    return new DefaultComboBoxModel(items.toArray(new Object[0]));
  }
}
