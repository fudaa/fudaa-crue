/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.pdt;

import java.awt.Component;
import java.beans.*;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.emh.PrendreClichePeriodique;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.nodes.Node;

/**
 *
 * @author Frédéric Deniger
 */
public class PrendreClichePeriodiqueEditor extends PropertyEditorSupport implements ExPropertyEditor, PropertyChangeListener {

  static void registerEditor() {
    PropertyEditorManager.registerEditor(PrendreClichePeriodique.class, PrendreClichePeriodiqueEditor.class);
  }
  PropertyEnv env;

  @Override
  public void attachEnv(PropertyEnv env) {
    this.env = env;
    env.addPropertyChangeListener(this);
    env.setState(PropertyEnv.STATE_NEEDS_VALIDATION);
    FeatureDescriptor featureDescriptor = env.getFeatureDescriptor();
    featureDescriptor.setValue("suppressCustomEditor", Boolean.FALSE);
    PropertyCrueUtils.configureNoEditAsText(featureDescriptor);
  }
  PrendreClichePeriodiquePanel prendreClichePeriodiquePanel;

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (PropertyEnv.PROP_STATE.equals(evt.getPropertyName())
            && evt.getNewValue() == PropertyEnv.STATE_VALID && prendreClichePeriodiquePanel !=  null) {
      setValue(prendreClichePeriodiquePanel.getCliche());
    }
  }

  @Override
  public boolean supportsCustomEditor() {
    return true;
  }

  @Override
  public Component getCustomEditor() {
    if (prendreClichePeriodiquePanel == null) {
      prendreClichePeriodiquePanel = new PrendreClichePeriodiquePanel();
    }
    Node.Property featureDescriptor = (Node.Property) env.getFeatureDescriptor();
    prendreClichePeriodiquePanel.setCliche((PrendreClichePeriodique) getValue());
    prendreClichePeriodiquePanel.setEditable(featureDescriptor.canWrite());
    return prendreClichePeriodiquePanel;
  }

  @Override
  public String getAsText() {
    PrendreClichePeriodique value = (PrendreClichePeriodique) getValue();
    if (value == null) {
      return StringUtils.EMPTY;
    }
    return value.getNomFic() + " " + value.getPdtRes().toString();
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
  }
}
