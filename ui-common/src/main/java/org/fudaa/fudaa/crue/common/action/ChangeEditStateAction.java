package org.fudaa.fudaa.crue.common.action;

import com.jidesoft.swing.ButtonStyle;
import com.jidesoft.swing.JideToggleButton;
import com.memoire.bu.BuBorders;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.KeyStroke;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.services.PerspectiveService;
import org.fudaa.fudaa.crue.common.services.PerspectiveState;
import org.fudaa.fudaa.crue.common.services.SelectedPerspectiveService;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.awt.Actions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.actions.BooleanStateAction;
import org.openide.util.lookup.Lookups;

/**
 * Bouton de changement de mode edition. ce bouton est affiché dans le menu et dans la toolbar
 * @author Frederic Deniger
 */
@ActionID(category = "File", id = ChangeEditStateAction.ACTION_ID)
@ActionRegistration(displayName = "#CTL_ChangeEditStateAction", asynchronous = false)
@ActionReference(path = "Menu/Window", position = 8)
public class ChangeEditStateAction extends BooleanStateAction implements LookupListener {

  public static final String ACTION_ID = "org.fudaa.fudaa.crue.common.action.ChangeEditStateAction";

  public static final String getActionLookupId() {
    return getFolderLookup() + ACTION_ID.replace('.', '-');
  }

  public static final String getFolderLookup() {
    return "Actions/File/";
  }
  private Lookup.Result result = null;
  final SelectedPerspectiveService service = Lookup.getDefault().lookup(SelectedPerspectiveService.class);
  private EditablePropertyListener editableListener;
  PerspectiveService currentPerspective;
  private DirtyPropertyListener dirtyListener;

  public ChangeEditStateAction() {
    putValue(NAME, NbBundle.getMessage(getClass(), "CTL_ChangeEditStateAction"));
    putValue(SHORT_DESCRIPTION, getValue(NAME));
    putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_DOWN_MASK));

    setEnabled(false);
    super.setBooleanState(false);
    result = service.getLookup().lookupResult(PerspectiveEnum.class);
    result.allItems();
    result.addLookupListener(this);
    resultChanged(null);
  }
  JideToggleButton toolbarPresenter;

  @Override
  public java.awt.Component getToolbarPresenter() {
    toolbarPresenter = new JideToggleButton(this);
    toolbarPresenter.setButtonStyle(ButtonStyle.TOOLBOX_STYLE);
    toolbarPresenter.setOpaque(true);
    toolbarPresenter.setBorderPainted(true);
    toolbarPresenter.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(), BuBorders.EMPTY1212));
    final KeyStroke keyStroke = (KeyStroke) getValue(ACCELERATOR_KEY);
    toolbarPresenter.setToolTipText(getDefaultTooltip());
    toolbarPresenter.getInputMap(JCheckBox.WHEN_IN_FOCUSED_WINDOW).put(keyStroke, this);
    toolbarPresenter.getActionMap().put(this, this);
    toolbarPresenter.setEnabled(false);
    updateIcon();
    return toolbarPresenter;
  }

  private String getDefaultTooltip() {
    return getName() + " " + Actions.findKey(this);
  }

  private class EditablePropertyListener implements PropertyChangeListener {

    @Override
    public void propertyChange(final PropertyChangeEvent evt) {
      updateHereFromPerspective();
    }
  }

  private class DirtyPropertyListener implements PropertyChangeListener {

    @Override
    public void propertyChange(final PropertyChangeEvent evt) {
      updatePerspectiveAction();
    }
  }

  private void updatePerspectiveAction() {
    if (!EventQueue.isDispatchThread()) {
      EventQueue.invokeLater(() -> updatePerspectiveAction());
      return;
    }
    if (currentPerspective != null) {
      final String FOLDER = "Actions/File/";
      final boolean dirty = currentPerspective.isDirty();
      final Lookup pathLookup = Lookups.forPath(FOLDER);
      final Collection<? extends AbstractPerspectiveAction> lookupAll = pathLookup.lookupAll(AbstractPerspectiveAction.class);
      for (final AbstractPerspectiveAction abstractPerspectiveAction : lookupAll) {
        if (abstractPerspectiveAction.getPerspectiveId() != currentPerspective.getPerspective()) {
          abstractPerspectiveAction.setEnabled(!dirty);
        }
      }
    }
  }

  private void updateHereFromPerspective() {
    final PerspectiveService currentPerspectiveService = service.getCurrentPerspectiveService();
    setEnabled(
            currentPerspectiveService != null && currentPerspectiveService.supportEdition() && currentPerspectiveService.getState() != null && currentPerspectiveService.getState().isEditActionEnable());
    setBooleanState(currentPerspectiveService != null && PerspectiveState.MODE_EDIT.equals(currentPerspectiveService.getState()));
    if (currentPerspectiveService == null || currentPerspectiveService.getState() == null) {
      putProperty(Action.SHORT_DESCRIPTION, getDefaultTooltip());
    } else {
      putProperty(Action.SHORT_DESCRIPTION, "<html><body>" + getDefaultTooltip() + "<br>" + NbBundle.getMessage(
              PerspectiveState.class, currentPerspectiveService.getState().getMsg()));
      firePropertyChange(Action.SHORT_DESCRIPTION, null, getValue(Action.SHORT_DESCRIPTION));
    }
    if (toolbarPresenter != null) {
      toolbarPresenter.setSelected(getBooleanState());
      updateIcon();
      toolbarPresenter.setEnabled(isEnabled());
    }

  }

  @Override
  public void resultChanged(final LookupEvent lookupEvent) {
    if (currentPerspective != null) {
      if (editableListener != null) {
        currentPerspective.removeStateListener(editableListener);
      }
      if (dirtyListener != null) {
        currentPerspective.removeDirtyListener(dirtyListener);
      }
    }
    updateHereFromPerspective();
    currentPerspective = service.getCurrentPerspectiveService();
    if (currentPerspective != null) {
      if (editableListener == null) {
        editableListener = new EditablePropertyListener();
      }
      if (dirtyListener == null) {
        dirtyListener = new DirtyPropertyListener();
      }
      currentPerspective.addStateListener(editableListener);
      currentPerspective.addDirtyListener(dirtyListener);
    }
  }

  @Override
  public String getName() {
    return (String) getValue(NAME);
  }

  @Override
  public HelpCtx getHelpCtx() {
    return new HelpCtx(getClass());
  }

  void updateIcon() {
    if (toolbarPresenter != null) {
      toolbarPresenter.setIcon(CrueIconsProvider.getIconFromFilename(getBooleanState() ? "edit.png" : "nonedit.png"));
    }

  }

  @Override
  public void setBooleanState(final boolean value) {
    if (value == getBooleanState()) {
      return;
    }
    super.setBooleanState(value);
    if (service.getCurrentPerspectiveService() != null) {
      service.getCurrentPerspectiveService().setState(getBooleanState() ? PerspectiveState.MODE_EDIT : PerspectiveState.MODE_READ);
    }
    updateHereFromPerspective();
  }

  @Override
  public void actionPerformed(final ActionEvent ev) {
    setBooleanState(!getBooleanState());
  }
}
