/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.action;

import com.jidesoft.swing.JideSplitButton;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.fudaa.fudaa.crue.common.services.SysdocContrat;
import org.openide.ErrorManager;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.awt.Actions;
import org.openide.awt.Mnemonics;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Lookup;
import org.openide.util.actions.BooleanStateAction;
import org.openide.util.actions.Presenter;

@ActionID(category = "File",
        id = "org.fudaa.fudaa.crue.common.action.ShowMainMenuAction")
@ActionRegistration(displayName = "#ShowMainMenuAction")
@ActionReferences({
    @ActionReference(path = "Toolbars/Main", position = 1)
})
public class ShowMainMenuAction extends AbstractAction implements Presenter.Toolbar {

    public ShowMainMenuAction() {
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
    }

    @Override
    public Component getToolbarPresenter() {
        JideSplitButton res = new JideSplitButton("");
        res.setIcon(CrueIconsProvider.getIconFromFilename("configuration.png"));
        res.setAlwaysDropdown(true);
        FileObject menuFolder = FileUtil.getConfigFile("Menu");
        FileObject[] menuKids = menuFolder.getChildren();
        for (FileObject menuKid : FileUtil.getOrder(Arrays.asList(menuKids), true)) {

            JMenu m = new JMenu(menuKid.getName());
            try {
                DataObject find = DataObject.find(menuKid);
                if (find != null) {
                    final String displayName = find.getNodeDelegate().getDisplayName();
                    Mnemonics.setLocalizedText(m, displayName);
                }
            } catch (DataObjectNotFoundException dataObjectNotFoundException) {
            }
//         
            if (m != null) {
                res.add(m);
            }
            buildPopup(menuKid, m);
        }
        return res;
    }

    private void buildPopup(FileObject fo, JComponent comp) {

        DataFolder df = DataFolder.findFolder(fo);
        DataObject[] childs = df.getChildren();

        // Accès au service SyDoc afin de connaitre l'activation de l'aide via la méthode isSyDocActivated
        // et de pouvoir identifier le menu via la constante SysdocContrat.SYDOC_ID
        final SysdocContrat service = Lookup.getDefault().lookup(SysdocContrat.class);
      
        for (int i = 0; i < childs.length; i++) {
            DataObject dob = childs[i];
            if (dob.getPrimaryFile().isFolder()) {

                // Si le menu SyDoc est désactivé, on cache l'ensemble du menu SyDoc
                if (!service.isSyDocActivated() && dob.getNodeDelegate().getName().equals(SysdocContrat.SYDOC_ID)) {
                    continue;
                }

                FileObject childFo = childs[i].getPrimaryFile();
                JMenu menu = new JMenu();
                final String displayName = dob.getNodeDelegate().getDisplayName();
                Mnemonics.setLocalizedText(menu, displayName);
                comp.add(menu);
                buildPopup(childFo, menu);
            } else {
                //Cookie or Lookup API discovery:
                InstanceCookie ck = dob.getCookie(InstanceCookie.class);
                if (ck == null) {
                    continue;
                }
                Object instanceObj = null;
                try {
                    instanceObj = ck.instanceCreate();
                } catch (Exception ex) {
                    instanceObj = null;
                    ErrorManager.getDefault().notify(ErrorManager.EXCEPTION, ex);
                }
                if (instanceObj == null) {
                    continue;
                }
                if (instanceObj instanceof JSeparator) {
                    comp.add((JSeparator) instanceObj);
                } else if (instanceObj instanceof BooleanStateAction) {
                    JCheckBoxMenuItem menuItem = new JCheckBoxMenuItem();
                    Actions.connect(menuItem, (BooleanStateAction) instanceObj, true);
                } else if (instanceObj instanceof Action) {
                    JMenuItem menuItem = new JMenuItem();
                    Actions.connect(menuItem, (Action) instanceObj, true);
                    comp.add(menuItem);
                }
            }
        }
    }
}
