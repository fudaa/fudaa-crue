package org.fudaa.fudaa.crue.common.log.property;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.beans.PropertyEditorSupport;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;

/**
 *
 * @author deniger
 */
public class LevelDetailIconPropertyEditor extends PropertyEditorSupport {

  @Override
  public void setValue(Object value) {
    super.setValue(value);
  }

  @Override
  public boolean isPaintable() {
    return true;
  }

  @Override
  public CtuluLogLevel getValue() {
    return (CtuluLogLevel) super.getValue();
  }

  @Override
  public void paintValue(Graphics gfx, Rectangle box) {
    Image image = LogIconTranslationProvider.getImage(getValue());
    Graphics2D g2d = (Graphics2D) gfx;
    int imageWidth = image.getWidth(null);
    int imageHeight = image.getWidth(null);
    int x = Math.max(box.x, (int) ((box.getWidth() - imageWidth) / 2));
    int y = Math.max(box.y, (int) ((box.getHeight() - imageHeight) / 2));
    g2d.drawImage(image, x, y, null);
//    super.paintValue(gfx, box);
  }

  @Override
  public String getAsText() {
    return LogIconTranslationProvider.getTranslation(getValue());
  }
}
