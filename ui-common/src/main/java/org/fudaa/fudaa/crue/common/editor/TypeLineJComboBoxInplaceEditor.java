package org.fudaa.fudaa.crue.common.editor;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.beans.PropertyEditor;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import org.fudaa.ebli.trace.TraceTraitRenderer;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;

/**
 *
 * @author deniger
 */
public class TypeLineJComboBoxInplaceEditor implements InplaceEditor {

  private final JComboBox combobox;
  private PropertyEditor editor = null;
  private PropertyModel model;
  final List<Integer> iconTypeToUse;
  final TraceTraitRenderer typeIconCellRenderer;

  public TypeLineJComboBoxInplaceEditor(List<Integer> lineTypeToUse) {
    this.combobox = new JComboBox(lineTypeToUse.toArray(new Integer[0]));
    this.iconTypeToUse = lineTypeToUse;
    typeIconCellRenderer = new TraceTraitRenderer();
    combobox.setRenderer(typeIconCellRenderer);
  }

  @Override
  public void connect(PropertyEditor propertyEditor, PropertyEnv env) {
    editor = propertyEditor;
    reset();
  }

  @Override
  public JComponent getComponent() {
    return combobox;
  }

  @Override
  public void clear() {
    //avoid memory leaks:
    editor = null;
    model = null;
  }

  @Override
  public Object getValue() {
    return combobox.getSelectedItem();
  }

  @Override
  public void setValue(Object object) {
    try {
      combobox.setSelectedItem(object);
    } catch (IllegalArgumentException e) {
      combobox.setSelectedItem(null);
    }
  }

  @Override
  public boolean supportsTextEntry() {
    return true;
  }

  @Override
  public void reset() {
    Object d = editor.getValue();
    if (d != null) {
      setValue(d);
    }
  }

  @Override
  public KeyStroke[] getKeyStrokes() {
    return new KeyStroke[0];
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    return editor;
  }

  @Override
  public PropertyModel getPropertyModel() {
    return model;
  }

  @Override
  public void setPropertyModel(PropertyModel propertyModel) {
    this.model = propertyModel;
  }

  @Override
  public boolean isKnownComponent(Component component) {
    return component == combobox || combobox.isAncestorOf(component);
  }

  @Override
  public void addActionListener(ActionListener actionListener) {
    //do nothing - not needed for this component
  }

  @Override
  public void removeActionListener(ActionListener actionListener) {
    //do nothing - not needed for this component
  }
}
