package org.fudaa.fudaa.crue.common.helper;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.io.Crue10FileFormat;
import org.fudaa.dodico.crue.io.Crue10FileFormatFactory;
import org.fudaa.dodico.crue.io.common.CrueDataImpl;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.dodico.crue.projet.ScenarioLoader;
import org.fudaa.dodico.crue.projet.ScenarioLoaderOperation;

import java.io.File;

public class LoadEtuHelper {
    public static EMHScenarioContainer loadRun(final String nom, final EMHProjet projet, final int idxOfRun) {
        final ManagerEMHScenario managerEMHScenario = projet.getScenario(nom);
        final ScenarioLoader loader = new ScenarioLoader(managerEMHScenario, projet, projet.getCoeurConfig());
        final EMHRun run = managerEMHScenario.getListeRuns().get(idxOfRun);
        final ScenarioLoaderOperation result = loader.load(run);
        return new EMHScenarioContainer(managerEMHScenario, result.getResult());
    }

    public static EMHProjet loadEtu(final File etuFile, final CoeurConfigContrat crueVersion) {
        final Crue10FileFormat<EMHProjet> fileFormat = Crue10FileFormatFactory.getInstance().getFileFormat(CrueFileType.ETU, crueVersion);
        final CtuluLog log = new CtuluLog();
        final EMHProjet project = fileFormat.read(etuFile, log, new CrueDataImpl(crueVersion.getCrueConfigMetier())).getMetier();
        project.getInfos().setXmlVersion(fileFormat.getVersionName());
        project.setPropDefinition(crueVersion);
        return project;
    }
}
