package org.fudaa.fudaa.crue.common.property;

import java.awt.Component;
import java.beans.PropertyEditor;
import java.beans.PropertyEditorSupport;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import org.fudaa.dodico.crue.common.CrueFileHelper;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public abstract class PropertyFileToPath extends Node.Property<File> {

  private boolean canRead = true;
  private boolean canWrite = true;

  public PropertyFileToPath() {
    super(File.class);
  }

  public PropertyFileToPath(String name, String displayName, String shortDescription, boolean canRead, boolean canWrite) {
    super(File.class);
    this.setName(name);
    setDisplayName(displayName);
    setShortDescription(shortDescription);
    this.canRead = canRead;
    this.canWrite = canWrite;
  }

  public void setCanRead(boolean canRead) {
    this.canRead = canRead;
  }

  public void setCanWrite(boolean canWrite) {
    this.canWrite = canWrite;
  }

  @Override
  public boolean canRead() {
    return canRead;
  }

  @Override
  public boolean canWrite() {
    return canWrite;
  }

  protected abstract void setFileAsString(String path);

  protected abstract String getFileAsString();

  @Override
  public File getValue() throws IllegalAccessException, InvocationTargetException {
    final String fileAsString = getFileAsString();
    if (fileAsString == null) {
      return null;
    }
    File res = new File(fileAsString);
    return res;
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    if (canWrite()) {
      return super.getPropertyEditor();
    }
    return new ReadOnlyFileEditor(this, (File) getValue("baseDir"));
  }

  @Override
  public void setValue(File val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    if (val == null) {
      setFileAsString(null);
    } else if (val.exists()) {
      setFileAsString(val.getAbsolutePath());
    } else {
      setFileAsString(val.getPath());
    }
  }

  protected static class ReadOnlyFileEditor extends PropertyEditorSupport {

    private final File baseDir;

    public ReadOnlyFileEditor(Object source, File baseDir) {
      super(source);
      this.baseDir = baseDir;
    }

    @Override
    public String getAsText() {
      return super.getAsText();
    }

    @Override
    public Component getCustomEditor() {
      File file = (File) getValue();
      if (baseDir != null) {
        file = CrueFileHelper.getCanonicalBaseDir(baseDir, file.getPath());
      }
      final String absolutePath = file.getAbsolutePath();

      if (absolutePath.length() <= 80) {
        final JTextField jTextField = new JTextField(absolutePath);
        jTextField.setColumns(absolutePath.length());
        jTextField.setEditable(false);

        // encapsulation dans un JDialog pour pouvoir gérer la persistence
        return DialogHelper.createCloseDialog(jTextField, this.getClass().getName(), NbBundle.getMessage(ReadOnlyFileEditor.class,
                "etudeDisplayerTitle"), true, DialogHelper.NO_VERSION_FOR_PERSISTENCE);

      } else {
        final JTextArea txt = new JTextArea(3, 80);
        txt.setText(absolutePath);
        txt.setEditable(false);

        // encapsulation dans un JDialog pour pouvoir gérer la persistence
        return DialogHelper.createCloseDialog(new JScrollPane(txt), this.getClass().getName(), NbBundle.getMessage(ReadOnlyFileEditor.class,
                "etudeDisplayerTitle"), true, DialogHelper.NO_VERSION_FOR_PERSISTENCE);
      }
    }

    @Override
    public boolean supportsCustomEditor() {
      return getValue() != null;
    }
  }
}
