package org.fudaa.fudaa.crue.common.node;

import com.memoire.bu.BuLib;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JComponent;
import org.fudaa.dodico.crue.metier.emh.EMH;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author deniger
 */
public class NodeHelper {

  public static final String PROP_NAME_ICON = "valueIcon";

  public static void expandAll(ExplorerManager em, OutlineView view) {
    expandNode(em.getRootContext(), view);
  }

  public static Node[] createNodesWithLookup(Collection lookups) {
    List<Node> res = new ArrayList<>();
    for (Object lookup : lookups) {
      res.add(new AbstractNode(Children.LEAF, Lookups.singleton(lookup)));
    }
    return res.toArray(new Node[0]);
  }

  public static void collapseAll(ExplorerManager em, OutlineView view) {
    if (view.getOutline().isRootVisible()) {
      collapseNode(em.getRootContext(), view);
    } else {
      Node[] nodes = em.getRootContext().getChildren().getNodes();
      for (Node node : nodes) {
        collapseNode(node, view);
      }
    }
  }

  public static Set<EMH> getEMHs(Node[] activatedNodes) {
    Set<EMH> res = new HashSet<>();
    for (Node node : activatedNodes) {
      EMH emh = node.getLookup().lookup(EMH.class);
      if (emh != null) {
        res.add(emh);
      }
    }
    return res;
  }

  public static <T extends JComponent> T useBoldFont(T in) {
    in.setFont(BuLib.deriveFont(in.getFont(), Font.BOLD, 0));
    return in;
  }

  public static boolean containEMH(Node[] activatedNodes) {
    for (Node node : activatedNodes) {
      EMH emh = node.getLookup().lookup(EMH.class);
      if (emh != null) {
        return true;
      }
    }
    return false;
  }

  public static Children createArray(List<? extends Node> nodes) {
    return createArray(nodes, true);
  }

  public static Children createArray(List<? extends Node> nodes, boolean useLeaf) {
    return NodeChildrenHelper.createChildren(nodes, useLeaf);
  }

  public static Node createNode(List<? extends Node> nodes, String displayName) {
    return createNode(nodes, displayName, true);
  }

  public static Node createNode(List<? extends Node> nodes, String displayName, boolean useLeaf) {
    AbstractNode node = new AbstractNode(createArray(nodes, useLeaf));
    node.setDisplayName(displayName);
    return node;
  }

  public static void expandNode(Node node, OutlineView view) {
    if (node == null) {
      return;
    }
    Children children = node.getChildren();
    Node[] nodes = children.getNodes();
    if (nodes.length != 0) {
      view.expandNode(node);
      for (Node child : nodes) {
        Node[] childrenNodes = child.getChildren().getNodes();
        if (childrenNodes.length != 0) {
          expandNode(child, view);
        }
      }
    }
  }

  public static void collapseNode(Node node, OutlineView view) {
    if (node == null) {
      return;
    }
    Children children = node.getChildren();
    Node[] nodes = children.getNodes();
    if (nodes.length != 0) {
      view.collapseNode(node);
      for (Node child : nodes) {
        Node[] childrenNodes = child.getChildren().getNodes();
        if (childrenNodes.length != 0) {
          collapseNode(child, view);
        }
      }
    }
  }
}
