/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.helper;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.table.TableModel;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.netbeans.swing.etable.ETableColumn;
import org.netbeans.swing.etable.ETableColumnModel;
import org.netbeans.swing.outline.Outline;
import org.openide.awt.NotificationDisplayer;
import org.openide.explorer.view.OutlineView;
import org.openide.util.NbBundle;

/**
 * Cette classe sert à désactiver les tris. Cela est nécessaire pour des problèmes d'édition lorsque les tris sont actifs : après une édition,
 * l'affichage est mise à jour automatiquement ce qui peut perturber l'utilisateur
 *
 * @author Frederic Deniger
 */
public class DisableSortHelper {

  private final OutlineView view;
  private boolean disableSort;

  // variables permettant de mémoriser le tri => tri par défaut première colonne (Numéro de ligne) croissante
  private int sortColumnRank = 1;
  private int sortColumnIndex = 0;
  private boolean sortColumnIsAscending = true;

  public DisableSortHelper(OutlineView view) {
    this.view = view;

    final Outline outline = this.view.getOutline();
    // abonnement au clic de souris sur le header pour désactiver le tri le cas échéant
    outline.getTableHeader().addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON3 || e.getClickCount() != 1) {
          return;
        }
        if (disableSort) {
          performDisable();
        } else if (!isSorted()) {
          sortColumnRank = 1;
          sortColumnIndex = 0;
          sortColumnIsAscending = true;
        }

      }
    });

    // force le tri par défaut : première colonne
    ETableColumnModel columnModel = (ETableColumnModel) view.getOutline().getColumnModel();
    columnModel.clearSortedColumns();

    // refresh
    repaint();
  }

  /**
   * Permet de définir si le tri est autorisé (hors mode édition) ou non (mode édition)
   *
   * @param disableSort true si le tri doit-être désactivé, false sinon
   */
  public void setDisableSort(boolean disableSort) {
    this.disableSort = disableSort;

    if (disableSort) {
      // passage en édition : recherche de la colonne triée pour mémorisation
      sortColumnRank = 1;
      sortColumnIndex = 0;
      sortColumnIsAscending = true;
      ETableColumnModel columnModel = (ETableColumnModel) view.getOutline().getColumnModel();
      int nb = columnModel.getColumnCount();
      for (int i = 0; i < nb; i++) {
        ETableColumn col = (ETableColumn) columnModel.getColumn(i);
        if (col.isSorted()) {
          // mémorisation des caractéristiques de tri
          sortColumnRank = col.getSortRank();
          sortColumnIndex = i;
          sortColumnIsAscending = col.isAscending();
          break;
        }
      }
    }
  }

  /**
   * Permet de savoir si la grille est triée
   *
   * @return true si la grille est triée, false sinon
   */
  private boolean isSorted() {
    ETableColumnModel columnModel = (ETableColumnModel) view.getOutline().getColumnModel();
    int nb = columnModel.getColumnCount();
    for (int i = 0; i < nb; i++) {
      ETableColumn col = (ETableColumn) columnModel.getColumn(i);
      if (col.isSorted()) {
        return true;
      }
    }
    return false;
  }

  /**
   * Désactivation du tri par forçage du tri mémorisé
   */
  private void performDisable() {
    // force le tri mémorisé en lieu et place de celui que l'utilisateur souhaite mettre en place via le clic de souris
    if (sortColumnIndex != -1) {
      ETableColumnModel columnModel = (ETableColumnModel) view.getOutline().getColumnModel();
      columnModel.clearSortedColumns();
      int nb = columnModel.getColumnCount();
      for (int i = 0; i < nb; i++) {
        ETableColumn col = (ETableColumn) columnModel.getColumn(i);
        if (i == sortColumnIndex) {
          columnModel.setColumnSorted(col, sortColumnIsAscending, sortColumnRank);
        }
      }
      repaint();
    }

    // affiche un message à l'utilisateur comme quoi le tri est désactivé
    NotificationDisplayer.getDefault().notify(NbBundle.getMessage(DisableSortHelper.class, "sortDisableForEdition.title"), CrueIconsProvider.getIcon(
            "org/fudaa/fudaa/crue/common/icons/avertissement.png"), NbBundle.getMessage(
                    DisableSortHelper.class, "sortDisableForEdition.message"), null);
  }

  /**
   * Rafraîchissement de la grille
   */
  private void repaint() {
    TableModel model = view.getOutline().getModel();
    view.getOutline().tableChanged(new TableModelEvent(model, 0, model.getRowCount()));
  }

}
