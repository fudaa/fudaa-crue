package org.fudaa.fudaa.crue.common.log;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogRecord;
import org.openide.nodes.Node;

/**
 *
 * @author Fred Deniger
 */
public class CtuluLogChildFactory extends AbstractCtuluLogChildFactory {

  public CtuluLogChildFactory(CtuluLog log) {
    super(log);
  }

  @Override
  protected Node createNodeForKey(CtuluLogRecord key) {
    return new CtuluLogRecordNode(key, log.getDefaultResourceBundle());
  }
}
