package org.fudaa.fudaa.crue.common.services;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.coeur.CoeurConfigContrat;
import org.fudaa.dodico.crue.metier.CrueFileType;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.EMHRun;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.fudaa.dodico.crue.projet.calcul.CalculCrueRunnerManager;
import org.fudaa.dodico.crue.projet.calcul.ExecInputDefault;
import org.fudaa.dodico.crue.projet.create.RunCalculOption;
import org.openide.util.Lookup;

import java.util.Map;

/**
 * Utiliser pour charger un run.
 *
 * @author deniger
 */
public interface PostRunService extends Lookup.Provider {
    boolean loadRun(EMHProjet projet, ManagerEMHScenario scenario, EMHRun run);

    /**
     * @param scenario le scenario
     * @return true si le scenario peut être lancé.
     */
    boolean validConfigurationForRun(ManagerEMHScenario scenario, CoeurConfigContrat coeurConfig);

    /**
     * Lancement d'un run
     *
     * @param execInputDefault les données pour le lancement du run
     * @param options          les options de lanccement par type de fichier
     */
    void run(final ExecInputDefault execInputDefault, Map<CrueFileType, RunCalculOption> options);

    /**
     * @return la manager pour le lancement des runs
     */
    CalculCrueRunnerManager getCalculCrueRunnerManagerOtfa();

    /**
     * @return true un run est chargé
     */
    boolean isRunLoaded();

    /**
     * @return le scenario parent du run chargé
     */
    EMHScenario getScenarioLoaded();

    /**
     * @return le {@link ManagerEMHScenario} chargé
     */
    ManagerEMHScenario getManagerScenarioLoaded();

    /**
     * @return le run chargé
     */
    EMHRun getRunLoaded();

    /**
     * @return le projet chargé
     */
    EMHProjet getSelectedProjet();

    /**
     * @return le CCM du projet chargé
     */
    CrueConfigMetier getCcm();

    /**
     * fermeture du run
     */
    void unloadRun();

    /**
     * @return si le scenario est de type crue9, permet de valider l'installation
     */
    boolean validCrue9();

    /**
     * @return CR de la dernière opération
     */
    CtuluLogGroup getLastLogsGroup();
}
