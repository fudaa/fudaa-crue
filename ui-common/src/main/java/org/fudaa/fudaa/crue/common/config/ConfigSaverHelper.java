package org.fudaa.fudaa.crue.common.config;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.ctulu.converter.ToStringTransformerFactory;
import org.openide.nodes.Node;
import org.openide.nodes.Node.Property;
import org.openide.nodes.Sheet;
import org.openide.nodes.Sheet.Set;
import org.openide.util.Exceptions;

/**
 *
 * @author deniger
 */
public class ConfigSaverHelper {

  public static LinkedHashMap<String, Node.Property> getProperties(Sheet.Set set) {
    LinkedHashMap<String, Node.Property> res = new LinkedHashMap<>();
    ConfigSaverHelper.addProperties(res, set);
    return res;
  }

  public static void addProperties(LinkedHashMap<String, Property> target, Sheet.Set set) {
    Property<?>[] properties = set.getProperties();
    for (Property property : properties) {
      String propertyName = set.getName() + "." + property.getName();
      if (target.containsKey(propertyName)) {
        Logger.getLogger(ConfigSaverHelper.class.getName()).log(Level.INFO, propertyName);
      }
      assert !target.containsKey(propertyName);
      target.put(propertyName, property);
    }
  }

  public static LinkedHashMap<String, String> getProperties(List<Sheet.Set> sets) {
    LinkedHashMap<String, Property> res = getNodeProperties(sets);
    return ConfigSaverHelper.transform(res);
  }

  public static LinkedHashMap<String, Property> getNodeProperties(List<Set> sets) {
    LinkedHashMap<String, Node.Property> res = new LinkedHashMap<>();
    ConfigSaverHelper.addProperties(res, sets);
    return res;
  }

  public static void addProperties(LinkedHashMap<String, Property> target, List<Sheet.Set> sets) {
    for (Sheet.Set set : sets) {
      addProperties(target, set);
    }
  }

  public static void addVisibility(LinkedHashMap<String, Property> target, Property property) {
    assert !target.containsKey(property.getName());
    target.put(property.getName(), property);
  }

  public static LinkedHashMap<String, String> transform(LinkedHashMap<String, Property> in) {

    LinkedHashMap<String, String> res = new LinkedHashMap<>();
    Map<Class, AbstractPropertyToStringTransformer> createTransformers = ToStringTransformerFactory.createTransformers();
    for (Map.Entry<String, Property> entry : in.entrySet()) {
      String string = entry.getKey();
      Property property = entry.getValue();
      final Class valueType = property.getValueType();
      AbstractPropertyToStringTransformer transformer = createTransformers.get(valueType);
      if (transformer == null) {
        Logger.getLogger(ConfigSaverHelper.class.getName()).log(Level.SEVERE, "no transformer for {0}", valueType);
      }
      assert transformer != null;
      try {
        res.put(string, transformer.toString(property.getValue()));
      } catch (Exception ex) {
        Exceptions.printStackTrace(ex);
      }
    }
    return res;

  }
}
