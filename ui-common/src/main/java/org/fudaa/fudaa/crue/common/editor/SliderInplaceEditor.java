package org.fudaa.fudaa.crue.common.editor;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.beans.PropertyEditor;
import javax.swing.BoundedRangeModel;
import javax.swing.JComponent;
import javax.swing.JSlider;
import javax.swing.KeyStroke;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;

/**
 *
 * @author deniger
 */
public class SliderInplaceEditor implements InplaceEditor {

  private final JSlider slider;
  private PropertyEditor editor = null;
  private PropertyModel model;

  public SliderInplaceEditor(BoundedRangeModel model) {
    this.slider = new JSlider(model);
  }

  @Override
  public void connect(PropertyEditor propertyEditor, PropertyEnv env) {
    editor = propertyEditor;
    reset();
  }

  @Override
  public JComponent getComponent() {
    return slider;
  }

  @Override
  public void clear() {
    //avoid memory leaks:
    editor = null;
    model = null;
  }

  @Override
  public Object getValue() {
    return slider.getValue();
  }

  @Override
  public void setValue(Object object) {
    try {
      slider.setValue((Integer) object);
    } catch (IllegalArgumentException e) {
      slider.setValue(0);
    }
  }

  @Override
  public boolean supportsTextEntry() {
    return false;
  }

  @Override
  public void reset() {
    Object d = editor.getValue();
    if (d != null) {
      setValue(d);
    }
  }

  @Override
  public KeyStroke[] getKeyStrokes() {
    return new KeyStroke[0];
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    return editor;
  }

  @Override
  public PropertyModel getPropertyModel() {
    return model;
  }

  @Override
  public void setPropertyModel(PropertyModel propertyModel) {
    this.model = propertyModel;
  }

  @Override
  public boolean isKnownComponent(Component component) {
    return component == slider || slider.isAncestorOf(component);
  }

  @Override
  public void addActionListener(ActionListener actionListener) {
    //do nothing - not needed for this component
  }

  @Override
  public void removeActionListener(ActionListener actionListener) {
    //do nothing - not needed for this component
  }
}
