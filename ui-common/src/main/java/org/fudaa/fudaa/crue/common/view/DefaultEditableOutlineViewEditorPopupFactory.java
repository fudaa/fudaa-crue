package org.fudaa.fudaa.crue.common.view;

import com.memoire.bu.BuResource;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ebli.commun.EbliActionAbstract;
import org.fudaa.ebli.commun.EbliLib;
import org.openide.explorer.ExplorerUtils;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Arrays;

/**
 * une menu popup pour afficher les actions copier, copier tout, couper, coller et importer.
 */
public class DefaultEditableOutlineViewEditorPopupFactory extends CustomNodePopupFactory {
  private final DefaultEditableOutlineViewEditor view;
  private final EbliActionAbstract cutAction;
  private final EbliActionAbstract copyAction;
  private final EbliActionAbstract pasteAction;

  /**
   * @param view la vue
   */
  DefaultEditableOutlineViewEditorPopupFactory(DefaultEditableOutlineViewEditor view) {
    this.view = view;
    setShowQuickFilter(false);
    copyAction = createCopyAction(view);
    pasteAction = createPasteAction(view);
    cutAction = createCutAction(view);

    EbliLib.updateMapKeyStroke(view.getView(), Arrays.asList(copyAction, cutAction, pasteAction));
  }

  private EbliActionAbstract createCopyAction(DefaultEditableOutlineViewEditor view) {
    KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK);
    EbliActionAbstract action = new EbliActionAbstract(NbBundle.getMessage(DefaultEditableOutlineViewEditorPopupFactory.class, "Copy.Action"),
        BuResource.BU.getToolIcon("copier"), "COPY") {
      @Override
      public void actionPerformed(ActionEvent e) {
        view.copyToClipboard(false);
      }
    };
    action.setKey(keyStroke);

    return action;
  }

  private EbliActionAbstract createPasteAction(DefaultEditableOutlineViewEditor view) {
    KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK);
    EbliActionAbstract action = new EbliActionAbstract(NbBundle.getMessage(DefaultEditableOutlineViewEditorPopupFactory.class, "Paste.Action"),
        BuResource.BU.getToolIcon("coller"), "PASTE") {
      @Override
      public void actionPerformed(ActionEvent e) {
        view.pasteInSelectedNodes();
      }
    };
    action.setKey(keyStroke);

    return action;
  }

  private EbliActionAbstract createCutAction(DefaultEditableOutlineViewEditor view) {
    KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_MASK);
    EbliActionAbstract action = new EbliActionAbstract(NbBundle.getMessage(DefaultEditableOutlineViewEditorPopupFactory.class, "Cut.Action"),
        BuResource.BU.getToolIcon("couper"), "CUT") {
      @Override
      public void actionPerformed(ActionEvent e) {
        view.cutToClipboard();
      }
    };
    action.setKey(keyStroke);

    return action;
  }

  private boolean canCut(Node[] nodes) {
    if (CtuluLibArray.isNotEmpty(nodes)) {
      return nodes[0].canCut();
    }
    return false;
  }

  private boolean canDestroy(Node[] nodes) {
    if (CtuluLibArray.isNotEmpty(nodes)) {
      return nodes[0].canDestroy();
    }
    return false;
  }

  @Override
  public void addSpecificMenuItems(JPopupMenu popupMenu, int row, int column, Node[] selectedNodes, Component component) {
    if (view.isUseRemoveButton()) {
      JMenuItem deleteItem = popupMenu.add(ExplorerUtils.actionDelete(view.getExplorerManager(), false));
      popupMenu.add(deleteItem);
      deleteItem.setEnabled(view.isEditable() && selectedNodes.length > 0 && canDestroy(selectedNodes));
      deleteItem.setText(NbBundle.getMessage(DefaultEditableOutlineViewEditorPopupFactory.class, "DeleteItem.Action"));
      popupMenu.addSeparator();
      cutAction.cleanListener();
      popupMenu.add(cutAction);
      cutAction.setEnabled(view.isEditable() && canCut(selectedNodes));
    }
    copyAction.cleanListener();
    popupMenu.add(copyAction);
    copyAction.setEnabled(view.isEditable() && selectedNodes.length > 0);

    JMenuItem copyAllItem = popupMenu.add(NbBundle.getMessage(DefaultEditableOutlineViewEditorPopupFactory.class, "CopyAll.Action"));
    copyAllItem.setEnabled(true);
    copyAllItem.addActionListener(e -> view.copyToClipboard(true));

    pasteAction.cleanListener();
    popupMenu.add(pasteAction);
    pasteAction.setEnabled(view.isEditable());

    popupMenu.addSeparator();
    JMenuItem exportItem = popupMenu.add(NbBundle.getMessage(DefaultEditableOutlineViewEditorPopupFactory.class, "Export.Action"));
    exportItem.setEnabled(true);
    exportItem.addActionListener(e -> view.exportAll());
  }
}
