package org.fudaa.fudaa.crue.common.editor;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.beans.PropertyEditorSupport;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropertyEnv;

/**
 * BoundedRangeModel model
 * @author deniger
 */
public class SliderPropertyEditor extends PropertyEditorSupport implements ExPropertyEditor, InplaceEditor.Factory {

  SliderInplaceEditor inplaceEditor;
  final SliderInplaceEditor renderer;
  private final SliderRangeModelFactory rangeModelFactory;

  public SliderPropertyEditor(SliderRangeModelFactory factory) {
    this.rangeModelFactory = factory;
    renderer = new SliderInplaceEditor(rangeModelFactory.create());
  }

  @Override
  public boolean isPaintable() {
    return true;
  }

  @Override
  public void paintValue(Graphics gfx, Rectangle box) {
    renderer.setValue(getValue());
    Graphics g = gfx.create(box.x, box.y, box.width, box.height);
    renderer.getComponent().setOpaque(false);
    renderer.getComponent().setSize(box.width, box.height);
    renderer.getComponent().paint(g);
    g.dispose();
  }

  @Override
  public void attachEnv(PropertyEnv env) {
    env.registerInplaceEditorFactory(this);
  }

  @Override
  public InplaceEditor getInplaceEditor() {
    if (inplaceEditor == null) {
      inplaceEditor = new SliderInplaceEditor(rangeModelFactory.create());
    }
    return inplaceEditor;
  }
}
