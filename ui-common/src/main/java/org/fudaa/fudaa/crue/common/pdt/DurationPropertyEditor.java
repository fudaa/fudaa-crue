/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.pdt;

import java.awt.Component;
import java.beans.*;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.joda.time.Duration;
import org.joda.time.Period;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.nodes.Node;

/**
 *
 * @author Frédéric Deniger
 */
public class DurationPropertyEditor extends PropertyEditorSupport implements ExPropertyEditor, PropertyChangeListener {
  
  public static void registerEditor() {
    PropertyEditorManager.registerEditor(Duration.class, DurationPropertyEditor.class);
  }

  PropertyEnv env;

  @Override
  public void attachEnv(PropertyEnv env) {
    this.env = env;
    env.addPropertyChangeListener(this);
    env.setState(PropertyEnv.STATE_NEEDS_VALIDATION);
    FeatureDescriptor featureDescriptor = env.getFeatureDescriptor();
    featureDescriptor.setValue("suppressCustomEditor", Boolean.FALSE);
    PropertyCrueUtils.configureNoEditAsText(featureDescriptor);
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
  }
  
  

  @Override
  public boolean supportsCustomEditor() {
    return true;
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (durationEditor != null && PropertyEnv.PROP_STATE.equals(evt.getPropertyName())
            && evt.getNewValue() == PropertyEnv.STATE_VALID) {
      setValue(durationEditor.getDuration());
    }
  }
  DurationPanelEditor durationEditor;

  @Override
  public String getAsText() {
    Duration duration = (Duration) getValue();
    return DateDurationConverter.HMDS_DURATION_FORMATTER.print(new Period(duration));
  }

  @Override
  public Component getCustomEditor() {
    if (durationEditor == null) {
      durationEditor = new DurationPanelEditor();
    }
    Node.Property featureDescriptor = (Node.Property) env.getFeatureDescriptor();
    durationEditor.setPdt((Duration) getValue());
    durationEditor.setEditable(featureDescriptor.canWrite());
    return durationEditor;
  }
}
