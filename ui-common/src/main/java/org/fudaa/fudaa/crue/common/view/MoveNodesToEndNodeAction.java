/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.view;

import java.util.Arrays;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.openide.nodes.Node;
import org.openide.util.ContextAwareAction;
import org.openide.util.NbBundle;

/**
 *
 * @author Frédéric Deniger
 */
public class MoveNodesToEndNodeAction extends AbstractEditNodeAction implements ContextAwareAction {
  
  public MoveNodesToEndNodeAction() {
    super(NbBundle.getMessage(MoveNodesToEndNodeAction.class, "MoveNodeToEnd.DisplayName"));
  }
  
  @Override
  protected boolean isEnable(final Node[] activatedNodes) {
    return activatedNodes.length >= 1;
  }
  
  @Override
  protected void performAction(final Node[] activatedNodes) {
    DefaultNodePasteType.nodeToEnd(Arrays.asList(activatedNodes), DefaultNodePasteType.isIndexUsedAsDisplayName(activatedNodes));
  }


  
}
