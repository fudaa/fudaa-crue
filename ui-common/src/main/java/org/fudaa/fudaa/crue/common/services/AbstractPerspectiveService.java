/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.services;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.helper.TopComponentHelper;
import org.openide.nodes.Node;
import org.openide.windows.TopComponent;

import java.awt.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.Map;

/**
 * Template pour les services Perspective: ils assurent la gestion des perspective ( le mode EDIT/READ, le mode modifié/non modifiée...).
 *
 * @author Fred Deniger
 */
public abstract class AbstractPerspectiveService implements PerspectiveService {
    private final PerspectiveEnum perspective;
    private final PropertyChangeSupport propertyChangeSupport;
    private PerspectiveState state = PerspectiveState.MODE_READ_ONLY_TEMP;
    private boolean dirty = false;
    private boolean active;

    /**
     * @param perspective l'enum identifiant la perspective
     */
    public AbstractPerspectiveService(PerspectiveEnum perspective) {
        this.perspective = perspective;
        propertyChangeSupport = new PropertyChangeSupport(this);
    }

    @Override
    public String getPathForViewsAction() {
        return null;
    }

    /**
     * @return l'enum identifiant la perspective
     */
    @Override
    public PerspectiveEnum getPerspective() {
        return perspective;
    }

    /**
     * Rafraichit les TopComponent ouverts ( l'état editable). Concrètement, cette methode modifie la sélection ce qui permet de mettre à jour les propriétés
     * "editable" des JComponents du TopComponent
     *
     * @see #setState(org.fudaa.fudaa.crue.common.services.PerspectiveState)
     */
    private void refreshTopComponents() {
        //pour être sur de toujours travailler dans le Thread Swing
        if (!EventQueue.isDispatchThread()) {
            EventQueue.invokeLater(() -> refreshTopComponents());
            return;
        }
        //on enregistre la sélection en cours du TopComponent actif
        TopComponent activated = TopComponent.getRegistry().getActivated();
        Node[] saved = null;
        if (activated != null) {
            saved = TopComponentHelper.getSelectedNode(activated);
        }
        //on efface la sélection de tous les TopComponent ouverts
        Map<String, Collection<TopComponent>> openedTopComponentById = TopComponentHelper.getOpenedTopComponentById();
        for (String string : getDefaultTopComponents()) {
            Collection<TopComponent> listOfTc = openedTopComponentById.get(string);
            if (listOfTc != null) {
                for (TopComponent topComponent : listOfTc) {
                    TopComponentHelper.clearSelection(topComponent);
                }
            }
        }
        //on réapplique la sélection du TopComponent actif
        if (activated != null) {
            if (saved != null) {
                TopComponentHelper.activateTopComponentAndSetSelection(activated, saved);
            }
            //on remet le focus sur le TopComponent actif
            activated.requestActive();
        }
    }

    @Override
    public void addStateListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener("state", listener);
    }

    @Override
    public void removeStateListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener("state", listener);
    }

    @Override
    public void addDirtyListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener("dirty", listener);
    }

    @Override
    public void removeDirtyListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener("dirty", listener);
    }

    @Override
    public PerspectiveState getState() {
        return state;
    }

    /**
     * si le mode courant est readOnlyAlways ne fait rien.
     *
     * @param state le nouvel etat
     */
    @Override
    public void setState(PerspectiveState state) {
        if (PerspectiveState.MODE_READ_ONLY_ALWAYS.equals(this.state)) {
            return;
        }
        doSetState(state);
    }

    /**
     * @return true si la perspective est active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active nouvel etat actif de la perspective.
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Si la perspective est modifiée (dirty), la désactivation n'est pas possible. Si elle est en mode edit, elle est reinitialié en mode READ.
     *
     * @return true si la perspective peut être desactivée.
     */
    @Override
    public boolean deactivate() {
        if (isDirty()) {
            return false;
        }
        if (getState().equals(PerspectiveState.MODE_EDIT)) {
            //on désactive le mode édition
            setState(PerspectiveState.MODE_READ);
        }
        return true;
    }

    @Override
    public boolean isDirty() {
        return dirty;
    }

    /**
     * @param dirty true si le contenu de la perspective a été modifié
     */
    public void setDirty(boolean dirty) {
        boolean old = this.dirty;
        this.dirty = dirty;
        propertyChangeSupport.firePropertyChange("dirty", old, this.dirty);
    }

    /**
     * Change l'état pour {@link org.fudaa.fudaa.crue.common.services.PerspectiveState#MODE_READ_ONLY_TEMP}
     */
    protected void resetStateToReadOnlyTemp() {
        doSetState(PerspectiveState.MODE_READ_ONLY_TEMP);
    }

    /**
     * @param state le nouvel etat. Envoie les evenements et rafraichit la vue.
     */
    private void doSetState(PerspectiveState state) {
        if (canStateBeModifiedTo(state)) {
            PerspectiveState old = this.state;
            this.state = state;
            refreshAfterStateChanged();
            propertyChangeSupport.firePropertyChange("state", old, state);
        }
    }

    /**
     * peut etre redefini pour rafraichir la perspective apres changement d'etat. Par default rafraichit les TopComponent via {@link #refreshTopComponents() }.
     *
     * @see #refreshTopComponents()
     */
    protected void refreshAfterStateChanged() {
        refreshTopComponents();
    }

    /**
     * @return true si la perspective est mode edit : si {@link  #getState()} renvoie {@link  org.fudaa.fudaa.crue.common.services.PerspectiveState#MODE_EDIT}.
     * @see #getState()
     * @see org.fudaa.fudaa.crue.common.services.PerspectiveState
     */
    public boolean isInEditMode() {
        return PerspectiveState.MODE_EDIT.equals(getState());
    }

    /**
     * Cette methode ne doit s'occuper uniquement de valider le changement d'état.
     * Les methodes appelantes {@link #setState(org.fudaa.fudaa.crue.common.services.PerspectiveState)
     * et {@link #doSetState(org.fudaa.fudaa.crue.common.services.PerspectiveState)} se chargent de la modfication d'envoyer les events nécessaires.
     *
     * @param newState le nouvel état de la perspective.
     * @return true si le mode a été changé. Par exemple, peut renvoyer false, si une modification est cours et l'utilisateur annule le changement de mode.
     */
    protected abstract boolean canStateBeModifiedTo(PerspectiveState newState);
}
