/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.services;

import org.fudaa.dodico.crue.metier.emh.EMH;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

import java.util.Collection;

/**
 * conserve les UID des EMH sélectionnés dans une perspective.
 *
 * <table  border="1"  cellpadding="3" cellspacing="0"  summary="lookups">
 * <tr>
 * <th align="left">Lookup</th>
 * <th align="left">Commentaire</th>
 * <th align="left">Méthodes utilisant le lookup</th>
 * </tr>
 * <tr>
 * <td>{@link java.lang.Long}: </td>
 * <td> les uids des EMHS sélectionnés.
 * </td>
 * <td>{@link #setSelectedEMHs(java.util.Collection)},  {@link #clearContent()}</td>
 * </tr>
 * </table>
 *
 * @author Frederic Deniger
 */
public class AbstractSelectedEMHService implements Lookup.Provider {

  /**
   * le contenu du lookup du service
   */
  private final InstanceContent dynamicContent = new InstanceContent();
  /**
   * le lookup du service
   */
  private final Lookup lookup = new AbstractLookup(dynamicContent);
  /**
   * true si le lookup est en cours de modifications ( vaudra true lors de l'ajout de plusieurs éléments et false à la fin de l'action).
   */
  private boolean updating;

  /**
   *
   * @return le lookup du service
   */
  @Override
  public Lookup getLookup() {
    return lookup;
  }

  /**
   * @return si la selection est en cours de mise à jour.
   */
  public boolean isUpdating() {
    return updating;
  }

  /**
   * efface la sélection en cours et la remplace par la nouvelle sélection {@code emhs}.
   * Au cours des opérations {@link #isUpdating()} vaudra true, puis false à la fin de la modification du lookup du service
   *
   * @see EMH#getUiId()
   * @param emhs la nouvelle sélectiond'EMHs ( on récupère les uid).: ne doit pas être null.
   */
  public void setSelectedEMHs(final Collection<EMH> emhs) {
    //on nettoie le contenu actuel.
    clearContent();
    //on ajoute la nouvelle selection
    final int size = emhs.size();
    //en cours de modification si au moins un element à ajouter
    if (size > 1) {
      updating = true;
    }
    int idx = 0;
    for (final EMH emh : emhs) {
      //pour envoyer un event à la dernière modification
      if (++idx == size) {
        updating = false;
      }
      dynamicContent.add(emh.getUiId());
    }
    updating = false;
  }

  /**
   * efface la sélection en cours et la remplace par la nouvelle sélection {@code emhs}.
   * Au cours des opérations {@link #isUpdating()} vaudra true, puis false à la fin de la modification du lookup du service
   *
   * @param uids la nouvelle sélection d'uid ne doit pas être null
   */
  public void setSelectedUids(final Collection<Long> uids) {
    //on nettoie le contenu actuel.
    clearContent();
    //on ajoute la nouvelle selection
    final int size = uids.size();
    //en cours de modification si au moins un element à ajouter
    if (size > 1) {
      updating = true;
    }
    int idx = 0;
    for (final Long uid : uids) {
      //pour envoyer un event à la dernière modification
      if (++idx == size) {
        updating = false;
      }
      dynamicContent.add(uid);
    }
    updating = false;
  }


  /**
   * Supprime la sélection courante.
   */
  private void clearContent() {
    //on recupere tous les uids sélectionnés et on les enleve du lookup.
    final Collection<? extends Long> lookupAll = lookup.lookupAll(Long.class);
    final int size = lookupAll.size();
    if (size > 1) {
      updating = true;
    }
    int idx = 0;
    for (final Long uid : lookupAll) {
      //pour envoyer un event à la dernière modification
      if (++idx == size) {
        updating = false;
      }
      dynamicContent.remove(uid);
    }
    updating = false;
  }
}
