/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.view;

import java.util.Arrays;
import org.fudaa.fudaa.crue.common.action.AbstractEditNodeAction;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 *
 * @author Frédéric Deniger
 */
public class MoveNodesToStartNodeAction extends AbstractEditNodeAction {

  public MoveNodesToStartNodeAction() {
    super(NbBundle.getMessage(MoveNodesToStartNodeAction.class, "MoveNodeToStart.DisplayName"));
  }

  @Override
  protected boolean isEnable(Node[] activatedNodes) {
    return activatedNodes.length >= 1;
  }

  @Override
  protected void performAction(Node[] activatedNodes) {
    boolean useIndexAsDisplayName = DefaultNodePasteType.isIndexUsedAsDisplayName(activatedNodes);
    DefaultNodePasteType.nodeToStart(Arrays.asList(activatedNodes), useIndexAsDisplayName);
  }

}
