package org.fudaa.fudaa.crue.common.log;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.ArrayUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;

/**
 * A utiliser uniquement pour les groupes qui ne contiennent pas de sous-groupes
 *
 * @author deniger
 */
public class CtuluLogGroupChildFactory {

  public interface LogRecordBuilder {

    AbstractCtuluLogChildFactory create(CtuluLog log);
  }

  public static class LogRecordBuilderSimple implements LogRecordBuilder {

    @Override
    public AbstractCtuluLogChildFactory create(CtuluLog log) {
      return new CtuluLogChildFactory(log);
    }
  }

  public static class LogRecordBuilderTrace implements LogRecordBuilder {

    @Override
    public AbstractCtuluLogChildFactory create(CtuluLog log) {
      return new CtuluLogChildTraceFactory(log);
    }
  }

  public static Children createChildren(CtuluLogGroup group, LogRecordBuilder childFactoryBuilder) {
    List<Node> childs = new ArrayList<>();
    List<CtuluLog> logs = group.getLogs();
    int nb = logs.size();
    for (int i = 0; i < nb; i++) {
      childs.add(new CtuluLogNode(logs.get(i), childFactoryBuilder.create(logs.get(i))));
    }
    List<CtuluLogGroup> groups = group.getGroups();
    for (CtuluLogGroup sousGroup : groups) {
      childs.add(new CtuluLogGroupNode(sousGroup, childFactoryBuilder));
    }
    Children.Array res = new Children.Array();
    Node[] nodes = childs.toArray(new Node[0]);
    res.add(nodes);
    return res;
  }

  static Children createOnlyErrorChildren(CtuluLogGroupNode aThis) {
    Node[] nodes = aThis.getChildren().getNodes();
    if (ArrayUtils.isEmpty(nodes)) {
      return null;
    }
    List<Node> newChildren = new ArrayList<>(nodes.length);
    for (Node node : nodes) {
      FilterNode newNode = ((CtuluLogRecordContainer) node).createOnlyErrorNode();
      if (newNode != null) {
        newChildren.add(newNode);
      }
    }
    if (newChildren.isEmpty()) {
      return null;
    }
    Children.Array res = new Children.Array();
    res.add(newChildren.toArray(new Node[0]));
    return res;

  }
}
