package org.fudaa.fudaa.crue.common;

import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuResource;
import java.awt.BorderLayout;
import java.awt.Color;
import java.beans.PropertyEditorManager;
import java.io.File;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.fudaa.crue.common.editor.CustomColorChooser;
import org.fudaa.fudaa.crue.common.editor.FileEditor;
import org.fudaa.fudaa.crue.common.editor.LocalDateTimeEditor;
import org.fudaa.fudaa.crue.common.pdt.DurationPropertyEditor;
import org.fudaa.fudaa.crue.common.pdt.PrendreClicheFinPermanentEditor;
import org.fudaa.fudaa.crue.common.services.PerspectiveService;
import org.fudaa.fudaa.crue.common.services.SelectedPerspectiveService;
import org.fudaa.fudaa.crue.common.swing.CrueBasicComponentFactory;
import org.openide.modules.ModuleInstall;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Item;
import org.openide.util.NbBundle.Messages;
import org.openide.util.actions.BooleanStateAction;
import org.openide.util.lookup.Lookups;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
import static org.fudaa.fudaa.crue.common.Bundle.*;

public class InitPerspectiveInstaller extends ModuleInstall implements Runnable {

  @Override
  protected void initialize() {
    BuPreferences.BU.putBooleanProperty("button.text",true);
    BuResource.BU.setIconFamily("crystal");
    BPalettePlage.setDisplayReversed();
    WindowManager.getDefault().invokeWhenUIReady(this);
  }

  @Override
  public void validate() throws IllegalStateException {
    super.validate();
  }

  @Override
  public void restored() {
    BuPreferences.BU.putBooleanProperty("button.text",true);
    BuResource.BU.setIconFamily("crystal");
    BPalettePlage.setDisplayReversed();

    WindowManager.getDefault().invokeWhenUIReady(this);
  }

  @Override
  @Messages("FudaaCrueClosing.Label=Fudaa-Crue est en cours de fermeture...")
  public boolean closing() {

    SelectedPerspectiveService selectedPerspectiveService = Lookup.getDefault().lookup(SelectedPerspectiveService.class);
    boolean closing = true;
    if (selectedPerspectiveService.getCurrentPerspectiveService() != null) {
      closing = selectedPerspectiveService.getCurrentPerspectiveService().closing();
    }
    if (closing) {
      Set<TopComponent> opened = TopComponent.getRegistry().getOpened();
      for (TopComponent topComponent : opened) {
        topComponent.close();
      }
      PerspectiveService perspectiveService = selectedPerspectiveService.getPerspectiveService(PerspectiveEnum.STUDY);
      if (perspectiveService != null) {
        closing = perspectiveService.closing();
      }
      JDialog d = new JDialog();
      JLabel jLabel = new JLabel(FudaaCrueClosing_Label());
      d.setTitle(jLabel.getText());
      JPanel pn = new JPanel(new BorderLayout());
      pn.add(jLabel);
      d.setContentPane(pn);
      d.pack();
      d.setSize(300, d.getPreferredSize().height);
      d.setLocationRelativeTo(WindowManager.getDefault().getMainWindow());
      d.repaint(0);
      d.setModal(false);
      d.setVisible(true);
    }
    return closing;
  }

  @Override
  public void run() {
    UIManager.put("ToggleButton.select", Color.RED);
    UIManager.put("ToggleButton.focus", Color.RED);
    UIManager.put("ToggleButton.highlight", Color.RED);
    EbliComponentFactory.INSTANCE.setComponentFactory(new CrueBasicComponentFactory());
    DurationPropertyEditor.registerEditor();
    LocalDateTimeEditor.registerEditor();
    PrendreClicheFinPermanentEditor.registerAllClicheEditors();
    //https://fudaa-project.atlassian.net/browse/CRUE-633 
    //La version 7.4 corrige le probleme mais apporte des régressions sur la persistances des informations sur les OutlineView. <br>
    PropertyEditorManager.registerEditor(File.class, FileEditor.class);
    PropertyEditorManager.registerEditor(Color.class, CustomColorChooser.class);
    PropertyEditorManager.setEditorSearchPath(new String[]{"org.netbeans.beaninfo.editors"});
    Set<TopComponent> opened = WindowManager.getDefault().getRegistry().getOpened();
    for (TopComponent topComponent : opened) {
      topComponent.close();
    }
    final String folder = "Actions/File/";
    final String actionId = folder + "org-fudaa-fudaa-crue-study-perspective-ActiveStudy";
    Lookup.Template<BooleanStateAction> template = new Lookup.Template<>(BooleanStateAction.class, actionId, null);
    Item<BooleanStateAction> lookupAll = Lookups.forPath(folder).lookupItem(template);
    if (lookupAll == null) {
      Logger.getLogger(InitPerspectiveInstaller.class.getName()).log(Level.SEVERE, actionId + " must be set to set to the default action to activate");
    } else {
      lookupAll.getInstance().actionPerformed(null);
    }
  }
}
