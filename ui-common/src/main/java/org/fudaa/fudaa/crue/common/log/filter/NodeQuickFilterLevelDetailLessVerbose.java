package org.fudaa.fudaa.crue.common.log.filter;

import gnu.trove.TObjectIntHashMap;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class NodeQuickFilterLevelDetailLessVerbose extends AbstractNodeQuickFilter {

  private final int levelVerbosity;
  private final boolean strict;
  private final TObjectIntHashMap<String> verbosityByLevel;

  public NodeQuickFilterLevelDetailLessVerbose(final String propertyId, final String propertyName, final String level, final boolean strict,
                                               final TObjectIntHashMap<String> verbosityByLevel) {
    super(propertyId);
    this.verbosityByLevel = verbosityByLevel;
    this.levelVerbosity = verbosityByLevel.get(level);
    this.strict = strict;
    final String translated = level;
    name = NbBundle.getMessage(NodeQuickFilterLevelDetailLessVerbose.class,
                               strict ? "filter.lessVerboseStrict.name" : "filter.lessVerbose.name", propertyName, translated);
    description = NbBundle.getMessage(NodeQuickFilterLevelDetailLessVerbose.class,
                                      strict ? "filter.lessVerboseStrict.description" : "filter.lessVerbose.description",
                                      propertyName, translated);
  }

  @Override
  protected boolean acceptValue(final Object value) {
    final int other = verbosityByLevel.get((String) value);
    if (strict) {
      return levelVerbosity > other;
    }
    return levelVerbosity >= other;

  }
}
