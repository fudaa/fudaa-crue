/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.property;

import java.beans.PropertyEditor;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.Pdt;
import org.fudaa.fudaa.crue.common.pdt.PdtEditorSupport;

/**
 *
 * @author Frédéric Deniger
 */
public class PropertySupportReflectionPdt extends PropertySupportReflection<Pdt> /**
 * implements PropertyChangeListener *
 */
{

  public static final String PROP_CCM = "CCM";
  String property;

  public PropertySupportReflectionPdt(AbstractNodeFirable node, Object instance, String property, CrueConfigMetier ccm) throws NoSuchMethodException {
    super(node, instance, Pdt.class, property);
    setValue(PROP_CCM, ccm);
    this.property = property;
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    PdtEditorSupport editor = new PdtEditorSupport((CrueConfigMetier) getValue(PROP_CCM));
    return editor;
  }
}
