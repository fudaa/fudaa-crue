package org.fudaa.fudaa.crue.common.view;

import com.memoire.dnd.DndConstants;
import org.fudaa.ctulu.gui.CtuluTableSimpleExporter;
import org.fudaa.ctulu.table.CtuluTableModelDefault;
import org.fudaa.dodico.crue.validation.ValidatingItem;
import org.fudaa.fudaa.crue.common.helper.CtuluUIForNetbeans;
import org.fudaa.fudaa.crue.common.node.NodeChildrenHelper;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.node.NodeTreeListener;
import org.fudaa.fudaa.crue.common.node.OutlineViewExportTableModel;
import org.fudaa.fudaa.crue.common.property.AbstractNodeFirable;
import org.fudaa.fudaa.crue.common.property.AbstractNodeValueFirable;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Vue par default avec des actions pour les copy/paste et import.
 *
 * @author deniger
 */
public abstract class DefaultEditableOutlineViewEditor<T extends DefaultTopComponentParent, O, N extends AbstractNodeValueFirable<O>> extends DefaultOutlineViewEditor {
    public static final String FIRST_COLUMN_NAME_SELECTION = "Sel.";
    protected JButton btImport;
    private final T topComponentParent;
    /**
     * true tant que la liste est mise à jour par le topComponent parent
     */
    private boolean updating;
    private boolean usedIndexAsDisplayName = true;

    /**
     * Constructeur par defaut
     */
    public DefaultEditableOutlineViewEditor(T topComponentParent) {
        this.topComponentParent = topComponentParent;
        view.setNodePopupFactory(new DefaultEditableOutlineViewEditorPopupFactory(this));
        view.setAllowedDragActions(DndConstants.NONE);
    }

    /**
     * @param availableEntries entrees disponibles
     * @param usedEntries      entree utilisees
     * @return premiere section non utilisée
     */
    protected static String getFirstUsable(String[] availableEntries, Set<String> usedEntries) {
        for (String availableEntry : availableEntries) {
            if (!usedEntries.contains(availableEntry)) {
                return availableEntry;
            }
        }
        return null;
    }

    private static void cutToClipboard(ExplorerManager explorerManager) {
        Node[] sel = explorerManager.getSelectedNodes();
        if (sel.length > 0) {
            copyToClipboard(explorerManager, false);
            for (Node node : sel) {
                if (node.canDestroy()) {
                    try {
                        node.destroy();
                    } catch (IOException e) {
                        Exceptions.printStackTrace(e);
                    }
                }
            }
        }
    }

    /**
     * copy les cellules sélectionnées ou toutes les cellules dans le presse-papier
     *
     * @param all si tout doit être copié
     */
    private static void copyToClipboard(ExplorerManager explorerManager, boolean all) {
        Node[] sel = all ? NodeChildrenHelper.getNodesAsArray(explorerManager) : explorerManager.getSelectedNodes();
        StringBuilder data = new StringBuilder();
        for (Node node : sel) {
            if (data.length() > 0) {
                data.append('\n');
            }
            data.append(((AbstractNodeFirable) node).getDataAsString());
        }
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        if (clipboard != null) {
            clipboard.setContents(new StringSelection(data.toString()), null);
        }
    }

    public void setUsedIndexAsDisplayName(boolean usedIndexAsDisplayName) {
        this.usedIndexAsDisplayName = usedIndexAsDisplayName;
    }

    public void update(List<N> nodes) {
        update(nodes, usedIndexAsDisplayName);
    }

    public void update(List<N> nodes, boolean usedIndexAsDisplayName) {
        setUpdating(true);
        getExplorerManager().setRootContext(NodeHelper.createNode(nodes, getClass().getName(), false));
        validateData();
        if (usedIndexAsDisplayName) {
            DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
        }
        setUpdating(false);
    }

    protected abstract boolean validateData();

    /**
     * @return liste des EchelleSection éditées dans les noeuds
     */
    public List<N> getAllNodes() {
        return NodeChildrenHelper.getNodes(getExplorerManager());
    }

    /**
     * @return liste des EchelleSection éditées dans les noeuds
     */
    public List<O> getAllNodesValues() {
        List<N> allNodes = getAllNodes();
        List<O> res = new ArrayList<>();
        if (allNodes != null) {
            for (N node : allNodes) {
                res.add(node.getMainValue());
            }
        }
        return res;
    }

    /**
     * @return liste des EchelleSection éditées dans les noeuds
     */
    public List<ValidatingItem<O>> getAllNodesForValidation() {
        List<N> allNodes = getAllNodes();
        List<ValidatingItem<O>> res = new ArrayList<>();
        if (allNodes != null) {
            for (N node : allNodes) {
                res.add(new ValidatingItem<>(node.getMainValue(), node));
            }
        }
        return res;
    }

    public boolean isEditable() {
        return topComponentParent.isEditable();
    }

    @Override
    public void setEditable(boolean editable) {
        super.setEditable(editable);
        if (btImport != null) {
            btImport.setEnabled(editable);
        }
    }

    protected T getTopComponentParent() {
        return topComponentParent;
    }

    @Override
    protected void changeInNode(String propertyName) {
        if (!isUpdating()) {
            getTopComponentParent().setModified(true);
            if (NodeTreeListener.isPropertyRelatedToOrder(propertyName) && this.usedIndexAsDisplayName) {
                DefaultNodePasteType.updateIndexedDisplayName(getExplorerManager().getRootContext());
            }
        }
    }

    @Override
    protected JComponent[] createOtherButtons() {
        btImport = new JButton(NbBundle.getMessage(DefaultEditableOutlineViewEditor.class, "Import.DisplayName"));
        btImport.addActionListener(e -> importData());
        btImport.setToolTipText(NbBundle.getMessage(DefaultEditableOutlineViewEditor.class, "ReplaceAndImport.Details"));
        return new JComponent[]{btImport};
    }

    public boolean isUpdating() {
        return updating;
    }

    public void setUpdating(boolean updating) {
        this.updating = updating;
    }

    protected abstract void importData();

    @Override
    protected JButton createPasteComponent() {
        JButton btPaste = new JButton(NbBundle.getMessage(DefaultEditableOutlineViewEditor.class, "ReplaceAndPaste.DisplayName"));
        btPaste.addActionListener(e -> replaceAllByClipboardData());
        btPaste.setToolTipText(NbBundle.getMessage(DefaultEditableOutlineViewEditor.class, "ReplaceAndPaste.Details"));
        return btPaste;
    }

    /**
     * coupe les noeuds selectionnes dans le clipboard
     */
    public void cutToClipboard() {
        cutToClipboard(getExplorerManager());
    }

    /**
     * copy les cellules sélectionnées ou toutes les cellules dans le presse-papier
     *
     * @param all si tout doit être copié
     */
    public void copyToClipboard(boolean all) {
        copyToClipboard(getExplorerManager(), all);
    }

    protected abstract void replaceAllByClipboardData();

    /**
     * copie le clipboard dans les noeuds selectionnes
     */
    public abstract void pasteInSelectedNodes();

    /**
     * exporte toutes les lignes.
     */
    public void exportAll() {
        CtuluTableModelDefault defaultModel = new OutlineViewExportTableModel(view.getOutline(), null, null);
        CtuluTableSimpleExporter.doExport(';', defaultModel, CtuluUIForNetbeans.DEFAULT);
    }
}
