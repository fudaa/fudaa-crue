package org.fudaa.fudaa.crue.common.log.filter;

import java.util.ArrayList;
import java.util.List;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;

/**
 *
 * @author deniger
 */
public class NodeQuickFilterProcess {

  /**
   * la propriete a utiliser pour stocker la map propriete par id
   */
  public static final String PROPERTY_BY_ID_KEY = "propertyById";

  public Node filter(Node init, NodeQuickFilter filter) {
    List<Node> filteredChildren = new ArrayList<>();
    Node[] nodes = init.getChildren().getNodes();

    for (Node node : nodes) {
      node.getPropertySets();//astuce... pour forcer la création du sheet qui permet d'avoir la map des valeurs.
      if (filter.accept(node)) {
        if (node.getChildren().getNodes().length > 0) {
          filteredChildren.add(filter(node, filter));
        } else {
          filteredChildren.add(new FilterNode(node, Children.LEAF));
        }
      }
    }
    if (filteredChildren.isEmpty()) {
      return new FilterNode(init, Children.LEAF);
    }
    Children.Array childrenNode = new Children.Array();
    childrenNode.add(filteredChildren.toArray(new Node[0]));
    return new FilterNode(init, childrenNode);
  }
}
