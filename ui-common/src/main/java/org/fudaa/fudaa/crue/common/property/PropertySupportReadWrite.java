/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.property;

import org.apache.commons.lang3.ObjectUtils;
import org.openide.nodes.PropertySupport;

import java.lang.reflect.InvocationTargetException;

/**
 * Une propriété qui envoie des evts via AbstractNodeFirable
 * I instance de l'objet T type de retour
 *
 * @author Fred Deniger
 */
public abstract class PropertySupportReadWrite<I, T> extends PropertySupport.ReadWrite<T> {
    protected final AbstractNodeFirable node;
    private final I instance;
    private boolean canWrite = true;

    public PropertySupportReadWrite(AbstractNodeFirable node, I instance, Class<T> valueType, String name, String displayName) {
        this(node, instance, valueType, name, displayName, null);
    }

    /**
     * @param node        le node contenant l'objet. Enverra les evts.
     * @param instance    l'objet
     * @param valueType   le type de la valeur
     * @param name        le nom de le propriété
     * @param displayName le nom d'affichage
     * @param description la description
     */
    public PropertySupportReadWrite(AbstractNodeFirable node, I instance, Class<T> valueType, String name, String displayName,
                                    String description) {
        super(name, valueType, displayName, description);
        this.node = node;
        this.instance = instance;
    }

    public I getInstance() {
        return instance;
    }

    @Override
    public boolean canRead() {
        return super.canRead();
    }

    public void setCanWrite(boolean canWrite) {
        this.canWrite = canWrite;
    }

    @Override
    public boolean canWrite() {
        return canWrite && node.isEditMode() && super.canWrite();
    }

    protected abstract void setValueInInstance(T newVal);

    /**
     * Envoie un evt s'il la propriété est modifiée.
     *
     * @param newValue la nouvelle valeur
     */
    @Override
    public void setValue(T newValue) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        if (!canWrite()) {
            return;
        }
        Object old = getValue();
        if (!ObjectUtils.equals(newValue, old)) {
            setValueInInstance(newValue);
            node.fireObjectChange(getName(), old, newValue);
        }
    }

    protected void fireObjectChange(String name, Object oldValue, Object newValue) {
        node.fireObjectChange(getName(), oldValue, newValue);
    }
}
