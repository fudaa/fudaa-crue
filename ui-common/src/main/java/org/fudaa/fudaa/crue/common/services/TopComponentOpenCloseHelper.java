package org.fudaa.fudaa.crue.common.services;

import javax.swing.JComponent;
import org.openide.windows.TopComponent;

/**
 *
 * @author deniger
 */
public class TopComponentOpenCloseHelper {

  public static boolean isTemporarelyClosed(JComponent target) {
    Object clientProperty = target.getClientProperty(PROPERTY_TOP_COMPONENT_CLOSED_TEMPORARILY);
    final boolean closeTemporarely = Boolean.TRUE.equals(clientProperty);
    return closeTemporarely;
  }

  /**
   * Seuls des TopComponent doivent hériter de cette interface.
   */
  public interface Target {

    /**
     * Appele lors d'un changement de perspective.
     */
    void componentClosedTemporarily();

    /**
     * Appele lors d'une fermeture par l'utilisateur du TopComponent.
     */
    void componentClosedDefinitly();
  }
  private static final String PROPERTY_TOP_COMPONENT_CLOSED_TEMPORARILY = "PROPERTY_TOP_COMPONENT_CLOSED_TEMPORARILY";

  public void componentClosedHandler(Target targetTopComponent) {
    JComponent target = (JComponent) targetTopComponent;
    boolean closeTemporarely = isTemporarelyClosed(target);
    if (closeTemporarely) {
      targetTopComponent.componentClosedTemporarily();
    } else {
      targetTopComponent.componentClosedDefinitly();
    }
    target.putClientProperty(PROPERTY_TOP_COMPONENT_CLOSED_TEMPORARILY, null);
  }

  public void closeTemporarily(TopComponent target) {
    target.putClientProperty(PROPERTY_TOP_COMPONENT_CLOSED_TEMPORARILY, Boolean.TRUE);
    target.close();
  }
}
