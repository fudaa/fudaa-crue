package org.fudaa.fudaa.crue.common.editor;

import java.beans.PropertyEditorSupport;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;

/**
 *
 * @author deniger
 */
public class ItemVariableDoublePropertyEditorSupport extends PropertyEditorSupport {
  
  private final ItemVariable itemVariable;
  private final DecimalFormatEpsilonEnum formatType;
  
  public ItemVariableDoublePropertyEditorSupport(ItemVariable itemVariable, DecimalFormatEpsilonEnum formatType) {
    this.itemVariable = itemVariable;
    this.formatType = formatType;
  }
  
  @Override
  public String getAsText() {
    Double value = (Double) super.getValue();
    if (value == null) {
      return StringUtils.EMPTY;
    }
    if (itemVariable == null) {
      return value.toString();
    }
    return itemVariable.getFormatter(formatType).format(value);
  }
  
  @Override
  public void setValue(Object value) {
    if (value != null && itemVariable != null) {
      value = Double.valueOf(itemVariable.getNature().getNormalizedValue(((Number) value).doubleValue()));
    }
    super.setValue(value);
  }
  
  @Override
  public void setAsText(String string) {
    try {
      Double value = Double.parseDouble(string);
      if (itemVariable != null) {
        String error = itemVariable.getValidator().getValidateErrorForValue(value);
        if (error != null) {
          throw new IllegalArgumentException(error);
        }
      }

      setValue(value);
    } catch (IllegalArgumentException illegalArgumentException) {
      Logger.getLogger(ItemVariableDoublePropertyEditorSupport.class.getName()).log(Level.FINER, "message {0}", illegalArgumentException);
    }
  }
}
