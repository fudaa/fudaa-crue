package org.fudaa.fudaa.crue.common.log;

import java.awt.Component;
import java.util.List;
import javax.swing.JPopupMenu;
import org.fudaa.fudaa.crue.common.log.property.PropertyColumnDefault;
import org.fudaa.fudaa.crue.common.log.property.PropertyColumnFilterable;
import org.netbeans.swing.outline.Outline;
import org.openide.explorer.view.NodePopupFactory;
import org.openide.nodes.Node;

/**
 *
 * @author deniger
 */
public class LogNodePopupFactory extends NodePopupFactory {

  private final List<PropertyColumnFilterable> columns;

  public LogNodePopupFactory(List<PropertyColumnFilterable> columns) {
    this.columns = columns;
  }

  @Override
  public JPopupMenu createPopupMenu(int row, int column, Node[] selectedNodes, Component component) {
    Outline outline = (Outline) component;
    if (column > 0) {
      int modelIndex = outline.getColumnModel().getColumn(column).getModelIndex() - 1;
      return columns.get(modelIndex).createPopupMenu(row, column, selectedNodes, component);
    }
    return new PropertyColumnDefault().createPopupMenu(row, column, selectedNodes, component);
  }
}
