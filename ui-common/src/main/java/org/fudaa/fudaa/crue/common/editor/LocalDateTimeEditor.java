package org.fudaa.fudaa.crue.common.editor;

import java.awt.Component;
import java.beans.*;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.nodes.Node;

/**
 *
 * @author Frédéric Deniger
 */
public class LocalDateTimeEditor extends PropertyEditorSupport implements ExPropertyEditor, PropertyChangeListener {

  public static void registerEditor() {
    PropertyEditorManager.registerEditor(LocalDateTime.class, LocalDateTimeEditor.class);
  }
  PropertyEnv env;

  @Override
  public void attachEnv(PropertyEnv env) {
    this.env = env;
    env.addPropertyChangeListener(this);
    env.setState(PropertyEnv.STATE_NEEDS_VALIDATION);
    FeatureDescriptor featureDescriptor = env.getFeatureDescriptor();
    featureDescriptor.setValue("suppressCustomEditor", Boolean.FALSE);
    featureDescriptor.setValue("canEditAsText", Boolean.TRUE);
    PropertyCrueUtils.configureNoEditAsText(featureDescriptor);
  }
  LocalDateTimePanel localDateTimePanel;

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (PropertyEnv.PROP_STATE.equals(evt.getPropertyName())
            && evt.getNewValue() == PropertyEnv.STATE_VALID) {
      setValue(localDateTimePanel.getLocalDateTime());
    }
  }

  @Override
  public boolean supportsCustomEditor() {
    return true;
  }

  @Override
  public Component getCustomEditor() {
    if (localDateTimePanel == null) {
      localDateTimePanel = new LocalDateTimePanel();
    }
    Node.Property featureDescriptor = (Node.Property) env.getFeatureDescriptor();
    localDateTimePanel.setLocalDateTime((LocalDateTime) getValue());
    localDateTimePanel.setEditable(featureDescriptor.canWrite());
    return localDateTimePanel;
  }

  @Override
  public String getAsText() {
    LocalDateTime value = (LocalDateTime) getValue();
    if (value == null) {
      return StringUtils.EMPTY;
    }
    return DateDurationConverter.XSD_DATE_FORMATTER_UI.print(value);
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    try {
      DateTime parseDateTime = DateDurationConverter.XSD_DATE_FORMATTER_UI.parseDateTime(text);
      if (parseDateTime != null) {
        LocalDateTime local = new LocalDateTime(parseDateTime);
        setValue(local);
      }
    } catch (Exception e) {
    }
  }
}
