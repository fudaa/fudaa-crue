/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.node;

import org.openide.nodes.*;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Implementer la méthode changeDoneInNode pour etre avertit de toutes les modifications.
 *
 * @author Frederic Deniger
 */
public class NodeTreeListener {
    private static final String NODE_REMOVED = "nodeRemoved";
    private static final String NODE_DESTROYED = "nodeDestroyed";
    private static final String NODE_REORDERED = "nodeReordered";
    private static final String NODE_ADDED = "nodeAdded";
    private final CustomNodeListener customNodeListener = new CustomNodeListener();
    private final CustomPropertyChangeListener customPropertyChangeListener = new CustomPropertyChangeListener();

    public static boolean isPropertyRelatedToOrder(String propertyName) {
        return NODE_REMOVED.equals(propertyName)
                || NODE_DESTROYED.equals(propertyName)
                || NODE_REORDERED.equals(propertyName);
    }

    public void changeDoneInNode(String propertyName) {
        //nothing to do.
    }

    /**
     * installe un listener le parent et sur tous les enfants.
     *
     * @param parentNode le noeud parent
     */
    public void installListener(Node parentNode) {
        parentNode.removePropertyChangeListener(customPropertyChangeListener);
        parentNode.removeNodeListener(customNodeListener);
        parentNode.addPropertyChangeListener(customPropertyChangeListener);
        parentNode.addNodeListener(customNodeListener);
        Node[] nodes = parentNode.getChildren().getNodes();
        for (Node node : nodes) {
            installListener(node);
        }
    }

    /**
     * Enleve un listener le parent et sur tous les enfants.
     *
     * @param parentNode le noeud parent
     */
    public void removeListener(Node parentNode) {
        parentNode.removePropertyChangeListener(customPropertyChangeListener);
        parentNode.removeNodeListener(customNodeListener);
        Node[] nodes = parentNode.getChildren().getNodes();
        for (Node node : nodes) {
            removeListener(node);
        }
    }

    private class CustomPropertyChangeListener implements PropertyChangeListener {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            changeDoneInNode(evt.getPropertyName());
        }
    }

    private class CustomNodeListener implements NodeListener {
        @Override
        public void childrenAdded(NodeMemberEvent nme) {
            Node[] delta = nme.getDelta();
            for (Node node : delta) {
                removeListener(node);
                installListener(node);
            }
            changeDoneInNode(NODE_ADDED);
        }

        @Override
        public void childrenRemoved(NodeMemberEvent nme) {
            Node[] delta = nme.getDelta();
            for (Node node : delta) {
                removeListener(node);
            }
            changeDoneInNode(NODE_REMOVED);
        }

        @Override
        public void childrenReordered(NodeReorderEvent nre) {
            changeDoneInNode(NODE_REORDERED);
        }

        @Override
        public void nodeDestroyed(NodeEvent ne) {
            changeDoneInNode(NODE_DESTROYED);
            removeListener(ne.getNode());
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            changeDoneInNode(evt.getPropertyName());
        }
    }
}
