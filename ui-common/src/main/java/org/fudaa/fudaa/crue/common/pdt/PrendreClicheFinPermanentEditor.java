/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.pdt;

import java.beans.*;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.emh.PrendreClicheFinPermanent;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;

/**
 *
 * @author Frédéric Deniger
 */
public class PrendreClicheFinPermanentEditor extends PropertyEditorSupport implements ExPropertyEditor, PropertyChangeListener {

  public static void registerAllClicheEditors() {
    PrendreClicheFinPermanentEditor.registerEditor();
    IniCalcClicheEditor.registerEditor();
    PrendreClichePeriodiqueEditor.registerEditor();
    PrendreClichePonctuelEditor.registerEditor();
  }

  static void registerEditor() {
    PropertyEditorManager.registerEditor(PrendreClicheFinPermanent.class, PrendreClicheFinPermanentEditor.class);
  }
  PropertyEnv env;

  @Override
  public void attachEnv(PropertyEnv env) {
    this.env = env;
    env.addPropertyChangeListener(this);
    env.setState(PropertyEnv.STATE_NEEDS_VALIDATION);
    FeatureDescriptor featureDescriptor = env.getFeatureDescriptor();
    featureDescriptor.setValue("suppressCustomEditor", Boolean.TRUE);
    featureDescriptor.setValue("canEditAsText", Boolean.TRUE);
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
  }

  @Override
  public boolean supportsCustomEditor() {
    return false;
  }

  @Override
  public String getAsText() {
    PrendreClicheFinPermanent value = (PrendreClicheFinPermanent) getValue();
    if (value == null) {
      return StringUtils.EMPTY;
    }
    return value.getNomFic();
  }

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    try {
      PrendreClicheFinPermanent value = new PrendreClicheFinPermanent();
      value.setNomFic(text);
      setValue(value);
    } catch (Exception e) {
    }
  }
}
