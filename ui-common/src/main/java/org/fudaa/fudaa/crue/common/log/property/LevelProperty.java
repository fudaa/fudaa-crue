/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.log.property;

import java.beans.PropertyEditor;
import java.lang.reflect.InvocationTargetException;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.openide.nodes.PropertySupport;
import org.openide.util.NbBundle;

/**
 *
 * @author Fred Deniger
 */
public class LevelProperty extends PropertySupport.ReadOnly<CtuluLogLevel> {

  public static final String ID = "Level";
  private final CtuluLogRecord record;

  public LevelProperty(CtuluLogRecord record) {
    super(ID, CtuluLogLevel.class, getDefaultDisplayName(),
          getDescription());
    this.record = record;
  }

  public static String getDescription() {
    return NbBundle.getMessage(LevelProperty.class, "PropertyLogLevelDescription");
  }
  
   public static PropertyColumnFilterable createColumn() {
    PropertyColumnFilterable res = new LevelPropertyColumn();
    res.setColumnId(ID);
    res.setDescription(getDescription());
    res.setDisplayName(getDefaultDisplayName());
    return res;
  }

  public static String getDefaultDisplayName() {
    return NbBundle.getMessage(LevelProperty.class, "Level");
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    return new LevelPropertyEditor();
  }

  @Override
  public String toString() {
    return record.getLevel().toString();
  }

  @Override
  public CtuluLogLevel getValue() throws IllegalAccessException, InvocationTargetException {
    return record.getLevel();
  }
}
