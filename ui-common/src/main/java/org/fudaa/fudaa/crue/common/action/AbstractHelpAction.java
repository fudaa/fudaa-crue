/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.action;

import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.services.SysdocContrat;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * @author Frederic Deniger
 */
public class AbstractHelpAction extends AbstractAction {
  public static final String HELP_ICON = "org/openide/resources/propertysheet/propertySheetHelp.png";
  private final PerspectiveEnum perspectiveEnum;
  final SysdocContrat sysdocContrat = Lookup.getDefault().lookup(SysdocContrat.class);

  public AbstractHelpAction(PerspectiveEnum perspectiveEnum) {
    this.perspectiveEnum = perspectiveEnum;
    putValue(Action.NAME, NbBundle.getMessage(AbstractHelpAction.class, "PerpsectiveHelp.ActionName"));
    final Icon icon = ImageUtilities.loadImageIcon("org/openide/resources/propertysheet/propertySheetHelp.png", false);
    putValue(LARGE_ICON_KEY, icon);
    putValue(SMALL_ICON, icon);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    sysdocContrat.display(SysdocUrlBuilder.getPerspectiveHelpCtxId(perspectiveEnum));
  }
}
