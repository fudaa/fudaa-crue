package org.fudaa.fudaa.crue.common.log;

import gnu.trove.TIntObjectHashMap;
import gnu.trove.TObjectIntHashMap;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.ctulu.gui.ActionTransferSource;
import org.fudaa.ctulu.gui.CtuluTableExportAction;
import org.fudaa.ctulu.gui.PopupMenuReceiver;
import org.fudaa.fudaa.crue.common.AbstractTopComponent;
import org.fudaa.fudaa.crue.common.helper.DialogHelper;
import org.fudaa.fudaa.crue.common.helper.UiContext;
import org.fudaa.fudaa.crue.common.log.filter.NodeQuickFilter;
import org.fudaa.fudaa.crue.common.log.filter.NodeQuickFilterProcess;
import org.fudaa.fudaa.crue.common.log.property.*;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.fudaa.fudaa.crue.common.services.SysdocContrat;
import org.netbeans.swing.etable.ETableColumnModel;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.windows.TopComponent;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * Top component which displays something.
 */
public class CtuluLogsTopComponent extends AbstractTopComponent implements ExplorerManager.Provider {
    private final SysdocContrat sysdocContrat = Lookup.getDefault().lookup(SysdocContrat.class);
    private final org.openide.explorer.view.OutlineView outlineLogsView = new org.openide.explorer.view.OutlineView(
            NbBundle.getMessage(CtuluLogsTopComponent.class, "PropertyLogMessageName"));
    private final List<TableColumn> allColumns = new ArrayList<>();
    private ExplorerManager em = new ExplorerManager();
    private String preferencePrefix;
    private List<PropertyColumnFilterable> columns;
    private TObjectIntHashMap<String> verbosityByLevel;
    private NoFilterAction noFilter;
    private Node allItemRootNode;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jActivatedFilter;
    private javax.swing.JLabel jLogDescription;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanelErrorAndFilter;
    private javax.swing.JLabel lbError;

    public CtuluLogsTopComponent() {
        this(false, null);
    }

    public CtuluLogsTopComponent(boolean trace, TObjectIntHashMap<String> verbosityByLevel) {
        initComponents();
        outlineLogsView.getOutline().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        setName(NbBundle.getMessage(CtuluLogsTopComponent.class, "CTL_CtuluLogGroupsTopComponent"));
        setToolTipText(NbBundle.getMessage(CtuluLogsTopComponent.class, "HINT_CtuluLogGroupsTopComponent"));
        putClientProperty(TopComponent.PROP_CLOSING_DISABLED, Boolean.TRUE);
        putClientProperty(TopComponent.PROP_DRAGGING_DISABLED, Boolean.TRUE);
        putClientProperty(TopComponent.PROP_MAXIMIZATION_DISABLED, Boolean.TRUE);
        putClientProperty(TopComponent.PROP_UNDOCKING_DISABLED, Boolean.TRUE);
        if (trace) {
            initForTrace(verbosityByLevel);
        } else {
            setColumns(Collections.singletonList(LevelProperty.createColumn()));
        }
        associateLookup(ExplorerUtils.createLookup(em, getActionMap()));
        outlineLogsView.getOutline().setRootVisible(false);
        outlineLogsView.getOutline().putClientProperty(CtuluLogsTopComponent.class.getSimpleName(), this);
        jActivatedFilter.setText(StringUtils.EMPTY);
        jLogDescription.setText(StringUtils.EMPTY);
    }

    public void setEm(ExplorerManager em) {
        this.em = em;
    }

    @Override
    protected String getHelpCtxId() {
        return null;
    }

    public void addCommonMenuItem(JPopupMenu menu) {
        final CtuluTableExportAction ctuluTableExportAction = new CtuluTableExportAction(new UiContext(), outlineLogsView.getOutline());
        ctuluTableExportAction.setSeparator(';');
        menu.add(ctuluTableExportAction);
        ActionTransferSource.CopyAction actionCopy = new ActionTransferSource.CopyAction(outlineLogsView.getOutline());
        JMenuItem add = menu.add(actionCopy);
        add.setText(org.openide.util.NbBundle.getMessage(CtuluLogsTopComponent.class, "Copy.ActionName"));
        add.setAccelerator(KeyStroke.getKeyStroke("control C"));
        menu.add(PopupMenuReceiver.createCopyAllAction(outlineLogsView.getOutline()));
    }

    private void setColumns(List<PropertyColumnFilterable> newColumns) {
        allColumns.clear();
        ETableColumnModel columnModel = (ETableColumnModel) outlineLogsView.getOutline().getColumnModel();
        if (this.columns != null) {
            for (PropertyColumnFilterable col : this.columns) {
                outlineLogsView.removePropertyColumn(col.getColumnId());
            }
            for (int i = columnModel.getColumnCount() - 1; i > 0; i--) {
                final TableColumn column = columnModel.getColumn(i);
                columnModel.removeColumn(column);
            }
        }
        this.columns = new ArrayList<>(newColumns);
        for (PropertyColumnFilterable propertyColumn : newColumns) {
            outlineLogsView.addPropertyColumn(propertyColumn.getColumnId(), propertyColumn.getDisplayName(),
                    propertyColumn.getDescription());
        }

        int columnCount = Math.min(columnModel.getColumnCount(), newColumns.size());
        CollectionUtils.addAll(allColumns, columnModel.getColumns());
        for (int i = columnCount - 1; i > 0; i--) {//on commence par la fin car les colonnes sont enlevées au fur et à mesure
            final TableColumn column = columnModel.getColumn(i);
            //enleve la colonne des colonnes visibles.
            columnModel.setColumnHidden(column, !newColumns.get(i - 1).isVisibleByDefault());//enleve la colonne des colonnes visibles.
        }
        outlineLogsView.getOutline().getTableHeader().setToolTipText(null);
        outlineLogsView.setNodePopupFactory(new LogNodePopupFactory(this.columns));
    }

    private TableColumn getLinkTableColumn() {
        return getTableColumn(DocProperty.getDefaultDisplayName());
    }

    private TableColumn getMediaTableColumn() {
        return getTableColumn(MultimediaDocProperty.getDefaultDisplayName());
    }

    public void initForTrace(TObjectIntHashMap<String> verbosityByLevel) {
        setColumns(Arrays.asList(LogIdProperty.createColumn(),
                LevelDetailIconProperty.createColumn(),
                LevelDetailStringProperty.createColumn(),
                FunctionProperty.createColumn(),
                FileProperty.createColumn(),
                LineProperty.createColumn(),
                EntiteProperty.createColumn(),
                MessageProperty.createColumn(),
                DocProperty.createColumn(),
                MultimediaDocProperty.createColumn()));
        TableColumn linkTableColumn = getLinkTableColumn();
        TableColumn mediaTableColumn = getMediaTableColumn();
        if (linkTableColumn != null || mediaTableColumn != null) {
            installLinkCellRenderer(linkTableColumn, mediaTableColumn);
        }
        setVerbosityByLevel(verbosityByLevel);
    }

    public void setPreferencePrefix(String preferencePrefix) {
        this.preferencePrefix = preferencePrefix;
    }

    public TObjectIntHashMap<String> getVerbosityByLevel() {
        return verbosityByLevel;
    }

    private void setVerbosityByLevel(TObjectIntHashMap<String> verbosityByLevel) {
        this.verbosityByLevel = verbosityByLevel;
    }

    @Override
    public void componentClosedDefinitly() {
        setLog(null);
    }

    @Override
    public void componentClosedTemporarily() {
    }

    public void setLogGroup(CtuluLogGroup group) {
        setLogGroup(group, new CtuluLogGroupChildFactory.LogRecordBuilderSimple());
    }

    public void setLogGroup(CtuluLogGroup group, CtuluLogGroupChildFactory.LogRecordBuilder childFactoryBuilder) {
        if (group == null) {
            this.em.setRootContext(Node.EMPTY);
        } else {
            this.em.setRootContext(new CtuluLogGroupNode(group, childFactoryBuilder));
        }
        allItemRootNode = em.getExploredContext();
        expandAll();
        if (group != null) {
            jLogDescription.setText(group.getDesci18n());
            updateLabel(group.containsError());
        } else {
            lbError.setText(StringUtils.EMPTY);
            jLogDescription.setText(StringUtils.EMPTY);
        }
    }

    private void updateLabel(boolean containsError) {
        String code = containsError ? "CtuluLogsTopComponent.ContainsError" : "CtuluLogsTopComponent.ContainsNoError";
        lbError.setText(NbBundle.getMessage(CtuluLogsTopComponent.class, code));
        lbError.setForeground(containsError ? Color.RED : Color.GREEN.darker());
    }

    private void expandAll() {
        NodeHelper.expandAll(em, outlineLogsView);
    }

    private void installLinkCellRenderer(TableColumn docColumn, TableColumn multimediaColumn) {
        final TableCellRenderer defaultRenderer = outlineLogsView.getOutline().getDefaultRenderer(Node.Property.class);
        if (docColumn != null) {
            docColumn.setCellRenderer(new LinkedNodeCellRenderer(defaultRenderer, false));
        }
        if (multimediaColumn != null) {
            multimediaColumn.setCellRenderer(new LinkedNodeCellRenderer(defaultRenderer, true));
        }
        final int modelIndex = docColumn == null ? -1 : docColumn.getModelIndex();
        final int multimediaIndex = multimediaColumn == null ? -1 : multimediaColumn.getModelIndex();
        final LinkMouseListener linkMouseListener = new LinkMouseListener(modelIndex, multimediaIndex);
        outlineLogsView.getOutline().addMouseListener(linkMouseListener);
        outlineLogsView.getOutline().addMouseMotionListener(linkMouseListener);
    }

    private void followLink(boolean media) {
        Node[] selectedNodes = em.getSelectedNodes();
        if (selectedNodes.length == 1) {
            CtuluLogRecord record = selectedNodes[0].getLookup().lookup(CtuluLogRecord.class);
            if (record != null) {
                if (media) {
                    if (record.getHelpSupportUrl() != null) {
                        sysdocContrat.displayMedia(record.getHelpSupportUrl());
                    }
                } else if (record.getHelpUrl() != null) {
                    sysdocContrat.display(record.getHelpUrl());
                }
            }
        }
    }

    private TableColumn getTableColumn(final String columnId) {
        for (TableColumn tableColumn : allColumns) {
            final Object id = tableColumn.getIdentifier();
            if (id.equals(columnId)) {
                return tableColumn;
            }
        }
        return null;
    }

    public Action getNoFilterAction() {
        if (noFilter == null) {
            noFilter = new NoFilterAction();
        }
        noFilter.updateEnableState();
        return noFilter;
    }

    public void setLog(CtuluLog log) {
        setLog(log, new CtuluLogGroupChildFactory.LogRecordBuilderSimple());
    }

    private void setLog(CtuluLog log, CtuluLogGroupChildFactory.LogRecordBuilder childFactoryBuilder) {
        if (log == null) {
            this.em.setRootContext(Node.EMPTY);
        } else {
            this.em.setRootContext(new CtuluLogNode(log, childFactoryBuilder.create(log)));
        }
        allItemRootNode = em.getRootContext();
        expandAll();
        if (log != null) {
            updateLabel(log.containsErrorOrSevereError());
            jLogDescription.setText(log.getDesci18n());
        } else {
            lbError.setText(StringUtils.EMPTY);
            jLogDescription.setText(StringUtils.EMPTY);
        }
    }

    @Override
    public ExplorerManager getExplorerManager() {
        return em;
    }

    @Override
    public void componentOpened() {
        super.componentOpened();
    }

    /**
     * Pas de peristance Netbeans RCP car géré par methodes custom:
     * org.fudaa.fudaa.crue.common.UserPreferencesSaver.loadDialogLocationAndDimension(JDialog) et
     * org.fudaa.fudaa.crue.common.log.CtuluLogsTopComponent.readPreferences() En fait cette fenetre n'est pas tout le temps affiché comme un
     * TopComponent (pour visualiser le contenu d'une cellule par exemple) et donc on ne peut pas utiliser le mécanisme netbeans RCP.
     *
     * @return PERSISTENCE_NEVER
     */
    @Override
    public int getPersistenceType() {
        return PERSISTENCE_NEVER;
    }

    private String getPreferencesPrefix(String version) {
        if (this.preferencePrefix != null) {
            return preferencePrefix;
        }
        if (StringUtils.isBlank(version)) {
            return getClass().getName() + ".";
        }
        return getClass().getName() + "." + version + ".";
    }

    public void applyFilter(NodeQuickFilter filter) {

        outlineLogsView.getOutline().clearSelection();
        jActivatedFilter.setText(StringUtils.EMPTY);

        if (filter != null) {
            jActivatedFilter.setText(NbBundle.getMessage(CtuluLogsTopComponent.class, "log.filterAppliedLabel", filter.getDescription()));
        }
        final Node filterNode = new NodeQuickFilterProcess().filter(this.allItemRootNode, filter);
        em.setRootContext(filterNode);
        expandAll();
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanelErrorAndFilter = new javax.swing.JPanel();
        lbError = new javax.swing.JLabel();
        jActivatedFilter = new javax.swing.JLabel();
        jLogDescription = new javax.swing.JLabel();

        setForeground(java.awt.Color.white);
        setMinimumSize(null);
        setLayout(new java.awt.BorderLayout());
        add(outlineLogsView, java.awt.BorderLayout.CENTER);

        jPanel2.setLayout(new java.awt.BorderLayout());

        jPanelErrorAndFilter.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 5, 1, 1));
        jPanelErrorAndFilter.setMinimumSize(null);
        jPanelErrorAndFilter.setLayout(new java.awt.GridLayout(2, 1));

        lbError.setForeground(new java.awt.Color(255, 51, 51));
        org.openide.awt.Mnemonics
                .setLocalizedText(lbError, org.openide.util.NbBundle.getMessage(CtuluLogsTopComponent.class, "CtuluLogsTopComponent.lbError.text")); // NOI18N
        lbError.setMaximumSize(null);
        lbError.setMinimumSize(null);
        jPanelErrorAndFilter.add(lbError);

        jActivatedFilter.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        org.openide.awt.Mnemonics.setLocalizedText(jActivatedFilter,
                org.openide.util.NbBundle.getMessage(CtuluLogsTopComponent.class, "CtuluLogsTopComponent.jActivatedFilter.text")); // NOI18N
        jActivatedFilter.setMaximumSize(null);
        jActivatedFilter.setMinimumSize(null);
        jPanelErrorAndFilter.add(jActivatedFilter);

        jPanel2.add(jPanelErrorAndFilter, java.awt.BorderLayout.CENTER);

        jLogDescription.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        org.openide.awt.Mnemonics.setLocalizedText(jLogDescription,
                org.openide.util.NbBundle.getMessage(CtuluLogsTopComponent.class, "CtuluLogsTopComponent.jLogDescription.text")); // NOI18N
        jLogDescription.setMaximumSize(null);
        jLogDescription.setMinimumSize(null);
        jPanel2.add(jLogDescription, java.awt.BorderLayout.NORTH);

        add(jPanel2, java.awt.BorderLayout.NORTH);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Methode utilisee par default par Netbeans
     *
     * @param properties les properties
     */
    protected void writeProperties(java.util.Properties properties) {
        writeProperties(properties, DialogHelper.NO_VERSION_FOR_PERSISTENCE);
    }
    // End of variables declaration//GEN-END:variables

    protected void writeProperties(java.util.Properties p, String version) {
        p.setProperty("version", "1.0");
        //pas de version pour l'instant
        final String preferencesPrefix = getPreferencesPrefix(version);
        p.setProperty(preferencesPrefix + "saved", "true");
        outlineLogsView.getOutline().writeSettings(p, preferencesPrefix);
    }

    /**
     * Methode utilisee par default par Netbeans
     *
     * @param properties les properties a lire
     */
    protected void readProperties(java.util.Properties properties) {
        //pas de version pour l'instant
        final String preferencesPrefix = getPreferencesPrefix(DialogHelper.NO_VERSION_FOR_PERSISTENCE);
        if ("true".equals(properties.getProperty(preferencesPrefix + "saved"))) {
            outlineLogsView.getOutline().readSettings(properties, preferencesPrefix);
            List<TableColumn> savedColumns = new ArrayList<>();
            Enumeration<TableColumn> enumeration = outlineLogsView.getOutline().getColumnModel().getColumns();
            CollectionUtils.addAll(savedColumns, enumeration);
            TIntObjectHashMap<TableColumn> currentColumnByModelIndex = new TIntObjectHashMap<>();
            for (TableColumn tableColumn : allColumns) {
                currentColumnByModelIndex.put(tableColumn.getModelIndex(), tableColumn);
            }
            ETableColumnModel columnModel = (ETableColumnModel) outlineLogsView.getOutline().getColumnModel();
            columnModel.clean();
            for (TableColumn savedColumn : savedColumns) {
                TableColumn currentColumn = currentColumnByModelIndex.remove(savedColumn.getModelIndex());
                if (currentColumn != null) {
                    columnModel.addColumn(currentColumn);
                    columnModel.setColumnHidden(currentColumn, false);
                    currentColumn.setPreferredWidth(savedColumn.getPreferredWidth());
                    currentColumn.setWidth(savedColumn.getWidth());
                }
            }
            Set hiddens = new HashSet(Arrays.asList(currentColumnByModelIndex.getValues()));
            for (TableColumn tableColumn : allColumns) {
                if (hiddens.contains(tableColumn)) {
                    columnModel.addColumn(tableColumn);
                    columnModel.setColumnHidden(tableColumn, true);
                }
            }
        }
    }

    protected void readPreferences(String versionForPreferences) {
        readPreferences(getClass(), versionForPreferences);
    }

    public void readPreferences(Class classForPreferences, String versionForPreferences) {
        Preferences pref = NbPreferences.forModule(classForPreferences);
        try {
            Properties prop = new Properties();
            String preferencesPrefix = getPreferencesPrefix(versionForPreferences);
            String[] childrenNames = pref.keys();
            for (String string : childrenNames) {
                if (string.startsWith(preferencesPrefix)) {
                    prop.put(string, pref.get(string, null));
                }
            }
            readProperties(prop);
        } catch (BackingStoreException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    protected void writePreferences(String version) {
        writePreferences(getClass(), version);
    }

    public void writePreferences(Class classForPreferences, String version) {
        Preferences pref = NbPreferences.forModule(classForPreferences);
        Properties prop = new Properties();
        writeProperties(prop, version);
        Set<Entry<Object, Object>> entrySet = prop.entrySet();
        for (Entry<Object, Object> entry : entrySet) {
            pref.put((String) entry.getKey(), (String) entry.getValue());
        }
    }

    private class NoFilterAction extends AbstractAction {
        public NoFilterAction() {
            super.putValue(NAME, NbBundle.getMessage(CtuluLogsTopComponent.class, "log.noFilter"));
        }

        public void updateEnableState() {
            setEnabled(em.getRootContext() != allItemRootNode);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            em.setRootContext(allItemRootNode);
            jActivatedFilter.setText(StringUtils.EMPTY);
            expandAll();
        }
    }

    private class LinkMouseListener extends MouseAdapter {
        private final int docModelIndex;
        private final int multimediaIndex;

        public LinkMouseListener(int docModelIndex, int multimediaIndex) {
            this.docModelIndex = docModelIndex;
            this.multimediaIndex = multimediaIndex;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
//      if (e.isAltDown()) {
            int columnAtPoint = outlineLogsView.getOutline().columnAtPoint(e.getPoint());
            TableColumn column = outlineLogsView.getOutline().getColumnModel().getColumn(columnAtPoint);
            boolean multimedia = multimediaIndex >= 0 && column.getModelIndex() == multimediaIndex;
            final boolean doc = docModelIndex >= 0 && column.getModelIndex() == docModelIndex;
            if (doc || multimedia) {
                followLink(multimedia);
            }
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            Cursor cursor = Cursor.getDefaultCursor();
            int columnAtPoint = outlineLogsView.getOutline().columnAtPoint(e.getPoint());
            TableColumn column = outlineLogsView.getOutline().getColumnModel().getColumn(columnAtPoint);
            boolean multimedia = multimediaIndex >= 0 && column.getModelIndex() == multimediaIndex;
            final boolean doc = docModelIndex >= 0 && column.getModelIndex() == docModelIndex;
            if (doc || multimedia) {
                int rowAtPoint = outlineLogsView.getOutline().rowAtPoint(e.getPoint());
                Node.Property valueAt = (Node.Property) outlineLogsView.getOutline().getValueAt(rowAtPoint, columnAtPoint);
                try {
                    if (StringUtils.isNotBlank((String) valueAt.getValue())) {
                        cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
                    }
                } catch (Exception exception) {
                    Logger.getLogger(LinkMouseListener.class.getName()).log(Level.FINE, "message {0}", exception);
                }
            }
            if (outlineLogsView.getOutline().getCursor().getType() != cursor.getType()) {
                outlineLogsView.getOutline().setCursor(cursor);
            }
        }
    }
}
