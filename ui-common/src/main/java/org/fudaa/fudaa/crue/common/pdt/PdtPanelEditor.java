/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.pdt;

import com.memoire.bu.BuGridLayout;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.metier.emh.Pdt;
import org.fudaa.dodico.crue.metier.emh.PdtCst;
import org.fudaa.dodico.crue.metier.emh.PdtVar;
import org.openide.util.NbBundle;

/**
 *
 * @author Frédéric Deniger
 */
public class PdtPanelEditor extends JPanel implements ActionListener {

  final JRadioButton btCst;
  final JRadioButton btVariable;
  final JPanel pnCenter;
  private CrueConfigMetier ccm;

  public CrueConfigMetier getCcm() {
    return ccm;
  }

  public void setCcm(CrueConfigMetier ccm) {
    this.ccm = ccm;
  }

  public PdtPanelEditor() {
    setLayout(new BorderLayout());
    JPanel pnNorth = new JPanel(new BuGridLayout(1));
    ButtonGroup bg = new ButtonGroup();
    btCst = new JRadioButton(NbBundle.getMessage(PdtPanelEditor.class, "Pdt.PdtCst.Label"));
    btVariable = new JRadioButton(NbBundle.getMessage(PdtPanelEditor.class, "Pdt.PdtVar.Label"));
    bg.add(btCst);
    bg.add(btVariable);
    pnNorth.add(btCst);
    pnNorth.add(btVariable);
    btCst.addActionListener(this);
    btVariable.addActionListener(this);
    pnCenter = new JPanel();
    add(pnNorth, BorderLayout.NORTH);
    add(pnCenter, BorderLayout.CENTER);
    setEditable(false);
  }

  public void setPdt(Pdt pdt) {
    if (pdt == null || pdt instanceof PdtCst) {
      btCst.setSelected(true);
    } else {
      btVariable.setSelected(true);
    }
    updatePanel(pdt);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    updatePanel(null);
  }
  DurationPanelEditor pdtCstEditor;
  PdtVarPanelEditor pdtVarEditor;

  private void updatePanel(Pdt pdt) {
    pnCenter.removeAll();
    if (btCst.isSelected()) {
      addPanelCst((PdtCst) pdt);
    } else {
      addPanelVar((PdtVar) pdt);
    }
    pnCenter.revalidate();
    pnCenter.repaint();
    revalidate();
    doLayout();
    pnCenter.repaint();
    pnCenter.invalidate();
    JDialog frameAncestor = (JDialog) SwingUtilities.getAncestorOfClass(JDialog.class, this);
    if (frameAncestor != null) {
      frameAncestor.pack();
      frameAncestor.repaint();
    }
  }

  private void addPanelCst(PdtCst pdtCst) {
    if (pdtCstEditor == null) {
      pdtCstEditor = new DurationPanelEditor();
      if (pdtCst != null) {
        pdtCstEditor.setPdt(pdtCst.getPdtCst());
      } else {
        pdtCstEditor.setPdt(DateDurationConverter.getDuration(0, 1, 0, 0));
      }
    } else if (pdtCst != null) {
      pdtCstEditor.setPdt(pdtCst.getPdtCst());

    }
    pdtCstEditor.setEditable(btCst.isEnabled());
    pnCenter.add(pdtCstEditor);
  }

  private void addPanelVar(PdtVar pdtVar) {
    if (pdtVarEditor == null) {
      pdtVarEditor = new PdtVarPanelEditor(ccm);
      if (pdtVar != null) {
        pdtVarEditor.init(pdtVar);
      } else {
        pdtVarEditor.init(new PdtVar());
      }
    } else if (pdtVar != null) {
      pdtVarEditor.init(pdtVar);

    }
    pdtVarEditor.setEditable(btVariable.isEnabled());
    pnCenter.add(pdtVarEditor);
  }

  public Pdt getPdt() {
    return btCst.isSelected() ? getPdtCst() : getPdtVar();
  }

  private Pdt getPdtCst() {
    return new PdtCst(pdtCstEditor.getDuration());
  }

  private Pdt getPdtVar() {
    return pdtVarEditor == null ? null : pdtVarEditor.getPdtVar();
  }

  public final void setEditable(boolean editable) {
    btCst.setEnabled(editable);
    // Pour le moment, le Pdt variable n'est pas supporté, on bloque donc l'édition
    //btVariable.setEnabled(editable);
    btVariable.setEnabled(false);
    if (pdtCstEditor != null) {
      pdtCstEditor.setEditable(editable);
    }
    if (pdtVarEditor != null) {
      pdtVarEditor.setEditable(editable);
    }
  }
}
