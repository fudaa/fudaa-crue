/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.log;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Fred Deniger
 *
 */
public class CtuluLogNode extends AbstractNode implements CtuluLogRecordContainer {
  
  public CtuluLogNode(CtuluLog record, ChildFactory<CtuluLogRecord> childFactory) {
    super(Children.create(childFactory, false), Lookups.singleton(record));
    setName(record.getDesc());
    setDisplayName(record.getDesci18n());
    CtuluLogLevel higherLevel = record.getHigherLevel();
    if (higherLevel != null) {
      setIconBaseWithExtension(LogIconTranslationProvider.getIconBase(higherLevel));
    }
  }
  
  @Override
  public FilterNode createOnlyErrorNode() {
    Node[] nodes = getChildren().getNodes();
    if (nodes.length > 0) {
      List<Node> filteredNode = new ArrayList<>(nodes.length);
      for (Node node : nodes) {
        FilterNode onlyErrorNode = ((CtuluLogRecordContainer) node).createOnlyErrorNode();
        if (onlyErrorNode != null) {
          filteredNode.add(onlyErrorNode);
        }
      }
      if (filteredNode.isEmpty()) {
        return null;
      }
      Children children = new Children.Array();
      children.add(filteredNode.toArray(new Node[0]));
      return new FilterNode(this, children);
    }
    return null;
    
  }
  
  CtuluLog getCtuluLog() {
    return getLookup().lookup(CtuluLog.class);
  }
}
