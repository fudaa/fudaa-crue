package org.fudaa.fudaa.crue.common.editor;

/**
 *
 * @author deniger
 */
public class SliderTransparencyPropertyEditor extends SliderPropertyEditor {

  public SliderTransparencyPropertyEditor() {
    super(new SliderRangeModelFactory.Transparency());
  }
}
