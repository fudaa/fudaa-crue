/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.property;

import java.beans.PropertyEditor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.fudaa.crue.common.editor.ItemVariableDoublePropertyEditorSupport;
import org.openide.util.Exceptions;

/**
 *
 * @author Frédéric Deniger
 */
public class PropertySupportReflectionDouble extends PropertySupportReflection<Double> {

  public static PropertySupportReflection createDouble(DecimalFormatEpsilonEnum formatType, AbstractNodeFirable node, Object instance, String property,
          String name, String displayName, ItemVariable itemVariable) {
    try {
      PropertySupportReflectionDouble res = new PropertySupportReflectionDouble(formatType, node, instance, property);
      PropertyCrueUtils.setNullValueEmpty(res);
      res.setName(name);
      res.setItemVariable(itemVariable);
      res.setDisplayName(displayName);
      return res;
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return null;
  }
  ItemVariable itemVariable;
  private final DecimalFormatEpsilonEnum formatType;

  public PropertySupportReflectionDouble(DecimalFormatEpsilonEnum formatType, AbstractNodeFirable node, Object instance, Method getter, Method setter) {
    super(node, instance, Double.TYPE, getter, setter);
    this.formatType = formatType;
  }

  public PropertySupportReflectionDouble(DecimalFormatEpsilonEnum formatType, AbstractNodeFirable node, Object instance, String getter, String setter) throws NoSuchMethodException {
    super(node, instance, Double.TYPE, getter, setter);
    this.formatType = formatType;
  }

  public PropertySupportReflectionDouble(DecimalFormatEpsilonEnum formatType, AbstractNodeFirable node, Object instance, String property) throws NoSuchMethodException {
    super(node, instance, Double.TYPE, property);
    this.formatType = formatType;
  }

  public ItemVariable getItemVariable() {
    return itemVariable;
  }

  public void setItemVariable(ItemVariable itemVariable) {
    this.itemVariable = itemVariable;
  }

  @Override
  public void setValue(Double newValue) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    super.setValue(newValue);
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    return new ItemVariableDoublePropertyEditorSupport(itemVariable, formatType);
  }
}
