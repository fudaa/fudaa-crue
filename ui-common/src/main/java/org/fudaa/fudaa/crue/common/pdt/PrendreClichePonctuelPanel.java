/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.pdt;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuGridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.dodico.crue.metier.emh.PrendreClichePonctuel;
import org.openide.util.NbBundle;

/**
 *
 * @author Frederic Deniger
 */
public class PrendreClichePonctuelPanel extends JPanel {

  final JTextField tfNom;
  final DurationPanelEditor pdtCstEditor;

  public PrendreClichePonctuelPanel() {
    setLayout(new BuGridLayout(2, 5, 5));
    setBorder(BuBorders.EMPTY3333);
    add(new JLabel(NbBundle.getMessage(PrendreClichePonctuelPanel.class, "PrendreCliche.NomFichier")));
    tfNom = new JTextField(20);
    add(tfNom);
    pdtCstEditor = new DurationPanelEditor();
    add(new JLabel(NbBundle.getMessage(PrendreClichePonctuelPanel.class, "PrendreCliche.PdtRes")));
    add(pdtCstEditor);

  }

  PrendreClichePonctuel getCliche() {
    String nom = tfNom.getText();
    if (StringUtils.isBlank(nom)) {
      return null;
    }
    return new PrendreClichePonctuel(nom, pdtCstEditor.getDuration());
  }

  void setCliche(PrendreClichePonctuel prendreClichePeriodique) {
    if (prendreClichePeriodique == null) {
      tfNom.setText(StringUtils.EMPTY);
      pdtCstEditor.setPdt(null);
    } else {
      tfNom.setText(prendreClichePeriodique.getNomFic());
      pdtCstEditor.setPdt(prendreClichePeriodique.getTempsSimu());
    }
  }

  void setEditable(boolean canWrite) {
    tfNom.setEditable(canWrite);
    pdtCstEditor.setEditable(canWrite);
  }
}
