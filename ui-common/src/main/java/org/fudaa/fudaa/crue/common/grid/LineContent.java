/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.grid;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Frederic Deniger
 */
public class LineContent {

  final List<ColumnContent> components = new ArrayList<>();

  public List<ColumnContent> getComponents() {
    return components;
  }

  public void setEnabled(boolean b) {
    for (ColumnContent t : components) {
      t.setEnabled(b);
    }
  }
}
