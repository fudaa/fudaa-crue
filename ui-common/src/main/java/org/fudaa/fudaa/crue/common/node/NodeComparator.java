package org.fudaa.fudaa.crue.common.node;

import java.util.Comparator;
import org.apache.commons.lang3.StringUtils;
import org.openide.nodes.Node;

/**
 *
 * @author Fred Deniger
 */
public class NodeComparator implements Comparator<Node> {
  
  public static final NodeComparator INSTANCE=new NodeComparator();

  @Override
  public int compare(Node o1, Node o2) {
    if(o1==o2){
      return 0;
    }
    if(o1==null){
      return -1;
    }
    if(o2==null){
      return 1;
    }
    return StringUtils.defaultString(o1.getName()).compareTo(o2.getName());
  }
  
  
  
  
  
}
