/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.pdt;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.DateDurationConverter;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetierConstants;
import org.fudaa.dodico.crue.metier.emh.ElemPdt;
import org.fudaa.dodico.crue.metier.emh.PdtVar;
import org.fudaa.fudaa.crue.common.view.DefaultOutlineViewEditor;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

/**
 *
 * @author Frédéric Deniger
 */
public class PdtVarPanelEditor extends DefaultOutlineViewEditor {

  final CrueConfigMetier ccm;

  PdtVar getPdtVar() {
    Children children = getExplorerManager().getRootContext().getChildren();
    Node[] nodes = children.getNodes();
    PdtVar res = new PdtVar();
    for (Node node : nodes) {
      ElemPdtNode elemNode = (ElemPdtNode) node;
      res.addElemPdt(elemNode.getLookup().lookup(ElemPdt.class));
    }
    return res;

  }

  public boolean valide() {
    Children children = getExplorerManager().getRootContext().getChildren();
    Node[] nodes = children.getNodes();
    if (nodes.length == 0) {
      return false;
    }
    boolean valide = true;
    for (Node node : nodes) {
      CtuluLog log = ((ElemPdtNode) node).validate();
      if (log.containsErrorOrSevereError()) {
        valide = false;
      }
    }
    return valide;
  }

  public PdtVarPanelEditor(CrueConfigMetier ccm) {
    this.ccm = ccm;
  }

  @Override
  protected void addElement() {
    ElemPdt elem = new ElemPdt(ccm);
    elem.setDureePdt(DateDurationConverter.getDuration(0, 1, 0, 0));
    Children children = getExplorerManager().getRootContext().getChildren();
    final ElemPdtNode elemPdtNode = new ElemPdtNode(elem, ccm);
    elemPdtNode.setEditable(editable);
    children.add(new Node[]{elemPdtNode});
  }

  @Override
  protected void createOutlineView() {
    view = new OutlineView(StringUtils.EMPTY);
    view.setPropertyColumns(CrueConfigMetierConstants.PROP_NBR_PDT, BusinessMessages.getString("nbrPdt.property"), CrueConfigMetierConstants.PROP_DUREE_PDT, BusinessMessages.getString("dureePdt.property"));
    view.getOutline().getColumnModel().getColumn(0).setWidth(35);
    view.getOutline().getColumnModel().getColumn(0).setMaxWidth(45);
    view.getOutline().getColumnModel().getColumn(0).setPreferredWidth(35);
    view.getOutline().setColumnHidingAllowed(false);
    view.getOutline().setRootVisible(false);
    view.setDragSource(true);
    view.setDropTarget(true);
  }

  public void init(PdtVar pdtVar) {
    List<ElemPdt> elemPdt = pdtVar.getElemPdt();
    Node[] res = new Node[elemPdt.size()];
    for (int i = 0; i < res.length; i++) {
      res[i] = new ElemPdtNode(elemPdt.get(i).clone(), ccm);
      ((ElemPdtNode) res[i]).setEditable(editable);
    }
    Children.Array children = new Children.Array();
    children.add(res);
    getExplorerManager().setRootContext(new AbstractNode(children));
  }

  @Override
  public void setEditable(boolean editable) {
    super.setEditable(editable);
    Children children = getExplorerManager().getRootContext().getChildren();
    Node[] nodes = children.getNodes();
    for (Node node : nodes) {
      ((ElemPdtNode) node).setEditable(editable);
    }
  }
}
