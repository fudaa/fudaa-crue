package org.fudaa.fudaa.crue.common.log;

import org.openide.nodes.FilterNode;

/**
 *
 * @author deniger
 */
public interface CtuluLogRecordContainer {
  
  
   FilterNode createOnlyErrorNode() ;
  
}
