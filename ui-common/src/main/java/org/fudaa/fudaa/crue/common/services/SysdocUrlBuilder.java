package org.fudaa.fudaa.crue.common.services;

import org.fudaa.dodico.crue.metier.emh.*;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHContainerBase;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.openide.DialogDescriptor;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Permet de construire les URL pour les différents EMH, DCLM,...
 *
 * @author Frederic Deniger
 */
public class SysdocUrlBuilder {
  private static final String PERSPECTIVE_PREFIX = "perspectives_";
  private static final String EMH_PREFIX = "notions_EMH_";
  private static final String INFOS_PREFIX = "notions_Infos_";
  private static final String ETUDE_PREFIX = "notions_Etude_";
  private static final KeyStroke KS_HELP = KeyStroke.getKeyStroke("F1");
  private static final Set<Class> CLASSES = new HashSet<>(Arrays.asList(Calc.class, OrdCalc.class, DonCLimM.class, DonPrtCIni.class,
      ElemBarrage.class));

  public static String getTopComponentHelpCtxId(String viewHelpId, PerspectiveEnum perspective) {
    return PERSPECTIVE_PREFIX + perspective.getHelpId() + "_vues_" + viewHelpId;
  }

  public static String getDialogHelpCtxId(String bdlId, PerspectiveEnum perspective) {
    if (perspective == null) {
      return PERSPECTIVE_PREFIX + "dialogues_" + bdlId;
    }
    return PERSPECTIVE_PREFIX + perspective.getHelpId() + "_dialogues_" + bdlId;
  }

  public static void installDialogHelpCtx(DialogDescriptor descriptor, String bdlId, PerspectiveEnum perspective, boolean bDisplayHelpButton) {
    final String id = getDialogHelpCtxId(bdlId, perspective);
    final HelpCtx helpCtx = new HelpCtx(id);
    if (bDisplayHelpButton) {
      descriptor.setHelpCtx(helpCtx);
    } else {
      // forcer à null le context d'aide pour ne pas afficher le bouton
      descriptor.setHelpCtx(null);
    }
    Object message = descriptor.getMessage();
    if (message instanceof JComponent) {
      installHelpShortcut((JComponent) message, id);
    }
  }

  public static String getPerspectiveHelpCtxId(PerspectiveEnum perspective) {
    return PERSPECTIVE_PREFIX + perspective.getHelpId() + "_index";
  }

  public static String getEMHHelpCtxId(EMH emh) {
    if (emh == null) {
      return HelpCtx.DEFAULT_HELP.getHelpID();
    }
    if (emh.getCatType().isContainer()) {
      return EMH_PREFIX + emh.getType();
    }
    return getHelpCtxId(emh.getCatType(), emh.getType());
  }

  public static String getHelpCtxId(EnumCatEMH catType, String type) {
    return addSignet(EMH_PREFIX + catType.getI18nKey(), type);
  }

  public static String getEMHHelpCtxId(ManagerEMHContainerBase manager) {

    return EMH_PREFIX + manager.getEMHType();
  }

  public static String getStudyCtxId() {
    return ETUDE_PREFIX + "index";
  }

  public static String getStudyFilesCtxId() {
    return ETUDE_PREFIX + "fichiers";
  }

  public static String getEMHHelpCtxId(Enum emhType) {
    return EMH_PREFIX + emhType.name();
  }

  public static void installHelpShortcut(JComponent jc, final String id) {
    jc.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KS_HELP, KS_HELP);
    jc.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KS_HELP, KS_HELP);
    jc.getActionMap().put(KS_HELP, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        SysdocContrat sysdocContrat = Lookup.getDefault().lookup(SysdocContrat.class);
        sysdocContrat.display(id);
      }
    });
  }

  public static JButton createButtonHelp() {
    JButton bt = new JButton();
    bt.setIcon(ImageUtilities.loadImageIcon("org/openide/resources/propertysheet/propertySheetHelp.png", false));
    return bt;
  }

  public static JButton createButtonHelpDialog(final PerspectiveEnum perspective, final String bdlId) {
    JButton bt = createButtonHelp();
    final String helpId = getDialogHelpCtxId(bdlId, perspective);
    bt.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        SysdocContrat sysdocContrat = Lookup.getDefault().lookup(SysdocContrat.class);
        sysdocContrat.display(helpId);
      }
    });
    installHelpShortcut(bt, helpId);
    return bt;
  }

  public static JButton createButtonHelpNotionDialog(final String bdlId) {
    JButton bt = createButtonHelp();
    bt.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        SysdocContrat sysdocContrat = Lookup.getDefault().lookup(SysdocContrat.class);
        sysdocContrat.display("notions_" + bdlId);
      }
    });
    return bt;
  }

  public static String getHelpInfosCtxId(Object infoObject) {
    if (infoObject == null) {
      return HelpCtx.DEFAULT_HELP.getHelpID();
    }
    final Class<?> infoClass = infoObject.getClass();
    return geHelpInfosCtxId(infoClass);
  }

  public static String geHelpInfosCtxId(Class<?> infoClass) {
    String url = infoClass.getSimpleName();
    String signet = null;
    for (Class parentClass : CLASSES) {
      if (parentClass.isAssignableFrom(infoClass)) {
        signet = url;
        url = parentClass.getSimpleName();
        break;
      }
    }
    if (ValParam.class.isAssignableFrom(infoClass)) {
      url = ValParam.class.getSimpleName();
    }
    if (PlanimetrageNbrPdzCst.class.equals(infoClass)) {
      signet = url;
      url = OrdPrtGeoModeleBase.class.getSimpleName();
    }
    return addSignet(INFOS_PREFIX + url, signet);
  }

  public static String addSignet(String url, String signet) {
    if (signet != null) {
      return url + "_" + signet;
    }
    return url;
  }
}
