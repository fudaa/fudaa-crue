package org.fudaa.fudaa.crue.common.helper;

import org.openide.filesystems.FileChooserBuilder;
import org.openide.filesystems.FileUtil;
import org.openide.util.NbPreferences;
import org.openide.util.Parameters;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileSystemView;
import javax.swing.filechooser.FileView;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Rerpise de FileChooserBuilder de Netbeans afin de corriger un bug sur le selectionApprover.
 *
 * @author deniger
 */
public class CrueFileChooserBuilder {
    //Just in case...
    private final static boolean PREVENT_SYMLINK_TRAVERSAL =
            !Boolean.getBoolean("allow.filechooser.symlink.traversal"); //NOI18N
    private static final boolean DONT_STORE_DIRECTORIES =
            Boolean.getBoolean("forget.recent.dirs");
    private final String dirKey;
    private final List<FileFilter> filters = new ArrayList<>(3);
    private boolean dirsOnly;
    private BadgeProvider badger;
    private String title;
    private String approveText;
    private File failoverDir;
    private FileFilter filter;
    private boolean fileHiding;
    private boolean controlButtonsShown = true;
    private String aDescription;
    private boolean filesOnly;
    private SelectionApprover approver;
    private boolean force = false;

    /**
     * Create a new FileChooserBuilder using the name of the passed class as the metadata for looking up a starting directory from previous application
     * sessions or invocations.
     *
     * @param type A non-null class object, typically the calling class
     */
    public CrueFileChooserBuilder(Class type) {
        this(type.getName());
    }

    /**
     * Create a new FileChooserBuilder. The passed key is used as a key into NbPreferences to look up the directory the file chooser should initially be
     * rooted on.
     *
     * @param dirKey A non-null ad-hoc string. If a FileChooser was previously used with the same string as is passed, then the initial directory
     */
    public CrueFileChooserBuilder(String dirKey) {
        Parameters.notNull("dirKey", dirKey);
        this.dirKey = dirKey;
    }

    /**
     * Set whether or not any file choosers created by this builder will show only directories.
     *
     * @param val true if files should not be shown
     * @return this
     */
    public CrueFileChooserBuilder setDirectoriesOnly(boolean val) {
        dirsOnly = val;
        assert !filesOnly : "FilesOnly and DirsOnly are mutually exclusive";
        return this;
    }

    public CrueFileChooserBuilder setFilesOnly(boolean val) {
        filesOnly = val;
        assert !dirsOnly : "FilesOnly and DirsOnly are mutually exclusive";
        return this;
    }

    /**
     * Provide an implementation of BadgeProvider which will "badge" the icons of some files.
     *
     * @param provider A badge provider which will alter the icon of files or folders that may be of particular interest to the user
     * @return this
     */
    public CrueFileChooserBuilder setBadgeProvider(BadgeProvider provider) {
        this.badger = provider;
        return this;
    }

    /**
     * Set the dialog title for any JFileChoosers created by this builder.
     *
     * @param val A localized, human-readable title
     * @return this
     */
    public CrueFileChooserBuilder setTitle(String val) {
        title = val;
        return this;
    }

    /**
     * Set the text on the OK button for any file chooser dialogs produced by this builder.
     *
     * @param val A short, localized, human-readable string
     * @return this
     */
    public CrueFileChooserBuilder setApproveText(String val) {
        approveText = val;
        return this;
    }

    /**
     * @param filter le nouveau {@link FileFilter}
     * @return this
     */
    public CrueFileChooserBuilder setFileFilter(FileFilter filter) {
        this.filter = filter;
        return this;
    }

    /**
     * Set the current directory which should be used <b>only if</b> a last-used directory cannot be found for the key string passed into this builder's
     * constructor.
     *
     * @param dir A directory to root any created file choosers on if there is no stored path for this builder's key
     * @return this
     */
    public CrueFileChooserBuilder setDefaultWorkingDirectory(File dir) {
        failoverDir = dir;
        return this;
    }

    /**
     * Enable file hiding in any created file choosers
     *
     * @param fileHiding Whether or not to hide files. Default is no.
     * @return this
     */
    public CrueFileChooserBuilder setFileHiding(boolean fileHiding) {
        this.fileHiding = fileHiding;
        return this;
    }

    /**
     * Show/hide control buttons
     *
     * @param val Whether or not to hide files. Default is no.
     * @return this
     */
    public CrueFileChooserBuilder setControlButtonsAreShown(boolean val) {
        this.controlButtonsShown = val;
        return this;
    }

    /**
     * Set the accessible description for any file choosers created by this builder
     *
     * @param aDescription The description
     * @return this
     */
    public CrueFileChooserBuilder setAccessibleDescription(String aDescription) {
        this.aDescription = aDescription;
        return this;
    }

    /**
     * Create a JFileChooser that conforms to the parameters set in this builder.
     *
     * @return A file chooser
     */
    private JFileChooser createFileChooser() {
        JFileChooser result = new SavedDirFileChooser(dirKey, failoverDir,
                force, approver);
        prepareFileChooser(result);
        return result;
    }

    /**
     * @param useDefault true si on doit utiliser le repertoire par defaut
     * @return this
     */
    public CrueFileChooserBuilder forceUseOfDefaultWorkingDirectory(boolean useDefault) {
        this.force = useDefault;
        return this;
    }

    /**
     * Tries to find an appropriate component to parent the file chooser to when showing a dialog.
     *
     * @return this
     */
    private Component findDialogParent() {
        Component parent = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (parent == null) {
            parent = KeyboardFocusManager.getCurrentKeyboardFocusManager().getActiveWindow();
        }
        if (parent == null) {
            Frame[] f = Frame.getFrames();
            parent = f.length == 0 ? null : f[f.length - 1];
        }
        return parent;
    }

    /**
     * Show an open dialog that allows multiple selection.
     *
     * @return An array of files, or null if the user cancelled the dialog
     */
    public File[] showMultiOpenDialog() {
        JFileChooser chooser = createFileChooser();
        chooser.setMultiSelectionEnabled(true);
        int result = chooser.showOpenDialog(findDialogParent());
        if (JFileChooser.APPROVE_OPTION == result) {
            File[] files = chooser.getSelectedFiles();
            return files;
        } else {
            return null;
        }
    }

    /**
     * Show an open dialog with a file chooser set up according to the parameters of this builder.
     *
     * @return A file if the user clicks the accept button and a file or folder was selected at the time the user clicked cancel.
     */
    public File showOpenDialog() {
        JFileChooser chooser = createFileChooser();
        chooser.setMultiSelectionEnabled(false);
        int dlgResult = chooser.showOpenDialog(findDialogParent());
        if (JFileChooser.APPROVE_OPTION == dlgResult) {
            File result = chooser.getSelectedFile();
            if (result != null && !result.exists()) {
                result = null;
            }
            return result;
        } else {
            return null;
        }
    }

    /**
     * Show a save dialog with the file chooser set up according to the parameters of this builder.
     *
     * @return A file if the user clicks the accept button and a file or folder was selected at the time the user clicked cancel.
     */
    public File showSaveDialog() {
        JFileChooser chooser = createFileChooser();
        int result = chooser.showSaveDialog(findDialogParent());
        if (JFileChooser.APPROVE_OPTION == result) {
            return chooser.getSelectedFile();
        } else {
            return null;
        }
    }

    private void prepareFileChooser(JFileChooser chooser) {
        chooser.setFileSelectionMode(dirsOnly ? JFileChooser.DIRECTORIES_ONLY
                : filesOnly ? JFileChooser.FILES_ONLY
                : JFileChooser.FILES_AND_DIRECTORIES);
        chooser.setFileHidingEnabled(fileHiding);
        chooser.setControlButtonsAreShown(controlButtonsShown);
        if (title != null) {
            chooser.setDialogTitle(title);
        }
        if (approveText != null) {
            chooser.setApproveButtonText(approveText);
        }
        if (badger != null) {
            chooser.setFileView(new CustomFileView(new BadgeIconProvider(badger),
                    chooser.getFileSystemView()));
        }
        if (PREVENT_SYMLINK_TRAVERSAL) {
            FileUtil.preventFileChooserSymlinkTraversal(chooser,
                    chooser.getCurrentDirectory());
        }
        if (filter != null) {
            chooser.setFileFilter(filter);
        }
        if (aDescription != null) {
            chooser.getAccessibleContext().setAccessibleDescription(aDescription);
        }
        if (!filters.isEmpty()) {
            for (FileFilter f : filters) {
                chooser.addChoosableFileFilter(f);
            }
        }
    }

    /**
     * Equivalent to calling
     * <code>JFileChooser.addChoosableFileFilter(filter)</code>. Adds another file filter that can be displayed in the file filters combo box in the
     * file chooser.
     *
     * @param filter The file filter to add
     * @return this
     * @since 7.26.0
     */
    public CrueFileChooserBuilder addFileFilter(FileFilter filter) {
        filters.add(filter);
        return this;
    }

    /**
     * Set a selection approver which can display an &quot;Overwrite file?&quot; or similar dialog if necessary, when the user presses the accept button
     * in the file chooser dialog.
     *
     * @param approver A SelectionApprover which will determine if the selection is valid
     * @return this
     * @since 7.26.0
     */
    public CrueFileChooserBuilder setSelectionApprover(SelectionApprover approver) {
        this.approver = approver;
        return this;
    }

    /**
     * Object which can approve the selection (enabling the OK button or equivalent) in a JFileChooser. Equivalent to overriding
     * <code>JFileChooser.approveSelection()</code>
     *
     * @since 7.26.0
     */
    public interface SelectionApprover {
        /**
         * Approve the selection, enabling the dialog to be closed. Called by the JFileChooser's
         * <code>approveSelection()</code> method. Use this interface if you want to, for example, show a dialog asking &quot;Overwrite File X?&quot; or
         * similar.
         *
         * @param selection The selected file(s) at the time the user presses the Open, Save or OK button
         * @return true if the selection is accepted, false if it is not and the dialog should not be closed Genesis call if multiple files selected:
         */
        boolean approve(File[] selection);

        boolean approve(File selection);
    }

    //Can open this API later if there is a use-case
    interface IconProvider {
        Icon getIcon(File file, Icon orig);
    }

    /**
     * Provides "badges" for icons that indicate files or folders of particular interest to the user.
     *
     * @see FileChooserBuilder#setBadgeProvider
     */
    public interface BadgeProvider {
        /**
         * Get the badge the passed file should use. <b>Note:</b> this method is called for every visible file. The negative test (deciding <i>not</i> to
         * badge a file) should be very, very fast and immediately return null.
         *
         * @param file The file in question
         * @return an icon or null if no change to the appearance of the file is needed
         */
        Icon getBadge(File file);

        /**
         * Get the x offset for badges produced by this provider. This is the location of the badge icon relative to the real icon for the file.
         *
         * @return a rightward pixel offset
         */
        int getXOffset();

        /**
         * Get the y offset for badges produced by this provider. This is the location of the badge icon relative to the real icon for the file.
         *
         * @return a downward pixel offset
         */
        int getYOffset();
    }

    private static final class SavedDirFileChooser extends JFileChooser {
        private final String dirKey;
        private final SelectionApprover approver;

        SavedDirFileChooser(String dirKey, File failoverDir, boolean force, SelectionApprover approver) {
            this.dirKey = dirKey;
            this.approver = approver;
            if (force && failoverDir != null && failoverDir.exists() && failoverDir.isDirectory()) {
                setCurrentDirectory(failoverDir);
            } else {
                String path = DONT_STORE_DIRECTORIES ? null
                        : NbPreferences.forModule(FileChooserBuilder.class).get(dirKey, null);
                if (path != null) {
                    File f = new File(path);
                    if (f.exists() && f.isDirectory()) {
                        setCurrentDirectory(f);
                    } else if (failoverDir != null) {
                        setCurrentDirectory(failoverDir);
                    }
                } else if (failoverDir != null) {
                    setCurrentDirectory(failoverDir);
                }
            }
        }

        @Override
        public void approveSelection() {
            if (approver != null) {
                boolean approved = false;
                if (isMultiSelectionEnabled()) {
                    approved = approver.approve(getSelectedFiles());
                } else {
                    approved = approver.approve(getSelectedFile());
                }
                if (approved) {
                    super.approveSelection();
                }
            } else {
                super.approveSelection();
            }
        }

        @Override
        public int showOpenDialog(Component parent) throws HeadlessException {
            int result = super.showOpenDialog(parent);
            if (result == APPROVE_OPTION) {
                saveCurrentDir();
            }
            return result;
        }

        @Override
        public int showSaveDialog(Component parent) throws HeadlessException {
            int result = super.showSaveDialog(parent);
            if (result == APPROVE_OPTION) {
                saveCurrentDir();
            }
            return result;
        }

        private void saveCurrentDir() {
            File dir = super.getCurrentDirectory();
            if (!DONT_STORE_DIRECTORIES && dir != null && dir.exists() && dir.isDirectory()) {
                NbPreferences.forModule(FileChooserBuilder.class).put(dirKey, dir.getPath());
            }
        }
    }

    private static final class BadgeIconProvider implements IconProvider {
        private final BadgeProvider badger;

        public BadgeIconProvider(BadgeProvider badger) {
            this.badger = badger;
        }

        @Override
        public Icon getIcon(File file, Icon orig) {
            Icon badge = badger.getBadge(file);
            if (badge != null && orig != null) {
                return new MergedIcon(orig, badge, badger.getXOffset(),
                        badger.getYOffset());
            }
            return orig;
        }
    }

    private static final class CustomFileView extends FileView {
        private final IconProvider provider;
        private final FileSystemView view;

        CustomFileView(IconProvider provider, FileSystemView view) {
            this.provider = provider;
            this.view = view;
        }

        @Override
        public Icon getIcon(File f) {
            Icon result = view.getSystemIcon(f);
            result = provider.getIcon(f, result);
            return result;
        }
    }

    private static class MergedIcon implements Icon {
        private Icon icon1;
        private Icon icon2;
        private int xMerge;
        private int yMerge;

        MergedIcon(Icon icon1, Icon icon2, int xMerge, int yMerge) {
            assert icon1 != null;
            assert icon2 != null;
            this.icon1 = icon1;
            this.icon2 = icon2;

            if (xMerge == -1) {
                xMerge = icon1.getIconWidth() - icon2.getIconWidth();
            }

            if (yMerge == -1) {
                yMerge = icon1.getIconHeight() - icon2.getIconHeight();
            }

            this.xMerge = xMerge;
            this.yMerge = yMerge;
        }

        @Override
        public int getIconHeight() {
            return Math.max(icon1.getIconHeight(), yMerge + icon2.getIconHeight());
        }

        @Override
        public int getIconWidth() {
            return Math.max(icon1.getIconWidth(), yMerge + icon2.getIconWidth());
        }

        @Override
        public void paintIcon(java.awt.Component c, java.awt.Graphics g, int x, int y) {
            icon1.paintIcon(c, g, x, y);
            icon2.paintIcon(c, g, x + xMerge, y + yMerge);
        }
    }
}
