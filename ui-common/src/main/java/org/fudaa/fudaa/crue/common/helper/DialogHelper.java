package org.fudaa.fudaa.crue.common.helper;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.action.ActionsInstaller;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;
import org.fudaa.fudaa.crue.common.UserPreferencesSaver;
import org.fudaa.fudaa.crue.common.services.SysdocUrlBuilder;
import org.jdesktop.swingx.JXTable;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.NotificationDisplayer;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.view.OutlineView;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.*;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * @author deniger
 */
public class DialogHelper {
  public static final String NO_VERSION_FOR_PERSISTENCE = StringUtils.EMPTY;

  public static <T extends JComponent> List<T> findComponent(JComponent parent, Class<T> type) {
    List<T> res = new ArrayList<>();
    findComponent(parent, type, res);
    return res;
  }

  private static <T extends JComponent> void findComponent(JComponent currentComponent, Class<T> type, List<T> target) {
    if (currentComponent instanceof JScrollPane) {
      findComponent((JComponent) ((JScrollPane) currentComponent).getViewport().getView(), type, target);
      return;
    }

    if (type.isAssignableFrom(currentComponent.getClass())) {
      target.add((T) currentComponent);
      return;
    }
    Component[] components = currentComponent.getComponents();
    if (components == null) {
      return;
    }
    for (Component component : components) {
      if (component instanceof JComponent) {
        findComponent((JComponent) component, type, target);
      }
    }
  }

  /**
   * Création d'un JDialog avec bouton "Fermer" à partir d'un component sans JXTable
   *
   * @param cpn                   le component qui sera le body de la fenêtre
   * @param dialogName            nom de la fenêtre
   * @param dialogTitle           titre de la fenêtre
   * @param createLayout          true s'il faut créer un layout de redimensionnement, false sinon
   * @param versionForPersistence utilise pour versionner les préférences et prendre en charge les modifications des contenus.
   * @return la JDialog construite, avec ses taille et position chargées si elles ont été précédemment mémorisées
   */
  public static JDialog createCloseDialog(final Component cpn, final String dialogName, final String dialogTitle, boolean createLayout,
                                          String versionForPersistence) {
    return createDialog(cpn, null, dialogName, dialogTitle, JOptionPane.CLOSED_OPTION, DialogDescriptor.CLOSED_OPTION, createLayout, null,
        versionForPersistence);
  }

  /**
   * Création d'un JDialog à partir d'un panel contenant un component
   *
   * @param cpn                   le component qui sera le body de la fenêtre
   * @param dialogName            nom de la fenêtre
   * @param dialogTitle           titre de la fenêtre
   * @param createLayout          true s'il faut créer un layout de redimensionnement, false sinon
   * @param env                   l'environnement qu'il faut valider si clic sur le bouton OK
   * @param versionForPersistence utilise pour versionner les préférences et prendre en charge les modifications des contenus.
   * @return la JDialog construite, avec ses taille et position chargées si elles ont été précédemment mémorisées
   */
  public static JDialog createOKCancelDialog(final Component cpn, final String dialogName, final String dialogTitle, boolean createLayout,
                                             PropertyEnv env, String versionForPersistence) {
    return createDialog(cpn, null, dialogName, dialogTitle, DialogDescriptor.OK_CANCEL_OPTION, DialogDescriptor.OK_OPTION, createLayout, env,
        versionForPersistence);
  }

  /**
   * Création d'un JDialog à partir d'un panel contenant une JXTable
   *
   * @param cpn                   le component qui sera le body de la fenêtre
   * @param table                 la table contenue dans le component
   * @param dialogName            nom de la fenêtre
   * @param dialogTitle           titre de la fenêtre
   * @param createLayout          true s'il faut créer un layout de redimensionnement, false sinon
   * @param versionForPersistence utilise pour versionner les préférences et prendre en charge les modifications des contenus.
   * @return la JDialog construite, avec ses taille et position chargées si elles ont été précédemment mémorisées
   */
  public static JDialog createCloseDialog(final Component cpn, final JXTable table, final String dialogName, final String dialogTitle,
                                          boolean createLayout, String versionForPersistence) {
    return createDialog(cpn, table, dialogName, dialogTitle, JOptionPane.CLOSED_OPTION, DialogDescriptor.CLOSED_OPTION, createLayout, null,
        versionForPersistence);
  }

  /**
   * Création d'un JDialog à partir d'un panel contenant une JXTable
   *
   * @param cpn                   le component qui sera le body de la fenêtre
   * @param table                 la table contenue dans le component
   * @param dialogName            nom de la fenêtre
   * @param dialogTitle           titre de la fenêtre
   * @param optionType            type de bouton(s) affiché(s)
   * @param initialValue          bouton sélectionner par défaut
   * @param createLayout          true s'il faut créer un layout de redimensionnement, false sinon
   * @param env                   l'environnement qui faut valider si clic sur le bouton OK
   * @param versionForPersistence utilise pour versionner les préférences et prendre en charge les modifications des contenus.
   * @return la JDialog construite, avec ses taille et position chargées si elles ont été précédemment mémorisées
   */
  private static JDialog
  createDialog(final Component cpn, final JXTable table, final String dialogName, final String dialogTitle, int optionType,
               Object initialValue, boolean createLayout, final PropertyEnv env, final String versionForPersistence) {
    Component finalComp;

    if (createLayout) {
      JPanel pn = createContainerPanel(cpn);
      finalComp = pn;
    } else {
      finalComp = cpn;
    }

    final DialogDescriptor dialDesc = new DialogDescriptor(
        finalComp,
        dialogTitle,
        true,
        optionType,
        initialValue,
        null);

    // forcer le bouton Fermer seul, car sinon, c'est 2 boutons Oui/Non qui apparaissent
    if (initialValue == DialogDescriptor.CLOSED_OPTION) {
      dialDesc.setOptions(new Object[]{initialValue});
    }

    final JDialog jd = (JDialog) DialogDisplayer.getDefault().createDialog(dialDesc);
    jd.setName(dialogName);

    // lecture asynchrone des taille et position + taille colonnes tableau
    EventQueue.invokeLater(() -> {
      UserPreferencesSaver.loadDialogLocationAndDimension(jd, versionForPersistence);
      if (table != null) {
        loadTablePreferencesLater(table);
      }
    });

    // sauvegarde des taille et position à la fermeture + taille tableau
    jd.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(WindowEvent e) {
        UserPreferencesSaver.saveLocationAndDimension(jd, versionForPersistence);
        if (table != null) {
          UserPreferencesSaver.saveTablePreferences(table);
        }

        if (dialDesc != null && dialDesc.getValue() == DialogDescriptor.OK_OPTION && env != null) {
          // si bouton ok, on bascule le state à VALID pour que l'éditeur de propriété prenne en compte la modif
          env.setState(PropertyEnv.STATE_VALID);
        }
      }
    });

    return jd;
  }

  public static JDialog createDialog(final DialogDescriptor dialDesc, final String dialogName, final PropertyEnv env, final String versionForPersistence) {

    final JDialog jd = (JDialog) DialogDisplayer.getDefault().createDialog(dialDesc);
    jd.pack();
    jd.setName(dialogName);

    // lecture asynchrone des taille et position + taille colonnes tableau
    EventQueue.invokeLater(() -> {
      UserPreferencesSaver.loadDialogLocationAndDimension(jd, versionForPersistence);
    });

    // sauvegarde des taille et position à la fermeture + taille tableau
    jd.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(WindowEvent e) {
        UserPreferencesSaver.saveLocationAndDimension(jd, versionForPersistence);

        if (dialDesc != null && dialDesc.getValue() == DialogDescriptor.OK_OPTION && env != null) {
          // si bouton ok, on bascule le state à VALID pour que l'éditeur de propriété prenne en compte la modif
          env.setState(PropertyEnv.STATE_VALID);
        }
      }
    });

    return jd;
  }

  /**
   * @param component le composant
   * @return un JPanel contenant cpn au centre
   */
  private static JPanel createContainerPanel(final Component component) {
    // création d'un layout pour le redimensionnement
    JPanel pn = new JPanel();
    pn.setLayout(new BorderLayout());
    pn.add(component, BorderLayout.CENTER);
    return pn;
  }

  /**
   * @param pn                    la panel a afficher
   * @param dialogOption          voir les options de CtuluDialog pour le dialog
   * @param table                 la table contenue
   * @param dialogName            le nom du dialog à utiliser pour la persisence des dimensions et position
   * @param dialogTitle           titre du dialogue
   * @param versionForPersistence la version de la persistence. A modifier si le contenu change.
   */
  public static void showDialogAndTable(final CtuluDialogPanel pn, final int dialogOption,
                                        final JXTable table, final String dialogName, final String dialogTitle, final String versionForPersistence) {

    final CtuluDialog s = pn.createDialog(WindowManager.getDefault().getMainWindow());
    s.setInitParent(WindowManager.getDefault().getMainWindow());
    s.setOption(dialogOption);
    s.setTitle(dialogTitle);
    s.setName(dialogName);

    s.afficheDialogModal(() -> {
      UserPreferencesSaver.loadDialogLocationAndDimension(s, versionForPersistence);
      loadTablePreferencesLater(table);
    });
    UserPreferencesSaver.saveLocationAndDimension(s, versionForPersistence);
    UserPreferencesSaver.saveTablePreferences(table);
  }

  public static void writeProperties(OutlineView view, String prefix, java.util.Properties p) {
    p.setProperty(prefix, "true");
    view.getOutline().writeSettings(p, prefix + ".");
  }

  public static void writeInPreferences(OutlineView view, String prefix, Class classForPreferences) {
    writeInPreferences(view, prefix, classForPreferences, "true");
  }

  public static void writeInPreferences(OutlineView view, String prefix, Class classForPreferences, String key) {
    Preferences pref = NbPreferences.forModule(classForPreferences);
    Properties prop = new Properties();
    String completePrefix = classForPreferences.getCanonicalName() + "." + prefix;
    prop.setProperty(completePrefix, key);
    view.getOutline().writeSettings(prop, completePrefix + ".");
    Set<Map.Entry<Object, Object>> entrySet = prop.entrySet();
    for (Map.Entry<Object, Object> entry : entrySet) {
      pref.put((String) entry.getKey(), (String) entry.getValue());
    }
  }

  public static void readInPreferences(OutlineView view, String prefix, Class classForPreferences) {
    readInPreferences(view, prefix, classForPreferences, "true");
  }

  public static void readInPreferences(OutlineView view, String prefix, Class classForPreferences, String key) {
    Preferences pref = NbPreferences.forModule(classForPreferences);
    String completePrefix = classForPreferences.getCanonicalName() + "." + prefix;
    try {
      Properties prop = new Properties();
      String preferencesPrefix = completePrefix;
      String[] childrenNames = pref.keys();
      for (String string : childrenNames) {
        if (string.startsWith(preferencesPrefix)) {
          prop.put(string, pref.get(string, null));
        }
      }
      if (key.equals(prop.getProperty(completePrefix))) {
        view.getOutline().readSettings(prop, completePrefix + ".");
      }
    } catch (BackingStoreException ex) {
      Exceptions.printStackTrace(ex);
    }
  }

  public static void readProperties(OutlineView view, String prefix, java.util.Properties p) {
    if ("true".equals(p.getProperty(prefix))) {
      view.getOutline().readSettings(p, prefix + ".");
    }
  }

  public static void showComponentError(Object msg, String panelInfo) {
    showError(org.openide.util.NbBundle.getMessage(DialogHelper.class, "Error"), msg, panelInfo);
  }

  public static void showError(String msg) {
    showError(org.openide.util.NbBundle.getMessage(DialogHelper.class, "Error"), msg);
  }

  public static void showError(String title, String msg) {
    showError(title, msg, msg);
  }

  public static void showError(String title, Object msg, String panelInfo) {
    NotifyDescriptor nd = new NotifyDescriptor.Message(msg, NotifyDescriptor.ERROR_MESSAGE);
    nd.setTitle(title);
    DialogDisplayer.getDefault().notify(nd);
    NotificationDisplayer.getDefault().notify(title, CrueIconsProvider.getIcon("org/fudaa/fudaa/crue/common/icons/erreur-bloquante.png"),
        panelInfo == null ? title : panelInfo,
        null, NotificationDisplayer.Priority.SILENT);
  }

  public static void showWarn(String title, String msg) {
    NotifyDescriptor nd = new NotifyDescriptor.Message(msg, NotifyDescriptor.WARNING_MESSAGE);
    nd.setTitle(title);
    DialogDisplayer.getDefault().notify(nd);
    NotificationDisplayer.getDefault().notify(title, CrueIconsProvider.getIcon("org/fudaa/fudaa/crue/common/icons/avertissement.png"), msg,
        null, NotificationDisplayer.Priority.SILENT);
  }

  public static void showWarn(String msg) {
    NotifyDescriptor nd = new NotifyDescriptor.Message(msg, NotifyDescriptor.WARNING_MESSAGE);
    nd.setTitle(org.openide.util.NbBundle.getMessage(DialogHelper.class, "Warn"));
    DialogDisplayer.getDefault().notify(nd);
    NotificationDisplayer.getDefault().notify(nd.getTitle(), CrueIconsProvider.getIcon("org/fudaa/fudaa/crue/common/icons/avertissement.png"), msg,
        null, NotificationDisplayer.Priority.SILENT);
  }

  public static void showInfo(String title, Object msg) {
    NotifyDescriptor nd = new NotifyDescriptor.Message(msg, NotifyDescriptor.INFORMATION_MESSAGE);
    nd.setTitle(title);
    DialogDisplayer.getDefault().notify(nd);
    if (msg instanceof String) {
      NotificationDisplayer.getDefault().notify(title, CrueIconsProvider.getIcon("org/fudaa/fudaa/crue/common/icons/information.png"),
          (String) msg, null, NotificationDisplayer.Priority.SILENT);
    }
  }

  public static void showInfo(final String title, final Object msg, boolean later) {
    if (later) {
      EventQueue.invokeLater(() -> showInfo(title, msg));
    } else {
      showInfo(title, msg);
    }
  }

  public static void show(String title, Object msg) {
    NotifyDescriptor nd = new NotifyDescriptor.Message(msg, NotifyDescriptor.PLAIN_MESSAGE);
    nd.setTitle(title);
    DialogDisplayer.getDefault().notify(nd);
  }

  public static boolean showQuestion(String msg) {
    NotifyDescriptor nd = new NotifyDescriptor.Confirmation(msg);
    nd.setOptions(new Object[]{NotifyDescriptor.YES_OPTION, NotifyDescriptor.NO_OPTION});
    return DialogDisplayer.getDefault().notify(nd).equals(NotifyDescriptor.YES_OPTION);
  }

  public static boolean showQuestion(String title, Object message) {
    NotifyDescriptor nd = new NotifyDescriptor.Confirmation(message, title);
    nd.setOptions(new Object[]{NotifyDescriptor.YES_OPTION, NotifyDescriptor.NO_OPTION});
    return DialogDisplayer.getDefault().notify(nd).equals(NotifyDescriptor.YES_OPTION);
  }

  public static boolean showQuestion(String title, Object message, String informationMessage) {
    NotifyDescriptor nd = new NotifyDescriptor.Confirmation(message, title);
    if (informationMessage != null) {
      nd.createNotificationLineSupport();
      nd.getNotificationLineSupport().setInformationMessage(informationMessage);
    }
    nd.setOptions(new Object[]{NotifyDescriptor.YES_OPTION, NotifyDescriptor.NO_OPTION});
    return DialogDisplayer.getDefault().notify(nd).equals(NotifyDescriptor.YES_OPTION);
  }

  /**
   * @param title       le titre du dialog.
   * @param message     une string pour le message un JComponent.
   * @param classToSave utiliser pour generer l'identifiant de persistance.
   * @param version     la version pour la persistence.
   * @return true si l'utilisateur a validé sa saisie
   */
  public static boolean showQuestionAndSaveDialogConf(String title, Object message, Class classToSave, String version) {
    return showQuestionAndSaveDialogConf(title, message, classToSave, null, null, false, version);
  }

  public static boolean showQuestionAndSaveDialogConf(String title, Object message, String idToSave) {
    DialogDescriptor descriptor = new DialogDescriptor(message, title);
    return showQuestion(descriptor, idToSave, false);
  }

  public static boolean showQuestion(DialogDescriptor dialogDescriptor, Class classToSave, String helpId, PerspectiveEnum perspective, String version) {
    if (helpId != null) {
      SysdocUrlBuilder.installDialogHelpCtx(dialogDescriptor, helpId, perspective, false);
    }
    return showQuestion(dialogDescriptor, classToSave, version);
  }

  public static boolean showQuestionAndSaveDialogConf(String title, Object message, Class classToSave, String helpId, PerspectiveEnum perspective,
                                                      boolean bDisplayHelpButton, String version) {
    DialogDescriptor descriptor = new DialogDescriptor(message, title);
    if (helpId != null) {
      SysdocUrlBuilder.installDialogHelpCtx(descriptor, helpId, perspective, bDisplayHelpButton);
    }
    return showQuestion(descriptor, classToSave, version);
  }

  /**
   * utiliser isCancelOption pour savoir si l'utilisateur a annulé l'action.
   *
   * @param title   titre du dialogue
   * @param message le message
   * @param options les options
   * @return WARN: si l'utilisateur appuie sur Echap la réponse est NotifyDescriptor.CLOSED_OPTION
   */
  public static Object showQuestion(String title, Object message, Object[] options) {
    NotifyDescriptor nd = new NotifyDescriptor.Confirmation(message, title);
    nd.setOptions(options);
    return DialogDisplayer.getDefault().notify(nd);
  }

  public static UserSaveAnswer confirmSaveOrNot() {
    return confirmSaveOrNot(
        NbBundle.getMessage(DialogHelper.class, "SaveDialog.Title"), NbBundle.getMessage(DialogHelper.class,
            "SaveDialog.Content"),
        NbBundle.getMessage(DialogHelper.class, "SaveDialog.SaveAction"),
        NbBundle.getMessage(DialogHelper.class, "SaveDialog.DontSaveAction"));
  }

  public static UserSaveAnswer confirmSaveOrNot(String title, Object message, String messageSave, String messageNotSave) {

    return confirmSaveOrNot(title, message, messageSave, messageNotSave, NbBundle.getMessage(DialogHelper.class,
        "dialog.cancel.label"));
  }

  public static UserSaveAnswer confirmSaveOrNot(String title, Object message, String messageSave, String messageNotSave,
                                                String cancelMessage) {
    assert !cancelMessage.equals(messageNotSave);
    Object[] values = new Object[]{messageSave, messageNotSave, cancelMessage};
    Object showQuestion = showQuestion(title, message, values);
    if (messageSave.equals(showQuestion)) {
      return UserSaveAnswer.SAVE;
    }
    if (messageNotSave.equals(showQuestion)) {
      return UserSaveAnswer.DONT_SAVE;
    }
    return UserSaveAnswer.CANCEL;
  }

  public static boolean showQuestionOkCancel(String title, Object msg) {
    NotifyDescriptor nd = new NotifyDescriptor.Confirmation(msg, title);
    nd.setOptions(new Object[]{NotifyDescriptor.OK_OPTION, NotifyDescriptor.CANCEL_OPTION});
    return NotifyDescriptor.OK_OPTION.equals(DialogDisplayer.getDefault().notify(nd));
  }

  public static boolean isCancelOption(Object in) {
    return NotifyDescriptor.CANCEL_OPTION.equals(in) || NotifyDescriptor.CLOSED_OPTION.equals(in);
  }

  public static boolean isYesOption(Object in) {
    return NotifyDescriptor.YES_OPTION.equals(in);
  }

  /**
   * @return NotifyDescriptor.YES_OPTION, NotifyDescriptor.NO_OPTION or NotifyDescriptor.CANCEL_OPTION
   */
  public static Object showQuestionCancel(String title, String msg) {
    NotifyDescriptor nd = new NotifyDescriptor.Confirmation(msg, title);
    nd.setOptions(new Object[]{NotifyDescriptor.YES_OPTION, NotifyDescriptor.NO_OPTION, NotifyDescriptor.CANCEL_OPTION});
    return DialogDisplayer.getDefault().notify(nd);
  }

  public static void showNotifyOperationTermine(String details) {
    showNotifyOperationTermine(NbBundle.getMessage(DialogHelper.class, "NotifyOperationSucceed"), details);
  }

  public static void showNotifyOperationTermine(String title, String details) {
    NotificationDisplayer.getDefault().notify(title, CrueIconsProvider.getIcon("org/fudaa/fudaa/crue/common/icons/ok.png"),
        details, null);
  }

  private static void loadTablePreferencesLater(final JXTable table) {
    EventQueue.invokeLater(() -> UserPreferencesSaver.loadTablePreferences(table));
  }

  /**
   * Affiche une dialogue attendant une saisie de l'utilisateur
   *
   * @param descriptor  le descriptor de la classe
   * @param classToSave la classe identifiant le dialog: utilise pour construire le canonicalName: identifiant unique du dialog
   * @param version     utilise pour versionner la persistence de la position/taille. Si le contenu du dialog a changer, il faut changer cette version pour
   *                    réinitialiser la taille.
   * @return true si l'utilisateur a validé sa saisie
   */
  public static boolean showQuestion(DialogDescriptor descriptor, Class classToSave, String version) {
    return showQuestion(descriptor, classToSave, version, false);
  }

  /**
   * Affiche une dialogue attendant une saisie de l'utilisateur
   *
   * @param descriptor   le descriptor de la classe
   * @param classToSave  la classe identifiant le dialog: utilise pour construire le canonicalName: identifiant unique du dialog
   * @param minMaxaction true si les actions minimize/maximize doivent être activées.
   * @return true si l'utilisateur a validé sa saisie
   */
  public static boolean showQuestion(DialogDescriptor descriptor, Class classToSave, boolean minMaxaction) {
    final String canonicalName = classToSave.getCanonicalName();
    return showQuestion(descriptor, canonicalName, minMaxaction);
  }

  /**
   * Affiche une dialogue attendant une saisie de l'utilisateur
   *
   * @param descriptor   le descriptor de la classe
   * @param classToSave  la classe identifiant le dialog: utilise pour construire le canonicalName: identifiant unique du dialog
   * @param version      utilise pour versionner la persistence de la position/taille. Si le contenu du dialog a changer, il faut changer cette version pour
   *                     réinitialiser la taille.
   * @param minMaxaction true si les actions minimize/maximize doivent être activées.
   * @return true si l'utilisateur a validé sa saisie
   */
  public static boolean showQuestion(DialogDescriptor descriptor, Class classToSave, String version, boolean minMaxaction) {
    final String canonicalName = classToSave.getCanonicalName();
    return showQuestion(descriptor, canonicalName, version, minMaxaction);
  }

  /**
   * Affiche une dialogue attendant une saisie de l'utilisateur
   *
   * @param descriptor    le descriptor de la classe
   * @param canonicalName le nom unique du dialogue: utilisé pour persister la position et pour l'aide
   * @param minMaxaction  true si les actions minimize/maximize doivent être activées.
   * @return true si l'utilisateur a validé sa saisie
   */
  public static boolean showQuestion(DialogDescriptor descriptor, final String canonicalName, boolean minMaxaction) {
    return showQuestion(descriptor, canonicalName, NO_VERSION_FOR_PERSISTENCE, minMaxaction);
  }

  /**
   * Affiche une dialogue attendant une saisie de l'utilisateur
   *
   * @param descriptor    le descriptor de la classe
   * @param canonicalName le nom unique du dialogue: utilisé pour persister la position et pour l'aide
   * @param version       utilise pour versionner la persistence de la position/taille. Si le contenu du dialog a changer, il faut changer cette version pour
   *                      réinitialiser la taille.
   * @param minMaxaction  true si les actions minimize/maximize doivent être activées.
   * @return true si l'utilisateur a validé sa saisie
   */
  protected static boolean showQuestion(DialogDescriptor descriptor, final String canonicalName, String version, boolean minMaxaction) {
    JDialog createDialog = (JDialog) DialogDisplayer.getDefault().createDialog(descriptor);
    createDialog.pack();
    createDialog.setName(canonicalName);
    UserPreferencesSaver.loadDialogLocationAndDimension(createDialog, version);
    if (minMaxaction) {
      ActionsInstaller.install(createDialog);
    }
    createDialog.setModal(true);
    createDialog.setVisible(true);
    UserPreferencesSaver.saveLocationAndDimension(createDialog, version);
    return NotifyDescriptor.OK_OPTION.equals(descriptor.getValue());
  }
}
