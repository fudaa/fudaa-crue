/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.log.property;

import java.lang.reflect.InvocationTargetException;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.fudaa.crue.common.property.PropertyCrueUtils;
import org.openide.nodes.PropertySupport;
import org.openide.util.NbBundle;

/**
 *
 * @author Fred Deniger
 */
public class FunctionProperty extends PropertySupport.ReadOnly<String> {

  public static final String ID = "Function";
  private final CtuluLogRecord record;

  public FunctionProperty(CtuluLogRecord record) {
    super(ID, String.class, getDefaultDisplayName(),
          getDescription());
    this.record = record;
    PropertyCrueUtils.configureNoCustomEditor(this);
  }

  public static String getDescription() {
    return NbBundle.getMessage(FunctionProperty.class, "FonctionPropertyDescription");
  }

  public static PropertyColumnFilterable createColumn() {
    PropertyColumnFilterable res = new PropertyColumnFilterable();
    res.setColumnId(ID);
    res.setDescription(getDescription());
    res.setDisplayName(getDefaultDisplayName());
    res.setVisibleByDefault(false);
    return res;
  }

  public static String getDefaultDisplayName() {
    return NbBundle.getMessage(FunctionProperty.class, "FonctionPropertyName");
  }

  @Override
  public String toString() {
    return record.getRessourceFunction();
  }

  @Override
  public String getValue() throws IllegalAccessException, InvocationTargetException {
    return toString();
  }
}
