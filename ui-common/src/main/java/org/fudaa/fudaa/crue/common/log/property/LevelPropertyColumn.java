package org.fudaa.fudaa.crue.common.log.property;

import java.awt.Component;
import java.util.MissingResourceException;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.fudaa.crue.common.log.CtuluLogsTopComponent;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.fudaa.fudaa.crue.common.log.filter.NodeQuickFilterLevelEqualsTo;
import org.fudaa.fudaa.crue.common.log.filter.NodeQuickFilterLevelLessVerbose;
import org.fudaa.fudaa.crue.common.log.filter.NodeQuickFilterLevelMoreVerbose;
import org.netbeans.swing.etable.ETable;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class LevelPropertyColumn extends PropertyColumnFilterable {

  @Override
  public void createFilterMenu(JPopupMenu res, Component component, int row, int column) throws MissingResourceException {
    JMenu menu = new JMenu(NbBundle.getMessage(LevelPropertyColumn.class, "filter.mainMenuName", displayName));
    ETable et = (ETable) component;
    if (et.getRowCount() == 0) {
      return;
    }
    res.add(menu);
    CtuluLogsTopComponent logs = (CtuluLogsTopComponent) ((JComponent) component).getClientProperty(
            CtuluLogsTopComponent.class.getSimpleName());
    Object valueAt = et.getValueAt(row, column);
    if (valueAt instanceof Node.Property) {
      try {
        valueAt = ((Node.Property) valueAt).getValue();
      } catch (Exception ex) {
        Exceptions.printStackTrace(ex);
      }
    }
    if (valueAt != null) {
      menu.add(create(new NodeQuickFilterLevelEqualsTo(columnId, displayName, (CtuluLogLevel) valueAt), logs));
    }
    JMenu sousMenu = new JMenu(LogIconTranslationProvider.getTranslation(CtuluLogLevel.FATAL));
    sousMenu.setIcon(new ImageIcon(LogIconTranslationProvider.getImage(CtuluLogLevel.FATAL)));
    menu.add(sousMenu);
    sousMenu.add(create(new NodeQuickFilterLevelEqualsTo(columnId, displayName, CtuluLogLevel.FATAL), logs));


    sousMenu = new JMenu(LogIconTranslationProvider.getTranslation(CtuluLogLevel.SEVERE));
    sousMenu.setIcon(new ImageIcon(LogIconTranslationProvider.getImage(CtuluLogLevel.SEVERE)));
    menu.add(sousMenu);
    sousMenu.add(create(new NodeQuickFilterLevelEqualsTo(columnId, displayName, CtuluLogLevel.SEVERE), logs));
    sousMenu.add(create(new NodeQuickFilterLevelLessVerbose(columnId, displayName, CtuluLogLevel.SEVERE, true), logs));
    sousMenu.add(create(new NodeQuickFilterLevelLessVerbose(columnId, displayName, CtuluLogLevel.SEVERE, false), logs));
    sousMenu.add(create(new NodeQuickFilterLevelMoreVerbose(columnId, displayName, CtuluLogLevel.SEVERE, true), logs));
    sousMenu.add(create(new NodeQuickFilterLevelMoreVerbose(columnId, displayName, CtuluLogLevel.SEVERE, false), logs));


    sousMenu = new JMenu(LogIconTranslationProvider.getTranslation(CtuluLogLevel.ERROR));
    sousMenu.setIcon(new ImageIcon(LogIconTranslationProvider.getImage(CtuluLogLevel.ERROR)));
    menu.add(sousMenu);
    sousMenu.add(create(new NodeQuickFilterLevelEqualsTo(columnId, displayName, CtuluLogLevel.ERROR), logs));
    sousMenu.add(create(new NodeQuickFilterLevelLessVerbose(columnId, displayName, CtuluLogLevel.ERROR, true), logs));
    sousMenu.add(create(new NodeQuickFilterLevelLessVerbose(columnId, displayName, CtuluLogLevel.ERROR, false), logs));
    sousMenu.add(create(new NodeQuickFilterLevelMoreVerbose(columnId, displayName, CtuluLogLevel.ERROR, true), logs));
    sousMenu.add(create(new NodeQuickFilterLevelMoreVerbose(columnId, displayName, CtuluLogLevel.ERROR, false), logs));

    sousMenu = new JMenu(LogIconTranslationProvider.getTranslation(CtuluLogLevel.WARNING));
    sousMenu.setIcon(new ImageIcon(LogIconTranslationProvider.getImage(CtuluLogLevel.WARNING)));
    menu.add(sousMenu);
    sousMenu.add(create(new NodeQuickFilterLevelEqualsTo(columnId, displayName, CtuluLogLevel.WARNING), logs));
    sousMenu.add(create(new NodeQuickFilterLevelLessVerbose(columnId, displayName, CtuluLogLevel.WARNING, true), logs));
    sousMenu.add(create(new NodeQuickFilterLevelLessVerbose(columnId, displayName, CtuluLogLevel.WARNING, false), logs));
    sousMenu.add(create(new NodeQuickFilterLevelMoreVerbose(columnId, displayName, CtuluLogLevel.WARNING, true), logs));
    sousMenu.add(create(new NodeQuickFilterLevelMoreVerbose(columnId, displayName, CtuluLogLevel.WARNING, false), logs));

    sousMenu = new JMenu(LogIconTranslationProvider.getTranslation(CtuluLogLevel.INFO));
    sousMenu.setIcon(new ImageIcon(LogIconTranslationProvider.getImage(CtuluLogLevel.INFO)));
    menu.add(sousMenu);
    sousMenu.add(create(new NodeQuickFilterLevelEqualsTo(columnId, displayName, CtuluLogLevel.INFO), logs));
    sousMenu.add(create(new NodeQuickFilterLevelLessVerbose(columnId, displayName, CtuluLogLevel.INFO, true), logs));
    sousMenu.add(create(new NodeQuickFilterLevelLessVerbose(columnId, displayName, CtuluLogLevel.INFO, false), logs));
    sousMenu.add(create(new NodeQuickFilterLevelMoreVerbose(columnId, displayName, CtuluLogLevel.INFO, true), logs));
    sousMenu.add(create(new NodeQuickFilterLevelMoreVerbose(columnId, displayName, CtuluLogLevel.INFO, false), logs));

    sousMenu = new JMenu(LogIconTranslationProvider.getTranslation(CtuluLogLevel.DEBUG));
    sousMenu.setIcon(new ImageIcon(LogIconTranslationProvider.getImage(CtuluLogLevel.DEBUG)));
    menu.add(sousMenu);
    sousMenu.add(create(new NodeQuickFilterLevelEqualsTo(columnId, displayName, CtuluLogLevel.DEBUG), logs));


  }
}
