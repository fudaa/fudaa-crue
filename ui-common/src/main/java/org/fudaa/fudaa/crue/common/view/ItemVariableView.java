package org.fudaa.fudaa.crue.common.view;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.config.ccm.*;
import org.fudaa.dodico.crue.metier.helper.PropertyFinder;
import org.fudaa.dodico.crue.metier.helper.PropertyFinder.Description;
import org.fudaa.dodico.crue.metier.helper.PropertyFinder.DescriptionIndexed;
import org.fudaa.fudaa.crue.common.helper.ToStringInternationalizableCellRenderer;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;

/**
 * Permet d'éditer puis valider une propriété definie dans un bean et ayant l'annotation PropertyDesc
 *
 * @author fred
 */
public abstract class ItemVariableView extends Observable {
  protected final ItemVariable property;
  private String labelName;
  private String propertyName;

  protected ItemVariableView(ItemVariable property) {
    this.property = property;
  }

  public String getLabelName() {
    return labelName;
  }

  public ItemVariable getProperty() {
    return property;
  }

  public abstract JComponent getPanelComponent();

  public abstract JComponent getEditComponent();

  public abstract void setEditable(boolean b);

  public abstract Object getValue();

  public abstract void setValue(Object o);

  public abstract boolean isValid();

  public static class ComboboxView extends ItemVariableView implements ActionListener {
    private final JComboBox cb;

    public ComboboxView(ItemVariable property, Object[] enums) {
      super(property);
      cb = new JComboBox(enums);
      cb.addActionListener(this);
      cb.setRenderer(new ToStringInternationalizableCellRenderer());
    }

    @Override
    public JComponent getEditComponent() {
      return cb;
    }

    @Override
    public boolean isValid() {
      return true;
    }

    @Override
    public JComponent getPanelComponent() {
      return cb;
    }

    @Override
    public void setValue(Object o) {
      cb.setSelectedItem(o);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      setChanged();
      notifyObservers();
    }

    @Override
    public void setEditable(boolean b) {
      cb.setEnabled(b);
    }

    @Override
    public Object getValue() {
      return cb.getSelectedItem();
    }
  }

  public static class TextField extends ItemVariableView implements DocumentListener {
    private final JFormattedTextField txt;
    protected JLabel label;
    private final JPanel pn;

    public TextField(ItemVariable property, DecimalFormatEpsilonEnum formatType) {
      super(property);
      if (property != null) {
        txt = new JFormattedTextField(property.getFormatter(formatType));
      } else {
        txt = new JFormattedTextField();
      }
      txt.setColumns(20);
      txt.getDocument().addDocumentListener(this);
      if (property != null) {
        txt.setToolTipText(property.getValidator().getHtmlDesc());
      }
      pn = new JPanel(new BorderLayout(2, 2)){
        @Override
        public void setEnabled(boolean enabled) {
          super.setEnabled(enabled);
          txt.setEditable(enabled);
          label.setVisible(enabled);
        }
      };
      label = new JLabel();
      label.setText(" ");
      pn.add(txt);
      pn.add(label, BorderLayout.EAST);
    }


    @Override
    public JComponent getEditComponent() {
      return txt;
    }

    public JTextField getTxt() {
      return txt;
    }

    public JLabel getLabel() {
      return label;
    }

    @Override
    public JComponent getPanelComponent() {
      return pn;
    }

    boolean valid;

    @Override
    public boolean isValid() {
      return valid;
    }

    public String getErrorMsg() {
      return label.getToolTipText();
    }

    protected void validateData() {
      label.setIcon(null);
      label.setText(" ");
      label.setToolTipText(null);
      valid = true;
      CtuluLog res = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
      if (property != null) {
        property.getValidator().validateNumber(StringUtils.EMPTY, getValue(), res, false);
      }
      if (res.isNotEmpty()) {
        CtuluLogRecord record = res.getRecords().get(0);
        BusinessMessages.updateLocalizedMessage(record);
        label.setToolTipText(record.getLocalizedMessage());
        label.setIcon(LogIconTranslationProvider.getIcon(record.getLevel()));
        pn.revalidate();
        pn.repaint();
      }
      valid = !res.containsErrorOrSevereError();
    }

    @Override
    public void setValue(Object o) {
      txt.setValue(o);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
      if (property != null) {
        String tltip = property.format(getValue(), DecimalFormatEpsilonEnum.COMPARISON);
        if (tltip != null) {
          tltip = org.openide.util.NbBundle.getMessage(ItemVariableView.class, "TooltipValue", tltip);
        }
        txt.setToolTipText(property.getValidator().getHtmlDesc(tltip));
      }
      setChanged();
      validateData();
      notifyObservers(this);
    }

    @Override
    public Number getValue() {
      String txtValue = txt.getText();
      if (StringUtils.isBlank(txtValue)) {
        return null;
      }
      Object value = null;
      try {
        value = Double.valueOf(txtValue);
      } catch (Exception ex) {
      }
      if (value == null) {
        return null;
      }
      if (property != null && property.isEntier()) {
        return Integer.valueOf(((Number) value).intValue());
      }
      double val = ((Number) value).doubleValue();
      if (property == null) {
        return (Number) value;
      }
      return Double.valueOf(property.getNormalizedValue(val));
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
      changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
      changedUpdate(e);
    }

    @Override
    public void setEditable(boolean b) {
      txt.setEditable(b);
    }
  }

  /**
   * Un editeur specifique pour une distance qui ne doit pas être égale à 0
   */
  public static class TextFiedDistance extends TextField {
    public TextFiedDistance(CrueConfigMetier ccm, DecimalFormatEpsilonEnum formatType) {
      super(ccm.getProperty(CrueConfigMetierConstants.PROP_XP), formatType);
    }

    @Override
    public void validateData() {
      super.validateData();
      if (valid) {
        Number distance = getValue();
        boolean isNull = distance == null;
        if (!isNull) {
          isNull = property.getEpsilon().isZero(distance.doubleValue());
        }
        if (isNull) {
          valid = false;
          label.setToolTipText(NbBundle.getMessage(ItemVariableView.class, "Editor.DistanceCantBeNull"));
          label.setIcon(LogIconTranslationProvider.getIcon(CtuluLogLevel.FATAL));
        }
      }
    }
  }

  /**
   * @param bean le bean à initialiser
   * @param properties les propriétés éditées
   */
  public static void transferToBean(Object bean, List<ItemVariableView> properties) {
    for (ItemVariableView propertyEditorPanel : properties) {
      try {
        PropertyUtils.setProperty(bean, propertyEditorPanel.propertyName, propertyEditorPanel.getValue());
      } catch (Exception exception) {
        Exceptions.printStackTrace(exception);
      }
    }
  }

  /**
   * crée des champs d'édition pour le bean. Pas besoin de cloner le bean car l'édition n'est que graphique et ne touche pas les valeurs des beans.
   *
   * @param bean le bean
   * @param ccm le {@link CrueConfigMetier}
   * @param properties les propriétés a afficher
   * @return la liste des variables editables
   */
  public static List<ItemVariableView> create(DecimalFormatEpsilonEnum formatType, Object bean, CrueConfigMetier ccm, String... properties) {
    PropertyFinder finder = new PropertyFinder();
    List<DescriptionIndexed> availableProperty = finder.getAvailablePropertiesNotSorted(bean.getClass());
    Map<String, PropertyFinder.Description> map = new HashMap<>();
    for (DescriptionIndexed descriptionIndexed : availableProperty) {
      map.put(descriptionIndexed.description.property, descriptionIndexed.description);
    }
    List<ItemVariableView> list = new ArrayList<>();
    for (String property : properties) {
      Description description = map.get(property);
      if (description == null) {
        throw new IllegalAccessError("property " + property + " not found in " + bean.getClass() + ". Be sure to use @PropertyDesc annotation");
      }
      final ItemVariable propertyItem = ccm.getProperty(property);
      PropertyNature nature = propertyItem == null ? null : propertyItem.getNature();
      ItemVariableView res;
      if (nature != null && nature.isEnum()) {
        Object[] enumConstants = description.propertyClass.getEnumConstants();
        res = new ComboboxView(propertyItem, enumConstants);
      } else {
        res = new ItemVariableView.TextField(propertyItem, formatType);
      }
      try {
        res.setValue(PropertyUtils.getProperty(bean, property));
      } catch (Exception exception) {
        Exceptions.printStackTrace(exception);
      }
      res.labelName = description.propertyDisplay + PropertyNature.getUniteSuffixe(propertyItem);
      res.propertyName = property;

      list.add(res);
    }
    return list;
  }
}
