package org.fudaa.fudaa.crue.common.editor;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;

/**
 *
 * @author deniger
 */
public abstract class CustomEditorAbstract implements PropertyChangeListener {

  protected final PropertyEditor editor;

  public CustomEditorAbstract(PropertyEditor editor, PropertyEnv env) {
    this.editor = editor;
    env.addPropertyChangeListener(this);
    env.setState(PropertyEnv.STATE_NEEDS_VALIDATION);
  }

  protected abstract Component createComponent();

  protected abstract Object getPropertyValue();

  public Component getComponent() {
    return createComponent();
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (PropertyEnv.PROP_STATE.equals(evt.getPropertyName())
            && evt.getNewValue() == PropertyEnv.STATE_VALID) {
      editor.setValue(getPropertyValue());
    }
  }
}
