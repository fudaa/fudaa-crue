package org.fudaa.fudaa.crue.common.action;

import org.fudaa.ctulu.Pair;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.importer.SmImportValidator;
import org.fudaa.fudaa.crue.common.helper.CrueFileChooserBuilder;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.io.File;

public class DrsoFileApprover implements CrueFileChooserBuilder.SelectionApprover {
  private final SmImportValidator smImporterValidator;
  final String dialogTitle;

  public DrsoFileApprover(EMHProjet projet, String dialogTitle, boolean createSousModele) {
    smImporterValidator = new SmImportValidator(projet,createSousModele);
    this.dialogTitle = dialogTitle;
  }

  @Override
  public boolean approve(final File[] files) {
    if (files.length != 1) {
      return false;
    }
    return approve(files[0]);
  }

  @Override
  public boolean approve(final File selection) {
    if (!selection.exists()) {
      JOptionPane.showMessageDialog(WindowManager.getDefault().getMainWindow(), NbBundle.getMessage(DrsoFileApprover.class, "AddFileAction.InvalidFile", selection.getName()),
        dialogTitle, JOptionPane.ERROR_MESSAGE);
      return false;
    }
    final Pair<Boolean, String> validationResult = smImporterValidator.canImportSousModele(selection, true);
    if (!validationResult.firstValue) {
      JOptionPane.showMessageDialog(WindowManager.getDefault().getMainWindow(), validationResult.secondValue,
        dialogTitle, JOptionPane.ERROR_MESSAGE);
    }
    return validationResult.firstValue;
  }
}
