package org.fudaa.fudaa.crue.common.editor;

import java.beans.PropertyEditorSupport;
import java.util.Map;

/**
 *
 * @author deniger
 */
public class ComboBoxPropertyEditorSupport extends PropertyEditorSupport {

  private String[] tags;

  public ComboBoxPropertyEditorSupport() {
  }
  Map<Object, String> i18nByValues;
  Map<String, Object> valuesByi18n;

  public void setTranslations(Map<Object, String> i18nByValues, Map<String, Object> valuesByi18n) {
    this.i18nByValues = i18nByValues;
    this.valuesByi18n = valuesByi18n;
  }

  @Override
  public String getAsText() {
    if (i18nByValues != null) {
      return i18nByValues.get(getValue());
    }
    return (String) getValue();
  }

  @Override
  public void setAsText(String string) {
    if (valuesByi18n != null) {
      final Object newValue = valuesByi18n.get(string);
      setValue(newValue);
    } else {
      setValue(string);
    }
  }

  public void setTags(String[] tags) {
    this.tags = tags;
  }

  @Override
  public String[] getTags() {
    return tags;
  }
}
