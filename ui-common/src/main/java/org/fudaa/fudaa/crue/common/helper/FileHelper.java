package org.fudaa.fudaa.crue.common.helper;

import java.io.File;

/**
 * @author deniger
 */
public class FileHelper {
    /**
     * @param file le fchier a recuperer sans extension
     * @return le nom en enlevant toute l'extension (premier point)
     */
    public static File getSansExtension(final File file) {
        final File parentDir = file.getParentFile();
        return new File(parentDir, getFilenameSansExtension(file.getName()));
    }

    /**
     * @param _fName doit être le nom du fichier
     * @return le nom en enlevant toute l'extension (premier point)
     */
    public static String getFilenameSansExtension(final String _fName) {
        if (_fName == null) {
            return null;
        }
        final int index = _fName.indexOf('.');
        if (index < 0) {
            return _fName;
        }
        return _fName.substring(0, index);
    }
}
