package org.fudaa.fudaa.crue.common.helper;

import java.awt.Component;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.CtuluTaskOperationAbstract;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.fudaa.fudaa.crue.common.log.LogsDisplayer;
import org.openide.awt.NotificationDisplayer;
import org.openide.windows.WindowManager;

/**
 *
 * @author deniger
 */
public class CtuluUIForNetbeans implements CtuluUI {

  public static final CtuluUIForNetbeans DEFAULT = new CtuluUIForNetbeans();

  @Override
  public void clearMainProgression() {
  }

  public static class NetbeansCtuluTaskDelegate extends CtuluTaskOperationAbstract {

    ProgressionInterface prog_;

    public NetbeansCtuluTaskDelegate(String _nom) {
      super(_nom);
    }

    @Override
    public ProgressionInterface getStateReceiver() {
      if (prog_ == null) {
        prog_ = new ProgressionInterface() {
          @Override
          public void setProgression(int _v) {
          }

          @Override
          public void setDesc(String _s) {
          }

          @Override
          public void reset() {
          }
        };
      }
      return prog_;
    }

    @Override
    public ProgressionInterface getMainStateReceiver() {
      return getStateReceiver();
    }
  }

  @Override
  public CtuluTaskDelegate createTask(String _name) {
    return new NetbeansCtuluTaskDelegate(_name);
  }

  @Override
  public void error(String _titre, String _msg, boolean _tempo) {
    if (_tempo) {
      NotificationDisplayer.getDefault().notify(_titre, CrueIconsProvider.getIcon("org/fudaa/fudaa/crue/common/icons/erreur-non-bloquante.png"), _msg,
              null);
    } else {
      DialogHelper.showError(_titre, _msg);
    }
  }

  @Override
  public void error(String _msg) {
    DialogHelper.showError(_msg);
  }

  @Override
  public ProgressionInterface getMainProgression() {
    return null;
//    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public Component getParentComponent() {
    return WindowManager.getDefault().getMainWindow();
  }

  @Override
  public boolean manageAnalyzeAndIsFatal(CtuluAnalyze _analyze) {

    if (_analyze == null || _analyze.isEmpty()) {
      return false;
    }
    CtuluLog log = new CtuluLog();
    log.setDesc(_analyze.getDesci18n());
    Collection<LogRecord> records = _analyze.getRecords();
    for (LogRecord logRecord : records) {
      Level level = logRecord.getLevel();
      CtuluLogLevel targetLevel = CtuluLogLevel.ERROR;
      if (level.equals(Level.WARNING)) {
        targetLevel = CtuluLogLevel.WARNING;
      } else if (level.equals(CtuluAnalyze.FATAL)) {
        targetLevel = CtuluLogLevel.FATAL;
      } else if (level.equals(Level.INFO)) {
        targetLevel = CtuluLogLevel.INFO;
      }
      log.addRecord(targetLevel, logRecord.getMessage());
    }
    LogsDisplayer.displayError(log, _analyze.getDesci18n());
    return _analyze.containsFatalError();
  }

  @Override
  public boolean manageAnalyzeAndIsFatal(CtuluLog log) {
    LogsDisplayer.displayError(log, log.getDesci18n());
    return log.containsSevereError();
  }

  @Override
  public boolean manageErrorOperationAndIsFatal(CtuluIOOperationSynthese _opResult) {
    if (_opResult == null || _opResult.getAnalyze() == null) {
      return false;
    }
    return manageAnalyzeAndIsFatal(_opResult.getAnalyze());
  }

  @Override
  public boolean manageErrorOperationAndIsFatal(CtuluIOResult _opResult) {
    if (_opResult == null || _opResult.getAnalyze() == null) {
      return false;
    }
    return manageAnalyzeAndIsFatal(_opResult.getAnalyze());
  }

  @Override
  public void message(String _titre, String _msg, boolean _tempo) {
    if (_tempo) {
      if (_titre == null && _msg != null) {
        NotificationDisplayer.getDefault().notify(_msg, CrueIconsProvider.getIcon("org/fudaa/fudaa/crue/common/icons/information.png"),
                StringUtils.EMPTY, null);
      } else {
        NotificationDisplayer.getDefault().notify(StringUtils.defaultString(_titre), CrueIconsProvider.getIcon(
                "org/fudaa/fudaa/crue/common/icons/information.png"), StringUtils.defaultString(_msg), null);
      }
    } else {
      DialogHelper.showInfo(_titre, _msg);
    }
  }

  @Override
  public boolean question(String _titre, String _text) {
    return DialogHelper.showQuestion(_titre, _text);
  }

  @Override
  public void warn(String _titre, String _msg, boolean _tempo) {
    if (_tempo) {
      NotificationDisplayer.getDefault().notify(_titre, CrueIconsProvider.getIcon("org/fudaa/fudaa/crue/common/icons/avertissement.png"), _msg, null);
    } else {
      DialogHelper.showWarn(_titre, _msg);
    }
  }
}
