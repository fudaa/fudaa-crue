/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.services;

import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.metier.etude.ManagerEMHScenario;
import org.openide.util.Lookup;

/**
 * Le lookup peut être utilise pour écouter les modifications sur le scénario chargé.
 *
 * @author Fred Deniger
 */
public interface ModellingScenarioService extends Lookup.Provider {

  boolean isScenarioLoaded();

  boolean unloadScenario();

  /**
   *
   * @return true si le scenario est en cours de rechargement ( ecriture)
   */
  boolean isReloading();

  /**
   * A utiliser lorsqu'un run a été ajouté par exemple
   * @param managerEMHScenario
   */
  void updateManagerScenarioLoaded(ManagerEMHScenario managerEMHScenario);

  EMHScenario loadScenario(EMHProjet projet, ManagerEMHScenario scenario);

  EMHScenario getScenarioLoaded();

  ManagerEMHScenario getManagerScenarioLoaded();

  EMHProjet getSelectedProjet();

  boolean saveScenario();

  boolean validScenario();


  /**
   *
   * @return le bilan de la dernière opération.
   */
  CtuluLogGroup getLastLogsGroup();
}
