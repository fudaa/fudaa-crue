/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common;

import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

import java.awt.*;
import java.text.MessageFormat;

/**
 * @author Fred Deniger
 */
public class CommonMessage {
  public static String getDefaultFrameTitle() {
    return MessageFormat.format(
        getCurrentVersion(),
        System.getProperty("netbeans.buildnumber"));
  }

  public static void resetFrameTitle() {
    if (!EventQueue.isDispatchThread()) {
      EventQueue.invokeLater(() -> resetFrameTitle());
      return;
    }
    WindowManager.getDefault().getMainWindow().setTitle(getDefaultFrameTitle());
  }

  public static void modifyFrameTitle(final String title) {
    if (EventQueue.isDispatchThread()) {
      WindowManager.getDefault().getMainWindow().setTitle(title + " | " + getDefaultFrameTitle());
    } else {
      EventQueue.invokeLater(() -> modifyFrameTitle(title));
    }
  }

  public static String getMessage(final String code) {
    return NbBundle.getMessage(CommonMessage.class, code);
  }

  public static String getMessage(final String code, final String value) {
    return NbBundle.getMessage(CommonMessage.class, code, value);
  }

  private static String getCurrentVersion() {
    return NbBundle.getBundle("org.netbeans.core.startup.Bundle").getString("currentVersion");
  }

  private CommonMessage() {
  }
}
