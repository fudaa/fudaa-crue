package org.fudaa.fudaa.crue.common.property;

import org.apache.commons.collections4.Transformer;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.fudaa.crue.common.editor.ComboBoxPropertyEditorSupport;
import org.openide.nodes.PropertySupport;
import org.openide.util.Exceptions;

import java.beans.PropertyEditor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Une propriété qui envoie des evts via AbstractNodeFirable
 *
 * @author Fred Deniger
 */
public class PropertySupportReflection<T> extends PropertySupport.Reflection<T> {
  protected final AbstractNodeFirable node;
  private boolean canWrite = true;
  private Map<Object, String> i18nByValues;
  private Map<String, Object> valuesByi18n;
  private Transformer tagsFilter;
  private String[] tags;
  private boolean usei18nAsTag;

  /**
   * @param node le node contenant l'objet. Enverra les evts.
   * @param instance l'objet
   * @param valueType la classe de la valeur
   * @param property le nom de la propriete
   * @throws NoSuchMethodException si echec dans introspection
   */
  public PropertySupportReflection(AbstractNodeFirable node, Object instance, Class valueType, String property) throws NoSuchMethodException {
    super(instance, valueType, property);
    this.node = node;
    PropertyCrueUtils.installForColorFont(this);
  }

  public PropertySupportReflection(AbstractNodeFirable node, Object instance, Class valueType, String getter, String setter) throws NoSuchMethodException {
    super(instance, valueType, getter, setter);
    this.node = node;
    PropertyCrueUtils.installForColorFont(this);
  }

  public PropertySupportReflection(AbstractNodeFirable node, Object instance, Class valueType, Method getter, Method setter) {
    super(instance, valueType, getter, setter);
    this.node = node;
    PropertyCrueUtils.installForColorFont(this);
  }

  public static PropertySupportReflection create(AbstractNodeFirable node, Object instance, Class valueType, String property,
                                                 String name, String displayName) {
    try {
      PropertySupportReflection res = new PropertySupportReflection(node, instance, valueType,
          property);
      PropertyCrueUtils.setNullValueEmpty(res);
      res.setName(name);
      res.setDisplayName(displayName);
      return res;
    } catch (NoSuchMethodException noSuchMethodException) {
      Exceptions.printStackTrace(noSuchMethodException);
    }
    return null;
  }

  public static PropertySupportReflection createString(AbstractNodeFirable node, Object instance, String property,
                                                       String name, String displayName) {
    return create(node, instance, String.class, property, name, displayName);
  }

  public boolean isUsei18nAsTag() {
    return usei18nAsTag;
  }

  public void setUsei18nAsTag(boolean usei18nAsTag) {
    this.usei18nAsTag = usei18nAsTag;
  }

  public void setCanWrite(boolean canWrite) {
    this.canWrite = canWrite;
  }

  @Override
  public boolean canWrite() {
    return node.isEditMode() && super.canWrite() && canWrite;
  }

  public void setTranslationForObjects(Map<String, Object> objectByTranslation) {
    valuesByi18n = objectByTranslation;
    i18nByValues = new HashMap<>();
    for (Map.Entry<String, Object> entry : valuesByi18n.entrySet()) {
      i18nByValues.put(entry.getValue(), entry.getKey());
    }
  }

  /**
   * Utiliser pour filtrer les valeurs avant envoi à l'éditeur.
   *
   * @param tagsFilter doit accepter un tableau de string et renvoyer le tableau
   */
  public void setTagsFilter(Transformer tagsFilter) {
    this.tagsFilter = tagsFilter;
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    if (tags != null) {
      final ComboBoxPropertyEditorSupport editor = new ComboBoxPropertyEditorSupport();
      editor.setTranslations(i18nByValues, valuesByi18n);
      String[] finalTags = tags;
      if (tagsFilter != null) {
        finalTags = (String[]) tagsFilter.transform(tags);
      }
      if (usei18nAsTag && tags != null) {
        String[] tagsi18n = new String[tags.length];
        for (int i = 0; i < tags.length; i++) {
          tagsi18n[i] = tags[i];
          String i18n = i18nByValues.get(tags[i]);
          if (StringUtils.isNotBlank(i18n)) {
            tagsi18n[i] = i18n;
          }
        }
        finalTags=tagsi18n;
      }
      editor.setTags(finalTags);
      return editor;
    }
    return super.getPropertyEditor();
  }

  public void setTags(String[] tags) {
    this.tags = tags;
  }

  public void setTags(List<String> tags) {
    this.tags = tags.toArray(new String[0]);
  }

  /**
   * Envoie un evt s'il la propriété est modifiée.
   *
   * @param newValue la nouvelle valeur
   */
  @Override
  public void setValue(T newValue) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    if (!canWrite()) {
      return;
    }
    forceSetValue(newValue);
  }

  protected void valueUpdated(T val) {

  }

  private void forceSetValue(T val) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    Object old = getValue();
    if (!ObjectUtils.equals(val, old)) {
      super.setValue(val);
      valueUpdated(val);
      node.fireObjectChange(getName(), old, val);
    }
  }
}
