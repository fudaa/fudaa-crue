/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.swing;

import com.jidesoft.swing.JideButton;
import com.jidesoft.swing.JideToggleButton;
import javax.swing.AbstractButton;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliBasicComponentFactory;

/**
 *
 * @author Frederic Deniger
 */
public class CrueBasicComponentFactory extends EbliBasicComponentFactory {

  @Override
  public AbstractButton createToolToggleButton() {
    final JideToggleButton jideToggleButton = new JideToggleButton();
    jideToggleButton.setButtonStyle(JideButton.TOOLBAR_STYLE);
    return jideToggleButton;
  }

  @Override
  public AbstractButton createToggleButton() {
    return new JideToggleButton();
  }


  @Override
  public AbstractButton createToolButton(EbliActionSimple _a) {
    final JideToolButton jideToolButton = new JideToolButton(_a);
    jideToolButton.setButtonStyle(JideButton.TOOLBAR_STYLE);
    return jideToolButton;
  }

  @Override
  public AbstractButton createButton() {
    return new JideButton();
  }


  @Override
  public AbstractButton createPaletteButton(EbliActionPaletteAbstract _a) {
    return new JidePaletteButton(_a);
  }

}
