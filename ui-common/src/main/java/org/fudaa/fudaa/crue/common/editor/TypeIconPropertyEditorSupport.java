package org.fudaa.fudaa.crue.common.editor;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.beans.PropertyEditorSupport;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;

/**
 *
 * @author deniger
 */
public class TypeIconPropertyEditorSupport extends PropertyEditorSupport {

  final TraceIcon ic;

  public TypeIconPropertyEditorSupport() {
    TraceIconModel icon = new TraceIconModel(TraceIcon.RIEN, 6, Color.BLACK);
    icon.setEpaisseurLigne(1f);
    ic = new TraceIcon(icon);
  }

  @Override
  public boolean isPaintable() {
    return true;
  }

  @Override
  public void paintValue(Graphics gfx, Rectangle box) {
    Integer value = (Integer) getValue();
    ic.setType(value);
    ic.paintIconCentre(null, gfx, box.x + ic.getTaille() / 2, box.y + box.height / 2);
  }
}
