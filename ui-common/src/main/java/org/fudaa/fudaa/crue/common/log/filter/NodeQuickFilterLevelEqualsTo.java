package org.fudaa.fudaa.crue.common.log.filter;

import org.apache.commons.lang3.ObjectUtils;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

/**
 *
 * @author deniger
 */
public class NodeQuickFilterLevelEqualsTo extends AbstractNodeQuickFilter {

  private final CtuluLogLevel level;

  public NodeQuickFilterLevelEqualsTo(final String propertyId, final String propertyName, final CtuluLogLevel level) {
    super(propertyId);
    this.level = level;
    final String translated = LogIconTranslationProvider.getTranslation(level);
    name = NbBundle.getMessage(NodeQuickFilterLevelEqualsTo.class,
                               "filter.verboseEqualsTo.name", propertyName, translated);
    description = NbBundle.getMessage(NodeQuickFilterLevelEqualsTo.class, "filter.verboseEqualsTo.description", propertyName,
                                      translated);
  }

  @Override
  public boolean accept(final Node node) {
    if (node == null) {
      return true;
    }
    final CtuluLogRecord lookup = node.getLookup().lookup(CtuluLogRecord.class);
    if (lookup == null) {
      return true;
    }
    return acceptValue(lookup);
  }

  @Override
  protected boolean acceptValue(final Object value) {
    final CtuluLogRecord lookup = (CtuluLogRecord) value;
    return ObjectUtils.equals(level, lookup.getLevel());
  }
}
