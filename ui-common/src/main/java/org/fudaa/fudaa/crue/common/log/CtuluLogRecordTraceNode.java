/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.log;

import java.awt.Image;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.Action;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.fudaa.crue.common.log.filter.NodeQuickFilterProcess;
import org.fudaa.fudaa.crue.common.log.property.DocProperty;
import org.fudaa.fudaa.crue.common.log.property.EntiteProperty;
import org.fudaa.fudaa.crue.common.log.property.FileProperty;
import org.fudaa.fudaa.crue.common.log.property.FunctionProperty;
import org.fudaa.fudaa.crue.common.log.property.LevelDetailIconProperty;
import org.fudaa.fudaa.crue.common.log.property.LevelDetailStringProperty;
import org.fudaa.fudaa.crue.common.log.property.LineProperty;
import org.fudaa.fudaa.crue.common.log.property.LogIdProperty;
import org.fudaa.fudaa.crue.common.log.property.MessageProperty;
import org.fudaa.fudaa.crue.common.log.property.MultimediaDocProperty;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Fred Deniger
 */
public final class CtuluLogRecordTraceNode extends AbstractNode implements CtuluLogRecordContainer {

  public static final DateTimeFormatter DATE_PATTERN = DateTimeFormat.forPattern("dd/MM/YYYY HH:mm:ss.SSS");

  public CtuluLogRecordTraceNode(CtuluLogRecord record, ResourceBundle resourceBundle) {
    super(Children.LEAF, Lookups.singleton(record));
    Date logDate = record.getLogDate();
    setName(logDate != null ? DATE_PATTERN.print(logDate.getTime()) : StringUtils.EMPTY);
    setDisplayName(logDate != null ? DATE_PATTERN.print(logDate.getTime()) : StringUtils.EMPTY);
  }

  @Override
  public String toString() {
    return getDisplayName(); // NOI18N
  }

  @Override
  public Image getIcon(int type) {
    return LogIconTranslationProvider.getNoImage();
  }

  @Override
  public FilterNode createOnlyErrorNode() {
    if (isErrorNode(getCtuluLogRecord())) {
      return new FilterNode(this);
    }
    return null;
  }

  @Override
  public Action[] getActions(boolean context) {
    return new Action[0];
  }

  public static boolean isErrorNode(CtuluLogRecord logRecord) {
    final CtuluLogLevel level = logRecord.getLevel();
    return (CtuluLogLevel.ERROR.equals(level) || CtuluLogLevel.SEVERE.equals(level));
  }

  @Override
  protected Sheet createSheet() {
    Sheet sheet = Sheet.createDefault();
    Sheet.Set set = Sheet.createPropertiesSet();
    Map<String, Node.Property> propertyById = new HashMap<>();
    final CtuluLogRecord record = getCtuluLogRecord();
    set.put(register(propertyById, new LevelDetailIconProperty(record)));
    set.put(register(propertyById, new LevelDetailStringProperty(record)));
    set.put(register(propertyById, new LogIdProperty(record)));
    set.put(register(propertyById, new FunctionProperty(record)));
    set.put(register(propertyById, new FileProperty(record)));
    set.put(register(propertyById, new LineProperty(record)));
    set.put(register(propertyById, new EntiteProperty(record)));
    set.put(register(propertyById, new MessageProperty(record)));
    set.put(register(propertyById, new DocProperty(record)));
    set.put(register(propertyById, new MultimediaDocProperty(record)));
    sheet.put(set);
    setValue(NodeQuickFilterProcess.PROPERTY_BY_ID_KEY, Collections.unmodifiableMap(propertyById));
    return sheet;
  }

  public static Node.Property register(Map<String, Node.Property> propertyById, Node.Property in) {
    propertyById.put(in.getName(), in);
    return in;
  }

  protected String buildMessage(CtuluLogRecord record, ResourceBundle resourceBundle) {
    String msg = null;
    if (record.getLocalizedMessage() != null) {
      msg = record.getLocalizedMessage();
    } else {
      final Object[] args = record.getArgs();
      if (resourceBundle != null) {
        try {
          msg = resourceBundle.getString(record.getMsg());
        } catch (java.util.MissingResourceException e) {
          msg = record.getMsg();
        }

        if (!ArrayUtils.isEmpty(args)) {
          msg = MessageFormat.format(msg, args);
        }
      }
      if (msg == null) {
        if (!ArrayUtils.isEmpty(args)) {
          msg = record.getMsg() + " " + StringUtils.join(args);
        } else {
          msg = record.getMsg();
        }
      }
    }
    return msg;
  }

  CtuluLogRecord getCtuluLogRecord() {
    return getLookup().lookup(CtuluLogRecord.class);
  }
}
