package org.fudaa.fudaa.crue.common.services;

import org.openide.util.Lookup;

/**
 * Interface commune pour savoir si une campagne AOC est chargée
 */
public interface AocServiceContrat extends Lookup.Provider {
    /**
     *
     * @return true si une campagne est chargée
     */
    boolean isAocCampagneLoaded();

    /**
     *
     * @return true si un fichier LHPT de l'étude est chargée
     */
    boolean isLhptLoaded();
}
