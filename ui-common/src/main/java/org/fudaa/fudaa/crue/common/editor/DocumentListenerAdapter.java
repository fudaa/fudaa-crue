package org.fudaa.fudaa.crue.common.editor;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * Un Adapter pour éviter d'implementer les 3 methodes si une seule action est commune
 * @author deniger
 */
public abstract class DocumentListenerAdapter implements DocumentListener {
  @Override
  public final void insertUpdate(DocumentEvent e) {
    update(e);
  }

  @Override
  public final void removeUpdate(DocumentEvent e) {
    update(e);
  }

  @Override
  public final void changedUpdate(DocumentEvent e) {
    update(e);
  }

  protected abstract void update(DocumentEvent e);
}
