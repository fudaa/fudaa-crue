/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.property;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogLevel;
import org.fudaa.dodico.crue.common.BusinessMessages;
import org.fudaa.dodico.crue.common.transformer.LogsHelper;
import org.fudaa.dodico.crue.config.ccm.CrueConfigMetier;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.metier.emh.Pdt;
import org.fudaa.dodico.crue.validation.ValidationHelper;
import org.fudaa.fudaa.crue.common.log.LogIconTranslationProvider;
import org.fudaa.fudaa.crue.common.node.NodeHelper;
import org.openide.nodes.Node;
import org.openide.nodes.Node.Property;
import org.openide.util.Exceptions;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import static org.openide.util.NbBundle.getMessage;

/**
 * Ce listener teste la validité d'une proprieté. Le noeud dans définir le CCM dans son lookup.
 *
 * @author Frédéric Deniger
 */
public class ValidityValueListener implements PropertyChangeListener {
  private final ItemVariable property;
  private final Node.Property nodeProperty;
  final AbstractNodeFirable node;

  /**
   * @param property la propriete Crue
   * @param nodeProperty la propriete dans Netbeans RCP
   * @param node avec un CCM dans le lookup.
   */
  public ValidityValueListener(ItemVariable property, Property nodeProperty, AbstractNodeFirable node) {
    this.property = property;
    this.nodeProperty = nodeProperty;
    this.node = node;
    node.addPropertyChangeListener(this);
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (nodeProperty.getName().equals(evt.getPropertyName())) {
      updateState();
    }
  }

  public void updateState() {
    CtuluLog log = new CtuluLog(BusinessMessages.RESOURCE_BUNDLE);
    try {
      Class valueType = nodeProperty.getValueType();
      Object value = nodeProperty.getValue();
      String htmlValue = null;
      if (Pdt.class.isAssignableFrom(valueType)) {
        CrueConfigMetier ccm = node.getLookup().lookup(CrueConfigMetier.class);
        ValidationHelper.validatePdt(StringUtils.EMPTY, log, (Pdt) value, ccm);
      } else {
        if (value != null && Double.class.equals(value.getClass())) {
          value = Double.valueOf(property.getNormalizedValue((Double) value));
          htmlValue = getMessage(ValidityValueListener.class, "DoubleDisplayInfo", property.format((Double) value, DecimalFormatEpsilonEnum.COMPARISON));
        }
        property.getValidator().validateValue(StringUtils.EMPTY, value, log, false);
      }
      if (log.isNotEmpty()) {
        if (log.containsErrors()) {
          nodeProperty.setValue(NodeHelper.PROP_NAME_ICON, LogIconTranslationProvider.getIcon(CtuluLogLevel.ERROR));
        } else {
          nodeProperty.setValue(NodeHelper.PROP_NAME_ICON, LogIconTranslationProvider.getIcon(CtuluLogLevel.WARNING));
        }
        nodeProperty.setShortDescription(LogsHelper.toHtml(log, htmlValue));
      } else {
        nodeProperty.setValue(NodeHelper.PROP_NAME_ICON, LogIconTranslationProvider.NO_ICON);
        nodeProperty.setShortDescription(property.getValidator().getHtmlDesc(htmlValue));
      }
    } catch (Exception exception) {
      Exceptions.printStackTrace(exception);
    }
  }
}
