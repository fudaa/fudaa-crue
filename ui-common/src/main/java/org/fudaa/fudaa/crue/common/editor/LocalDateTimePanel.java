/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.editor;

import java.awt.FlowLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Observer;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.fudaa.fudaa.crue.common.ObservablePublic;
import org.fudaa.fudaa.crue.common.pdt.DurationPanelEditor;
import org.jdesktop.swingx.JXDatePicker;
import org.joda.time.LocalDateTime;
import org.openide.util.NbBundle;

/**
 *
 * @author Frédéric Deniger
 */
public class LocalDateTimePanel extends JPanel {

  final JXDatePicker datePicker = new JXDatePicker();
  final JSpinner hour;
  final JSpinner min;
  final JSpinner sec;
  final ObservablePublic observable = new ObservablePublic();

  public synchronized void addObserver(Observer o) {
    observable.addObserver(o);
  }

  public synchronized void deleteObserver(Observer o) {
    observable.deleteObserver(o);
  }

  public synchronized void deleteObservers() {
    observable.deleteObservers();
  }

  public synchronized int countObservers() {
    return observable.countObservers();
  }

  public void setAllEnable(boolean enable) {
    datePicker.setEditable(enable);
    datePicker.setEnabled(enable);
    hour.setEnabled(enable);
    min.setEnabled(enable);
    sec.setEnabled(enable);
  }

  public LocalDateTimePanel() {
    setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 0));
    add(datePicker);
    datePicker.addPropertyChangeListener(new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        if ("date".equals(evt.getPropertyName())) {
          observable.setChangedAndNotify();
        }
      }
    });
    sec = new JSpinner(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
    ((JSpinner.DefaultEditor) sec.getEditor()).getTextField().setColumns(2);

    min = new JSpinner(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
    ((JSpinner.DefaultEditor) min.getEditor()).getTextField().setColumns(2);

    hour = new JSpinner(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
    ((JSpinner.DefaultEditor) hour.getEditor()).getTextField().setColumns(2);
    add(new JLabel(" "));
    add(hour);
    add(new JLabel(NbBundle.getMessage(DurationPanelEditor.class, "Hour.Label")));
    add(new JLabel(" "));
    add(min);
    add(new JLabel(NbBundle.getMessage(DurationPanelEditor.class, "Minute.Label")));
    add(new JLabel(" "));
    add(sec);
    add(new JLabel(NbBundle.getMessage(DurationPanelEditor.class, "Sec.Label")));
    add(new JLabel(" "));
    ChangeListener changeListener = new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        observable.setChangedAndNotify();
      }
    };
    sec.getModel().addChangeListener(changeListener);
    min.getModel().addChangeListener(changeListener);
    hour.getModel().addChangeListener(changeListener);
  }

  public void setLocalDateTime(LocalDateTime time) {
    if (time == null) {
      hour.setValue(0);
      min.setValue(0);
      sec.setValue(0);
      datePicker.setDate(null);
    } else {
      hour.setValue(time.getHourOfDay());
      min.setValue(time.getMinuteOfHour());
      sec.setValue(time.getSecondOfMinute());
      datePicker.setDate(time.toDateTime().toDate());
    }
  }

  public int getValue(JSpinner spinner) {
    return ((Number) spinner.getValue()).intValue();
  }

  public LocalDateTime getLocalDateTime() {
    if (datePicker.getDate() == null) {
      return null;
    }
//    hour.getValue()
//    time+=getValue(min)
    LocalDateTime dateTime = new LocalDateTime(datePicker.getDate());
    dateTime = dateTime.withHourOfDay(getValue(hour));
    dateTime = dateTime.withMinuteOfHour(getValue(min));
    dateTime = dateTime.withSecondOfMinute(getValue(sec));
    return new LocalDateTime(dateTime);
  }

  void setEditable(boolean canWrite) {
    datePicker.setEditable(canWrite);
    hour.setEnabled(canWrite);
    min.setEnabled(canWrite);
    sec.setEnabled(canWrite);
  }
}
