package org.fudaa.fudaa.crue.common.property;

import org.openide.nodes.Children;
import org.openide.util.Lookup;

import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.io.IOException;

public abstract class AbstractNodeValueFirable<O> extends AbstractNodeFirable {
    public AbstractNodeValueFirable(final Children children, final Lookup lookup) {
        super(children, lookup);
    }

    public AbstractNodeValueFirable(final Children children) {
        super(children);
    }

    public abstract O getMainValue();

    @Override
    public Transferable clipboardCopy() throws IOException {
        return new StringSelection(getDataAsString());
    }

    @Override
    public Transferable clipboardCut() throws IOException {
        final Transferable transferable = clipboardCopy();
        destroy();
        return transferable;
    }
}
