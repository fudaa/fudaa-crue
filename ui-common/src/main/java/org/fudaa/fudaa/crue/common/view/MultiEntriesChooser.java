/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.view;

import com.jidesoft.swing.CheckBoxList;
import org.fudaa.ctulu.gui.CtuluPopupListener;
import org.fudaa.dodico.crue.common.transformer.ToStringTransformer;
import org.fudaa.fudaa.crue.common.helper.ToStringTransfomerCellRenderer;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Frederic Deniger
 */
public class MultiEntriesChooser<T> {
  final List<T> keys;
  List<T> preSelectedEntries;
  final ToStringTransformer toStringTransformer;
  JPanel panel;
  CheckBoxList checkBoxList;

  public MultiEntriesChooser(List<T> keys, ToStringTransformer toStringTransformer) {
    this.keys = keys;
    this.toStringTransformer = toStringTransformer;
  }

  public MultiEntriesChooser(List<T> keys) {
    this(keys, null);
  }

  protected void selectAll() {
    checkBoxList.selectAll();
  }

  protected void selectNone() {
    checkBoxList.selectNone();
  }

  public List<T> getSelectedEntries() {
    List<T> res = new ArrayList<>();
    int[] selectedIndices = checkBoxList.getCheckBoxListSelectedIndices();
    for (int i = 0; i < selectedIndices.length; i++) {
      res.add((T) checkBoxList.getModel().getElementAt(selectedIndices[i]));
    }
    return res;
  }

  public void setPreSelectedEntries(List<T> preSelectedEntries) {
    this.preSelectedEntries = preSelectedEntries;
  }

  public JPanel getPanel() {
    if (panel == null) {
      createPanel();
    }
    return panel;
  }

  private void createPanel() {
    panel = new JPanel(new BorderLayout(2, 2));
    checkBoxList = new CheckBoxList(keys.toArray());
    if (toStringTransformer != null) {
      checkBoxList.setCellRenderer(new ToStringTransfomerCellRenderer(toStringTransformer));
    }
    panel.add(new JScrollPane(checkBoxList));

    CtuluPopupListener.PopupReceiver receiver = new CtuluPopupListener.PopupReceiver() {
      @Override
      public void popup(MouseEvent _evt) {
        showPopup(_evt);
      }
    };
    new CtuluPopupListener(receiver, checkBoxList);
    //on selectionne dans l'ui les entrees pré-sélectionnées
    if (preSelectedEntries != null) {
      for (int i = 0; i < keys.size(); i++) {
        if (preSelectedEntries.contains(checkBoxList.getModel().getElementAt(i))) {
          checkBoxList.addCheckBoxListSelectedIndex(i);
        }
      }
    }
  }

  private void showPopup(MouseEvent _evt) {
    createMenu().show(checkBoxList, _evt.getX(), _evt.getY());
  }

  private JPopupMenu createMenu() {
    JPopupMenu menu = new JPopupMenu();
    menu.add(NbBundle.getMessage(MultiEntriesChooser.class, "Chooser.SelectAll")).addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        selectAll();
      }
    });
    menu.add(NbBundle.getMessage(MultiEntriesChooser.class, "Chooser.SelectNone")).addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        selectNone();
      }
    });
    return menu;
  }
}
