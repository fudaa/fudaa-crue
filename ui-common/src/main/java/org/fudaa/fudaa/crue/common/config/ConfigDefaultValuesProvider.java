/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.config;

import org.fudaa.dodico.crue.edition.bean.CreationDefaultValue;

/**
 *
 * @author Frederic Deniger
 */
public interface ConfigDefaultValuesProvider {

  CreationDefaultValue getDefaultValues();
}
