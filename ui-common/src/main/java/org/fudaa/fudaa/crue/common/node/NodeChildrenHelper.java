package org.fudaa.fudaa.crue.common.node;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * @author deniger
 */
public class NodeChildrenHelper {
    public static Children createChildren(List<? extends Node> nodes) {
        return createChildren(nodes, true);
    }

    public static Children createChildren(List<? extends Node> nodes, boolean useLeaf) {
        if (useLeaf && CollectionUtils.isEmpty(nodes)) {
            return Children.LEAF;
        }
        Children.Array res = new Children.Array();
        if (nodes != null) {
            res.add(nodes.toArray(new Node[0]));
        }
        return res;
    }

    /**
     * @param explorerManager si null renvoie liste vide
     * @return liste de tous les noeuds directs
     */
    public static <T extends Node> List<T> getNodes(ExplorerManager explorerManager) {
        List<T> loiCalculPermanentNodes = new ArrayList<>();
        if (explorerManager != null) {
            for (Node otherNode : explorerManager.getRootContext().getChildren().getNodes()) {
                loiCalculPermanentNodes.add(((T) otherNode));
            }
        }
        return loiCalculPermanentNodes;
    }

    /**
     * @param explorerManager si null renvoie liste vide
     * @return liste de tous les noeuds directs
     */
    public static <T extends Node> T[] getNodesAsArray(ExplorerManager explorerManager) {
        final List<T> nodes = getNodes(explorerManager);
        if (nodes.isEmpty()) {
            return (T[]) new Node[0];
        }
        return nodes.toArray((T[]) Array.newInstance(nodes.get(0).getClass(), nodes.size()));
    }

    public static <N extends Node> Children createChildren(N[] nodes) {
        return createChildren(nodes, true);
    }

    public static <N extends Node> Children createChildren(N[] nodes, boolean useLeaf) {
        if (useLeaf && ArrayUtils.isEmpty(nodes)) {
            return Children.LEAF;
        }
        Children.Array res = new Children.Array();
        res.add(nodes);
        return res;
    }

    public static void clearChildren(Children children) {
        Node[] nodes = children.getNodes();
        if (ArrayUtils.isNotEmpty(nodes)) {
            children.remove(nodes);
        }
    }
}
