package org.fudaa.fudaa.crue.common.editor;

import javax.swing.BoundedRangeModel;
import javax.swing.DefaultBoundedRangeModel;

/**
 *
 * @author deniger
 */
public abstract class SliderRangeModelFactory {

  public abstract BoundedRangeModel create();

  public static class Transparency extends SliderRangeModelFactory {

    @Override
    public BoundedRangeModel create() {
      return new DefaultBoundedRangeModel(0, 1, 0, 255);
    }
  }
}
