/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.helper;

import com.jidesoft.swing.CheckBoxList;
import org.fudaa.ctulu.gui.CtuluPopupListener;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.event.MouseEvent;

/**
 *
 * @author Frederic Deniger
 */
public class CheckBoxListPopupListener implements CtuluPopupListener.PopupReceiver {

  final CheckBoxList checkBoxList;

  public CheckBoxListPopupListener(final CheckBoxList checkBoxList) {
    this.checkBoxList = checkBoxList;
    new CtuluPopupListener(this, checkBoxList);
  }

  @Override
  public void popup(final MouseEvent _evt) {
    createMenu().show(checkBoxList, _evt.getX(), _evt.getY());
  }

  protected void selectAll() {
    checkBoxList.selectAll();
  }

  protected void selectNone() {
    checkBoxList.selectNone();
  }

  private JPopupMenu createMenu() {
    final JPopupMenu menu = new JPopupMenu();
    menu.add(NbBundle.getMessage(CheckBoxListPopupListener.class, "SelectAll")).addActionListener(e -> selectAll());
    menu.add(NbBundle.getMessage(CheckBoxListPopupListener.class, "SelectNone")).addActionListener(e -> selectNone());

    return menu;
  }
}
