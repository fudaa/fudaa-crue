/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.log.property;

import java.beans.PropertyEditor;
import java.lang.reflect.InvocationTargetException;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLogRecord;
import org.fudaa.fudaa.crue.common.editor.HtmlReadEditorSupport;
import org.openide.nodes.PropertySupport;
import org.openide.util.NbBundle;

/**
 *
 * @author Fred Deniger
 */
public class MessageProperty extends PropertySupport.ReadOnly<String> {

  public static final String ID = "Message";
  private final CtuluLogRecord record;
  String html;

  public MessageProperty(CtuluLogRecord record) {
    super(ID, String.class, getDefaultDisplayName(),
            getDescription());
    this.record = record;
    String msg = record.getLocalizedMessage();
    html = msg;
    if (msg.contains("\\n")) {
      html = "<html><body>" + StringUtils.replace(msg, "\\n", "<br>") + "</body></html>";
      setValue("htmlDisplayValue", html);

    }

  }

  public static String getDescription() {
    return NbBundle.getMessage(MessageProperty.class, "MessagePropertyDescription");
  }

  public static PropertyColumnFilterable createColumn() {
    PropertyColumnFilterable res = new PropertyColumnFilterable();
    res.setColumnId(ID);
    res.setDescription(getDescription());
    res.setDisplayName(getDefaultDisplayName());
    return res;
  }

  public static String getDefaultDisplayName() {
    return NbBundle.getMessage(MessageProperty.class, "MessagePropertyName");
  }

  @Override
  public String getHtmlDisplayName() {
    return html;
  }

  @Override
  public String toString() {
    return record.getLocalizedMessage();
  }

  @Override
  public String getValue() throws IllegalAccessException, InvocationTargetException {
    return html;
  }

  @Override
  public PropertyEditor getPropertyEditor() {
    return new HtmlReadEditorSupport(html);
  }
}
