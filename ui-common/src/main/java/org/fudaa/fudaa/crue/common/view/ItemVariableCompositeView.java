/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.view;

import com.memoire.bu.BuGridLayout;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.fudaa.dodico.crue.config.ccm.DecimalFormatEpsilonEnum;
import org.fudaa.dodico.crue.config.ccm.ItemVariable;
import org.fudaa.dodico.crue.config.ccm.PropertyNature;
import org.fudaa.fudaa.crue.common.view.ItemVariableView.TextField;

/**
 *
 * @author Frederic Deniger
 */
public class ItemVariableCompositeView extends Observable implements Observer {

  Map<String, ItemVariableView.TextField> viewByVariableName;
  final JPanel panel = new JPanel(new BuGridLayout(4, 5, 5));

  @Override
  public void update(Observable o, Object arg) {
    setChanged();
    super.notifyObservers(arg);
  }

  public JPanel getPanel() {
    return panel;
  }

  public Double getValue(String variableName) {
    ItemVariableView view = viewByVariableName.get(variableName);
    return view == null ? null : (Double) view.getValue();
  }

  public void setValue(String variableName, Double value) {
    ItemVariableView.TextField view = viewByVariableName.get(variableName);
    if (view != null) {
      view.setValue(value);
    }
  }

  public JPanel setVariables(List<ItemVariable> v) {
    if (viewByVariableName != null) {
      for (ItemVariableView itemVariableView : viewByVariableName.values()) {
        itemVariableView.deleteObserver(this);
      }
    }
    if (panel != null) {
      panel.removeAll();
    }
    viewByVariableName = new HashMap<>();
    for (ItemVariable itemVariable : v) {
      ItemVariableView.TextField view = new ItemVariableView.TextField(itemVariable,DecimalFormatEpsilonEnum.COMPARISON);
      final JLabel jLabel = new JLabel(itemVariable.getNom());
      jLabel.setToolTipText(itemVariable.getNom() + PropertyNature.getUniteSuffixe(itemVariable));
      panel.add(jLabel);
      panel.add(view.getTxt());
      panel.add(new JLabel(PropertyNature.getUniteSuffixe(itemVariable)));
      panel.add(view.getLabel());
      viewByVariableName.put(itemVariable.getNom(), view);
      view.addObserver(this);
    }
    return panel;
  }

  public void setEditable(boolean editable) {
    if (viewByVariableName == null) {
      return;
    }
    Collection<TextField> values = viewByVariableName.values();
    for (TextField textField : values) {
      textField.getTxt().setEditable(editable);

    }
  }

  public boolean isValide() {
    Collection<TextField> values = viewByVariableName.values();
    for (TextField textField : values) {
      if (!textField.getProperty().getValidator().getRangeValidate().containsNumber(textField.getValue())) {
        return false;
      }
    }
    return true;
  }

  public boolean hasVariables() {
    return viewByVariableName != null && !viewByVariableName.isEmpty();
  }
}
