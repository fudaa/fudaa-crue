package org.fudaa.fudaa.crue.common;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gui.CtuluDialogPreferences;
import org.jdesktop.swingx.JXTable;
import org.openide.util.NbPreferences;

import javax.swing.*;
import java.awt.*;
import java.util.prefs.Preferences;

/**
 * Classe charger de sauvegarder l'etat des fenetres internes dans des preferences locales.
 */
public class UserPreferencesSaver {
  public static void saveLocationAndDimension(Component main, String version) {
    CtuluDialogPreferences.saveComponentLocationAndDimension(main, createPreferences(), generatePreferencesId(main, version));
  }

  public static void loadDialogLocationAndDimension(JDialog main, String version) {
    CtuluDialogPreferences.loadComponentLocationAndDimension(main, createPreferences(), generatePreferencesId(main, version));
    CtuluDialogPreferences.ensureComponentWillBeVisible(main, main.getLocation());
  }

  private static String generatePreferencesId(Component component, String version) {
    if (version == null || StringUtils.isBlank(version)) {
      return component.getName();
    }
    return component.getName().concat(".v").concat(version);

  }

  private static Preferences createPreferences() {
    return NbPreferences.forModule(UserPreferencesSaver.class);
  }

  public static boolean getPreference(String key, boolean defaultValue) {
    return createPreferences().getBoolean(key, defaultValue);
  }

  public static void savePreference(String key, boolean value) {
    createPreferences().putBoolean(key, value);
  }

  public static void saveTablePreferences(final JXTable table) {
    CtuluDialogPreferences.saveTablePreferences(createPreferences(), table);
  }

  /**
   * @param table la table a modifier
   */
  public static void loadTablePreferences(final JXTable table) {
    CtuluDialogPreferences.loadTablePreferences(createPreferences(), table, null);
  }

}
