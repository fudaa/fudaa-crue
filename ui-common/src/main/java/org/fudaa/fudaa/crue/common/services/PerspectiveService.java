package org.fudaa.fudaa.crue.common.services;

import java.beans.PropertyChangeListener;
import java.util.Set;
import org.fudaa.fudaa.crue.common.PerspectiveEnum;

/**
 * Interface pour les services gérant les différentes perspectives. Il y a un service par perspective.
 *
 * @author Fred Deniger
 */
public interface PerspectiveService {

  /**
   * @return la liste des vues à ouvrir par défaut.
   */
  Set<String> getDefaultTopComponents();

  /**
   * @return l'enum identifiant la perspective.
   */
  PerspectiveEnum getPerspective();

  /**
   * Methode appelée pour savoir si on peut désactiver une perspective (fermer). Utilisée lors de changement de perspective.
   *
   * @return si false, la perspective ne peut pas être fermée ( edition en cours, utilisateur annule l'action).
   * @see org.fudaa.fudaa.crue.common.services.SelectedPerspectiveService
   */
  boolean deactivate();

  /**
   * Methode appelee à l'activation d'une perspective pour initialiser les vues si nécessaires.
   *
   * @return toujours true, car une perspective peut toujours être activée
   */
  boolean activate();

  /**
   * Ajoute un listener appelé lors des changements de propriété "state" et notamment lors des appels à
   * {@link #setState(org.fudaa.fudaa.crue.common.services.PerspectiveState) }.
   *
   * @param listener le listener à ajouter
   * @see org.fudaa.fudaa.crue.common.services.PerspectiveState
   */
  void addStateListener(PropertyChangeListener listener);

  /**
   * Enleve le listener appelé lors des changements de propriété "state".
   *
   * @param listener le listener à enlever
   * @see org.fudaa.fudaa.crue.common.services.PerspectiveState
   */
  void removeStateListener(PropertyChangeListener listener);

  /**
   * Ajoute un listener appelé lors des changements de propriété "dirty": le contenu de la perspective en cours est modifié.
   * Le listener recevra des boolean pour donner l'état de la perspective.
   *
   * @param listener le listener à ajouter
   */
  void addDirtyListener(PropertyChangeListener listener);

  /**
   * Enleve le listener appelé lors des changements de propriété "dirty".
   *
   * @param listener le listener à enlever
   */
  void removeDirtyListener(PropertyChangeListener listener);

  /**
   *
   * @return l'etat de la perspective: readonly, edit,...
   */
  PerspectiveState getState();

  /**
   *
   * @param state le nouvel etat de la perspective: readonly, edit,...
   */
  void setState(PerspectiveState state);

  /**
   *
   * @return true si la perspective supporte les modification edition/lecture seul
   */
  boolean supportEdition();

  /**
   *
   * @return true si le contenu de la perspective est modifiée ( donc pas possible de changer de perspective)
   */
  boolean isDirty();

  /**
   * appelé lors de la demande de fermeture de l'application (par un ModuleInstall).
   *
   * @return true si Fudaa-Crue peut être fermée. Si false, la fermeture est annulée
   * @see org.fudaa.fudaa.crue.common.InitPerspectiveInstaller
   */
  boolean closing();

    String getPathForViewsAction();
}
