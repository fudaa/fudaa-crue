/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.node;

import java.lang.reflect.InvocationTargetException;
import javax.swing.JTable;
import org.fudaa.ctulu.table.CtuluTableModelDefault;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

/**
 * Le modele pour adapater une table netbeans a un export excel/csv utilisé par ctulu
 *
 * @author Frederic Deniger
 */
public class OutlineViewExportTableModel extends CtuluTableModelDefault {

  public OutlineViewExportTableModel(JTable _t, int[] _column, int[] _row) {
    super(_t, _column, _row);
  }

  @Override
  public Object getValueAt(int _row, int _col) {
    Object val = super.getValueAt(_row, _col);
    if (val instanceof Node.Property) {
      try {
        return ((Node.Property) val).getValue();
      } catch (IllegalAccessException ex) {
        Exceptions.printStackTrace(ex);
      } catch (InvocationTargetException ex) {
        Exceptions.printStackTrace(ex);
      }
    }
    return super.getValueAt(_row, _col);
  }
}
