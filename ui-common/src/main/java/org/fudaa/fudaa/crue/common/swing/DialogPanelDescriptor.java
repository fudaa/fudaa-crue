package org.fudaa.fudaa.crue.common.swing;

import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.crue.common.editor.DocumentListenerAdapter;
import org.openide.DialogDescriptor;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.function.BooleanSupplier;

public class DialogPanelDescriptor extends CtuluDialogPanel {
  private final DialogDescriptor dialogDescriptor;

  public DialogPanelDescriptor(String title) {
    super(false);
    this.dialogDescriptor = new DialogDescriptor(this, title);
    this.dialogDescriptor.setOptionType(DialogDescriptor.OK_CANCEL_OPTION);
  }

  public DialogDescriptor getDialogDescriptor() {
    return dialogDescriptor;
  }


  @Override
  public boolean isDataValid() {
    return dialogDescriptor.isValid();
  }

  public void addTextListener(JTextField tf, BooleanSupplier supplier) {
    dialogDescriptor.setValid(supplier.getAsBoolean());
    tf.getDocument().addDocumentListener(new DocumentListenerAdapter() {
      @Override
      protected void update(DocumentEvent e) {
        dialogDescriptor.setValid(supplier.getAsBoolean());
      }
    });
  }

  public void addComboListner(JComboBox jc, BooleanSupplier supplier) {
    dialogDescriptor.setValid(supplier.getAsBoolean());
    jc.addItemListener(e -> dialogDescriptor.setValid(supplier.getAsBoolean()));
  }
}
