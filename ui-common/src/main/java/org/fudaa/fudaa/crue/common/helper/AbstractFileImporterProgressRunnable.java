package org.fudaa.fudaa.crue.common.helper;

import com.Ostermiller.util.CSVParser;
import com.memoire.bu.BuFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.openide.util.Exceptions;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe mère des éléments proposant un Import XLSX ou CSV
 * Se trouve ici les méthodes de lecture XLS et CSV
 *
 * @author Yoan GRESSIER
 */
public abstract class AbstractFileImporterProgressRunnable {
  /**
   * le fichier à importer
   */
  protected final File file;

  public AbstractFileImporterProgressRunnable(File file) {
    this.file = file;
  }

  /**
   * Construction d'un CtuluFileChooser d'importer CSV/XLS/XLSX
   *
   * @return le CtuluFileChooser construit
   */
  public static CtuluFileChooser getImportXLSCSVFileChooser() {
    final BuFileFilter ftCsv = new BuFileFilter(new String[]{"csv", "txt"}, CtuluResource.CTULU.getString("Texte CSV"));
    final BuFileFilter ftXsl = new BuFileFilter(new String[]{"xls"}, CtuluResource.CTULU.getString("Fichier Excel 97-2003"));
    final BuFileFilter ftXslx = new BuFileFilter(new String[]{"xlsx"}, CtuluResource.CTULU.getString("Fichier Excel"));
    ftCsv.setExtensionListInDescription(true);
    ftXsl.setExtensionListInDescription(true);
    ftXslx.setExtensionListInDescription(true);
    CtuluFileChooser fileChooser = new CtuluFileChooser(true);
    fileChooser.addChoosableFileFilter(ftCsv);
    fileChooser.addChoosableFileFilter(ftXsl);
    fileChooser.addChoosableFileFilter(ftXslx);
    fileChooser.setFileFilter(ftXslx);
    fileChooser.setAcceptAllFileFilterUsed(false);
    fileChooser.setMultiSelectionEnabled(false);
    fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
    return fileChooser;
  }

  /**
   * Lecture du fichier XLSX ou CSV
   *
   * @return un tableau String de 2 dimensions contenant les lignes et colonnes du fichier lu
   */
  protected String[][] readFile() {
    final String nameToLowerCase = file.getName().toLowerCase();
    if (nameToLowerCase.endsWith("xlsx") || nameToLowerCase.endsWith("xls")) {
      // fichier XLSX
      List<String[]> rows = new ArrayList<>();
      try (InputStream is = new FileInputStream(file)) {
        try (Workbook wb = WorkbookFactory.create(is)) {
          Sheet sheetAt = wb.getSheetAt(0);
          int maxCol = 0;
          for (Row row : sheetAt) {
            maxCol = Math.max(maxCol, row.getLastCellNum());
          }
          for (Row row : sheetAt) {
            List<String> cols = new ArrayList<>();
            for (int i = 0; i < maxCol; i++) {
              cols.add(getValue(row.getCell(i, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK)));
            }
            rows.add(cols.toArray(new String[0]));
          }
        }
      } catch (Exception ex) {
        Exceptions.printStackTrace(ex);
      }
      return rows.toArray(new String[rows.size()][]);
    } else {
      // fichier CSV
      return readCsv();
    }
  }

  /**
   * Lecture du fichier au format CSV
   *
   * @return un tableau String de 2 dimensions contenant les lignes et colonnes du fichier lu
   */
  private String[][] readCsv() {

    String[][] values = null;
    try {
      char delimiter = ';';
      String firstLine = readFirstLine();
      if (firstLine != null && firstLine.indexOf('\t') > 0) {
        delimiter = '\t';
      }
      try (Reader reader = new FileReader(file)) {
        values = new CSVParser(new BufferedReader(reader), delimiter).getAllValues();
      }
    } catch (Exception ex) {
      Exceptions.printStackTrace(ex);
    }
    return values;
  }

  private String readFirstLine() {
    String firstLine = null;
    try (FileReader reader = new FileReader(file)) {
      LineNumberReader lineReader = new LineNumberReader(reader);
      firstLine = lineReader.readLine();
    } catch (Exception ex) {
      Exceptions.printStackTrace(ex);
    }
    return firstLine;
  }

  /**
   * Lecture de la valeur d'une cellule XLS
   *
   * @param cell la cellule à lire
   * @return la valeur lue dans la cellule
   */
  public String getValue(Cell cell) {
    switch (cell.getCellType()) {
      case BLANK:
        return "";
      case NUMERIC:
        return Double.toString(cell.getNumericCellValue());
      case STRING:
        return cell.getStringCellValue();
      default:
        try {
          return Double.toString(cell.getNumericCellValue());
        } catch (Exception ex) {
          Logger.getLogger(AbstractFileImporterProgressRunnable.class.getName()).log(Level.INFO, "getValue", ex);
          try {
            return cell.getStringCellValue();
          } catch (Exception ex1) {
            Logger.getLogger(AbstractFileImporterProgressRunnable.class.getName()).log(Level.INFO, "getValue", ex1);
          }
        }
    }
    return StringUtils.EMPTY;
  }
}
