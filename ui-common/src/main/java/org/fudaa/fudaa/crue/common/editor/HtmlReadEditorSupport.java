/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.editor;

import com.memoire.bu.BuBorders;
import java.awt.BorderLayout;
import java.awt.Component;
import java.beans.PropertyEditorSupport;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.text.html.HTMLEditorKit;

/**
 *
 * @author Frederic Deniger
 */
public class HtmlReadEditorSupport extends PropertyEditorSupport {

  private final String html;

  public HtmlReadEditorSupport(String html) {
    this.html = html;
  }

  @Override
  public Component getCustomEditor() {
    final JEditorPane pane = new JEditorPane();
    pane.setOpaque(false);
    pane.setEditable(false);
    pane.setEditorKit(new HTMLEditorKit());
    pane.setText(html);
    pane.setEditable(false);
    JPanel pn = new JPanel(new BorderLayout());
    pn.add(new JScrollPane(pane));
    pn.setBorder(BuBorders.EMPTY3333);
    return pn;
  }

  @Override
  public boolean supportsCustomEditor() {
    return true;
  }
}
