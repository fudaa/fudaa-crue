/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.swing;

import java.awt.Dimension;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LookAndFeel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
import org.fudaa.ctulu.gui.CtuluPopupListener;

/**
 *
 * @author Frederic Deniger
 */
public class TableRowHeaderInstaller {

  private static class OneColumnTableModel extends AbstractTableModel {

    private final List<String> rows;
    private final String columnName;

    public OneColumnTableModel(String columnName, List<String> rows) {
      this.rows = rows;
      this.columnName = columnName;
    }

    @Override
    public int getRowCount() {
      return rows.size();
    }

    @Override
    public String getColumnName(int column) {
      return columnName;
    }

    @Override
    public int getColumnCount() {
      return 1;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      return rows.get(rowIndex);
    }
  }

  private static class SelectionSynchronizer {

    private final JTable tableHeader;
    private final JTable tableContent;

    public SelectionSynchronizer(JTable tableHeader, JTable tableContent) {
      this.tableHeader = tableHeader;
      this.tableContent = tableContent;
      tableHeader.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
        @Override
        public void valueChanged(ListSelectionEvent e) {
          selectionFromHeader();
        }
      });
      tableContent.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
        @Override
        public void valueChanged(ListSelectionEvent e) {
          selectionFromContent();
        }
      });
    }
    boolean updating = false;

    protected void selectionFromHeader() {
      if (updating) {
        return;
      }
      updating = true;
      int[] selectedRows = tableHeader.getSelectedRows();
      tableContent.clearSelection();
      for (int i = 0; i < selectedRows.length; i++) {
        tableContent.getSelectionModel().addSelectionInterval(selectedRows[i], selectedRows[i]);
      }
      updating = false;
    }

    protected void selectionFromContent() {
      if (updating) {
        return;
      }
      updating = true;
      int[] selectedRows = tableContent.getSelectedRows();
      tableHeader.clearSelection();
      for (int i = 0; i < selectedRows.length; i++) {
        tableHeader.getSelectionModel().addSelectionInterval(selectedRows[i], selectedRows[i]);
      }
      updating = false;
    }
  }

  public static JScrollPane install(JTable table, List<String> rows, String columnName, CtuluPopupListener.PopupReceiver rowPopupReceiver) {
    return install(table, rows, columnName, rowPopupReceiver, null);
  }

  public static JScrollPane install(JTable table, List<String> rows, String columnName, CtuluPopupListener.PopupReceiver rowPopupReceiver,
          MouseListener listener) {
    JTable rowHeader = new JTable(new OneColumnTableModel(columnName, rows));
    if (rowPopupReceiver != null) {
      new CtuluPopupListener(rowPopupReceiver, rowHeader);
    }
    if (listener != null) {
      rowHeader.addMouseListener(listener);
    }
    LookAndFeel.installColorsAndFont(rowHeader, "TableHeader.background",
            "TableHeader.foreground", "TableHeader.font");
    rowHeader.setIntercellSpacing(new Dimension(0, 0));
    Dimension d = rowHeader.getPreferredScrollableViewportSize();
    rowHeader.setDefaultRenderer(Object.class, new RowHeaderRenderer());
    d.width = rowHeader.getPreferredSize().width + 150;
    rowHeader.setPreferredScrollableViewportSize(d);
    rowHeader.setRowHeight(table.getRowHeight());

    JScrollPane scrollPane = new JScrollPane(table);
    scrollPane.setRowHeaderView(rowHeader);
    JTableHeader corner = rowHeader.getTableHeader();
    corner.setReorderingAllowed(false);
    corner.setResizingAllowed(true);

    scrollPane.setCorner(JScrollPane.UPPER_LEFT_CORNER, corner);

    new JScrollPaneAdjuster(scrollPane);
    new SelectionSynchronizer(rowHeader, table);
    new JTableRowHeaderResizer(scrollPane).setEnabled(true);
    return scrollPane;
  }
}
