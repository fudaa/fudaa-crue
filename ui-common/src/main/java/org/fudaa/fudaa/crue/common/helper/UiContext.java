package org.fudaa.fudaa.crue.common.helper;

import com.memoire.bu.BuList;
import java.awt.Component;
import javax.swing.JList;
import javax.swing.ListModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.CtuluTaskOperationDefault;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionFuAdapter;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.PopupMenuReceiver;
import org.fudaa.fudaa.crue.common.CrueIconsProvider;
import org.jdesktop.swingx.JXTree;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.NotificationDisplayer;
import org.openide.windows.WindowManager;

/**
 * Factory of swing components.
 *
 * @author deniger
 */
public class UiContext implements CtuluUI {

  public UiContext() {
  }

  @Override
  public void clearMainProgression() {
  }

  @Override
  public CtuluTaskDelegate createTask(String _name) {
    return new CtuluTaskOperationDefault(_name);
  }

  @Override
  public void error(String _titre, String _msg, boolean _tempo) {
    NotifyDescriptor nd = new NotifyDescriptor(_msg, _titre, NotifyDescriptor.DEFAULT_OPTION, NotifyDescriptor.ERROR_MESSAGE,
            new Object[]{NotifyDescriptor.OK_OPTION}, NotifyDescriptor.OK_OPTION);
    DialogDisplayer.getDefault().notify(nd);
    NotificationDisplayer.getDefault().notify(_titre, CrueIconsProvider.getIcon("org/fudaa/fudaa/crue/common/icons/erreur-bloquante.png"), _msg,
            null, NotificationDisplayer.Priority.SILENT);

  }

  @Override
  public void error(String _msg) {
    DialogHelper.showError(_msg);
  }

  @Override
  public ProgressionInterface getMainProgression() {
    return new ProgressionFuAdapter();
  }

  @Override
  public Component getParentComponent() {
    return WindowManager.getDefault().getMainWindow();
  }

  @Override
  public boolean manageAnalyzeAndIsFatal(CtuluAnalyze _analyze) {
    return true;
  }

  @Override
  public boolean manageAnalyzeAndIsFatal(CtuluLog _analyze) {
    return true;
  }

  @Override
  public boolean manageErrorOperationAndIsFatal(CtuluIOResult _opResult) {
    return true;
  }

  @Override
  public boolean manageErrorOperationAndIsFatal(CtuluIOOperationSynthese _opResult) {
    return true;
  }

  @Override
  public void message(String _titre, String _msg, boolean _tempo) {
    NotifyDescriptor nd = new NotifyDescriptor.Message(_msg, NotifyDescriptor.INFORMATION_MESSAGE);
    DialogDisplayer.getDefault().notify(nd);
    NotificationDisplayer.getDefault().notify(_titre, CrueIconsProvider.getIcon("org/fudaa/fudaa/crue/common/icons/information.png"),
            _msg, null, NotificationDisplayer.Priority.SILENT);
  }

  @Override
  public boolean question(String _titre, String _text) {
    NotifyDescriptor nd = new NotifyDescriptor(_text, _titre, NotifyDescriptor.YES_NO_OPTION, NotifyDescriptor.QUESTION_MESSAGE, null,
            NotifyDescriptor.OK_OPTION);
    Object notify = DialogDisplayer.getDefault().notify(nd);
    return NotifyDescriptor.YES_OPTION.equals(notify);
  }

  @Override
  public void warn(String _titre, String _msg, boolean _tempo) {
    NotifyDescriptor nd = new NotifyDescriptor(_msg, _titre, NotifyDescriptor.DEFAULT_OPTION, NotifyDescriptor.WARNING_MESSAGE,
            new Object[]{NotifyDescriptor.OK_OPTION}, NotifyDescriptor.OK_OPTION);
    DialogDisplayer.getDefault().notify(nd);
    NotificationDisplayer.getDefault().notify(_titre, CrueIconsProvider.getIcon("org/fudaa/fudaa/crue/common/icons/avertissement.png"), _msg,
            null, NotificationDisplayer.Priority.SILENT);
  }

  public static JXTreeTableExtended createTreeTable(final DefaultTreeTableModel model, PopupMenuReceiver receiver) {
    JXTreeTableExtended jxTreeTableExtended = new JXTreeTableExtended(model);
    if (receiver != null) {
      receiver.install(jxTreeTableExtended, null);
    }
    return jxTreeTableExtended;
  }

  public JXTreeTableExtended createTreeTable(final DefaultTreeTableModel model, boolean installCopyPaste) {
    JXTreeTableExtended tableExtended = new JXTreeTableExtended(model);
    if (installCopyPaste) {
      PopupMenuReceiver.installDefault(tableExtended, null);
    }
    return tableExtended;
  }

  public JXTreeTableExtended createTreeTable(boolean installCopyPaste) {
    JXTreeTableExtended tableExtended = new JXTreeTableExtended();
    if (installCopyPaste) {
      PopupMenuReceiver.installDefault(tableExtended, null);
    }
    return tableExtended;
  }

  public JList createList(final ListModel model, final boolean installPopup, CtuluUI ui) {
    final BuList list = new BuList(model);
    if (installPopup) {
      PopupMenuReceiver.installDefault(list, ui);
    }
    return list;
  }

  public JList createList(final ListModel model) {
    final BuList list = new BuList(model);
    PopupMenuReceiver.installDefault(list, null);
    return list;
  }

  public static JXTree createTree(final DefaultMutableTreeNode model) {
    final JXTree list = new JXTree(new DefaultTreeModel(model));
    PopupMenuReceiver.installDefault(list, null);
    return list;
  }
//  /**
//   * @return the ui
//   */
//  public CtuluUI getCtuluUi() {
//    return ui;
//  }
}
