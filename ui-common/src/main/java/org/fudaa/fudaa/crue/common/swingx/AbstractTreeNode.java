package org.fudaa.fudaa.crue.common.swingx;

import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;

import javax.swing.*;

public abstract class AbstractTreeNode extends DefaultMutableTreeTableNode {
    private String linkId;
    private Icon icon;

    /**
     *
     */
    public AbstractTreeNode() {
    }

    /**
     * @param userObject l'objet
     */
    public AbstractTreeNode(Object userObject) {
        super(userObject);
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public String getLinkId(int column) {
        return column == 1 ? linkId : null;
    }

    public abstract boolean isActive();

    public boolean isLink(int column) {
        return column == 1 && linkId != null;
    }

    public void setLinkIdForColumnOne(String linkId) {
        this.linkId = linkId;
    }
}
