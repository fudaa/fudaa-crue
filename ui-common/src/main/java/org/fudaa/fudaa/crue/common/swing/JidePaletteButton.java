/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.swing;

import com.jidesoft.swing.JideToggleButton;
import com.memoire.bu.BuDesktop;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;

/**
 *
 * @author Frederic Deniger
 */
public class JidePaletteButton extends JideToggleButton {

  final EbliActionPaletteAbstract ac_;

  /**
   *
   */
  public JidePaletteButton(final EbliActionPaletteAbstract _ac) {
    ac_ = _ac;
  }

  @Override
  public final void setVisible(final boolean _b) {
    if (_b != isVisible()) {
      ac_.setMainButtonVisible(_b);
      super.setVisible(_b);
    }

  }

  public void setDesktop(final BuDesktop _d) {
    ac_.setDesktop(_d);
  }
}
