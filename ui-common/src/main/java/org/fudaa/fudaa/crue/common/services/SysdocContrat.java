/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common.services;

/**
 *
 * @author Frederic Deniger
 */
public interface SysdocContrat {

    // Constante d'identification du menu SyDoc Parent
    String SYDOC_ID = "Sysdoc";

    // Permet de connaitre l'état d'activation de l'aide SyDoc
    boolean isSyDocActivated();

    void display(String path);

    void displayMedia(String helpUrl);
}
