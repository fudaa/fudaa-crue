package org.fudaa.fudaa.crue.common.helper;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.dodico.crue.common.contrat.ObjetWithID;

/**
 *
 * @author deniger
 */
public class ObjetWithIdCellRenderer extends CtuluCellTextRenderer {

  @Override
  protected void setValue(Object _value) {
    if (_value == null) {
      super.setValue(StringUtils.EMPTY);
    } else {
      super.setValue(((ObjetWithID) _value).getId());
    }
  }
}
