/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.crue.common.pdt;

import java.awt.FlowLayout;
import javax.swing.*;
import org.joda.time.Duration;
import org.joda.time.Period;
import org.openide.util.NbBundle;

/**
 *
 * @author Frédéric Deniger
 */
public class DurationPanelEditor extends JPanel {

  final JSpinner day;
  final JSpinner hour;
  final JSpinner min;
  final JSpinner sec;
  final JSpinner millis;

  public DurationPanelEditor() {

    setLayout(new FlowLayout(FlowLayout.RIGHT, 4, 0));
    millis = new JSpinner(new SpinnerNumberModel(0, 0, 1000, 1));
    ((JSpinner.DefaultEditor) millis.getEditor()).getTextField().setColumns(3);

    sec = new JSpinner(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
    ((JSpinner.DefaultEditor) sec.getEditor()).getTextField().setColumns(3);

    min = new JSpinner(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
    ((JSpinner.DefaultEditor) min.getEditor()).getTextField().setColumns(3);

    hour = new JSpinner(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
    ((JSpinner.DefaultEditor) hour.getEditor()).getTextField().setColumns(3);

    day = new JSpinner(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
    ((JSpinner.DefaultEditor) day.getEditor()).getTextField().setColumns(3);
    add(day);
    add(new JLabel(NbBundle.getMessage(DurationPanelEditor.class, "Day.Label")));
    add(new JLabel(" "));
    add(hour);
    add(new JLabel(NbBundle.getMessage(DurationPanelEditor.class, "Hour.Label")));
    add(new JLabel(" "));
    add(min);
    add(new JLabel(NbBundle.getMessage(DurationPanelEditor.class, "Minute.Label")));
    add(new JLabel(" "));
    add(sec);
    add(new JLabel(NbBundle.getMessage(DurationPanelEditor.class, "Sec.Label")));
    add(new JLabel(" "));
    add(millis);
    add(new JLabel(NbBundle.getMessage(DurationPanelEditor.class, "Millis.Label")));
  }

  public int getValue(JSpinner spinner) {
    return ((Number) spinner.getValue()).intValue();
  }

  public Duration getDuration() {
    Period period = new Period().withDays(getValue(day)).withHours(getValue(hour)).withMinutes(getValue(min)).withSeconds(getValue(sec)).
            withMillis(getValue(millis));
    return period.toStandardDuration();
  }

  void setPdt(Duration duration) {
    if (duration == null) {
      final Integer zero = Integer.valueOf(0);
      day.setValue(zero);
      hour.setValue(zero);
      min.setValue(zero);
      sec.setValue(zero);
      millis.setValue(zero);
    }
    Period p = new Period(duration);
    day.setValue(p.getDays());
    hour.setValue(p.getHours());
    min.setValue(p.getMinutes());
    sec.setValue(p.getSeconds());
    millis.setValue(p.getMillis());
  }

  void setEditable(boolean editable) {
    day.setEnabled(editable);
    hour.setEnabled(editable);
    min.setEnabled(editable);
    sec.setEnabled(editable);
    millis.setEnabled(editable);
  }
}
