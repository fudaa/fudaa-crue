package org.fudaa.fudaa.crue.common.action;

import org.fudaa.ctulu.gui.CtuluDialogPreferences;
import org.fudaa.fudaa.crue.common.services.SelectedPerspectiveService;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.ContextAwareAction;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Collection;

/**
 * une action qui permet d'afficher dans une popup une liste d'actions enregistrées sous un path ( fonctionnement netbeans RCP).
 * Chaque action est enregistrée dans un path et peu être requetée.
 */
@ActionID(category = "View",
        id = "org.fudaa.fudaa.crue.common.action.ShowViewListAction")
//iconBase ne semble pas fonctionner !
@ActionRegistration(displayName = "#CTL_ShowViewListAction")
@ActionReference(path = "Shortcuts", name = "A-V")
public final class ShowViewListAction extends AbstractAction implements ContextAwareAction {
    protected final SelectedPerspectiveService perspectiveService = Lookup.getDefault().lookup(SelectedPerspectiveService.class);

    public ShowViewListAction() {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String path = perspectiveService.getCurrentPerspectiveService().getPathForViewsAction();
        if (path == null) {
            return;
        }
        Collection all = Lookups.forPath(path).lookupAll(Object.class);
        if (all.isEmpty()) {
            return;
        }
        JPopupMenu menu = new JPopupMenu();
        for (Object object : all) {
            if (object instanceof AbstractAction) {
                menu.add((AbstractAction) object);
            } else if (object instanceof JSeparator) {
                menu.addSeparator();
            }
        }
        final Point location = MouseInfo.getPointerInfo().getLocation();
        CtuluDialogPreferences.ensureComponentWillBeVisible(menu, location);
        SwingUtilities.convertPointFromScreen(location, WindowManager.getDefault().getMainWindow());
        menu.show(WindowManager.getDefault().getMainWindow(), location.x, location.y);
    }

    @Override
    public Action createContextAwareInstance(Lookup lkp) {
        return new ShowViewListAction();
    }
}
