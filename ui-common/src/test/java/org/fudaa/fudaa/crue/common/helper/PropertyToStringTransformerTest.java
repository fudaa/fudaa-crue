package org.fudaa.fudaa.crue.common.helper;

import java.awt.geom.Point2D;
import java.util.Map;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.ctulu.converter.ToStringTransformerFactory;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author deniger
 */
public class PropertyToStringTransformerTest {

  public PropertyToStringTransformerTest() {
  }

  @Test
  public void testToStringPoint2D() {
    Map<Class, AbstractPropertyToStringTransformer> createTransformers = ToStringTransformerFactory.createTransformers();
    AbstractPropertyToStringTransformer transformer = createTransformers.get(String.class);
    assertNotNull(transformer);
    transformer = createTransformers.get(Point2D.Double.class);
    assertNotNull(transformer);
    Point2D.Double pt = new Point2D.Double();
    pt.setLocation(100, 120);
    String toString = transformer.toString(pt);
    assertEquals(pt.getX() + " " + pt.getY(), toString);
    Point2D.Double fromString = (Point2D.Double) transformer.fromString(toString);
    assertEquals(pt.getX(), fromString.getX(), 1e-15);
    assertEquals(pt.getY(), fromString.getY(), 1e-15);
  }

  @Test
  public void testToStringPoint2DArray() {
    AbstractPropertyToStringTransformer<Point2D.Double[]> transformer = ToStringTransformerFactory.getPoint2dArrayTransformer();
    assertNotNull(transformer);
    Point2D.Double pt = new Point2D.Double();
    pt.setLocation(100, 120);
    Point2D.Double pt2 = new Point2D.Double();
    pt2.setLocation(101, 121);
    Point2D.Double[] in = new Point2D.Double[]{pt, pt2};
    String toString = transformer.toString(in);
    assertEquals(pt.getX() + " " + pt.getY() + ";" + pt2.getX() + " " + pt2.getY(), toString);
    Point2D.Double[] fromString = transformer.fromString(toString);
    assertEquals(2, fromString.length);
    assertEquals(pt.getX(), fromString[0].getX(), 1e-15);
    assertEquals(pt.getY(), fromString[0].getY(), 1e-15);
    assertEquals(pt2.getX(), fromString[1].getX(), 1e-15);
    assertEquals(pt2.getY(), fromString[1].getY(), 1e-15);
  }
}
