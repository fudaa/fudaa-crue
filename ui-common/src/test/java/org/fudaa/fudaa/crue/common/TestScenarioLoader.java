/*
 GPL 2
 */
package org.fudaa.fudaa.crue.common;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.dodico.crue.common.Pair;
import org.fudaa.dodico.crue.config.coeur.TestCoeurConfig;
import org.fudaa.dodico.crue.io.test.ReadHelperForTest;
import org.fudaa.dodico.crue.metier.emh.EMHScenario;
import org.fudaa.dodico.crue.metier.emh.IdRegistry;
import org.fudaa.dodico.crue.metier.etude.EMHProjet;
import org.fudaa.dodico.crue.projet.EMHScenarioContainer;
import org.fudaa.dodico.crue.test.AbstractTestParent;
import org.fudaa.fudaa.crue.common.helper.LoadEtuHelper;
import org.openide.util.Exceptions;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

/**
 * @author Frederic Deniger
 */
public class TestScenarioLoader {
    public static EMHScenario readScenario() {
        final EMHScenario emhScenario = new ReadHelperForTest(TestCoeurConfig.INSTANCE).readModeleAnsSetRelation(new CtuluLog(),
            "/M3-0_c9.dc", "/M3-0_c9.dh");
        IdRegistry.install(emhScenario);
        return emhScenario;
    }

    public static Pair<EMHScenarioContainer, File> readScenarioAndRun() {
        File target = null;
        try {
            target = CtuluLibFile.createTempDir();
        } catch (final IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        AbstractTestParent.exportZip(target, "/Etu3-0.zip");
        final File file = new File(new File(target, "Etu3-0"), "Etu3-0.etu.xml");
        final EMHProjet projet = LoadEtuHelper.loadEtu(file, TestCoeurConfig.INSTANCE_1_1_1);
        final EMHScenarioContainer runv8 = LoadEtuHelper.loadRun("Sc_M3-0_v8c9", projet, 0);
        return new Pair<>(runv8, target);
    }

    public static void display(final JComponent jc, final File toDelete) {
        final JFrame fr = new JFrame();
        fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fr.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                if (toDelete != null) {
                    CtuluLibFile.deleteDir(toDelete);
                }
            }
        });

        fr.setContentPane(jc);
        fr.setSize(500, 600);
        fr.setVisible(true);
    }
}
